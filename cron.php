<?php
/*
 * cron.php
 * 
 * Copyright 2016 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
include('const.php');
include('autoload.php');

//Meal-Adresse versenden
foreach(Plugin_Events_Manager::getAll24Events() as $event){
	if(Plugin_Events_Manager::countSubscribers($event->id) > 0){
		foreach(Plugin_Events_Manager::getSubscribesByEvent($event->id) as $user){
			Plugin_Events_Manager::sendAdress($user->userid,$event->id);
		}
	}
	Plugin_Events_Manager::updateSendmail($event->id);
}

//Bewertungs-Erinnerung versenden
$subject = utf8_decode('Wie war dein Erlebnis beim kürzlich durchgeführten Meal auf Social Meal?');
foreach(Plugin_Events_Manager::getAllAfterEventMail() as $event){
	foreach(Plugin_Events_Manager::getSubscribesByEvent($event->id) as $user){
		if(Plugin_Profile_Manager::getUserSetting('afterevent',$user->userid) == 1){
			$userdata = Plugin_Profile_Manager::getUserDataPerId($user->userid);
			$mailtext = 'Liebe*r '.ucfirst($userdata->firstname).'<br><br>Kürzlich hast du an dem Meal <a href="'.Settings::getInstance()->get("host").'event-details.html?event='.$event->id.'">'.$event->eventname.'</a> teilgenommen. Wir hoffen und sind sehr zuversichtlich, dass du dabei ein durchaus gutes und wertvolles Erlebnis hattest.<br><br>Wir würden uns sehr freuen, wenn du den Usern, die du bei diesem Meal kennengelernt hast, eine Bewertung hinterlassen würdest!<br><br>In deinem <a href="'.Settings::getInstance()->get("host").'mein-profil.html">Profil</a> unter dem Menu "Meine Meals" findest du all deine vergangenen Meals sowie alle Teilnehmer*innen davon. Dort hast du auch die Möglichkeit den*die Gastgeber*in von diesem Meal zu bewerten.<br><br>Alles was du zum Bewertungssystem wissen musst, erfährst du unter <a href="'.Settings::getInstance()->get("host").'FAQ/so-funktioniert-das-bewertungssystem.html">So funktioniert unser Bewertungssystem</a>.<br><br>Bei ausserordentlichen Erfahrungen, die du unbedingt mit der ganzen Community teilen möchtest, stellen wir dir nach Absprache gerne einen Blog-Eintrag zur Verfügung. Wie dass das geht, erfährst du unter <a href="'.Settings::getInstance()->get("host").'FAQ/so-funktioniert-unser-blog.html">So funktioniert unser Blog</a>. Oder du schickst uns ein Testimonial an <a href="mailto:info@socialmeal.ch?subject=Testimonial">info@socialmeal.ch</a>. Da wir für den Betrieb der Plattform viel Aufwand betreiben und auch gerne mal ein bisschen Werbung machen würden, freuen wir uns auch sehr über <a href="'.Settings::getInstance()->get("host").'FAQ/spenden-und-sonstige-unterstutzungen.html">Spenden und sonstige Unterstützungen</a>.<br><br>Falls du der Meinung bist, dass einer der User, die du bei diesem Meal kennengelernt hast, die Plattform von Social Meal missbräuchlich verwendet, melde uns das bitte über die entsprechende <a href="'.Settings::getInstance()->get("host").'melden.html?source=mail&g=aftereventmail_'.$event->id.'">Melde-Seite</a>.<br><br>Wir wünschen dir weiterhin gute Erfahrungen in unserer Community!<br>Social Meal<br><br>P.S.: Diesen E-Mail-Typ kannst du in den <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abbestellen, falls er dir überflüssig erscheint.';
			$h2t = new Plugin_Mailer_html2text($mailtext);
			$mail = new Plugin_PHPMailer_PHPMailer;
			$mail->IsSMTP();
			$mail->AddAddress($userdata->email);
			$mail->WordWrap = 50;
			$mail->IsHTML(true);
			$mail->Subject = $subject;
			$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
			$mail->AltBody = $h2t->get_text();
			if(!$mail->Send()){
				echo('Mail Error:'.$mail->ErrorInfo);
			}
			unset($userdata);
		}
	}
	if(Plugin_Events_Manager::countSubscribers($event->id) > 0){
		if(Plugin_Profile_Manager::getUserSetting('afterevent',$event->userid) == 1){
			$userdata = Plugin_Profile_Manager::getUserDataPerId($event->userid);
			$mailtext = 'Liebe*r '.ucfirst($userdata->firstname).'<br><br>Kürzlich hat das von dir organisierte Meal <a href="'.Settings::getInstance()->get("host").'event-details.html?event='.$event->id.'">'.$event->eventname.'</a> stattgefunden.<br><br>Wir würden uns sehr freuen, wenn du den Usern, die du bei diesem Meal kennengelernt hast, eine Bewertung hinterlassen würdest!<br><br>In deinem <a href="'.Settings::getInstance()->get("host").'mein-profil.html">Profil</a> unter dem Menu "Meine Meals" findest du all deine vergangenen Meals sowie alle Teilnehmer*innen davon. Dort hast du auch die Möglichkeit diesen eine Bewertung zu hinterlassen.<br><br>Alles was du zum Bewertungssystem wissen musst, erfährst du unter <a href="'.Settings::getInstance()->get("host").'FAQ/so-funktioniert-das-bewertungssystem.html">So funktioniert unser Bewertungssystem</a>.<br><br>Bei ausserordentlichen Erfahrungen, die du unbedingt mit der ganzen Community teilen möchtest, stellen wir dir nach Absprache gerne einen Blog-Eintrag zur Verfügung. Wie dass das geht, erfährst du unter <a href="'.Settings::getInstance()->get("host").'FAQ/so-funktioniert-unser-blog.html">So funktioniert unser Blog</a>. Oder du schickst uns ein Testimonial an <a href="mailto:info@socialmeal.ch?subject=Testimonial">info@socialmeal.ch</a>. Da wir für den Betrieb der Plattform viel Aufwand betreiben und auch gerne mal ein bisschen Werbung machen würden, freuen wir uns auch sehr über <a href="'.Settings::getInstance()->get("host").'FAQ/spenden-und-sonstige-unterstutzungen.html">Spenden und sonstige Unterstützungen</a>.<br><br>Falls du der Meinung bist, dass einer der User, die du bei diesem Meal kennengelernt hast, die Plattform von Social Meal missbräuchlich verwendet, melde uns das bitte über die entsprechende <a href="'.Settings::getInstance()->get("host").'melden.html?source=mail&g=aftereventmail_'.$event->id.'">Melde-Seite</a>.<br><br>Wir wünschen dir weiterhin gute Erfahrungen in unserer Community!<br>Social Meail<br><br>P.S.: Diesen E-Mail-Typ kannst du in den <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abbestellen, falls er dir überflüssig erscheint.';
			$h2t = new Plugin_Mailer_html2text($mailtext);
			$mail = new Plugin_PHPMailer_PHPMailer;
			$mail->IsSMTP();
			$mail->AddAddress($userdata->email);
			$mail->WordWrap = 50;
			$mail->IsHTML(true);
			$mail->Subject = $subject;
			$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
			$mail->AltBody = $h2t->get_text();
			if(!$mail->Send()){
				echo('Mail Error:'.$mail->ErrorInfo);
			}
			unset($userdata);
		}
	}
	Plugin_Events_Manager::setAfterMailDone($event->id);
}

//Mail senden wenn die Anmeldefrist abgelaufen ist
foreach(Plugin_Events_Manager::getAllAsEventMail() as $event){
	$userinfo = Plugin_Profile_Manager::getUserDataPerId($event->userid);
	if(Plugin_Events_Manager::countSubscribers($event->id) > 0){
		$subject = utf8_decode('Die Anmeldefrist für dein Meal '.$event->eventname.' auf Social Meal ist erfolgreich abgelaufen');
		$mailtext = 'Liebe*r '.ucfirst($userinfo->firstname).'<br><br>Wir möchten dich darüber informieren, dass die Anmeldefrist für dein Meal <a href="'.Settings::getInstance()->get("host").'event-details.html?event='.$event->id.'">'.$event->eventname.'</a> mit mindestens einer Anmeldung abgelaufen ist.<br><br>Hier schicken wir dir deine definitive Gästeliste sowie die notwendigen Kontaktdaten:<br>';
		foreach(Plugin_Events_Manager::getSubscribesByEvent($event->id) as $subscriber){
			$subscriberinfo = Plugin_Profile_Manager::getUserDataPerId($subscriber->userid);
			$mailtext .= '<a href="'.Settings::getInstance()->get("host").'mein-profil.html?profile='.$subscriberinfo->name.'">'.$subscriberinfo->firstname.' '.$subscriberinfo->lastname.'</a>';
			$mailtext .= '<br>Tel: '.$subscriberinfo->tel.'';
			$mailtext .= '<br>';
			if($subscriber->eventoption > 0){
				if($subscriber->eventoption == 1){
					$option = $event->option1;
				}elseif($subscriber->eventoption == 2){
					$option = $event->option2;
				}elseif($subscriber->eventoption == 3){
					$option = $event->option3;
				}
				$mailtext .= 'Anmeldeoption: '.$option.'<br>';
			}
			$mailtext .= '<br>';
		}
		$mailtext .= 'Für ein gutes Erlebnis für dich und deine Gäste möchten wir dir die Befolgung unserer Tipps, <a href="'.Settings::getInstance()->get("host").'FAQ/das-solltest-du-als-gastgeber-beachten.html">Das solltest du als Gastgeber*in beachten</a>, nahelegen.<br><br>Wir möchten dich darauf hinweisen, dass dein Meal einem verbindlichen Angebot entspricht und auf unsere <a href="'.Settings::getInstance()->get("host").'agb.html">AGBs</a> verweisen. Da die Anmeldefrist abgelaufen ist, ist es dir nun nicht mehr möglich das Meal über Social Meal abzusagen. In Notsituationen empfehlen wir dir die direkte Kontaktaufnahme mit deinen Gästen.<br><br>Wir wünschen dir ein spannendes Meal mit deinen Gästen!<br>Social Meal<br><br>P.S.: Dieser E-Mail-Typ ist nicht abbestellbar, da er für die Funktion der Plattform essentielle Inhalte übermittelt.';
	}else{
		$subject = utf8_decode('Die Anmeldefrist für dein Meal '.$event->eventname.' auf Social Meal ist erfolglos abgelaufen');
		$mailtext = 'Liebe*r '.ucfirst($userinfo->firstname).'<br><br>Leider müssen wir dich darüber informieren, dass die Anmeldefrist für dein Meal <a href="'.Settings::getInstance()->get("host").'event-details.html?event='.$event->id.'">'.$event->eventname.'</a> ohne Anmeldung abgelaufen ist.<br><br>Damit du mit deinem nächsten Meal mehr Erfolg hast, empfehlen wir dir, unsere Tipps für das erfolgreiche Erstellen von <a href="'.Settings::getInstance()->get("host").'FAQ/erstellung-von-privat-events.html">Privat-Meals</a>, <a href="'.Settings::getInstance()->get("host").'FAQ/erstellung-von-restaurant-events.html">Restaurant-Meals</a> oder <a href="'.Settings::getInstance()->get("host").'FAQ/erstellung-von-spezial-events.html">Spezial-Meals</a> zu berücksichtigen. Ansonsten findest du womöglich ein passendes Meal von einem anderen User unter <a href="'.Settings::getInstance()->get("host").'essen-gehen.html">Essen gehen</a>.<br><br>Das nächste Mal klappts bestimmt!<br>Social Meal<br><br>P.S.: Dieser E-Mail-Typ ist nicht abbestellbar, da er für die Funktion der Plattform essentielle Inhalte übermittelt.';
	}
	$h2t = new Plugin_Mailer_html2text($mailtext);
	$mail = new Plugin_PHPMailer_PHPMailer;
	$mail->IsSMTP();
	$mail->AddAddress($userinfo->email);
	if($event->eventtype == 3 && $event->orgid > 0){
		$orginfo = Plugin_Profile_Manager::loadOrganisation($event->orgid);
		if($orginfo->asmail == 1){
			$mail->AddAddress($orginfo->orgmail);
		}
	}
	$mail->WordWrap = 50;
	$mail->IsHTML(true);
	$mail->Subject = $subject;
	$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
	$mail->AltBody = $h2t->get_text();
	if(!$mail->Send()){
		echo('Mail Error:'.$mail->ErrorInfo);
	}
	Plugin_Events_Manager::setAllAsReadFromEvent($event->id);
	Plugin_Events_Manager::setAsMailDone($event->id);
}
?>
