<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

	if(isset($args['form']) && $args['form']->id == 1){
		$language = DataBase::Current()->EscapeString($args['form']->GetValue("token"));
		$language = strtoupper($language);
		DataBase::Current()->Execute("CREATE TABLE `cl_languagepack_".$language."_global` (
										`token` varchar(255) NOT NULL,
										`text` text NOT NULL,
										PRIMARY KEY  (`token`)
									  ) ENGINE=MyISAM DEFAULT CHARSET=latin1;");
	}
?>
