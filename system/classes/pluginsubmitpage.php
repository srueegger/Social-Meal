<?php
/**
 * Description of pluginsubmitpage
 *
 * @author Stefan
 */
 /*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

class PluginSubmitPage extends Editor{

    /**
     *
     * @param Page $page 
     */
    public function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
        
      if(isset($_POST['plugin_submit']))
      {
            $to      = "support@contentlion.org";
            $subject = "Neues Plugin! ".Settings::getValue("apikey");
            $message = print_r($_POST,true);
            $headers = "From:".$_POST['email'];
            if(@mail($to, $subject, $message, $headers))
            {
                echo "<p>".Language::DirectTranslateHtml("_PLUGIN_SUBMITTED")."</p>";
            }
            else
            {
                echo "<p>".Language::DirectTranslate("_PLUGIN_NOT_SUBMITTED")."</p>";
            }
      }
      else
      {
          echo Language::DirectTranslate("SUBMIT_PLUGIN_PLEASE");
          ?>
            <form method="POST">
                <table>
                    <tr>
                        <td><?PHP echo Language::DirectTranslateHtml("COMPANY_OPTIONAL"); ?>:</td>
                        <td><input name="company" /></td>
                    </tr>
                    <tr>
                        <td><?PHP echo Language::DirectTranslateHtml("FIRSTNAME"); ?>:</td>
                        <td><input name="firstname" /></td>
                    </tr>
                    <tr>
                        <td><?PHP echo Language::DirectTranslateHtml("LASTNAME"); ?>:</td>
                        <td><input name="lastname" /></td>
                    </tr>
                    <tr>
                        <td><?PHP echo Language::DirectTranslateHtml("EMAIL_ADDRESS"); ?>:</td>
                        <td><input name="email" /></td>
                    </tr>
                    <tr>
                        <td><?PHP echo Language::DirectTranslateHtml("URL_OF_ZIP"); ?>:</td>
                        <td><input type="url" name="zip" /></td>
                    </tr>
                    <tr>
                        <td><?PHP echo Language::DirectTranslateHtml("COMMENT"); ?></td>
                        <td>
                            <textarea name="comment"></textarea>
                            
                        </td>
                    </tr>
                </table>
                <input name="plugin_submit" type="submit" value="<?PHP echo Language::DirectTranslateHtml("SUBMIT_PLUGIN"); ?>" />
            </form>

          <?PHP
      }
    }
    
    function getHeader(){
    }
    
    public function getEditableCode(){
      return "<input name=\"save\" type=\"submit\" value=\"".htmlentities(utf8_encode(Language::DirectTranslate("CHANGE")))."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }    
    
    
}

?>
