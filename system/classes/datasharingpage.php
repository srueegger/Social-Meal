<?php
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

class DataSharingPage extends Editor{
    
    /**
     *
     * @param Page $page 
     */
    public function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
        if(isset($_POST['create'])){
            $share = new DataTypeShare();
            $share->SetApiKey($_POST['apikey']);
            $share->SetDataType(new DataType($_GET['datatype']));
            $share->SetName($_POST['name']);
            $share->Insert();
            echo "Die Freigabe wurde erteilt.";
        }
        else{
            echo "<form method=\"POST\">
                      <table>
                          <tr>
                              <td>Name:</td>
                              <td><input name=\"name\" /></td>
                          </tr>
                          <tr>
                              <td>API-Key:</td>
                              <td><input name=\"apikey\" /></td>
                          </tr>
                      </table>
                      <input name=\"create\" type=\"submit\" value=\"".Language::DirectTranslateHtml("CREATE")."\" />
                  </form>";
        }
    }

    public function getHeader(){
        
    }
    
    public function getEditableCode(){
        
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }    
    
    
    
}

?>
