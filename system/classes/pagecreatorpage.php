<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

  class PageCreatorPage extends Editor{

    /**
     *
     * @param Page $page
     */
    function __construct(Page $page){
      $this->page = $page;
    }

    public function display(){
      if(!isset($_POST['localalias'])){
        $template = new Template();
        $template->load("site_new");
        $template->assign_var("URL",$this->page->GetUrl());
        if(!isset($_POST['alias'])){
            $_POST['alias'] = "";
        }
        $template->assign_var("ALIAS",$_POST['alias']);
        if(substr($_SESSION['dir'],0,1) == "/" && $_SESSION['dir'] != "/"){
          $template->assign_var("DIR",substr($_SESSION['dir'],1)."/");
        }
        else if($_SESSION['dir'] == '' || $_SESSION['dir'] == "/"){
          $template->assign_var("DIR","");
        }
        else{
          $template->assign_var("DIR",$_SESSION['dir']."/");
        }
        $template->assign_var("HOST",Settings::getInstance()->get("host"));
        $template->assign_var("ENDING",".html");
        $types = Page::getPagesTypes();
        if($types){
          foreach($types as $type){
            $index = $template->add_loop_item("TYPES");
            $template->assign_loop_var("TYPES",$index,"CLASS",$type->class);
            $template->assign_loop_var("TYPES",$index,"NAME",utf8_encode($type->name));
          }
        }
        $template->output();
      }
      else{
        if($_SESSION['dir']){
          $dir = DataBase::Current()->EscapeString($_SESSION['dir'])."/";
        }
        else{
          $dir = "";
        }
        if(substr($dir,0,1) == "/"){
          $dir = substr($dir,1);
        }
        if($this->isValidPagename($_POST['localalias'])){
            $id = Page::create($dir.DataBase::Current()->EscapeString($_POST['localalias']),DataBase::Current()->EscapeString($_POST['type']),$_POST['title']);
            if($id){
              $template = new Template();
              $template->load("site_new_succeeded");
              $url = UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$dir.$_POST['localalias']);
              $template->assign_var("URL",$url);
              $template->output();
            }
            else{
                $failed = true;
            }
        }
        else{
            $failed = true;
        }
        if(isset($failed)){
            $template = new Template();
            $template->load("message");
            $template->assign_var("MESSAGE",Language::GetGlobal()->getString("PAGE_NOT_CREATED"));
            $template->output();
        }

      }
    }

    /**
     *
     * @return string
     */
    function getHeader(){
      $host = Settings::getInstance()->get("host");
      return "<script type=\"text/javascript\" src=\"".$host."system/plugins/jquery/jquery-1.4.2.js\"></script>";
   }

   /**
    *
    * @return string
    */
    public function getEditableCode(){
      return "<input name=\"save\" type=\"submit\" value=\"".htmlentities(utf8_encode(Language::DirectTranslate("CHANGE")))."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }

    /**
     *
     * @param Page $newPage
     * @param Page $oldPage
     */
    public function save(Page $newPage,Page $oldPage){
    }

    /**
     *
     * @param string $separator
     * @param string $class
     * @param string $idpraefix
     */
    public function displayBreadcrumb($separator,$class,$idpraefix){
      FolderBreadcrumb::display($this->page,$separator,$class,$idpraefix);
    }


    protected function isValidPagename($name){
        preg_match("/^[A-Za-z0-9_\-]*$/", $name,$matches);
        return isset($matches) && sizeof($matches) > 0;
    }
}
?>
