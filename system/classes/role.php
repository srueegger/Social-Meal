<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class Role{
    public $name = "";
    public $ID   = -1;
    
    /**
     *
     * @param int $id 
     */
    public function load($id){
      $id   = DataBase::Current()->EscapeString($id);
      $role = DataBase::Current()->ReadRow("SELECT name FROM {'dbprefix'}roles WHERE id = '".$id."'");
      if($role){
        $this->name = $role->name;
        $this->ID   = $id;
      }
    }
    
    /**
     *
     * @return Role 
     */
    public static function getAllRoles(){
      if(Cache::contains("roles","all")){
        $res = Cache::getData("roles","all");
      }
      else{
        $roles = DataBase::Current()->ReadRows("SELECT id FROM {'dbprefix'}roles ORDER BY name, id");
        foreach($roles as $role){
          $newRole = new Role();
          $newRole->load($role->id);
          $res[] = $newRole;
        }
        Cache::setData("roles","all",$res);
      }
      return $res;
    }
    
    /**
     *
     * @param Page $page
     * @return boolean 
     */
    public function canAccess(Page $page){
      $id     = DataBase::Current()->EscapeString($this->ID);
      $pageid = DataBase::Current()->EscapeString($page->id);
      return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}role_rights WHERE role = '".$id."' AND page = '".$pageid."'") > 0;
    }
    
    /**
     *
     * @param Page $page
     * @return boolean
     */
    public static function clearAccess(Page $page){
      $pageid = DataBase::Current()->EscapeString($page->id);
      return DataBase::Current()->Execute("DELETE FROM {'dbprefix'}role_rights WHERE page = '".$pageid."'");
    }
    
    /**
     *
     * @param Page $page
     * @return boolean
     */
    public function allowAccess(Page $page){
      return $this->allowAccessByID($page->id);
    }
    
    /**
     *
     * @param int $pageid
     * @return boolean 
     */
    public function allowAccessByID($pageid){
      $id     = DataBase::Current()->EscapeString($this->ID);
      $pageid = DataBase::Current()->EscapeString($pageid);
      return DataBase::Current()->Execute("INSERT INTO {'dbprefix'}role_rights (role,page) VALUES('".$id."','".$pageid."')");
    }

    /**
     *
     * @return boolean
     */
    public function insert(){
      $name = DataBase::Current()->EscapeString($this->name);
      $res =  DataBase::Current()->Execute("INSERT INTO {'dbprefix'}roles (name) VALUES('".$name."')");
      Cache::clear("roles");
      Cache::clear("tables","rolelist");
      EventManager::RaiseEvent("role_inserted", array("name" => $this->name));
      return $res;
    }

    /**
     *
     * @return boolean
     */
    public function delete(){
      $res = false;
      if($this->ID > 3){
        $id = DataBase::Current()->EscapeString($this->ID);
        $res =  DataBase::Current()->Execute("DELETE FROM {'dbprefix'}roles WHERE id = '".$id."'");
        if($res) $res = DataBase::Current()->Execute("DELETE FROM {'dbprefix'}role_rights WHERE role = '".$id."'");
        if($res) $res = DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET role = '1' WHERE role = '".$id."'");
        Cache::clear("roles");
        Cache::clear("tables","rolelist");
        EventManager::RaiseEvent("role_deleted", array("name" => $this->name,"obj" => $this));
      }
      return $res;
     }

  }
?>
