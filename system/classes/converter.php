<?PHP
class Converter{
  
  /**
   *
   * @param int $timestamp
   * @param string $notation
   * @return date 
   */
   /*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  function timestampToDate($timestamp,$notation){
	switch ($notation){
		case "iso8601":
			$date=date("Y-m-d",$timestamp);
			break;
		default:
			$date=date("d.M Y",$timestamp);
	}
	return $date;
  }
  
  /**
   *
   * @param int $timestamp
   * @param string $notation
   * @return date 
   */
  function timestampToTime($timestamp,$notation){
	switch ($notation){
		case "us":
			$time=date("h:i a",$timestamp);
			break;
		default:
			$time=date("H:M",$timestamp);
	}
	return $date;
  }
  
 }
