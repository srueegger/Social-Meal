<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  /**
  * @package ContentLion-Core
  * @author Stefan Wienstroeer
  */
  class DataType{
    private $id          = -1;
    private $displayName = "";
    private $dataName    = "";
    private $description = "";
    private $fields      = array();
    private $validators  = null;

    /**
    * 
    * Creates a new datatype object
    * @param int id of the datatype
    */
    public function __construct($id){
      $this->id = $id;
      $id  = DataBase::Current()->EscapeString($id);
      if($obj = DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}datatypes WHERE id='".$id."'")){
        $this->displayName = $obj->displayName;
        $this->dataName    = $obj->dataName;
        $this->description = $obj->description;
      }
    }
    
    /**
    * 
    * returns a datatype by name. Is null if nothing was found.
    * @param string name of the datatype
    * @return DataType founded datatype
    */
    public static function GetByName($name){
        $name = DataBase::Current()->EscapeString($name);
        $id = DataBase::Current()->ReadField("SELECT id FROM {'dbprefix'}datatypes WHERE dataname = '".$name."'");
        if($id){
            return new DataType($id);
        }
        else{
            return null;
        }
    }

    /**
    * 
    * returns the datafields
    * @return list of datafields
    */
    public function getFields(){
      if(sizeOf($this->fields) == 0){
        $this->fields = DataField::getByDataType($this);
      }
      return $this->fields;
    }

    /**
    * 
    * returns the id of the datatype
    * @return int id
    */
    public function getID(){
      return $this->id;
    }

    /**
     *
     * @param array $values
     * @param array $ignoreColumns
     * @return string 
     */
    public function getInsertStatement($values, $ignoreColumns = array()){
      return "INSERT INTO {'dbprefix'}".$this->dataName." (".$this->getCommaSeparatedColumnNames($ignoreColumns).") VALUES (".$this->commaSepearate($values).");";
    }

    /**
     *
     * @param array $ignoreColumns
     * @return string 
     */
    private function getCommaSeparatedColumnNames($ignoreColumns){
      $res = "";
      foreach($this->getFields() as $field){
        if(!is_array($ignoreColumns) || !in_array($field->getDataName(),$ignoreColumns)){
          $res .= $field->getDataName().",";
        }
      }
      if(strlen($res) > 0){
        $res = substr($res,0,-1);
      }
      return $res;
    }

    /**
     *
     * @param array $values
     * @return string 
     */
    private function commaSepearate($values){
      $res = "";
      foreach($values as $value){
        $res .= $value.",";
      }
      if(strlen($res) > 0){
        $res = substr($res,0,-1);
      }
      return $res;
    }

    /**
     *
     * @return array 
     */
    private function getValidators(){
      if($this->validators == null){
        $this->validators = DataTypeValidator::getByDataType($this);
      }
      return $this->validators;
    }

    /**
    * 
    * validates the params for insert
    * @param array key-value pairs, fields have to be escaped!
    * @return boolean is valid
    */
    public function validate($params){
      $res = true;
      foreach($this->getValidators() as $validator){
        $res = $validator->validate($params) && $res;
      }
      return $res;
    }

    /**
     *
     * @return array 
     */
    public function getValidatiingErrors(){
      $res = array();
      foreach($this->getValidators() as $validator){
        $message = $validator->getErrorMessage();
        if($message != ""){
          $res[] = $message;
        }
      }
      return $res;
    }

    /**
     *
     * @return array 
     */
    public function getAll(){
        return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}".$this->dataName);
    }
    
    /**
     *
     * @return string 
     */
    public function getDataName(){
        return $this->dataName;
    }
    
    /**
     *
     * @return array 
     */
    public function getShares(){
        return DatatypeShare::getByDataType($this);
    }
    
    /**
     *
     * @param string $apikey
     * @return DataTypeShare 
     */
    public function getShare($apikey){
        return DataTypeShare::GetByDataTypeAndApiKey($this,$apikey);
    }
    
    /**
     * @param string $apikey 
     * @return boolean 
     */
    public function allowShare($apikey){
        return $this->getShare($apikey) != null;
    }

}
?>
