<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class menueselector extends Control{
    public $style = '';
    
    /**
     *
     * @return string 
     */
    public function getCode(){
      $res = "<select name=\"".$this->name."\" style=\"".$this->style."\">";
      foreach(sys::getAllMenues() as $menue){
        if($menue->id == $this->value){
          $res .= "<option value=\"".$menue->id."\" selected=\"1\">".$menue->name."</option>";
        }
        else{
          $res .= "<option value=\"".$menue->id."\">".$menue->name."</option>";
        }
      }
      $res .= "</select>";
       return $res;
    }

  }
?>
