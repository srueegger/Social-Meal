<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class skinselector extends Control{
    public $type = "";
    
     /**
      *
      * @return string
      */
     public function getCode(){
      $template = new Template();
      $template->load("control_skinselector");
      $template->assign_var("NAME",$this->name);
      $template->assign_var("SELECTORNAME",$this->type);
      $template->assign_var("VALUE",$this->value);
      $template->assign_var("HOST",Settings::getValue("host"));
       $template->assign_var("CURRENTSKINNAME",SkinController::getSkinName($this->value));
      $i = 0;
      foreach(SkinController::getInstalledSkins() as $skin){
        $index = $template->add_loop_item("SKINS");
        $template->assign_loop_var("SKINS", $index, "SELECTORNAME",$this->type);
        $template->assign_loop_var("SKINS", $index, "INDEX", $i);
        $template->assign_loop_var("SKINS", $index, "SKINID", $skin->id);
        $template->assign_loop_var("SKINS", $index, "SKINNAME", $skin->name);
        $i++;
      }
      return $template->getCode();
    }

  }
?>
