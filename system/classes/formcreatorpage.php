<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class FormCreatorPage extends Editor{
      
    /**
     *
     * @param Page $page 
     */
    public function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
      $template = new Template();
      $template->load("new_form");
      $template->show_if("SUCCEEDED",false);
      $template->show_if("NOTSUCCEEDED",isset($_POST['name']) == false);
      if($_GET['blank'] == true){
        $url = UrlRewriting::GetUrlByAlias("admin/newform","blank=true");
      }
      else{
        $url = UrlRewriting::GetUrlByAlias("admin/newform");
      }
      $template->assign_var("URL",$url);
      if(isset($_POST['name'])){
        $template->show_if("SUCCEEDED");
        $id = Form::create($_POST['name'],$_SESSION['dir']);
        if(!$id){
          $template->load("message");
          $template->assign_var("MESSAGE",$language->getString("FORM_NOT_CREATED"));
        }
      }
      $template->output();
    }
    
    function getHeader(){
    }
    
    /**
     *
     * @return string
     */
    public function getEditableCode(){
      $change = htmnlentities(Language::GetGlobal()->getString("CHANGE"));
      return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }    
  }
?>
