<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

class sys{
	static function includeContent(){
		Page::Current()->getContent();
	}

	static function includeHeader(){
		echo "<title>".htmlentities(Page::Current()->title).Settings::getValue("title_extention")."</title>";
		if(Cache::contains("htmlmeta", Page::Current()->id)){
			echo Cache::getData ("htmlmeta", Page::Current()->id);
		}
		else{
				$cache = "";
				$rows = DataBase::Current()->ReadRows("SELECT name, content
														FROM {'dbprefix'}meta_global
														UNION SELECT name, content
														FROM {'dbprefix'}meta_local
														WHERE page = '". Page::Current()->id."'");
				if($rows){
					foreach($rows as $row){
						echo "<meta name=\"".htmlentities(utf8_encode($row->name))."\" content=\"".htmlentities(utf8_encode($row->content))."\" />";
						$cache .= "<meta name=\"".htmlentities(utf8_encode($row->name))."\" content=\"".htmlentities(utf8_encode($row->content))."\" />";
					}
				}
				Cache::setData("htmlmeta", Page::Current()->id, $cache);
		}
		echo	Page::Current()->getHeader();
		EventManager::RaiseEvent("header_included",null);
	}

	/**
	 *
	 * @param string $separator
	 * @param string $class
	 * @param string $idpraefix 
	 */
	static function displayBreadcrumb($separator,$class,$idpraefix){
		 Page::Current()->displayBreadcrumb($separator,$class,$idpraefix);
	}
	
	/**
	 *
	 * @param int $id
	 * @param string $globalstart
	 * @param string $globalend
	 * @param string $elementstart
	 * @param string $elementend
	 * @param string $class 
	 */
	static function displayMenu($id, $globalstart,$globalend, $elementstart,$elementend,
											 $class){
		Menu::display($id, $globalstart,$globalend, $elementstart,$elementend,$class);
	}

	/**
	 *
	 * @param string $globalstart
	 * @param string $globalend
	 * @param string $elementstart
	 * @param string $elementend
	 * @param string $class 
	 */
	static function displayGlobalMenu($globalstart,$globalend, $elementstart,$elementend,
											 $class){
			 Menu::display(Settings::getInstance()->get("mainmenu"),
										 $globalstart,
												$globalend,
										 $elementstart,
										 $elementend,
										 $class);
	}
 
	/**
	 *
	 * @param string $globalstart
	 * @param string $globalend
	 * @param string $elementstart
	 * @param string $elementend
	 * @param string $class 
	 */
	static function displayLocalMenu($globalstart,$globalend, $elementstart,$elementend,
											 $class){
		if(Page::Current()->menu > -1){
			Menu::display(Page::Current()->menu, $globalstart,$globalend, $elementstart,$elementend,$class);
		}
	}

	
	/**
	 *
	 * @param string $area
	 * @param string $areaType
	 * @param string $id
	 * @return string
	 */
	static function getColor($area,$areaType,$id){
		return "#".Settings::getInstance()->specify($areaType,$area)->get("skin".$id);
	}

	/**
	 *
	 * @return boolean
	 */
	static function localMenuExists(){
		if(Page::Current()->menu){
			return true;
		}
		else{
			return false;
		}
	}
	
	/**
	 *
	 * @return string
	 */
	static function getTitle(){
		return	Settings::getInstance()->get("title");
	}
	 
	/**
	 *
	 * @return string
	 */
	static function getFullSkinPath(){
		return Settings::getInstance()->get("host").SkinController::getCurrentSkinPath()."/";
	}

	/**
	 *
	 * @param string $dir
	 * @return array
	 */
	static function getMenues($dir){
		$dir = DataBase::Current()->EscapeString($dir);
		return DataBase::Current()->ReadRows("SELECT id, name, (
																			 SELECT COUNT( * ) 
																			 FROM {'dbprefix'}menu
																			 WHERE menuID = {'dbprefix'}menu_names.id
																		 )count
																		 FROM `{'dbprefix'}menu_names`
																		 WHERE dir = '".$dir."'");
	}

	/**
	 *
	 * @return array
	 */
	public static function getAllMenues(){
		return DataBase::Current()->ReadRows("SELECT id, name, (
																			 SELECT COUNT( * ) 
																			 FROM {'dbprefix'}menu
																			 WHERE menuID = {'dbprefix'}menu_names.id
																		 )count
																		 FROM `{'dbprefix'}menu_names`");
	}

	/**
	 *
	 * @return string
	 */
	static function getFooter(){
		return VERSION_TEXT;
	}

	/**
	 *
	 * @return string
	 */
	static function getCurrentUserName(){
		return User::Current()->name;
	}

	public static function parseGetParams(){
		$pos = strpos($_SERVER['REQUEST_URI'],'?');
		foreach(explode("&", substr($_SERVER['REQUEST_URI'], $pos + 1, strlen($_SERVER['REQUEST_URI']) - 8)) as $param){
			$splitter = explode('=', $param);
			if(sizeOf($splitter) > 1){
				$_GET[$splitter[0]] = urldecode($splitter[1]);
				if(strpos($_GET[$splitter[0]], "&#") !== false){
					$_GET[$splitter[0]] = urldecode($splitter[1]);
				}
			}
		}
	}

	/**
	 *
	 * @return string 
	 */
	public static function getAlias(){
		if(!isset($_GET['include'])){
				$_GET['include'] = "home";
		}
		return $_GET['include'];
	}
}
?>
