<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class CustomList extends View{
    public $template    = "";
    public $showButtons = true;
    public $paddingLeft = -1;
    public $escape      = true;

    /**
     *
     * @return string 
     */
    public function getCode(){
      $outerTemplate = new Template();
      $outerTemplate->load("list");
      $outerTemplate->assign_var("NAME",$this->name);
      $outerTemplate->assign_var("STYLE",$this->getListStyle());
      if($rows = DataBase::Current()->ReadRows($this->fillSelect)){
        foreach($rows as $row){
          $index = $outerTemplate->add_loop_item("ITEMS");
          $innerTemplate = new Template();
          $innerTemplate->load($this->template);
          $vars = get_object_vars($row);
          foreach($vars as $key=>$value){
            $innerTemplate->assign_var(strtoupper($key),$value);
          }
          $outerTemplate->assign_loop_var("ITEMS",$index,"ITEM",$innerTemplate->getCode($this->escape));
        }
      }
      $res = $outerTemplate->getCode();
      return $res;
    }

     private function getListStyle(){
       $res = "";
       if(!$this->showButtons){
         $res .= "list-style-type:none;";
       }
       if($this->paddingLeft != -1){
         $res .= "padding-left:".$this->paddingLeft.";";
       }
       return $res;
     }

  }
?>
