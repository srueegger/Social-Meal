<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class DataValidator{
    private $id        = -1;
    private $name      = "";
    private $regex     = "";
    private $message   = "";
    private $htmlCode  = "";
    private $lastError = "";

    public function __construct($id){
      $id  = DataBase::Current()->EscapeString($id);
      if($obj = DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}data_validator WHERE id = '".$id."'")){
        $this->id       = $obj->id;
        $this->name     = $obj->name;
        $this->regex    = $obj->regex;
        $this->message  = $obj->message;
        $this->htmlCode = $obj->htmlCode;
      }
    }

    /**
     *
     * @param string $name
     * @param string $value
     * @return string
     */
    public function getControlCode($name,$value){
      $res = $this->htmlCode;
      $res = str_ireplace("{VAR:NAME}",$name,$res);
      $res = str_ireplace("{VAR:VALUE}",htmlentities(utf8_encode($value)),$res);
      return $res;
    }

    /**
     *
     * @param string $value
     * @return boolean
     */
    public function validate($value){
      $res = true;
      preg_match($this->regex, $value, $results);
      $res = sizeOf($results) > 0;
      if(!$res){
        $this->lastError = $this->message."<br />";
      }
      return $res;
    }

    /**
     *
     * @return string
     */
    public function getLastError(){
      return $this->lastError;
    }

  }
?>
