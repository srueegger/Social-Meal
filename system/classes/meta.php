<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class Meta{
    public $pageid      = '';
    public $description = '';
    public $keywords    = '';
    public $robots      = 'index, follow';
    
    public function load(){
      $rows = DataBase::Current()->ReadRows("SELECT name, content
                                       FROM {'dbprefix'}meta_local
                                       WHERE page = '".$this->pageid."'");
      if($rows){
        foreach($rows as $row){
          if(strtolower($row->name == 'description')){
            $this->description = $row->content;
          }
          else if(strtolower($row->name == 'keywords')){
            $this->keywords = $row->content;
          }
          else if(strtolower($row->name == 'robots')){
              $this->robots = $row->content;
          }
        }
      }
    }
    
    public function save(){
      $keywords    = DataBase::Current()->EscapeString($this->keywords);
      $description = DataBase::Current()->EscapeString($this->description);
      $robots      = DataBase::Current()->EscapeString($this->robots);
      DataBase::Current()->Execute("DELETE FROM {'dbprefix'}meta_local WHERE page = '".$this->pageid."'");
      if(trim($keywords) != ""){
        DataBase::Current()->Execute("INSERT INTO {'dbprefix'}meta_local (page,               name      , content)
                                                              VALUES('".$this->pageid."','keywords','".$keywords."')");
      }
      if(trim($description) != ""){
        DataBase::Current()->Execute("insert into {'dbprefix'}meta_local (page,               name      , content)
                                                              VALUES('".$this->pageid."','description','".$description."')");
      }
      if(trim(strtolower($robots)) != 'index, follow'){
        DataBase::Current()->Execute("insert into {'dbprefix'}meta_local (page,               name      , content)
                                                              VALUES('".$this->pageid."','robots','".$robots."')");
      }                                        
    }
    
  }
?>
