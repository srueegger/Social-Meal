<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  abstract class Editor{
    
    protected $content = null;
    public   $page     = null; 
  
    abstract function display();
    abstract function getEditableCode();
    abstract function save(Page $newPage,Page $oldPage);
    abstract function getHeader();
    abstract function __construct(Page $page);
    
    function displayEditable(){
      echo $this->getEditableCode();
    }

    function displayBreadcrumb($separator,$class,$idpraefix){
      $i = 1;
      $breadcrumb = $this->page->getBreadcrumb();
      $host = Settings::getInstance()->get("host");
      while($i <= count($breadcrumb)){
        $url = UrlRewriting::GetUrlByAlias($breadcrumb[$i-1][0]);
        echo "<a href=\"".$url."\" class=\"".$class."\" 
              id=\"".$idpraefix.$i."\">".htmlentities(utf8_encode($breadcrumb[$i-1][1]))."</a>";
        if($i < count($breadcrumb)){
          echo $separator;
        }
        $i++;
      }
    }
  
    /**
     * Function for executing Http-Header
     */
    public function ExecuteHttpHeader(){
    }
  }
?>
