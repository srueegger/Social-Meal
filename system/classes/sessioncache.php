<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class SessionCache extends Cache{
      
    /**
     *
     * @param string $area
     * @param string $key
     * @param mixed $value 
     */
    public static function setData($area, $key, $value){
      if(USE_CACHING)
      {
        if(!isset($_SESSION['cache'])){
          $_SESSION['cache'] = array();
        }
        if(!isset($_SESSION['cache'][$area])){
          $_SESSION['cache'][$area] = array();
        }
        $_SESSION['cache'][$area][$key] = $value;
      }
    }

    /**
     *
     * @param string $area
     * @param string $key
     * @return mixed
     */
    public static function getData($area, $key){
      return $_SESSION['cache'][$area][$key];
    }

    /**
     *
     * @param string $area
     * @param string $key
     * @return boolean
     */
    public static function contains($area, $key){
      return USE_CACHING && isset($_SESSION['cache'][$area][$key]);
    }

    /**
     *
     * @param string $area
     * @param string $key 
     */
    public static function clear($area = "",$key = ""){
      $_SESSION['cache'] = array();
    }
  }

?>
