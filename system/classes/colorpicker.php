<?PHP
  /**
  * @package ContentLion-Core
  * @author Stefan Wienstroeer
  */
  /*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class colorpicker extends Control{

    /**
     *
     * @global boolean $colorPickerIncludes
     * @return string 
     */
    public function getCode(){
      global $colorPickerIncludes;
      $res = "";
      if(!$colorPickerIncludes){
        $colorPickerIncludes = true;
        $res .= "<script type=\"text/javascript\" src=\"/system/jscolor/jscolor.js\"></script>";
      }
    $res .=  "<input class=\"color\" name=\"".str_replace("\"","&quot;",htmlentities(utf8_encode($this->name)))."\" value=\"".str_replace("\"","&quot;",htmlentities(utf8_encode($this->value)))."\" />";
        return $res;
    }

  }
?>
