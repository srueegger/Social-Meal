<?PHP
  /**
  * @package ContentLion-Core
  * @author Stefan Wienstroeer
  */
  /*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class ActionList {

    public $actions  = array();
    public $category = "";

    protected function __construct(){}

    /**
     *
     * @return string 
     */
    public function getCategory(){
      return $this->category;
    }

    public function setCategory($value){
      $this->category = $value;
    }

    /**
     *
     * @param string $category
     * @return ActionList 
     */
    public static function get($category){
      $res = new ActionList();
      $res->setCategory($category);
      $res->loadActions();
      return $res;
    }

    protected function loadActions(){
      $category = DataBase::Current()->EscapeString($this->category);
      $actions = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}actionlists WHERE category='".$category."' ORDER BY id");
      foreach($actions as $action){
        $newAction = new Action();
        $newAction->setID($action->id);
        $newAction->setIcon($action->icon);
        $newAction->setDestination($action->destination);
        $newAction->setLabel($action->label);
        $this->actions[] = $newAction;
      }
    }

    /**
     *
     * @param array $params
     * @return string 
     */
    public function getCode($params){
      $res = "";

      foreach($this->actions as $action){
        $res .= $action->getCode($params)." ";
      }

      return $res;
    }

  }
?>
