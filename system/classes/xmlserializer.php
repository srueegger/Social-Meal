<?php
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

include_once(Settings::getValue("root")."system/interfaces/iapiprovider.php");

/**
 * Serializes a datatype to xml.
 *
 * @author Stefan Wienstroeer
 */
class XmlSerializer implements IApiProvider{
    
    /**
     *
     * @param mixed $object
     * @param DataType $datatype 
     */
    public function serialize($object,DataType $datatype) {
        header("Content-Type:text/xml");
        echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        echo "<apiresponse datatype=\"".$datatype->getDataName()."\">";
        echo $this->getSerializeCode($object,$datatype->getDataName());
        echo "</apiresponse>";
    }
    
    /**
     *
     * @param int $id
     * @param string $message 
     */
    public function raiseError($id,$message){
        header("Content-Type:text/xml");
        echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        echo "<apiresponse datatype=\"error\">";
        echo "<id>".htmlentities(utf8_encode($id))."</id>";
        echo "<message>".htmlentities(utf8_encode($message))."</message>";
        echo "</apiresponse>";
        
    }
    
    /**
     *
     * @param mixed $object
     * @return string 
     */
    private function getSerializeCode($object){
        $res = "";
        
        if(is_array($object) || is_object($object)){
            foreach($object as $key=>$value){
                if(is_numeric($key)){
                    $res .= "<item id=\"".$key."\">";
                    $res .= $this->getSerializeCode($value);
                    $res .= "</item>";
                }
                else{
                    $res .= "<".htmlspecialchars($key).">";
                    $res .= $this->getSerializeCode($value);
                    $res .= "</".htmlspecialchars($key).">";
                }
            }
        }
        else{
            $res .= utf8_encode(htmlspecialchars($object));
        }
        
        
        return $res;
    }
    
    /**
     *
     * @param string $url 
     */
    public function importFromUrl($url){
        $response = simplexml_load_file($url);
        if($response){
            $meta = $response->attributes();
            $dataType = DataType::GetByName($meta['datatype']);
            foreach($response->item as $item){
                $params = array();
                foreach($item as $key=>$value){
                    $params[$key] = $value."";
                }
                if($dataType->validate($params)){
                    foreach($params as $key=>$value){
                        $params[$key] = "'".DataBase::Current()->EscapeString($params[$key])."'";
                    }
                    DataBase::Current()->Execute($dataType->getInsertStatement($params));
                    echo "1";
                }
                else{
                    echo "0";
                }
            }
        }
    }
    
}

?>
