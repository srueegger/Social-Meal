<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  $feedback_content = "";
  
  function send_feedback_mail($info = null){
    $GLOBALS['feedback_content'] = $info;
  }

  class FeedbackPage extends Editor{
      
    /**
     *
     * @param Page $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
       if(isset($_POST['submit'])){
           ob_start("send_feedback_mail");
           echo "_POST\n";
           print_r($_POST);
           if($_POST['allowphpinfo']){
               echo "\n\n\n_GET\n";
               print_r($_GET);
               echo "\n\n\n_SERVER\n";
               print_r($_SERVER);
               echo "\n\n\nphpinfo\n";
               phpinfo();
               echo "\n\n\nsettings\n";
               print_r(DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}settings"));
               echo "\n\n\nskins\n";
               print_r(DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}skins"));
               echo "\n\n\nactivated_plugins\n";
               print_r(DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}activated_plugins"));
           }
           ob_end_clean();
           $to      = "support@contentlion.org";
           $subject = $_POST['subject'];
           $message = $GLOBALS['feedback_content'];
           $headers = "From:".$_POST['email'];
           if(@mail($to, $subject, $message, $headers))
           {
               if(strtoupper(Settings::getValue("language")) == "DE"){
                   echo "Feedback gesendet";
               }
               else{
                   echo "Feedback sent";
               }
           }
           else {
               if(strtoupper(Settings::getValue("language")) == "DE"){
                   echo "<p>Feedback konnte leider nicht gesendet werden. 
                        Melden Sie sich einfach in der <a href='http://beratung.contentlion.de'>CMS Beratung</a> oder an
                        <a href='mailto:support@contentlion.org'>support@contentlion.org</a></p>";
               }
               else{
                   echo "<p>Sorry, we cannot sent the feedback. Please contact <a href='mailto:support@contentlion.org'>support@contentlion.org</a></p>";
               }
 
           }
       }
       if(strtoupper(Settings::getValue("language")) == "DE"){
       ?>
<p>
    Durch Ihr Feedback kann sich ContentLion zu ihren Anspr&uuml;chen entwickeln!<br />
    Schreiben Sie uns einfach alles was Sie sich w&uuml;nchen oder anzumerken haben.
</p>
<form method="POST">
    <table>
        <tr>
            <td>Name:</td>
            <td><input name="name" style="width:300px" /></td>
        </tr>
        <tr>
            <td>E-Mail:</td>
            <td><input type="email" name="email" style="width:300px" /></td>
        </tr>
        <tr>
            <td>Betreff:</td>
            <td><input name="subject" style="width:300px" /></td>
        </tr>
    </table>
    <h2>Ihr Feedback</h2>
    <textarea style="width:600px;height:350px;" name="content"></textarea><br />
    <input type="checkbox" name="allowphpinfo" checked="checked" />
    <label for="allowphpinfo">Ich bin damit einverstanden, dass Informationen &uuml;ber <br />
        meine Einstellungen mitgesendet werden.</label><br />
    <input type="submit" name="submit" value="Absenden" />
</form>
           <?PHP
        }
        else{
       ?>
<p>
    You can help us make a better Content Manangement System!
    Just tell us all you wish about ContentLion.
</p>
<form method="POST">
    <table>
        <tr>
            <td>Name:</td>
            <td><input name="name" style="width:300px" /></td>
        </tr>
        <tr>
            <td>E-Mail:</td>
            <td><input type="email" name="email" style="width:300px" /></td>
        </tr>
        <tr>
            <td>Subject:</td>
            <td><input name="subject" style="width:300px" /></td>
        </tr>
    </table>
    <h2>Your Feedback</h2>
    <textarea style="width:600px;height:350px;" name="content"></textarea><br />
    <input type="checkbox" name="allowphpinfo" checked="checked" />
    <label for="allowphpinfo">Send my configuration</label><br />
    <input type="submit" name="submit" value="Absenden" />
</form>
           <?PHP
        }
    }
    
    function getHeader(){
    }
    
    /**
     *
     * @return string
     */
    public function getEditableCode(){
      $change = htmlentities(utf8_encode(Language::GetGlobal()->getString("CHANGE")));
      return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }    
}
?>
