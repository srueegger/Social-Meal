<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  abstract class WidgetBase{
    public $headline           = "";
    public $content            = "";
    public $minwidth           = -1;
    public $maxwidth           = -1;
    public $minheight          = -1;
    public $maxheight          = -1;
    public $displayType        = "default";
    public $allowDisplayRandom = true;
    public $path               = "";

    public function display(){
      echo $this->getCode();
    }

    /**
     *
     * @return string
     */
    public function getCode(){
      $template = new Template();
      $template->load("widget_".$this->displayType);
      $template->assign_var("HEADLINE",$this->headline);
      $template->assign_var("CONTENT",$this->content);
      $template->assign_var("MINWIDTH",$this->minwidth);
      $template->show_if("HASMINWIDTH",$this->minwidth != -1);
      $template->assign_var("MAXWIDTH",$this->maxwidth);
      $template->show_if("HASMAXWIDTH",$this->maxwidth != -1);
      $template->assign_var("MINHEIGHT",$this->minheight);
      $template->show_if("HASMINHEIGHT",$this->minheight != -1);
      $template->assign_var("MAXHEIGHT",$this->maxheight);
      $template->show_if("HASMAXHEIGHT",$this->maxheight != -1);
      return $template->getCode();
    }
    
    public abstract function load();

    /**
     *
     * @param int $dashboard
     * @param int $column
     * @param int $row
     * @return boolean
     */
    public function save($dashboard,$column,$row){
      $dashboard = DataBase::Current()->EscapeString($dashboard);
      $row       = DataBase::Current()->EscapeString($row);
      $column    = DataBase::Current()->EscapeString($column);
      $path      = DataBase::Current()->EscapeString($this->path);
      return DataBase::Current()->Execute("INSERT INTO {'dbprefix'}dashboards (alias, col, row, path) VALUES ('".$dashboard."','".$column."','".$row."','".$path."')");
    }
  }
?>
