<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class PageEditPage extends Editor{
    
    /**
     *
     * @param Page $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
      $template = new Template();
      $template->load("site_edit");
      $page = new Page();
      if(isset($_GET['site'])){
        $page->loadProperties(DataBase::Current()->EscapeString($_GET['site']));
        if(isset($_POST['save'])){
          $oldpage = $page;
          $page->title = $_POST['title'];
          $page->menu = $_POST['menu'];
          $page->getMeta()->description = $_POST['meta-description'];
          $page->getMeta()->keywords = $_POST['meta-keywords'];
          $page->getMeta()->robots = $_POST['meta-robots'];
          $page->alias = $page->getDir().$_POST['localalias'];
          $page->ownerid = $_POST['owner'];
          $page->canonical = $_POST['canonical'];
          $page->advancedHtmlHeader = $_POST['advanced_html_header'];
          $page->SetChangeFrequence($_POST['change_frequence']);
          if(isset($_POST['in_sitemap']))
          {
            $page->inSitemap = $_POST['in_sitemap'];
          }
	  else
	  {
            $page->inSitemap = false;
          }
          $page->priority = $_POST['priority'];
          $page->save();
          $page->getEditor()->save($page,$oldpage);
          Role::clearAccess($page);
          if($_POST['rights']){
            foreach($_POST['rights'] as $right){
              $role = new Role();
              $role->load($right);
              $role->allowAccess($page);
            }
          }
        }
        $url = UrlRewriting::GetUrlByAlias("admin/pageedit", "site=".$page->alias);
        $template->assign_var("PATH",$url);
        $template->assign_var("ALIAS",$page->alias);
        $template->assign_var("LOCALALIAS",$page->getLocalAlias());
        if(!isset($_POST['menu'])){
          foreach(Role::getAllRoles() as $role){
            $index = $template->add_loop_item("RIGHTS");
            $template->assign_loop_var("RIGHTS", $index, "LABEL","/");
            $template->assign_loop_var("RIGHTS", $index, "VALUE",$role->ID);
            $template->assign_loop_var("RIGHTS", $index, "NAME",utf8_encode($role->name));
            if($role->canAccess($page)){
              $template->assign_loop_var("RIGHTS", $index, "SELECTED", "selected=\"1\" ");
            }
            else{
              $template->assign_loop_var("RIGHTS", $index, "SELECTED", "");
            }
          }
        }
        else{
          foreach(Role::getAllRoles() as $role){
            $index = $template->add_loop_item("RIGHTS");
            $template->assign_loop_var("RIGHTS", $index, "LABEL","/");
            $template->assign_loop_var("RIGHTS", $index, "VALUE",$role->ID);
            $template->assign_loop_var("RIGHTS", $index, "NAME",utf8_encode($role->name));
            if(isset($_POST['rights']) && in_array($role->ID,$_POST['rights'])){
              $template->assign_loop_var("RIGHTS", $index, "SELECTED", "selected=\"1\" ");
            }
            else{
              $template->assign_loop_var("RIGHTS", $index, "SELECTED", "");
            }
          }
        }
        $template->assign_var("METADESCRIPTION",htmlentities($page->getMeta()->description));
        $template->assign_var("METAKEYWORDS",htmlentities($page->getMeta()->keywords));
        $template->assign_var("METAROBOTS",htmlentities($page->getMeta()->robots));
        $template->assign_var("CANONICAL",$page->canonical);
        $template->assign_var("ADVANCED_HTML_HEADER",htmlentities(utf8_encode($page->advancedHtmlHeader)));
        $template->assign_var("TITLE",$page->title);
        $template->assign_var("EDITOR",$page->getEditor()->getEditableCode());
        $template->assign_var("HOST",Settings::getRootInstance()->dir($page->alias."§page")->get("host").$page->getDir());
        $template->assign_var("CHANGE_FREQUENCE",$page->GetChangeFrequence());
        $template->assign_var("PRIORITY",$page->priority);
        if($page->inSitemap){
            $template->assign_var("IN_SITEMAP_CHECKED", " checked=\"checked\"");
        }
        else{
            $template->assign_var("IN_SITEMAP_CHECKED", "");
        }
        $selector = new PageSelector();
        $selector->name  = 'owner';
        $selector->value = $page->ownerid;
        $selector->noValueText = "-- ".Language::DirectTranslate("NOOWNER")." --";
        $template->assign_var("OWNERCONTROL", $selector->getCode());
        
        //Neuer Event werfen, Handänderung
        EventManager::RaiseEvent("sidebar_loaded",$page);

        $template->output();
      }
    }
 
   function getHeader(){
      return "";
   }
    
   /**
    *
    * @return string
    */
   public function getEditableCode(){
     return "<input name=\"save\" type=\"submit\" value=\"".htmlentities(utf8_encode(Language::DirectTranslate("CHANGE")))."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
   }
    
   /**
    *
    * @param Page $newPage
    * @param Page $oldPage 
    */
   public function save(Page $newPage,Page $oldPage){
   }

   /**
    *
    * @param string $separator
    * @param string $class
    * @param string $idpraefix 
    */
   public function displayBreadcrumb($separator,$class,$idpraefix){
     FolderBreadcrumb::display($this->page,$separator,$class,$idpraefix);
   }
}
?>
