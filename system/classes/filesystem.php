<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class FileSystem{

    /**
     *
     * @param string $path 
     */
    public static function deleteDir($path){
      $dir = opendir ($path);
	  if($dir){
		  while(($entry = readdir($dir)) !== false) {
			if($entry == '.' || $entry == '..') continue;
			if(is_file ($path.'/'.$entry) || is_link ($path.'/'.$entry)) {
			  unlink($path.'/'.$entry);
			}
			else if(is_dir($path."/".$entry)){
			  self::deleteDir($path."/".$entry);
			}
		  }
		  closedir ($dir);
		  rmdir ($path);
	  }
    }

    /**
     *
     * @param string $path
     * @return boolean 
     */
    public static function deleteFile($path){
      if(file_exists($path)){
        return unlink($path);
      }
      else{
        return false;
      }
    }

  }
?>
