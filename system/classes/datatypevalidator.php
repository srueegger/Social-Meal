<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class DataTypeValidator{
    private $id           = -1;
    private $dataTypeID   = -1;
    private $dataType     = null;
    private $select       = "";
    private $message      = "";
    private $finalMessage = "";

    /**
     *
     * @param DataType $type
     * @return DataTypeValidator 
     */
    public static function getByDataType(DataType $type){
      $res = array();
      $dataTypeID = DataBase::Current()->EscapeString($type->getID());
      $rows = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}datatype_validator WHERE dataType = '".$dataTypeID."'");
      foreach($rows as $row){
        $validator             = new DataTypeValidator();
        $validator->id         = $row->id;
        $validator->dataTypeID = $row->dataType;
        $validator->dataType   = $type;
        $validator->select     = $row->select;
        $validator->message    = $row->message;
        $res[] = $validator;
      }
      return $res;
    }

    /**
     *
     * @param array $params
     * @return boolean 
     */
    public function validate($params){
      $select = $this->select;
      foreach($params as $key=>$value){
        $select = str_ireplace("{VAR:".strtoupper($key)."}",DataBase::Current()->EscapeString($value),$select);
      }
      $res = DataBase::Current()->ReadField($select) > 0;
      if(!$res){
        $message = $this->message;
        foreach($params as $key=>$value){
          $message = str_ireplace("{VAR:".strtoupper($key)."}",DataBase::Current()->EscapeString($value),$message);
        }
        $this->finalMessage=$message;
      }
      return $res;
    }

    /**
     *
     * @return string 
     */
    public function getErrorMessage(){
      return $this->finalMessage;
    }

  }
?>
