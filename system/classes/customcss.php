<?PHP
  /**
  * @package ContentLion-Core
  * @author Stefan Wienstroeer
  * @link http://blog.stevieswebsite.de/2009/11/logout-customcss-cms/
  */
  /*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class CustomCSS{
 
    
    /**
    * 
    * Displays a custom stylesheet (html <link>) for a page id
    * @param int id of a page
    */
    function printStylesheet($id){
      $path = self::getStylePath($id);
      if($path){
        echo "<link href=\"".$path."\" rel=\"stylesheet\" type=\"text/css\" />";
      }
    }
    
    /**
    * 
    * Returns the url of custom stylesheet for a page id
    * @param int id of a page
    * @return string path of the stylesheet
    */
    function getStylePath($id){
      $id = DataBase::Current()->EscapeString($id);
      return DataBase::Current()->ReadField("SELECT stylePath FROM {'dbprefix'}custom_css
                        WHERE id = '".$id."'");
    }

  }
?>
