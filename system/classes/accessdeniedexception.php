<?PHP
  /**
   * Exception, if access is denied
   * @package ContentLion-Core
   * @author Stefan Wienströer
   * @link http://blog.stevieswebsite.de/2010/08/exception-system-cms/
   */
   /*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class AccessDeniedException extends ContentLionException{

    /**
     * Exception, if access is denied
     * @param string $message message for the user
     * @param int $code errorcode for automatic handling
     */
    public function __construct($message, $code = 0) {
      parent::__construct($message, $code);
      $page = new Page();
      $page->loadPropertiesById(Settings::getInstance()->get("accessdenied"));
      parent::setErrorPage($page);
    }

  }
?>
