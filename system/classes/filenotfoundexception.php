<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
  class FileNotFoundException extends ContentLionException{

    /**
     *
     * @param string $message
     * @param int $code 
     */
    public function __construct($message, $code = 0) {
      parent::__construct($message, $code);
      header("HTTP/1.0 404 Not Found");
      $page = new Page();
      $page->setEditor(new BlankEditor($page));
      $page->getEditor()->setContent("<h2>".Language::DirectTranslateHtml("PAGE_NOT_FOUND")."</h2><p>".Language::DirectTranslateHtml("PAGE_NOT_FOUND_LONG")."!</p>");
      parent::setErrorPage($page);
    }

  }
?>
