<?PHP
  /**
  * @package ContentLion-Core
  * @author Stefan Wienstroeer
  * @link http://blog.stevieswebsite.de/2010/09/caching-cms/
  */
  /*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  Class Cache{
    static function setData($area, $key, $value){
      FileCache::setData($area,$key,$value);
    }

    /**
     *
     * @param string $area
     * @param string $key
     * @return mixed cached data 
     */
    static function getData($area,$key){
      return FileCache::getData($area,$key);
    }

    static function clear($area = "",$key = ""){
      FileCache::clear($area,$key);
      SessionCache::clear($area,$key);
    }

    /**
     *
     * @param string $area
     * @param string $key
     * @return boolean
     */
    static function contains($area,$key){
      return FileCache::contains($area,$key);
    }
  }
?>
