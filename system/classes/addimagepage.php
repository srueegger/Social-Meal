<?PHP
  /**
  * @package ContentLion-Core
  * @author Stefan Wienstroeer
  */
  /*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

  class AddImagePage extends Editor{

    /**
     *
     * @param Page $page
     */
    public function __construct(Page$page){
      $this->page = $page;
    }

    public function display(){
      $template = new Template();
      $template->load("message");
      if(ImageServer::insert($_POST['path'],$_POST['name'],$_POST['description'])){
        $template->assign_var("MESSAGE",utf8_decode(Language::DirectTranslateHtml("IMAGE_ADDED")));
		$redirect = UrlRewriting::GetUrlByAlias("admin/home","dir=".urlencode($_SESSION['dir']));
		if(isset($_POST['referrer'])){
			$redirect = $_POST['referrer'];
		}
        echo "<script type='text/javascript'>setTimeout('window.location.href= \'".$redirect."\'', 1000)</script>";
      }
      else{
        $template->assign_var("MESSAGE",Language::DirectTranslateHtml("IMAGE_NOT_ADDED"));
      }
      $template->output();
    }

    function getHeader(){
    }

    /**
     *
     * @return string
     */
    public function getEditableCode(){
      return "<input name=\"save\" type=\"submit\" value=\"&Auml;ndern\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }

    /**
     *
     * @param Page $newPage
     * @param Page $oldPage
     */
    public function save(Page $newPage,Page $oldPage){
    }
}
?>
