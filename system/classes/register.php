<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class Register extends Editor{
      
    /**
     *
     * @param Page $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
        ?>
            <h2><?PHP echo Language::DirectTranslateHtml("SIGN_UP"); ?></h2>
        <?PHP
        if(User::Current()->isGuest()){
            ?>
            <form action="<?PHP echo $_SERVER['REQUEST_URI']; ?>" method="POST">
                <table>
                    <tr>
                        <td><?PHP Language::DirectTranslateHtml("USERNAME"); ?>:</td>
                        <td><input name="name" /></td>
                    </tr>
                    <tr>
                        <td><?PHP Language::DirectTranslateHtml("EMAIL"); ?>:</td>
                        <td> <input name="email" /></td>
                    </tr>
                </table>
                <input type="submit" value="<?PHP echo Language::DirectTranslateHtml("SIGN_UP"); ?>" />
            </form>
            <?PHP
        }
    }
    
    function getHeader(){
    }
    
    /**
     *
     * @return string
     */
    public function getEditableCode(){
      return "<input name=\"save\" type=\"submit\" value=\"".htmlentities(Language::DirectTranslate(utf8_encode("CHANGE")))."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }    
}
?>
