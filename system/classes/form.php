<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
class Form{

  public  $id              = -1;
  public  $name            = "";
  public  $dir             = "";
  public  $fields          = array();
  public  $buttonText      = "";
  public  $type            = 1;
  public  $dataTypeID      = -1;
  public  $captcha         = false;
  public  $showAfterInsert = false;
  public  $succeedMessage  = "";
  private $dataType        = null;
  private $inserted        = false;
  private $submissionTried = false;
  private $errorMessage    = "";

  /**
   *
   * @param int $id 
   */
  public function __construct($id){
    $this->id              = DataBase::Current()->EscapeString($id);
    $obj                   = DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}forms WHERE id = '".$id."'");
    $this->name            = $obj->name;
    $this->dir             = $obj->dir;
    $this->destinationType = $obj->destinationType;
    $this->buttonText      = $obj->buttonText;
    $this->dataTypeID      = $obj->datatype;
    $this->captcha         = $obj->captcha;
    $this->succeedMessage  = $obj->succeedMessage;
    $this->showAfterInsert = $obj->showAfterInsert;
  }

  /**
   *
   * @return array 
   */
  public function getFields(){
    if(sizeOf($this->fields) == 0){
      $this->loadFields();
    }
    return $this->fields;
  }

  /**
   *
   * @param FormField $field 
   */
  public function addField(FormField $field){
    $this->fields[] = $field;
    $field->form = $this;
  }

  private function loadFields(){
      $fields = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}form_fields WHERE form = '".$this->id."' ORDER BY sortIndex");
      if($fields){
        foreach($fields as $field){
          $objField = new FormField();
          $objField->form         = $this;
          $objField->sortIndex    = $field->sortIndex;
          $objField->label        = $field->label;
          $objField->show         = $field->show;
          $objField->insert       = $field->insert;
          $objField->mandatory    = $field->mandatory;
          $objField->dataName     = $field->dataName;
          $objField->preAllocate  = $field->preallocate;
          $objField->edit         = $field->edit;
          $objField->clear();
          $this->fields[] = $objField;
        }
      }
  }

  public function display(){
    echo $this->getCode();
  }

  /**
   *
   * @return string 
   */
  public function getCode(){
    $res = "";
    if(!$this->submissionTried){
      $this->submit();
    }
    if($this->inserted){
      $res .= "<p class='success_message'>".$this->succeedMessage."</p>";
    }
    if(!$this->inserted || $this->showAfterInsert){
      $res .= $this->getErrorMessage()."<form method=\"POST\">";
      if($fields = $this->getFields()){
        foreach($fields as $field){
          if($this->inserted){
            $field->clear();
          }
          $res .= $field->getCode();
        }
      }
      $res .= "<input class=\"formsubmit\" name=\"form".$this->id."submit\" type=\"submit\" value=\"".htmlentities(utf8_encode($this->buttonText))."\" /></form>";
    }
    return $res;
  }

  public function submit(){
    if($this->isSubmitted()){
      $values = array();
      $validate = true;
      $fields = $this->getFields();
      foreach($fields as $field){
        if($field->insert){
          $field->setValueFromArray($_POST);
          $values[] = $field->getSqlValue();
        }
        $validate = $field->validate() && $validate;
        $this->errorMessage .= $field->getLastError();
      }
      if($validate && $this->validateDataType($fields)){
        $this->inserted = DataBase::Current()->Execute($this->getDataType()->getInsertStatement($values,$this->getIgnoredColumns()));
      }
      $args = array();
      $args['form'] = $this;
      EventManager::RaiseEvent("FORM_SUBMITTED",$args);
    }
    $this->submissionTried = true;
  }
  
  public function GetValue($name){
      $res = null;
      
      $fields = $this->getFields();
      foreach($fields as $field){
        if($field->getDataField()->getDataName() == $name){
            $res = $field->getValue();
            break;
        }
      }
      
      return $res;
  }

  public function save(){
    $name           = DataBase::Current()->EscapeString($this->name);
    $buttonText     = DataBase::Current()->EscapeString($this->buttonText);
    $dataType       = DataBase::Current()->EscapeString($this->dataTypeID);
    $succeedMessage = DataBase::Current()->EscapeString($this->succeedMessage);
    if($this->captcha && $this->captcha != "off"){
      $captcha = '1';
    }
    else{
      $captcha = '0';
    }
    if(($this->showAfterInsert && $this->showAfterInsert != "off") || ($this->showAfterInsert == '1' && $this->showAfterInsert != '0')){
      $showAfterInsert = '1';
    }
    else{
      $showAfterInsert = '0';
    }
    DataBase::Current()->Execute("UPDATE {'dbprefix'}forms SET name = '".$name."', buttonText = '".$buttonText."',captcha = '".$captcha."',datatype='".$dataType."',succeedMessage = '".$succeedMessage."',showAfterInsert = '".$showAfterInsert."' WHERE id = '".$this->id."'");
    $this->clearFieldsFromDB();
    foreach($this->fields as $field){
      $field->save();
    }
  }

  public function clearFieldsFromDB(){
    DataBase::Current()->Execute("DELETE FROM {'dbprefix'}form_fields WHERE form = '".$this->id."'");
  }

  /**
   *
   * @return boolean
   */
  public function IsEmpty(){
    return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}form_fields WHERE form = '".$this->id."'") == 0;
  }

  /**
   *
   * @param string $name
   * @param string $dir
   * @return int
   */
  public static function create($name,$dir = ""){
    $name = DataBase::Current()->EscapeString($name);
    $dir = DataBase::Current()->EscapeString($dir);
    $res = DataBase::Current()->Execute("INSERT INTO {'dbprefix'}forms (name, dir) VALUES ('".$name."', '".$dir."')");
    if($res){
      $args['name']  = $name;
      EventManager::raiseEvent("form_created",$args);
    }
    return DataBase::Current()->InsertID();
  }
  
  /**
   * Deletes the form.
   */
  public function Delete(){
      $id = DataBase::Current()->EscapeString($this->id);
      DataBase::Current()->Execute("DELETE FROM {'dbprefix'}forms WHERE id = '".$id."'");
      DataBase::Current()->Execute("DELETE FROM {'dbprefix'}form_fields WHERE id = '".$id."'");#
      $args = array();
      $args['form'] = $this;
      EventManager::RaiseEvent("FORM_DELETED",$args);
  }

  /**
   *
   * @return array
   */
  public static function getAll(){
    return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}forms");
  }

  /**
   *
   * @param string $dir
   * @return array 
   */
  public static function getByDir($dir){
    $dir = DataBase::Current()->EscapeString($dir); 
    return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}forms WHERE dir = '".$dir."'");
  }

  /**
   *
   * @return DataType 
   */
  public function getDataType(){
    if($this->dataType == null){
      $this->dataType = new DataType($this->dataTypeID);
    }
    return $this->dataType;
  }

  /**
   *
   * @return array
   */
  private function getIgnoredColumns(){
    $res = array();
    foreach($this->fields as $field){
       if(!$field->insert){
         $res[] = $field->dataName;
       }
    }
    return $res;
  }

  /**
   *
   * @return boolean
   */
  private function isSubmitted(){
    return isset($_POST["form".$this->id."submit"]);
  }

  /**
   *
   * @return string 
   */
  private function getErrorMessage(){
    $res = "";
    if($this->errorMessage != ""){
      $res = "<p class='error_message'>".$this->errorMessage."</p>";
    }
    return $res;
  }

  /**
   *
   * @param array $fields
   * @return array 
   */
  private function getParams($fields){
    $res = array();
    foreach($fields as $field){
      $res[$field->dataName] = $field->getValue();
    }
    return $res;
  }

  /**
   *
   * @param array $fields
   * @return boolean
   */
  private function validateDataType($fields){
     $res = $this->getDataType()->validate($this->getParams($fields));
     if(!$res){
       foreach($this->getDataType()->getValidatiingErrors() as $error){
         $this->errorMessage .= $error."<br />";
       }
     }
     return $res;
  }
}
?>
