<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class MenuEditor extends Editor{
      
    /**
     *
     * @param Page $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
      SessionCache::clear();
      $template = new Template();
      $template->load("menu_edit");
      if(isset($_POST['add'])){
        if(trim($_POST['newtitle']) != ""){
          if(trim($_POST['newurl']) != ""){
            $entry        = new MenuEntry();
            $entry->menu  = $_GET['menu'];
            $entry->title = $_POST['newtitle'];
            $entry->type  = $_POST['type'];
            if($entry->type == 0){
              $entry->href  = $_POST['newPage'];
            }
            else if($entry->type == 1){
              $entry->href  = $_POST['newurl'];
            }
            else{
              $entry->href  = $_POST['newMenu'];
            }
            $res = $entry->save();
            if($res){
              $template->assign_var("MESSAGE",Language::DirectTranslate("ENTRY_ADDED"));
            }
            else{
              $template->assign_var("MESSAGE",Language::DirectTranslate("ENTRY_NOT_ADDED"));
            }
            Cache::clear("menu");
          }
          else{
            $template->assign_var("MESSAGE",Language::DirectTranslate("ENTER_LINK_TARGET"));
          }
        }
        else{
            $template->assign_var("MESSAGE",Language::DirectTranslate("ENTER_LINK_TITLE"));
        }
      }
      else if(isset($_POST['save'])){
        foreach($_POST as $param=>$value){
          if(strlen($param) >= 6){
            if(substr($param,-5,5) == "_href"){
              $entries[substr($param,0,-5)]['href'] = $value;
            }
            else if(strlen($param) >= 7 && substr($param,-6,6) == "_title"){
              $entries[substr($param,0,-6)]['title'] = $value;
            }
            else if(substr($param,-5,5) == "_type"){
              $entries[substr($param,0,-5)]['type'] = $value;
            }
          }
        }

       foreach($entries as $id=>$params){
          $entry        = new MenuEntry();
          $entry->id    = $id;
          $entry->menu  = $_GET['menu'];
          $entry->title = $params['title'];
          $entry->href  = $params['href'];
          $entry->type  = $params['type'];
          $entry->save();
        }
      }
      else if(isset($_GET['delete'])){
        Menu::deleteEntry(DataBase::Current()->EscapeString($_GET['menu']),
                          DataBase::Current()->EscapeString($_GET['delete']));
      }
      else if(isset($_GET['moveup'])){
        Menu::changePositions($_GET['menu'],$_GET['moveup'],$_GET['moveup']-1);
      }
      else if(isset($_GET['movedown'])){
        Menu::changePositions($_GET['menu'],$_GET['movedown'],$_GET['movedown']+1);
      }
  
      if(isset($_GET['menu'])){
        $template->assign_var("URL",$this->page->getUrl("menu=".urlencode($_GET['menu'])));
        $template->assign_var("EDITABLEMENU",Menu::getEditableCode(DataBase::Current()->EscapeString($_GET['menu'])));
      }
      else{
        $template->assign_var("URL","");
        $template->assign_var("EDITABLEMENU","");
      }
        
      $selector = new MenueSelector();
      $selector->name  = 'newMenu';
      $selector->value = -1;
      $selector->style = 'width:200px;position:relative;left:-210px;visibility:hidden';
      $template->assign_var("MENUSELECTOR", $selector->getCode());

      $selector = new PageSelector();
      $selector->name  = 'newPage';
      $selector->value = -1;
      $selector->style = 'width:200px;position:relative;left:-415px;';
      $template->assign_var("PAGESELECTOR", $selector->getCode());
  
      $template->assign_var("MESSAGE","");
      $template->output();
    }
    
    function getHeader(){
    }
    
    /**
     *
     * @return string
     */
    public function getEditableCode(){
      return "<input name=\"save\" type=\"submit\" value=\"".htmlentities(utf8_encode(Language::DirectTranslate("CHANGE")))."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }

    /**
     *
     * @param string $separator
     * @param strung $class
     * @param string $idpraefix 
     */
    public function displayBreadcrumb($separator,$class,$idpraefix){
      FolderBreadcrumb::display($this->page,$separator,$class,$idpraefix);
    }     
}
?>
