<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class WidgetController{
  
    /**
     *
     * @param string $class
     * @param string $name
     * @param string $path
     * @return boolean 
     */
    public static function register($class,$name,$path){
      $class = DataBase::Current()->EscapeString($class);
      $name  = DataBase::Current()->EscapeString($name);
      $path  = DataBase::Current()->EscapeString($path);
      return DataBase::Current()->Execute("INSERT {'dbprefix'}widgets 
                                     (class       ,name       , path)
                                     VALUES('".$class."','".$name."','".$path."')");
    }
    
    /**
     *
     * @param string $path
     */
    public static function unregister($path){
      $path = DataBase::Current()->EscapeString($path);
      DataBase::Current()->Execute("DELETE FROM {'dbprefix'}dashboards WHERE path = '".$path."'");
      DataBase::Current()->Execute("DELETE FROM {'dbprefix'}widgets WHERE path = '".$path."'");
    }
    
    /**
     *
     * @return array
     */
    function getAllWidgets(){
      $widgetData = self::getAllWidgetData();
      if($widgetData){
        foreach($widgetData as $widget){
          $res[] = self::getWidget($widget);
        }
      }
      return $res;
    }
    
    /**
     *
     * @param string $path
     * @return mixed
     */
    public static function getWidgetData($path){
      $path = DataBase::Current()->EscapeString($path);
      return DataBase::Current()->ReadRow("SELECT class,path FROM {'dbprefix'}widgets WHERE path = '".$path."'");
    }
    
    /**
     *
     * @param mixed $data
     * @return WidgetBase 
     */
    public static function getWidget($data){
      if(file_exists(Settings::getInstance()->get("root").'system/plugins/'.$data->path)){
          include_once(Settings::getInstance()->get("root").'system/plugins/'.$data->path);
          $res = new $data->class();
          $res->path = $data->path;
          return $res;
      }
      else{
           return null;
      }
    }
    
    /**
     *
     * @return array
     */
    function getAllWidgetData(){
      return DataBase::Current()->ReadRows("SELECT class,path FROM {'dbprefix'}widgets");
    }

    /**
     *
     * @param int $category
     * @return int
     */
    public static function countWidgets($category = -1){
      if($category > -1){
       $category = DataBase::Current()->EscapeString($category);
       $res = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}widgets WHERE category = '".$category."'");
      }
      else{
       $res = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}widgets");
      }
      return $res;
    }

    /**
     *
     * @param int $category
     * @return WidgetBase 
     */
    public static function getRandomWidget($category = -1){
      $start = rand(0,self::countWidgets($category) - 1);
      if($category > -1){
       $category = DataBase::Current()->EscapeString($category);
       $data = DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}widgets WHERE category = '".$category."' LIMIT ".$start.",1");
      }
      else{
       $data = DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}widgets LIMIT ".$start.",1");
      }
      $res =  self::getWidget($data);
      $res->load();
      return $res;
    }
  }
?>
