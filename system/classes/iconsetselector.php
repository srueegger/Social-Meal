<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class iconsetselector extends Control{
    public $style = '';
    
    public function getCode(){
      $res =  "<select name=\"".$this->name."\" style=\"".$this->style."\">";
      foreach(FileServer::getFolders(Settings::getInstance()->get("root")."system/images/icons") as $iconset){
        if($this->value == $iconset){
          $res .= "<option value=\"".htmlentities(utf8_encode($iconset))."\" selected=\"1\">".htmlentities($iconset)."</option>";
        }
        else{
          $res .= "<option value=\"".htmlentities(utf8_encode($iconset))."\">".htmlentities(utf8_encode($iconset))."</option>";
        }
      }
      $res .= "</select>";
      return $res;
    }
  }
?>
