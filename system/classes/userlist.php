<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

	class UserList extends Editor{
		/**
		 *
		 * @param Page $page
		 */
		function __construct(Page $page){
			$this->page = $page;
		}

		public function display(){
			$userlist = new Template();
			$userlist->load("user_list");
			$userlist->assign_var("URL",$this->page->GetUrl());

			if(isset($_POST['insert'])){
				$user = new User();
				$user->name = $_POST['name'];
				$user->role = $_POST['new_user_role'];
				$user->email = $_POST['email'];

				if(!$user->insert($_POST['password'])){
					$userlist->assign_var("MSG",Language::DirectTranslateHtml("USER_NOT_CREATED"));
				}
			}

			if(isset($_GET['delete'])){
				Plugin_Profile_Manager::markUserAsDel($_GET['delete']);
			}
			$userlist->assign_var("MSG","");

			if(isset($_GET['delopenreg'])){
				$delid = DataBase::Current()->EscapeString($_GET['delopenreg']);
				DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_plugindata_data WHERE property = '".$delid."' AND area = 'register2' LIMIT 1");
			}

			if(isset($_GET['delorgprofile'])){
				Plugin_Profile_Manager::delOrgProfile($_GET['delorgprofile']);
			}

			if(isset($_GET['uid']) && isset($_GET['changemark'])){
				Plugin_Profile_Manager::changeAdminMark($_GET['uid']);
			}

			if(isset($_GET['useredit'])){
				$userdata = Plugin_Profile_Manager::getUserDataPerId($_GET['useredit']);
				if(isset($_GET['deluserimg']) == true){
					Plugin_Profile_Manager::deleteUserImage($userdata->profilimg);
					$userdata = Plugin_Profile_Manager::getUserDataPerId($_GET['useredit']);
				}

				if(isset($_POST['usersave'])){
					Plugin_Profile_Manager::updateUserFromAdmin($_POST,$_FILES,$_GET['useredit']);
					unset($userdata);
					$userdata = Plugin_Profile_Manager::getUserDataPerId($_GET['useredit']);
				}
				$userlist->show_if('useredit',true);
				$userlist->assign_var('userid',$userdata->id);
				$userlist->assign_var('username',$userdata->name);
				$userlist->assign_var('password',$userdata->password);
				$userlist->assign_var('email',$userdata->email);
				$userlist->assign_var('punkte',$userdata->punkte);
				$userlist->assign_var('lastlogin',Plugin_Events_Manager::formatDate($userdata->last_access_timestamp));
				$userlist->assign_var('registerdate',Plugin_Events_Manager::formatDate($userdata->create_timestamp));
				$userlist->assign_var('onlinedate',Plugin_Events_Manager::formatDate($userdata->plugin_onlinestatus_lastOnline));
				$userlist->assign_var('firstname',$userdata->firstname);
				$userlist->assign_var('lastname',$userdata->lastname);
				$userlist->assign_var('plz',$userdata->plz);
				$userlist->assign_var('ort',$userdata->ort);
				$userlist->assign_var('kanton',$userdata->kanton);
				$userlist->assign_var('strasse',$userdata->strasse);
				$userlist->assign_var('strnr',$userdata->strnr);
				$userlist->assign_var('aboutme',$userdata->aboutme);
				$userlist->assign_var('fb',$userdata->fblink);
				$userlist->assign_var('twit',$userdata->twitlink);
				$userlist->assign_var('gp',$userdata->gplink);
				$userlist->assign_var('flickr',$userdata->flickrlink);
				$userlist->assign_var('linkedin',$userdata->linkedinlink);
				$userlist->assign_var('skype',$userdata->skypelink);
				$userlist->assign_var('webpage',$userdata->webpage);
				$userlist->assign_var('gbdatum',$userdata->gbdatum);
				$userlist->assign_var('job',$userdata->job);
				$userlist->assign_var('tel',$userdata->tel);
				$userlist->assign_var('gen'.$userdata->gender,'selected="selected"');
				$userlist->assign_var('bzs'.$userdata->bzstatus,'selected="selected"');
				$userlist->assign_var('role'.$userdata->role,'selected="selected"');
				$userlist->assign_var('wettbewerb'.$userdata->wettbewerb,'selected="selected"');
				$userlist->assign_var('falselogin',$userdata->falselogin);
				if($userdata->profilimg == ''){
					$userlist->show_if('showupload',true);
					$userlist->show_if('profilimg',false);
				}else{
					$userlist->show_if('showupload',false);
					$userlist->show_if('profilimg',true);
					$userlist->assign_var('profilimg',$userdata->profilimg);
				}
				if($userdata->openimg == 1){
					$userlist->assign_var('openimgchecked','checked="checked"');
				}else{
					$userlist->assign_var('openimgchecked','');
				}
				if(Plugin_Profile_Manager::getUserSetting('nlsm',$userdata->id) == 1){
					$userlist->assign_var('nlsm','Ja');
				}else{
					$userlist->assign_var('nlsm','Nein');
				}
				if(Plugin_Profile_Manager::getUserSetting('nlob',$userdata->id) == 1){
					$userlist->assign_var('nlob','Ja');
				}else{
					$userlist->assign_var('nlob','Nein');
				}
				$userlist->assign_var('gen0','');
				$userlist->assign_var('gen1','');
				$userlist->assign_var('gen2','');
				$userlist->assign_var('gen3','');
				$userlist->assign_var('gen4','');
				$userlist->assign_var('gen5','');
				$userlist->assign_var('bzs0','');
				$userlist->assign_var('bzs1','');
				$userlist->assign_var('bzs2','');
				$userlist->assign_var('bzs3','');
				$userlist->assign_var('bzs4','');
				$userlist->assign_var('bzs5','');
				$userlist->assign_var('bzs6','');
				$userlist->assign_var('bzs7','');
				$userlist->assign_var('bzs8','');
				$userlist->assign_var('bzs9','');
				$userlist->assign_var('bzs10','');
				$userlist->assign_var('bzs11','');
				$userlist->assign_var('role2','');
				$userlist->assign_var('role4','');
				$userlist->assign_var('role5','');
				$userlist->assign_var('role6','');
				$userlist->assign_var('wettbewerb0','');
				$userlist->assign_var('wettbewerb1','');
			}else{
				$userlist->show_if('useredit',false);
			}

			if(isset($_GET['orgedit'])){
				if(isset($_GET['delorgimg']) == true){
					Plugin_Profile_Manager::delOrgImage($_GET['orgedit']);
				}
				if(isset($_GET['deladmin'])){
					Plugin_Profile_Manager::delOrgAdmin($_GET['deladmin']);
				}
				if(isset($_POST['orgsave'])){
					Plugin_Profile_Manager::updateOrgProfile($_GET['orgedit'],$_POST,$_FILES);
				}
				$userlist->show_if('orgedit',true);
				$org = Plugin_Profile_Manager::loadOrganisation($_GET['orgedit']);
				$userlist->assign_var('orgid',htmlentities($org->id));
				$userlist->assign_var('orgname',htmlentities($org->orgname));
				$userlist->assign_var('orgdesc',htmlentities($org->description));
				$userlist->assign_var('orgweb',htmlentities($org->orgweb));
				$userlist->assign_var('orgmail',htmlentities($org->orgmail));
				$userlist->assign_var('orgfb',htmlentities($org->orgfb));
				$userlist->assign_var('orgtwitter',htmlentities($org->orgtwitter));
				$userlist->assign_var('orggplus',htmlentities($org->orggplus));
				$userlist->assign_var('orgtel',htmlentities($org->orgtel));
				$userlist->assign_var('orgstrasse',htmlentities($org->strasse));
				$userlist->assign_var('orgstrnr',htmlentities($org->strnr));
				$userlist->assign_var('orgplz',htmlentities($org->plz));
				$userlist->assign_var('orgort',htmlentities($org->ort));
				if($org->profilimg == ''){
					$userlist->show_if('showupload',true);
					$userlist->show_if('profilimg',false);
				}else{
					$userlist->show_if('showupload',false);
					$userlist->show_if('profilimg',true);
					$userlist->assign_var('profilimg',$org->profilimg);
				}
				foreach(Plugin_Profile_Manager::getOrgAdmins($org->id) as $orgadmin){
					$index = $userlist->add_loop_item('orgadmins');
					$username = DataBase::Current()->ReadField("SELECT name FROM {'dbprefix'}user WHERE id = '".$orgadmin->userid."'");
					$userlist->assign_loop_var("orgadmins",$index,"adminusername",htmlentities($username));
					$userlist->assign_loop_var("orgadmins",$index,"adminuserid",htmlentities($orgadmin->id));
					$userlist->assign_loop_var("orgadmins",$index,"orgid",htmlentities($org->id));
				}
			}else{
				$userlist->show_if('orgedit',false);
			}

			$openregs = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_plugindata_data WHERE area = 'register2'");
			foreach($openregs as $openreg){
				$data = Plugin_PluginData_Data::getData($openreg->property,'plugin','register2');
				$md5mail = explode('_',$openreg->property);
				$index = $userlist->add_loop_item('openregs');
				$userlist->assign_loop_var("openregs",$index,"username",htmlentities($data['name']));
				$userlist->assign_loop_var("openregs",$index,"firstname",htmlentities($data['firstname']));
				$userlist->assign_loop_var("openregs",$index,"lastname",htmlentities($data['lastname']));
				$userlist->assign_loop_var("openregs",$index,"email",htmlentities($data['email']));
				$userlist->assign_loop_var("openregs",$index,"md5mail",$md5mail[1]);
				$userlist->assign_loop_var("openregs",$index,"datum",Plugin_Events_Manager::formatDate($openreg->last_update));
				$userlist->assign_loop_var("openregs",$index,"acode",Settings::getInstance()->get("host").'registrieren.html?user='.$md5mail[1].'&acode='.$data['acode']);
			}

			$orgprofiles = Plugin_Profile_Manager::getAllOrgs();
			foreach($orgprofiles as $org){
				$orgadminsstring = '';
				$orgadmins = Plugin_Profile_Manager::getOrgAdmins($org->id);
				foreach($orgadmins as $admin){
					$admininfo = Plugin_Profile_Manager::getUserDataPerId($admin->userid);
					$orgadminsstring .= '<a href="/mein-profil.html?profile='.$admininfo->name.'" target="_blank">'.$admininfo->name.'</a> ';
				}
				$index = $userlist->add_loop_item('orgs');
				$userlist->assign_loop_var("orgs",$index,"orgname",htmlentities($org->orgname));
				$userlist->assign_loop_var("orgs",$index,"orgid",htmlentities($org->id));
				$userlist->assign_loop_var("orgs",$index,"orgadmins",$orgadminsstring);
				unset($orgadminsstring);
			}

			if(isset($_GET['show']) == "mark"){
				$getusers = User::getAllMarkUser();
			}else{
				$getusers = User::getAllUser();
			}
			foreach($getusers as $user){
				$index = $userlist->add_loop_item('user');
				$userlist->assign_loop_var("user",$index,"id",htmlentities($user->id));
				$userlist->assign_loop_var("user",$index,"name",htmlentities($user->name));
				$userlist->assign_loop_var("user",$index,"rolle",htmlentities(utf8_encode($user->role->name)));
				$userlist->assign_loop_var("user",$index,"email",htmlentities($user->email));
				if($user->adminmark == 1){
					$userlist->assign_loop_var("user",$index,"bg",'style="background-color: red;"');
				}else{
					$userlist->assign_loop_var("user",$index,"bg",'');
				}
				if(isset($_GET['show'])){
					$userlist->assign_loop_var("user",$index,"showmark",'&show=mark');
				}else{
					$userlist->assign_loop_var("user",$index,"showmark",'');
				}
			}
			$userlist->output();
		}

		function getHeader(){
			echo('<script type="text/javascript">
function confirm_alert(node) {
	return confirm("Möchtest du den Freund wirklich löschen?");
}
</script>');
		}

		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			return "<input name=\"save\" type=\"submit\" value=\"".htmlentities(utf8_encode(Language::DirectTranslate("CHANGE")))."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
		}

		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage
		 */
		public function save(Page $newPage,Page $oldPage){
		}
}
?>
