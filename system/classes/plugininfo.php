<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class PluginInfo{
    public $path               = '';
    public $name               = '';
    public $authorName         = '';
    public $authorLink         = '';
    public $version            = '';
    public $configurationFile  = '';
    public $license            = '';
    public $licenseUrl         = '';
    public $supportedLanguages = array();
    
    /**
     *
     * @return boolean
     */
    public function isActivated(){
      $path     = DataBase::Current()->EscapeString($this->path);
      $rowCount = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}activated_plugins WHERE path = '".$path."'");
      if($rowCount){
        return $rowCount > 0;
      }
      else{
        return false;
      }
    }
    
    /**
     *
     * @return boolean
     */
    public function activate(){
      $res = false;
      if(!$this->isActivated()){
          $path = DataBase::Current()->EscapeString($this->path);
          @include(Settings::getValue("root")."system/plugins/".$path."/activate.php");
          foreach($this->supportedLanguages as $supported_language){
            Language::CreateLanguagePack("plugin_".$path,$supported_language);
            $tokens = array();
            @include(Settings::getValue("root")."system/plugins/".$path."/languages/".strtolower($supported_language).".php");
            $language = new Language($supported_language);
            foreach($tokens as $token=>$value){
              $language->addTranslation("plugin_".$path."_".$token, $value);
            }
          }
          $res = DataBase::Current()->Execute("INSERT INTO {'dbprefix'}activated_plugins (path) VALUES ('".$path."')");
          EventManager::RaiseEvent("activated_plugin", array("plugininfo",$this));
      }
      return $res;
    }
    
    /**
     *
     * @return boolean
     */
    public function deactivate(){
      $res = false;
      if($this->isActivated()){
          $path = DataBase::Current()->EscapeString($this->path);
          @include(Settings::getValue("root")."system/plugins/".$path."/deactivate.php");
          foreach($this->supportedLanguages as $language){
            Language::DropLanguagePack("plugin_".$path,$language);
          }
          $res = DataBase::Current()->Execute("DELETE FROM {'dbprefix'}activated_plugins WHERE path = '".$path."'");
          EventManager::RaiseEvent("deactivated_plugin", array("plugininfo",$this));
      }
      return $res;
    }
    
    /**
     *
     * @return string description of the plugin 
     */
    public function getDescription(){
        $res = "";
        $tokens = array();
        @include(Settings::getValue("root")."system/plugins/".$this->path."/languages/".strtolower(Language::GetGlobal()->language).".php");
        if(isset($tokens['plugin_description'])){
            $res = $tokens['plugin_description'];
        }
        return $res;
    }
    
    /**
     * Creates an plugininfo-Object from a plugin name
     * @param string $plugin_name Name of the Plugin
     * @return PluginInfo - null if not found. 
     */
    public static function Get($plugin_name)
    {
        return PluginList::GetInfo($plugin_name);
    }
  }
?>
