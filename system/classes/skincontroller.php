<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

class SkinController{

  private static $skinNames = array();

  /**
   *
   * @return int
   */
  public static function getCurrentSkinId(){
    return Settings::getInstance()->get("selectedskin");
  }
  
  /**
   *
   * @return int
   */
  public static function getCurrentMobileSkinId(){
    return Settings::getInstance()->get("selectedmobileskin");
  }

  /**
   *
   * @return string
   */
  public static function getCurrentSkinName(){
    if(self::isMobileDevice()){
      return self::getCurrentMobileSkinName();
    }
    else{
      return self::getCurrentDesktopSkinName();
    }
  } 
  
  /**
   * 
   * @return boolean
   */
  private static function isMobileDevice(){
      $res = false;
      if(isset($_SESSION['ismobiledevice'])){
          $res = $_SESSION['ismobiledevice'];
      }
      else{
          $res = Mobile::isMobileDevice();
      }
      return $res;
  }
  
  /**
   *
   * @return string
   */
  public static function getCurrentMobileSkinName(){
    $res = DataBase::Current()->ReadField("SELECT name FROM {'dbprefix'}skins WHERE id = '".SkinController::getCurrentMobileSkinId()."'");
    if($res){
      return $res;
    }
    else{
      return "mobile";
    }  
  }

  /**
   *
   * @param int $id
   * @return string 
   */
  public static function getSkinName($id){
    $res = "";
    if(isset(self::$skinNames[$id])){
      $res = self::$skinNames[$id];
    }
    else{
      $id = DataBase::Current()->EscapeString($id);
      $res = DataBase::Current()->ReadField("SELECT name FROM {'dbprefix'}skins WHERE id = '".$id."'");
      self::$skinNames[$id] = $res;
    }
    return $res;
  }
  
  /**
   *
   * @return string
   */
  public static function getCurrentDesktopSkinName(){
    if(isset($_GET['blank']) && $_GET['blank'] == "true"){
      $res =  "blank";
    }
    else{
      if((isset($_GET['skin']) || isset($_GET['save_settings'])) && User::Current()->isAdmin()){
        if(isset($_GET['save_settings']) && User::Current()->isAdmin()){
          $res = "backenddefault";
        }
        else{
          $res = $_GET['skin'];
        }
      }
      else {
        $res = self::getSkinName(SkinController::getCurrentSkinId());
      }
    }
    if($res){
      return $res;
    }
    else{
      return "default";
    }  
  }

  /**
   *
   * @return string
   */
  public static function getCurrentSkinPath(){
    return "system/skins/".SkinController::getCurrentSkinName();
  }

  /**
   *
   * @return array
   */
  public static function getInstalledSkins(){
    //Read from file system and refresh db
    $res = glob(Settings::getValue("root")."system/skins/*",GLOB_ONLYDIR);
    
    foreach($res as &$skin){
        $skin = str_replace(Settings::getValue("root")."system/skins/", "", $skin);
        $skin_escaped = DataBase::Current()->EscapeString($skin);
        DataBase::Current()->Execute("INSERT IGNORE INTO {'dbprefix'}skins (name) VALUES ('".$skin_escaped."')");
    }
      
      
    return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}skins");
  }

  public static function displayCurrent(){
    EventManager::RaiseEvent("BEFORE_DISPLAYSKIN",array());
    $skin = self::getCurrentSkinName();
    if(file_exists(Settings::getInstance()->get("root")."/system/skins/".$skin."/index.php")){
        include(Settings::getInstance()->get("root")."/system/skins/".$skin."/index.php");
    EventManager::RaiseEvent("AFTER_DISPLAYSKIN",array());
    }
    else if(file_exists(Settings::getInstance()->get("root")."/system/skins/default/index.php")){
        if(DEVELOPMENT){
            die("Skin ".$skin." cannot be found!");
        }
        include(Settings::getInstance()->get("root")."/system/skins/".$skin."/index.php");
    }
    else{
        die("Skin ".$skin." cannot be found!");
    }
  }
}
?>
