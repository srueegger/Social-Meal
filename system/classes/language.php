<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class Language{
  
    public $language      = null;
    public $root          = "";
    private $cache        = array();
    private static $isLoading = false;
    
    /**
     *
     * @param string $token 
     */
    public function __construct($token = null){
      if($token == null){
        $token = Settings::getInstance()->get("language");
      }
      $this->language = $token;
      $alias = Page::Current()->alias;
      if(Cache::Contains("language",$token."/".$alias)){
          $this->cache = Cache::getData("language",$token."/".$alias);
      }
    }
    
    /**
     *
     * @param string $token
     * @return string
     */
    public function Translate($token){
      if(!isset($this->cache[$token])){
          $packageName = DataBase::Current()->EscapeString(self::getLanguagePackName($token,$this->language));
          $this->cache[$token] = DataBase::Current()->ReadField("SELECT text FROM {'dbprefix'}languagepack_".$packageName." WHERE token = '".DataBase::Current()->EscapeString($token)."'");
	  if(!$this->cache[$token]){
		$this->cache[$token] = $token;
		if(DEVELOPMENT){
			die("Language-Token ".$token." not found");
		}
	  }
      }
      return $this->cache[$token];
    }
    
    /**
     *
     * @param string $token
     * @return string
     */
    public function getString($token){
        return $this->Translate($token);
    }
    
    /**
     * returns the translation of $token
     * @return string Translation
     */
    public static function DirectTranslate($token){
        return self::GetGlobal()->Translate($token);
    }
    
    /**
     *
     * @param string $token
     * @return string
     */
    public static function DirectTranslateHtml($token){
        return htmlentities(utf8_encode(self::GetGlobal()->Translate($token)));
    }
    
    /**
     *
     * @param string $token
     * @return string
     */
    private static function getLanguagePackName($token,$language){
      $namespaces = explode("_",strtolower($token));
      if(sizeOf($namespaces) >= 2){
        if($namespaces[0] == "plugin"){
          return strtoupper($language)."_plugin_".$namespaces[1];
        }
        else if($namespaces[0] == "skin"){
          return strtoupper($language)."_skin_".$namespaces[1];
        }
      }
      return strtoupper($language)."_global";
    }
    
    /**
     *
     * @param string $token
     * @param string $text 
     */
    public function addTranslation($token,$text){
      $token = DataBase::Current()->EscapeString($token);
      $text = DataBase::Current()->EscapeString($text);
      $packageName = DataBase::Current()->EscapeString(self::getLanguagePackName($token,$this->language));
      DataBase::Current()->Execute("INSERT INTO  {'dbprefix'}languagepack_".$packageName." (token,text) VALUES ('".$token."','".$text."')");
    }

    /**
     *
     * @param string $token 
     */
    public function deleteTranslation($token){
      $token = DataBase::Current()->EscapeString($token);
      $packageName = DataBase::Current()->EscapeString(self::getLanguagePackName($token,$this->language));
      DataBase::Current()->Execute("DELETE FROM {'dbprefix'}languagepack_".$packageName." WHERE token = '".$token."'");
    }
    
    /**
     *
     * @param string $token
     * @param string $text 
     */
    public function updateTranslation($token,$text){
      $this->deleteTranslation($token);
      $this->addTranslation($token,$text);
    }

    /**
     *
     * @param mixed $obj
     * @return mixed 
     */
    public function replaceLanguageTokensByObject($obj){
      if($obj){
        foreach($obj as $key => $value) {
          preg_match_all("/{LANG:([^}]+)}/", $value, $tokens, PREG_SET_ORDER);
          foreach($tokens as $token){
            $translation = Language::DirectTranslate($token[1]);
            $obj->{$key} = str_ireplace($token[0],$translation,$obj->{$key});
          }
        }
      }
      return $obj;
    }
    
    
    /**
    * 
    * returns a the global instance of the language class
    * @return Language 
    */
    public static function GetGlobal(){
        if(!isset($GLOBALS['language'])){
	    self::$isLoading = true;
            $GLOBALS['language'] = new Language();
  	    self::$isLoading = false;
            self::GetGlobal()->replaceLanguageTokensByObject(Page::Current());
        }
        return $GLOBALS['language'];
    }
    
    public function __destruct() {
      $alias = Page::Current()->alias;
      if(!Cache::Contains("language",$this->language."/".$alias)){
          Cache::setData("language",$this->language."/".$alias, $this->cache);
      }
    }
    
    /**
     *
     * @return boolean 
     */
    public static function IsLoading(){
        return isset(self::$isLoading) && self::$isLoading;
    }
    
    public static function CreateLanguagePack($name,$language = null){
        $name = self::getLanguagePackName($name, $language);
        DataBase::Current()->Execute("CREATE TABLE {'dbprefix'}languagepack_".$name." (
                    `token` varchar(255) NOT NULL,
                    `text` TEXT NOT NULL,
                        PRIMARY KEY (`token`)
                       ) ENGINE=MyISAM DEFAULT CHARSET=utf8");
    }
    
    public static function DropLanguagePack($name,$language = null){
        $name = self::getLanguagePackName($name, $language);
        DataBase::Current()->Execute("DROP TABLE {'dbprefix'}languagepack_".$name);
    }
    
    public function ClearCache(){
        Cache::clear("language");
        $this->cache = array();
    }
    
  
  }
?>
