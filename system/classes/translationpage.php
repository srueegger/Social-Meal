<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class TranslationPage extends Editor{
      
    /**
     *
     * @param Page $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
      $form = new Form(1);
      $form->submit();

      $table = new Table();
      $token = new TableColumn("token","Token");
      $token->autoWidth = true;
      $name  = new TableColumn("name",Language::DirectTranslate("NAME"));
      $table->columns->add($token);
      $table->columns->add($name);
      $table->name       = "{'dbprefix'}languages";
      $table->actions    = "translations";
      $table->orderBy    = "name";
      $table->display();
      
      $newTranslation = Language::DirectTranslateHtml("NEW_TRANSLATION");
      echo "<h2>".$newTranslation."</h2>";
      $form->display();
    }

    /**
     *
     * @return string
     */
    function getHeader(){
      return "";
   }
    
   /**
    *
    * @return string
    */
    public function getEditableCode(){
      return "<input name=\"save\" type=\"submit\" value=\"".htmlentities(utf8_encode(Language::DirectTranslate("CHANGE")))."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }
}
?>
