<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class SettingsPage extends Editor{
    /**
     *
     * @param Page $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
        $settings = new SettingsForm();
        $settings->role = 3;
        $settings->url  = UrlRewriting::GetUrlByAlias($_GET['include']);
        if(isset($_GET['areatype'])){
          $settings->areaType = $_GET['areatype'];
        }
        if(isset($_GET['area'])){
          $settings->area = $_GET['area'];
        }
        if(isset($_GET['role'])) $settings->role = $_GET['role'];

        $settings->display();
      ?>
        <div style="margin-left:500px;">
        <h2>Skins</h2>
      <?PHP
        $skins = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}skins WHERE LOWER(name) IN (SELECT DISTINCT name FROM {'dbprefix'}settings WHERE areaType = 'skins' AND area = {'dbprefix'}skins.name)");
        if($skins){
          foreach($skins as $skin){
            $url = UrlRewriting::GetUrlByAlias("admin/settings","areatype=skins&area=".urlencode($skin->name));
            echo "<a href=\"".$url."\">".$skin->name."</a><br />";
          }
        }
      ?>
        <h2>Plugins</h2>
      <?PHP
        $plugins = new PluginList();
        $plugins->loadAll();
        foreach($plugins->plugins as $plugin){
          if($plugin->configurationFile != ''){
            $url = UrlRewriting::GetUrlByAlias("admin/pluginsettings","plugin=".$plugin->path);
            ?>
              <a href="<?PHP echo $url; ?>"><?PHP echo $plugin->name; ?></a><br />
            <?PHP
          }
        }
      ?>
        </div>
      <?PHP
    }
    function getHeader(){
    }
    
    /**
     *
     * @return string
     */
    public function getEditableCode(){
      return "<input name=\"save\" type=\"submit\" value=\"".htmlentities(utf8_encode(Language::DirectTranslate("CHANGE")))."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }    
}
?>
