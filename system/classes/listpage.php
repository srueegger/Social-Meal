<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class ListPage extends Editor{
    /**
     *
     * @param type $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
      $list = unserialize($this->page->getEditorContent($this));
      $list->fillSelect = str_replace('\\\'','\'',$list->fillSelect);
      $list->display();
    }

    function getHeader(){
    }
    
    /**
     *
     * @return string
     */
    public function getEditableCode(){
        $template = new Template();
        $template->load("listpage");
        if($list = unserialize($this->page->getEditorContent($this))){
          $template->assign_var("SELECT",str_replace('\\\'','\'',$list->fillSelect));
          $template->assign_var("TEMPLATE",$list->template);
        }
        else{
          $template->assign_var("SELECT","");
          $template->assign_var("TEMPLATE","");
        }

        $template->assign_var("HOST",Settings::getInstance()->get("host"));
        $template->assign_var("ALIAS",$this->page->alias);
        $url = UrlRewriting::GetUrlByAlias("admin/pageedit", "site=".$this->page->alias);
        $template->assign_var("URL",$url);
        $template->assign_var("LANG",Settings::getInstance()->get("language"));
        return $template->getCode();
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
        $list = new CustomList();
        $list->template    = $_POST['template'];
        $list->fillSelect  = $_POST['select'];
        $list->showButtons = false;
        $list->paddingLeft = 0;

        $this->page = $newPage;
        $this->page->setEditorContent(serialize($list));
        $this->page->save();
    }
      
  }
?>
