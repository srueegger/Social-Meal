<?php
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

/**
 * Manages the rights for data sharing
 *
 * @author Stefan Wienströer
 */
class DataTypeShare {
    
    protected $dataType = null;
    protected $apiKey   = null;
    protected $name     = null;
    
    /**
     *
     * @return string 
     */
    public function GetName(){
        return $this->name;
    }
    
    /**
     *
     * @param string $name 
     */
    public function SetName($name){
        $this->name = $name;
    }
    
    /**
     *
     * @return string 
     */
    public function GetApiKey(){
        return $this->apiKey;
    }
    
    /**
     *
     * @param string $apikey 
     */
    public function SetApiKey($apikey){
        $this->apiKey = $apikey;
    }
    
    /**
     *
     * @param DataType $type 
     */
    public function SetDataType(DataType $type){
        $this->dataType = $type;
    }
    
    /**
    * 
    * returns all shares by a given datatype
    * @param array datatype
    * @return array the datatypesa
    */
    public static function GetByDataType(DataType $type){
        $res = array();
        
        $id = DataBase::Current()->EscapeString($type->getID());
        $rows = DataBase::Current()->ReadRows("SELECT 
                                            * 
                                          FROM 
                                            {'dbprefix'}datatype_sharing
                                          WHERE
                                            datatype = ".$id);
        foreach($rows as $row){
            $share           = new DataTypeShare();
            $share->dataType = $type;
            $share->apiKey   = $row->apikey;
            $share->name     = $row->name;
            $res[] = $share;
        }
        
        return $res;
    }
    
    /**
     *
     * @return string 
     */
    public function getUrl(){
        return Settings::getValue("host")."api.php?action=export&datatype=".$this->dataType->getID()."&apikey=".urlencode($this->GetApiKey());
    }
    
    /**
     *
     * @param DataType $type
     * @param string $apikey
     * @return DataTypeShare 
     */
    public static function GetByDataTypeAndApiKey(DataType $type,$apikey){
        $res = null;
        
        $id = DataBase::Current()->EscapeString($type->getID());
        $apikey = DataBase::Current()->EscapeString($apikey);
        $row = DataBase::Current()->ReadRow("SELECT 
                                           * 
                                        FROM 
                                           {'dbprefix'}datatype_sharing
                                        WHERE
                                           datatype = ".$id."
                                           AND apikey = '".$apikey."'");
        if($row){
            $res           = new DataTypeShare();
            $res->dataType = $type;
            $res->apiKey   = $row->apikey;
            $res->name     = $row->name;
        }
        
        return $res;
    }
    
    public function Insert(){
        $name   = DataBase::Current()->EscapeString($this->name);
        $apiKey = DataBase::Current()->EscapeString($this->apiKey);
        $typeID = DataBase::Current()->EscapeString($this->dataType->getID());
        DataBase::Current()->Execute("INSERT IGNORE INTO {'dbprefix'}datatype_sharing (datatype     ,apikey       ,name       )
                                                                          VALUES ('".$typeID."','".$apiKey."','".$name."')");
    }
}

?>
