<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

  class PluginPage extends Editor{

    /**
     *
     * @param Page $page
     */
    public function __construct(Page $page){
      $this->page = $page;
    }

    public function display(){
      $template = new Template();
      $template->load("plugins");
      $plugins = new PluginList();
      $plugins->loadAll();
      foreach($plugins->plugins as $plugin){
        $index = $template->add_loop_item("PLUGINS");
        if(isset($_GET['activate']) && $_GET['activate'] == $plugin->path){
          $plugin->activate();
        }
        elseif(isset($_GET['deactivate']) && $_GET['deactivate'] == $plugin->path){
          $plugin->deactivate();
        }
        $template->assign_loop_var("PLUGINS",$index,"NAME",htmlentities(utf8_encode($plugin->name)));
        $template->assign_loop_var("PLUGINS",$index,"PATH",htmlentities(utf8_encode($plugin->path)));
        $template->assign_loop_var("PLUGINS",$index,"DESCRIPTION",htmlentities(utf8_encode($plugin->getDescription())));
        $template->assign_loop_var("PLUGINS",$index,"VERSION",$plugin->version);
        $template->assign_loop_var("PLUGINS",$index,"AUTHORLINK",$plugin->authorLink);
        $template->assign_loop_var("PLUGINS",$index,"AUTHORNAME",htmlentities(utf8_encode($plugin->authorName)));
        $template->assign_loop_var("PLUGINS",$index,"LICENSE",htmlentities(utf8_encode($plugin->license)));
        $template->assign_loop_var("PLUGINS",$index,"LICENSEURL",htmlentities(utf8_encode($plugin->licenseUrl)));
        if($plugin->isActivated()){
          $myurl = UrlRewriting::GetUrlByAlias($this->page->alias,"deactivate=".urlencode($plugin->path));
          $disable = Language::DirectTranslateHtml("DISABLE");
          $template->assign_loop_var("PLUGINS",$index,"ACTIVATIONLINK","<a href=\"".$myurl."\">".$disable."</a>");
        }
        else{
          $myurl = UrlRewriting::GetUrlByAlias($this->page->alias,"activate=".urlencode($plugin->path));
          $enable = Language::DirectTranslateHtml("ENABLE");
          $template->assign_loop_var("PLUGINS",$index,"ACTIVATIONLINK","<a href=\"".$myurl."\">".$enable."</a>");
        }
        if(file_exists(Settings::getValue("root").'system/plugins/'.$plugin->path.'/languages/de.php')){
			$template->assign_loop_var("PLUGINS",$index,"DELINK",'<a href="/admin/sprachdateien-bearbeiten.html?plugin='.$plugin->path.'&lng=de">'.Language::DirectTranslate('plugin_editlngfiles_de').'</a>');
		}else{
			$template->assign_loop_var("PLUGINS",$index,"DELINK","");
		}
		if(file_exists(Settings::getValue("root").'system/plugins/'.$plugin->path.'/languages/en.php')){
			$template->assign_loop_var("PLUGINS",$index,"ENLINK",'<a href="/admin/sprachdateien-bearbeiten.html?plugin='.$plugin->path.'&lng=en">'.Language::DirectTranslate('plugin_editlngfiles_en').'</a>');
		}else{
			$template->assign_loop_var("PLUGINS",$index,"ENLINK","");
		}
		if(file_exists(Settings::getValue("root").'system/plugins/'.$plugin->path.'/languages/fr.php')){
			$template->assign_loop_var("PLUGINS",$index,"FRLINK",'<a href="/admin/sprachdateien-bearbeiten.html?plugin='.$plugin->path.'&lng=fr">'.Language::DirectTranslate('plugin_editlngfiles_fr').'</a>');
		}else{
			$template->assign_loop_var("PLUGINS",$index,"FRLINK","");
		}
      }
      $template->assign_var("HOST",Settings::getValue("host"));
      $template->assign_var("APIKEY",Settings::getValue("apikey"));
      $template->output();
    }

    function getHeader(){
    }

    public function getEditableCode(){
      return "<input name=\"save\" type=\"submit\" value=\"".htmlentities(utf8_encode(Language::DirectTranslate("CHANGE")))."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }

    /**
     *
     * @param Page $newPage
     * @param Page $oldPage
     */
    public function save(Page $newPage,Page $oldPage){
    }
}
?>
