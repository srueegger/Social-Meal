<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class RoleList extends Editor{
    
    /**
     *
     * @param Page $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
      $rolelist = new Template();
      $rolelist->load("role_list");

      $actions = ActionList::get("rolelist");

      if(isset($_POST['insert'])){
        $role = new Role();
        $role->name = $_POST['insert'];
        $role->insert();
      }

      if(isset($_GET['delete'])){
        $role = new Role();
        $role->ID = $_GET['delete'];
        $role->delete();
      }

      $table = new Table();
      $id    = new TableColumn("id",Language::DirectTranslate("ID"));
      $id->autoWidth = true;
      $name  = new TableColumn("name",Language::DirectTranslate("NAME"));
      $table->columns->add($id);
      $table->columns->add($name);
      $table->name    = "{'dbprefix'}roles";
      $table->actions = "rolelist";
      $table->orderBy = "name";
      $table->cacheName = "rolelist";

      $rolelist->assign_var("TABLE",$table->getCode());
      $rolelist->output();
    }

    function getHeader(){
    }
    
    /**
     *
     * @return string
     */
    public function getEditableCode(){
      return "<input name=\"save\" type=\"submit\" value=\"".htmlentities(utf8_encode(Language::DirectTranslate("CHANGE")))."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }    
  }
?>
