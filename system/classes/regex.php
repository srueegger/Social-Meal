<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

	class Regex{
	
                /**
                 *
                 * @param string $pattern
                 * @param string $subject
                 * @param array $matchesarray
                 * @param int $flags 
                 */
		public static function MatchAll($pattern, $subject, &$matchesarray = array(),$flags = null){
                    $split = split("\n",$subject);
                    foreach($split as $item){
                        preg_match_all($pattern,$item,$results,$flags);
                        foreach($results as $result){
                            $matchesarray[] = $result;
                        }
                    }
                }
	
	}
?>
