<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class ImageServer{
      
    /**
     *
     * @param string $path
     * @param string $name
     * @param string $description
     * @return mixed 
     */
    public static function insert($path,$name,$description){
      $path        = DataBase::Current()->EscapeString($path);
      $name        = DataBase::Current()->EscapeString($name);
      $description = DataBase::Current()->EscapeString($description);
      $res         = DataBase::Current()->Execute("INSERT INTO {'dbprefix'}images (path,name,description) 
                                              VALUES ('".$path."','".$name."','".$description."')");
      if($res){
        $args['path']        = $path;
        $args['name']        = $name;
        $args['description'] = $description;
        EventManager::RaiseEvent("image_registered",$args);
      }
      return $res;
    }
    
    /**
     *
     * @return array 
     */
    public static function getImages(){
      return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}images");
    }

    /**
     *
     * @param string $path
     * @return mixed
     */
    public static function getImageData($path){
      $path = DataBase::Current()->EscapeString($path);
      return DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}images WHERE path = '".$path."'");
    }

    /**
     *
     * @param string $path
     * @return mixed 
     */
    public static function delete($path){
      $path = DataBase::Current()->EscapeString($path);
      return DataBase::Current()->Execute("DELETE FROM {'dbprefix'}images WHERE path = '".$path."'");
    }

    /**
     *
     * @param string $path
     * @return boolean
     */
    public static function contains($path){
      return self::getImageData($path) != false;;
    }

    /**
     *
     * @param string $oldPath
     * @param string $newPath
     * @return boolean
     */
    public static function move($oldPath,$newPath){
      $oldPath = DataBase::Current()->EscapeString($oldPath);
      $newPath = DataBase::Current()->EscapeString($newPath);
      return DataBase::Current()->Execute("UPDATE {'dbprefix'}images SET path = '".$newPath."' WHERE path = '".$oldPath."'");
    }
  }
?>
