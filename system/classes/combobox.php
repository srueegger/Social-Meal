<?PHP
  /**
  * @package ContentLion-Core
  * @author Stefan Wienstroeer
  */
  /*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class combobox extends Control{
    public $type       = "";
    public $fillSelect = "";
    public $onChange   = "";
    public $style      = "";
    public $id         = "";

    /**
     *
     * @return string 
     */
    public function getCode(){
      $res =  "<select name=\"".$this->name."\" style=\"".$this->style."\"";
      if($this->onChange != ""){
        $res .= " onchange=\"".$this->onChange."\"";
      }
      if($this->id != ""){
        $res .= " id=\"".$this->id."\"";
      }
      $res .= ">";
      $items = DataBase::Current()->ReadRows($this->fillSelect);
      foreach($items as $item){
        $res .= "<option value=\"".htmlentities(utf8_encode($item->value))."\"";
        if($this->value == $item->value){
          $res .= " selected=\"true\"";
        }
        $res .= ">".htmlentities(utf8_encode($item->label))."</option>";
      }

      $res .= "</select>";
     return $res;
    }

  }
?>
