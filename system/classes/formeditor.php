<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
  class FormEditor extends Editor{
      
    /**
     *
     * @param Page $page
     */
    public function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
      $template = new Template();
      $template->load("formeditor");
      $form = new Form($_GET['form']);
      if(isset($_POST['datatype']) && !isset($_POST['save'])){
        $form->dataTypeID = $_POST['datatype'];
        $form->clearFieldsFromDB();
      }

      foreach($_POST as $key=>$value){
        $id = explode('_',$key);
        $id = $id[0];
        if(is_numeric($id)){
          $key = substr($key,strlen($id)+1);
          $fields[$id][$key] = $value;
        }
      }

      if(isset($fields)){
        foreach($fields as $field){
          $formField = new FormField();
          $formField->sortIndex = $field['sortIndex'];
          $formField->dataName  = $field['dataname'];
          $formField->label     = $field['label'];
          if($field['insert']){
            $formField->insert = true;
          }
          else{
            $formField->insert = false;
          }
          if(isset($field['mandatory']) && $field['mandatory']){
            $formField->mandatory = true;
          }
          else{
            $formField->mandatory = false;
          }
          if($field['show']){
            $formField->show = true;
          }
          else{
            $formField->show = false;
          }
          $formField->edit        = $field['edit'];
          $formField->preAllocate = $field['value'];
    
          $form->addField($formField);
        }
      }

      if(isset($_POST['save'])){
        $form->buttonText     = $_POST['buttontext'];
        $form->succeedMessage = $_POST['succeedmessage'];
        $form->dataTypeID     = $_POST['datatype'];
        if(isset($_POST['captcha']) && $_POST['captcha']){
          $form->captcha = true;
        }
        else{
          $form->captcha = false;
        } 
        if(isset($_POST['showAfterInsert']) && $_POST['showAfterInsert']){
          $form->showAfterInsert = true;
        }
        else{
          $form->showAfterInsert = false;
        } 
        $form->save();
      }

      $combobox = new combobox();
      $combobox->fillSelect = "SELECT 0 as value, '' as label UNION SELECT id as value, displayName as label FROM {'dbprefix'}datatypes ORDER BY label";
      $combobox->value = $form->dataTypeID; 
      $combobox->name = 'datatype';
      $combobox->onChange = "document.form.submit();";
      $template->assign_var("DATATYPE",$combobox->getCode());

      $buttonText        = new textbox();
      $buttonText->name  = "buttontext";
      $buttonText->value = $form->buttonText;
      $template->assign_var("BUTTONTEXT",$buttonText->getCode());

      $succeedMessage        = new textbox();
      $succeedMessage->name  = "succeedmessage";
      $succeedMessage->value = $form->succeedMessage;
      $template->assign_var("SUCCEEDMESSAGE",$succeedMessage->getCode());

      $captcha = new checkbox();
      $captcha->name = "captcha";
      $captcha->value = $form->captcha;
      $template->assign_var("CAPTCHA",$captcha->getCode());

      $showAfterInsert = new checkbox();
      $showAfterInsert->name = "showAfterInsert";
      $showAfterInsert->value = $form->showAfterInsert;
      $template->assign_var("SHOWAFTERINSERT",$showAfterInsert->getCode());


      $list = new CustomList();
      if($form->isEmpty()){
        $list->fillSelect = "SELECT *, @rownum:=@rownum+1 AS sortindex, 'checked' as show_check, '' as mandatory_check, 'checked' as insert_check, '\'\'' as preallocate FROM (SELECT @rownum:=0) r, {'dbprefix'}datafields WHERE dataType = '".$form->dataTypeID."' ORDER BY displayName";
      }
      else{
        $list->fillSelect = "SELECT *, label as displayname, @rownum:=@rownum+1 as id, IF( `show` = '1', 'checked', '' ) as show_check, IF( `insert` = '1', 'checked', '' ) as insert_check, IF( `mandatory` = '1', 'checked', '' ) as mandatory_check FROM (SELECT @rownum:=0) r, {'dbprefix'}form_fields WHERE form = '".$form->id."' ORDER by sortIndex";
      }
      $list->template = "formfield_editor";
      $list->paddingLeft = 0;
      $list->showButtons = false;
      $template->assign_var("FIELDS",$list->getCode());
      $template->output();
    }

    function getHeader(){
    }
    
    /**
     *
     * @return string 
     */
    public function getEditableCode(){
      $change = htmnlentities(Language::GetGlobal()->getString("CHANGE"));
      return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }

    /**
     *
     * @param string $separator
     * @param string $class
     * @param string $idpraefix 
     */
    public function displayBreadcrumb($separator,$class,$idpraefix){
      FolderBreadcrumb::display($this->page,$separator,$class,$idpraefix);
    }  
}
?>
