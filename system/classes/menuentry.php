<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class MenuEntry{
    public $id    = -1;
    public $type  = 0;
    public $href  = '';
    public $menu  = '';
    
    /**
     *
     * @param string $globalstart
     * @param string $globalend
     * @param string $elementstart
     * @param string $elementend
     * @param string $class
     * @param int $index 
     */
    public function display($globalstart,$globalend, $elementstart,$elementend,$class,$index){
      echo $this->getCode($globalstart,$globalend, $elementstart,$elementend,$class,$index);
    }
    
    /**
     *
     * @param string $globalstart
     * @param string $globalend
     * @param string $elementstart
     * @param string $elementend
     * @param string $class
     * @param string $index
     * @return string 
     */
    public function getCode($globalstart,$globalend, $elementstart,$elementend,$class,$index){
        if($this->type == 0){
          $id = DataBase::Current()->EscapeString($this->href);
          $link = DataBase::Current()->ReadField("SELECT alias FROM {'dbprefix'}pages WHERE id = '".$id."'");
		  if(Page::Current()->alias == $link){
			$class .= " active";
		  }
		  if($link == "home"){
			$res = "<li class=\"".$class."\"><a href=\"".Settings::getInstance()->get("host")."\" title=\"".htmlentities($this->title)."\" 
				class=\"".$class." menue-".$id."-".$index."\">".htmlentities($this->title)."</a>".$elementend;
		  }
		  else{
                        $link = UrlRewriting::GetUrlByAlias($link);
                        if($link != "")
                        {
                            $res = $elementstart."<a href=\"".$link."\" title=\"".htmlentities($this->title)."\" 
                                    class=\"".$class." menue-".$id."-".$index."\">".htmlentities($this->title)."</a>".$elementend;
                        }
                        else{
                            //Page doesn't exist
                            $res = "";
                        }
		  }
        }
        else if($this->type == 1){
          $href = str_replace("{VAR:HOST}",Settings::getInstance()->get("host"),$this->href);
          $res = $elementstart."<a href=\"".$href."\" title=\"".htmlentities($this->title)."\" 
                 class=\"".$class." menue-".$this->id."-".$index."\">".htmlentities($this->title)."</a>".$elementend;
        }
        else if($this->type == 2){
          $res .= Menu::getCode($this->href,$globalstart,$globalend, $elementstart,$elementend,$class);
        }
        else{
          $res = "<li>".htmlentities($this->title)." <div>&gt;</div>";
          $inside = Menu::getCode($this->href,$globalstart,$globalend, $elementstart,$elementend,$class);
          if($inside != "<ul></ul>"){
            $res .= $inside;
          }
          $res .= "</li>";
        }
        return $res;
    }
    
    /**
     *
     * @param Actionlist $actions
     * @return string
     */
    public function getEditableCode(Actionlist $actions){
      $template = new Template();
      $template->load("menuentry_editable");
      $template->assign_var("ID", $this->id);
      $template->assign_var("TITLE",$this->title);
      if($this->type == 0){
        $template->assign_var("SELECTEDINTERNAL"," selected=\"1\"");
        $selector = new PageSelector();
        $selector->name  = $this->id.'_href';
        $selector->value = $this->href;
        $template->assign_var("VALUECONTROL",$selector->getCode());
      $template->assign_var("SELECTEDEXTERNAL","");
      $template->assign_var("SELECTEDSUBMENU","");
      }
      else{
        if($this->type == 1){
          $template->assign_var("SELECTEDEXTERNAL"," selected=\"1\"");
          $template->assign_var("SELECTEDINTERNAL","");
          $template->assign_var("SELECTEDSUBMENU","");
          $template->assign_var("VALUECONTROL","<input name=\"".$this->id."_href\" value=\"".$this->href."\" />");
        }
        else{
          if($this->type == 2){
            $template->assign_var("SELECTEDSUBMENU"," selected=\"1\"");
            $template->assign_var("SELECTEDINTERNAL","");
            $template->assign_var("SELECTEDEXTERNAL","");
            $selector = new MenueSelector();
            $selector->name  = $this->id.'_href';
            $selector->value = $this->href;
            $template->assign_var("VALUECONTROL",$selector->getCode());
          }
        }
      }
      $params = array();
      $params['menu'] = $this->menu;
      $params['ID'] = $this->id;
      $template->assign_var("ACTIONS",$actions->getCode($params));

      return $template->getCode();
    }
    
    public function displayEditAble(){
      echo $this->getEdiableCode();
    }
    
    /**
     *
     * @return boolean 
     */
    public function save(){
      if($this->id == -1){
        return $this->insert();
      }
      else{
        return $this->update();
      }
    }
    
    /**
     *
     * @return boolean 
     */
    private function insert(){
      $menu  = DataBase::Current()->EscapeString($this->menu);
      $title = DataBase::Current()->EscapeString($this->title);
      $href  = DataBase::Current()->EscapeString($this->href);
      $type  = DataBase::Current()->EscapeString($this->type);
      $maxID = DataBase::Current()->ReadField("SELECT MAX(id) FROM {'dbprefix'}menu 
                                          WHERE menuID = '".$menu."'");
      $id = $maxID + 1;
      $res = DataBase::Current()->Execute("INSERT INTO {'dbprefix'}menu (id,menuID,title,href,type)
                                      VALUES('".$id."','".$menu."',
                                      '".$title."','".$href."','".$type."')");
      if($res){
        $args['menu']  = $menu;
        $args['title'] = $title;
        $args['href']  = $href;
        EventManager::RaiseEvent("menu_entry_added",$args);
      }
      Cache::clear("menu");
      return $res;
    }
    
    /**
     *
     * @return boolean
     */
    private function update(){
      $id    = DataBase::Current()->EscapeString($this->id);
      $menu  = DataBase::Current()->EscapeString($this->menu);
      $title = DataBase::Current()->EscapeString($this->title);
      $href  = DataBase::Current()->EscapeString($this->href);
      $type  = DataBase::Current()->EscapeString($this->type);
      $res =  DataBase::Current()->Execute("UPDATE {'dbprefix'}menu SET 
                                       href = '".$href."', 
                                         title = '".$title."', 
                                         type = '".$type."' 
                                       WHERE id = '".$id."' 
                                       AND menuID = '".$menu."'");
      if($res){
        $args['menu']  = $menu;
        $args['title'] = $title;
        $args['href']  = $href;
        $args['id']    = $id;
        EventManager::RaiseEvent("menu_entry_edit",$args);
      }
      Cache::clear("menu");
      return $res;
    }
    
    /**
     * Deletes all menuentries to a giben page
     * @param Page $page 
     */
    public static function DeleteByPage(Page $page){
        $id = DataBase::Current()->EscapeString($page->id);
        DataBase::Current()->Execute("DELETE FROM {'dbprefix'}menu WHERE type=0 AND href = '".$id."'");
        Cache::clear("menu");
    }
  }

?>
