<?php
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

class User{
	public $name								= "";
	public $role								= null;
	public $id									= -1;
	public $email							 = "";
	public $createTimestamp		 = "";
	public $lastAccessTimestamp = "";
	public $firstname						= "";
	public $lastname						= "";
	public $plz								= "";
	public $ort								= "";
	public $kanton							= "";
	public $strasse							= "";
	public $strnr							= "";
	public $aboutme							= "";
	public $adminmark						= "";

	public function __construct(){
		$this->role = new Role();
		$this->role->load(1);
	}
	
	/**
	 *
	 * @param string $name
	 * @param string $password
	 * @return boolean
	 */
	public function login($name,$password){
		if($this->checkPassword($password,trim($name))){
			$this->load(trim($name));
			$this->setLastAccess();
			$args = array();
			$args["user"] = $this;
			EventManager::RaiseEvent("user_login", $args);
			return true;
		}
		else{
			if(Plugin_Profile_Manager::getLoginCounter($name) == 0){
				sleep(0);
			}elseif(Plugin_Profile_Manager::getLoginCounter($name) > 2){
				sleep(3);
			}elseif(Plugin_Profile_Manager::getLoginCounter($name) >= 10){
				sleep(10);
			}
			$name = DataBase::Current()->EscapeString($name);
			if(Plugin_Profile_Manager::userExist($name)){
				Plugin_Profile_Manager::updateLoginCounter($name);
				if(Plugin_Profile_Manager::getLoginCounter($name) == 20){
					$muserdata = Plugin_Profile_Manager::getUserData($name);
					Plugin_Profile_Manager::setUserBlock($name);
					Plugin_Profile_Manager::changeAdminMark($muserdata->id);
					$mailtext = 'Liebe*r '.ucfirst($muserdata->firstname).'<br><br>Du oder jemand der sich als dich ausgibt versuchte sich mehrmals erfolglos mit deinem Account auf Social Meal einzuloggen.<br><br>Aus Sicherheitsgründen haben wir dein Profil vorübergehend gesperrt. Um die Sperre aufzuheben, kontaktiere bitte den Administrator von Social Meal!<br><br>Mail schreiben an: <a href="mailto:info@socialmeal.ch?subject=Account-wurde-gesperrt">info@socialmeal.ch</a><br>Social Meal';
					$h2t = new Plugin_Mailer_html2text($mailtext);
					$subject = utf8_decode('Dein Account wurde gesperrt!');
					$mail = new Plugin_PHPMailer_PHPMailer;
					$mail->IsSMTP();
					$mail->AddAddress($muserdata->email);
					$mail->AddAddress('info@socialmeal.ch');
					$mail->WordWrap = 50;
					$mail->IsHTML(true);
					$mail->Subject = $subject;
					$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
					$mail->AltBody = $h2t->get_text();
					if(!$mail->Send()){
						echo('Mail Error:'.$mail->ErrorInfo);
					}
				}
			}
			return false;
		}
	}
	
	public function autoLogin($name,$password){
		if($this->checkPasswordAutoLogin($password,trim($name))){
			$this->load(trim($name));
			$this->setLastAccess();
			$args = array();
			$args["user"] = $this;
			EventManager::RaiseEvent("user_login",$args);
			if(Plugin_Profile_Manager::getLoginCounter($name) > 0){
				Plugin_Profile_Manager::resetLoginCounter($name);
			}
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 *
	 * @return boolean
	 */
	private function setLastAccess(){
	$res = false;
	$id = DataBase::Current()->EscapeString($this->id);
	$this->lastAccessTimestamp = time();
	$res = DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET 
										last_access_timestamp=NOW() 
									WHERE id = '".$id."'");
	return $res;
	}

	/**
	 *
	 * @param string $password
	 * @param string $name
	 * @return boolean
	 */
	public function checkPassword($password,$name){
		$name = DataBase::Current()->EscapeString(trim($name));
		$count = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}user WHERE 
												role != '5' AND name='".$name."' AND password = '".md5($password.Settings::getValue("salt"))."'");
		return $count == 1;
	}
	
	public function checkPasswordAutoLogin($password,$name){
		$name = DataBase::Current()->EscapeString(trim($name));
		$count = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}user WHERE 
												name='".$name."' AND password = '".$password."'");
		return $count == 1;
	}
	
	public function logout(){
		 $args = array();
		 $args["user"] = $this;
		 EventManager::RaiseEvent("user_logout", $args);
		 session_destroy();
	}
	
	/**
	 *
	 * @return array 
	 */
	public function getAllUser(){
		$res = array();
		$users = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}user ORDER by id ASC");
		foreach($users as $user){
			$newUser = new User();
			$newUser->id	 = $user->id;
			$newUser->name = $user->name;
			$newUser->role->load($user->role);
			$newUser->email = $user->email;
			$newUser->created = $user->created;
			$newUser->access = $user->access;
			$newUser->firstname = $user->firstname;
			$newUser->lastname = $user->lastname;
			$newUser->plz = $user->plz;
			$newUser->ort = $user->ort;
			$newUser->kanton = $user->kanton;
			$newUser->strasse = $user->strasse;
			$newUser->strnr = $user->strnr;
			$newUser->aboutme = $user->aboutme;
			$newUser->adminmark = $user->adminmark;
			$res[] = $newUser;
		}
		return $res;
	}
	
	/**
	 *
	 * @return array 
	 */
	public function getAllMarkUser(){
		$res = array();
		$users = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}user WHERE adminmark = '1' ORDER by id ASC");
		foreach($users as $user){
			$newUser = new User();
			$newUser->id	 = $user->id;
			$newUser->name = $user->name;
			$newUser->role->load($user->role);
			$newUser->email = $user->email;
			$newUser->created = $user->created;
			$newUser->access = $user->access;
			$newUser->firstname = $user->firstname;
			$newUser->lastname = $user->lastname;
			$newUser->plz = $user->plz;
			$newUser->ort = $user->ort;
			$newUser->kanton = $user->kanton;
			$newUser->strasse = $user->strasse;
			$newUser->strnr = $user->strnr;
			$newUser->aboutme = $user->aboutme;
			$newUser->adminmark = $user->adminmark;
			$res[] = $newUser;
		}
		return $res;
	}

	/**
	 *
	 * @param string $name 
	 */
	public function load($name){
		$name = DataBase::Current()->EscapeString(strtolower(trim($name)));
		$user = DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}user WHERE name = '".$name."'");
		if($user){
			$this->id									= $user->id;
			$this->name								= $user->name;
			$this->email							 = $user->email;
			$this->role->load($user->role);
			$this->createTimestamp		 = $user->create_timestamp;
			$this->lastAccessTimestamp = $user->last_access_timestamp;
			$this->firstname = $user->firstname;
			$this->lastname = $user->lastname;
			$this->plz = $user->plz;
			$this->ort = $user->ort;
			$this->kanton = $user->kanton;
			$this->strasse = $user->strasse;
			$this->strnr = $user->strnr;
			$this->aboutme = $user->aboutme;
			$this->adminmark = $user->adminmark;
		}
	}

	/**
	 *
	 * @return boolean
	 */
	public function isGuest(){
		return $this->role->ID == 1;
	}

	/**
	 *
	 * @return boolean
	 */
	public function isAdmin(){
		return $this->role->ID == 2;
	}

	/**
	 *
	 * @return boolean
	 */
	public function exists(){
		$name		 = DataBase::Current()->EscapeString(strtolower(trim($this->name)));
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}user WHERE 
												name='".$name."'") > 0;
	}

	/**
	 *
	 * @param string $password
	 * @return boolean
	 */
	public function insert($password){
		$res = false;
		if($this->validate($password)){
			$name		 = DataBase::Current()->EscapeString(trim($this->name));
			$role		 = DataBase::Current()->EscapeString($this->role);
			$password = DataBase::Current()->EscapeString(/*md5($password)*/md5($password./*md5(*/Settings::getInstance()->get("salt")/*)*/));
			$email 	= DataBase::Current()->EscapeString($this->email);
			$res = DataBase::Current()->Execute("INSERT INTO {'dbprefix'}user (name, password, role, email, create_timestamp) 
										VALUES ('".$name."','".$password."','".$role."','".$email."',NOW())");
			$args = array();
			$args["user"] = $this;
			EventManager::RaiseEvent("user_inserted", $args);
			Cache::clear("tables","userlist");
		}
		return $res;
	}
	
	/**
	 *
	 * @param string $password
	 * @return boolean
	 */
	public function validate($password){
		$res = true;
		if($res) $res = trim($this->name) != "";
		if($res) $res = trim($this->role) != "";
		if($res) $res = trim($password) != "";
	if($res) $res = !$this->exists();
		return $res;
	}

	/**
	 *
	 * @return boolean
	 */
	public function delete(){
		$res = false;
		if(!$this->equals(User::Current())){
			$id = DataBase::Current()->EscapeString(strtolower(trim($this->id)));
			$res = DataBase::Current()->Execute("DELETE FROM {'dbprefix'}user WHERE id = '".$id."'");
			Cache::clear("tables","userlist");
			$args = array();
			$args["user"] = $this;
			EventManager::RaiseEvent("user_deleted", $args);
		}
		return $res;
	}

	/**
	 *
	 * @param User $user
	 * @return boolean
	 */
	public function equals(User $user){
		return $this->id == $user->id;
	}
	
	/**
	 * 
	 * @return User
	 */
	public static function Current(){
			if(!isset($_SESSION['user'])){
					$_SESSION['user'] = new User();
			}
			return $_SESSION['user'];
	}
}
?>
