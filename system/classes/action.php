<?PHP
  /**
  * @package ContentLion-Core
  * @author Stefan Wienstroeer
  */
  /*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class Action {

    private $id          = -1;
    private $icon        = "";
    private $destination = "";
    private $label       = "";

    /**
     *
     * @return int id of the action
     */
    public function getID(){
      return $this->id;
    }

    public function setID($value){
      $this->id = $value;
    }
    
    /**
     *
     * @return string 
     */
    public function getIcon(){ 
      return $this->icon;
    }

    public function setIcon($value){
      $this->icon = $value;
    }

    /**
     *
     * @param array $params
     * @return string 
     */
    public function getDestination($params){
      $res = $this->destination;
      foreach($params as $key=>$value){
        $res = str_replace("{VAR:".strtoupper($key)."}",$value, $res);
      }
      $res = str_replace("{VAR:ROOT}",Settings::getInstance()->get("root"), $res);
      $res = str_replace("{VAR:HOST}",Settings::getInstance()->get("host"), $res);

      return $res;
    }

    public function setDestination($value){
      $this->destination = $value;
    }
    
    /**
     *
     * @return string 
     */
    public function getLabel(){
      return $this->label;
    }

    public function setLabel($value){
      $this->label = $value;
    }

    /**
     *
     * @param array $params
     * @return string
     */
    public function getCode($params){
      return "<a href=\"".$this->getDestination($params)."\" onclick=\"return confirm_alert(this);\" title=\"".htmlentities(utf8_encode($this->label))."\"><img src=\"".Icons::getIcon($this->icon)."\" /></a>";
    }

  }
?>
