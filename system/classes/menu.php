<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

class Menu{
  /**
   *
   * @param int $id
   * @param string $globalstart
   * @param string $globalend
   * @param string $elementstart
   * @param string $elementend
   * @param string $class 
   */
  static function display($id, $globalstart,$globalend, $elementstart,$elementend,$class){
    echo self::getCode($id, $globalstart,$globalend, $elementstart,$elementend,$class);
  }
  
  /**
   *
   * @param int $id
   * @param string $globalstart
   * @param string $globalend
   * @param string $elementstart
   * @param string $elementend
   * @param string $class
   * @return string 
   */
  static function getCode($id, $globalstart,$globalend, $elementstart,$elementend,$class){
    $res = "";
    if(SessionCache::contains("menu", $id."_".$globalstart."_".$globalend."_".$elementstart."_".$elementend."_".$class)){
        $res = SessionCache::getData("menu", $id."_".$globalstart."_".$globalend."_".$elementstart."_".$elementend."_".$class);
    }
    else{
        $res     = $globalstart;
        $entries = self::getEntries($id);
        $i = 1;
        if($entries){
          foreach($entries as $entry){ 
            $res .= $entry->getCode($globalstart,$globalend, $elementstart,$elementend,$class,$i);
            $i++;
          }
        }
        $res .= $globalend;
        SessionCache::setData("menu", $id."_".$globalstart."_".$globalend."_".$elementstart."_".$elementend."_".$class, $res);
    }
    return $res;
  }
  
  /**
   *
   * @param string $name
   * @return int
   */
  function getIdByName($name){
    $name = DataBase::Current()->EscapeString(strtolower(trim($name)));
    return DataBase::Current()->ReadField("SELECT id FROM {'dbprefix'}menu_names WHERE lower(trim(name)) = '".$name."' LIMIT 0,1");;
  }

  /**
   *
   * @param int $id
   * @return string
   */
  public static function getEditableCode($id){
    $res = "";
    $entries = self::getEntries($id);
    if($entries){
      $actions = ActionList::get("menuedit");
      foreach($entries as $entry){ 
        $res .= $entry->getEditableCode($actions);
      }
    }
    return $res;
  }

  /**
   *
   * @param int $id 
   */
  function displayEditable($id){
    echo $this->getEdiableCode($id);
  }

  /**
   *
   * @param int $menu
   * @param string $title
   * @param string $href
   * @return boolean 
   */
  function addEntry($menu,$title,$href){
    $menu = DataBase::Current()->EscapeString($menu);
    $maxID = DataBase::Current()->ReadField("SELECT MAX(id) FROM {'dbprefix'}menu 
                                        WHERE menuID = '".$menu."'");
    $id = $maxID + 1;
    $res = DataBase::Current()->Execute("INSERT INTO {'dbprefix'}menu (id,menuID,title,href)
                                    VALUES('".$id."','".$menu."',
                                    '".$title."','".$href."')");
    if($res){
      $args['menu']  = $menu;
      $args['title'] = $title;
      $args['href']  = $href;
      $args['id']    = $id;
      EventManager::RaiseEvent("menu_entry_added",$args);
    }
    SessionCache::clear();
    return $res;
  }

  /**
   *
   * @param int $menu
   * @param int $id
   * @param string $title
   * @param string $href
   * @return boolean
   */
  function editEntry($menu,$id,$title,$href){
    $menu = Database::Current()->EscapeString($menu);
    $id    = Database::Current()->EscapeString($id);
    $title = Database::Current()->EscapeString($title);
    $href = Database::Current()->EscapeString($href);
    $res =  DataBase::Current()->Execute("UPDATE {'dbprefix'}menu SET 
                                     href = '".$href."', 
                                     title = '".$title."' 
                                     WHERE id = '".$id."' 
                                     AND menuID = '".$menu."'");
    if($res){
      $args['menu']  = $menu;
      $args['title'] = $title;
      $args['href']  = $href;
      $args['id']    = $href;
      EventManager::RaiseEvent("menu_entry_edit",$args);
    }
    SessionCache::clear();
    return $res;
  }

  /**
   *
   * @param int $menu
   * @param int $id
   * @return boolean 
   */
  public static function deleteEntry($menu,$id){
    $menu = DataBase::Current()->EscapeString($menu);
    $id   = Database::Current()->EscapeString($id);
    $res  = DataBase::Current()->Execute("DELETE FROM {'dbprefix'}menu WHERE menuID = '".$menu."' AND id = '".$id."'");
    if($res){
      $res = DataBase::Current()->Execute("UPDATE {'dbprefix'}menu SET id = id - 1 WHERE menuID = '".$menu."'  AND id > ".$id."");
      if($res){
        $args['menu']  = $menu;
        $args['id']    = $id;
        EventManager::RaiseEvent("menu_entry_deleted",$args);
      }
    }
    SessionCache::clear();
    return $res;
  }

  /**
   *
   * @param string $name
   * @param string $dir
   * @return boolean 
   */
  public static function create($name,$dir = ""){
    $name = DataBase::Current()->EscapeString($name);
    $dir = DataBase::Current()->EscapeString($dir);
    $res = DataBase::Current()->Execute("INSERT INTO {'dbprefix'}menu_names (name, dir) VALUES ('".$name."', '".$dir."')");
    if($res){
      $args['name']  = $name;
      EventManager::RaiseEvent("menu_created",$args);
    }
    SessionCache::clear();
    Cache::clear("menu");
    return DataBase::Current()->InsertID();
  }

  /**
   *
   * @param int $id
   * @return string 
   */
  static function delete($id){
    $id = DataBase::Current()->EscapeString($id);
    $res = DataBase::Current()->Execute("DELETE FROM {'dbprefix'}menu_names WHERE id = '".$id."'");
    if($res){
      $res = DataBase::Current()->Execute("DELETE FROM {'dbprefix'}menu WHERE menuID = '".$id."'");
      if($res){
        $args['id']  = $id;
        EventManager::RaiseEvent("menu_deleted",$args);
      }
    }
    Cache::clear();
    SessionCache::clear();
    return $res;
  }
  
  /**
   *
   * @param int $id
   * @return MenuEntry 
   */
  static function getEntries($id){
    $res = null;
    $id = DataBase::Current()->EscapeString($id);
    $rows = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}menu WHERE menuID = '".$id."' ORDER BY id");
    if($rows){
      foreach($rows as $row){
        $entry = new MenuEntry();
        $entry->id    = $row->id;
        $entry->type  = $row->type;
        $entry->href  = $row->href;
        $entry->title = $row->title;
        $entry->menu  = $id;
        $res[] = $entry;
      }
    }
    return $res;
  }

  /**
   *
   * @param int $menu
   * @return int 
   */
  public static function countEntries($menu){
    $menu  = DataBase::Current()->EscapeString($menu);
    return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}menu WHERE menuID = '".$menu."'");
  }

  /**
   *
   * @param int $menu
   * @param string $item1
   * @param string $item2 
   */
  public static function changePositions($menu, $item1, $item2){
    $entries = self::countEntries($menu);
    if($item1 > 0 && $item2 > 0 && $item1 < $entries + 1 && $item2 < $entries + 1){
      $item1 = DataBase::Current()->EscapeString($item1);
      $item2 = DataBase::Current()->EscapeString($item2);
      $menu  = DataBase::Current()->EscapeString($menu);
      DataBase::Current()->Execute("UPDATE {'dbprefix'}menu SET id = -1 WHERE menuID = '".$menu."'  AND id = ".$item1);
      DataBase::Current()->Execute("UPDATE {'dbprefix'}menu SET id = ".$item1." WHERE menuID = '".$menu."'  AND id = ".$item2);
      DataBase::Current()->Execute("UPDATE {'dbprefix'}menu SET id = ".$item2." WHERE menuID = '".$menu."'  AND id = -1");
    SessionCache::clear();
    }
  }

}
?>
