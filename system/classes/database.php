<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  abstract class DataBase{
  
      public $Name     = '';
      public $Prefix   = '';
      public $User     = '';
      public $Password = '';
      public $Host     = '';
      public $Type     = '';
      
      public abstract function Execute($sql);
      public abstract function EscapeString($text);
      public abstract function GetTables();
      public abstract function GetColumns($table);
      public abstract function ReadField($sql);
      public abstract function ReadRow($sql);
      public abstract function ReadRows($sql);
      public abstract function Connect();
      public abstract function Disconnect();
      
      public function __construct($config){
        if(file_exists($config)){
            include($config);
            $this->Prefix   = $dbpraefix;
            $this->Host     = $dbhost;
            $this->Password = $dbpassword;
            $this->User     = $dbuser;
            $this->Name     = $db;
            $this->Connect();
        }
      }
      
    
    /**
    * 
    * returns the current database instance
    * @return DataBase instance
    */
      public static function Current(){
          if(!isset($GLOBALS['db'])){
            $GLOBALS['db'] = new MySQL('system/dbsettings.php');
            $GLOBALS['db']->Connect();
          }
          return $GLOBALS['db'];
      }
      
      /**
       *
       * @param DataBase $db 
       */
      public static function SetCurrent(DataBase $db){
          $GLOBALS['db'] = $db;
      }
  
  }
?>
