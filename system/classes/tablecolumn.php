<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class TableColumn {

    public $field        = "";
    public $displayName  = "";
    public $customSelect = "";
    public $autoWidth    = false;
    public $visible      = true;
    public $value        = null;

    /**
     *
     * @param string $field
     * @param string $displayName
     * @param string $customSelect 
     */
    public function __construct($field = "",$displayName = "", $customSelect = ""){
      $this->field        = $field;
      $this->displayName  = $displayName;
      $this->customSelect = $customSelect;
    }

    /**
     *
     * @return string 
     */
    public function getSelect(){
      $res = $this->field;
      if(strlen($this->customSelect) > 0){
        $res = $this->customSelect." as ".$this->field;
      }
      return $res;
    }

    /**
     *
     * @param array $row
     * @return string 
     */
    public function getBodyCode($row){
      $res = "";
      if($this->visible){
        $res = "<td";
        if($this->autoWidth){
          $res .= " style='width:auto'";
        }
        $res .= ">".htmlentities(utf8_encode($this->getValue($row)))."</td>";
      }
      return $res;
    }

    /**
     *
     * @param array $row
     * @return string
     */
    private function getValue($row){
      $res = $this->value;
      if($res == null){
        $res = $row[$this->field];
      }
      return $res;
    }

    /**
     *
     * @return string 
     */
    public function getHeaderCode(){
      $res = "";
      if($this->visible){
        $res = "<td";
        if($this->autoWidth){
          $res .= " style='width:auto'";
        }
        $res .= ">".htmlentities(utf8_encode($this->displayName))."</td>";
      }
      return $res;
    }

  }
?>
