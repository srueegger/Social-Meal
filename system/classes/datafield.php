<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class DataField{
    private $id          = -1;
    private $dataType    = null;
    private $dataTypeID  = -1;
    private $displayName = "";
    private $dataName    = "";
    private $validatorID = -1;
    private $validator   = null;

    /**
     *
     * @param DataType $type
     * @param string $name
     * @return DataField 
     */
    public static function getByFormAndName(DataType $type, $name){
      $res = new DataField();
      $typeID = DataBase::Current()->EscapeString($type->getID());
      $name = DataBase::Current()->EscapeString($name);
      
      if($field = DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}datafields WHERE dataType = '".$typeID."' AND dataName = '".$name."'")){
        $res->id          = $field->id;
        $res->dataTypeID  = $field->dataType;
        $res->dataType    = $type;
        $res->displayName = $field->displayName;
        $res->dataName    = $field->dataName;
        $res->validatorID = $field->validator;
      }

      return $res;
    }

    /**
     *
     * @param DataType $dataType
     * @return array 
     */
    public static function getByDataType(DataType $dataType){
      $res = array();
      $dataTypeID = DataBase::Current()->EscapeString($dataType->getID());
      $fields = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}datafields WHERE datatype = '".$dataTypeID."'");
      foreach($fields as $field){
        $fieldObj = new DataField();
        $fieldObj->id          = $field->id;
        $fieldObj->dataTypeID  = $field->dataType;
        $fieldObj->dataType    = $dataType;
        $fieldObj->displayName = $field->displayName;
        $fieldObj->dataName    = $field->dataName;
        $fieldObj->validatorID = $field->validator;
        $res[] = $fieldObj;
      }
      return $res;
    }

    /**
     *
     * @return string 
     */
    public function getDataName(){
      return $this->dataName;
    }

    /**
     *
     * @return DataValidator 
     */
    public function getValidator(){
      if($this->validator == null){
        $this->validator = new DataValidator($this->validatorID);
      }
      return $this->validator;
    }

    /**
     *
     * @param mixed $value
     * @return boolean 
     */
    public function validate($value){
      return $this->getValidator()->validate($value);
    }

    /**
     *
     * @return string 
     */
    public function getLastError(){
      return $this->getValidator()->getLastError();
    }
  }
?>
