<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class FolderBreadcrumb{

    /**
     *
     * @param Page $page
     * @param string $separator
     * @param string $class
     * @param string $idpraefix 
     */
    public static function display(Page $page,$separator,$class,$idpraefix){
      $host = Settings::getInstance()->get("host");
      $i = 1;
      $breadcrumb  = $page->getBreadcrumb();
      $j = 0;
      foreach($breadcrumb as $item){
        $breadcrumb[$j][0] = UrlRewriting::GetShortUrlByAlias($breadcrumb[$j][0]);
        $j++;
      }
      $currentItem = array_pop($breadcrumb);
      $fulldir = "";
      foreach(explode("/",$_SESSION['dir']) as $dir){
        if($dir != ""){
          $url = UrlRewriting::GetShortUrlByAlias("admin/home","dir=/".$fulldir."/".$dir);
          $breadcrumb[] = array($url,$dir);
          $fulldir .= "/".$dir;
        }
      }
      $breadcrumb[] = $currentItem;
      while($i <= count($breadcrumb)){
        echo "<a style='display:inline' href=\"".$host.$breadcrumb[$i-1][0]."\" class=\"".$class."\" 
              id=\"".$idpraefix.$i."\">".$breadcrumb[$i-1][1]."</a>";
        if($i < count($breadcrumb)){
          echo $separator;
        }
        $i++;
      }
    }

  }
?>
