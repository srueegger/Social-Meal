<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

	class UrlRewriting{
	
                /**
                 *
                 * @param string $alias
                 * @param string $parameter
                 * @return string
                 */
		public static function GetUrlByAlias($alias,$parameter = ""){
			return Settings::getValue("host").self::GetShortUrlByAlias($alias,$parameter);
		}
	
                /**
                 *
                 * @param string $alias
                 * @param string $parameter
                 * @return string 
                 */
		public static function GetShortUrlByAlias($alias,$parameter = ""){
			$res = $alias.".html";
			if(!self::hasModRewrite()){
				$res = "index.php?include=".$alias;
			}
			if($parameter != ""){
				$res .= self::GetFirstParameterSeperator().$parameter;
			}
			return $res;
		}
	
                /**
                 *
                 * @param Page $page
                 * @param string $parameter
                 * @return string
                 */
		public static function GetUrlByPage(Page $page,$parameter = ""){
			return self::getUrlByAlias($page->alias,$parameter);
		}
	
                /**
                 *
                 * @return boolean
                 */
		private static function hasModRewrite(){
			return !(getenv('HTTP_MOD_REWRITE')=='Off' || (function_exists("apache_get_modules") && !in_array("mod_rewrite",@apache_get_modules())));
		}
		
                /**
                 *
                 * @return string 
                 */
		public static function GetFirstParameterSeperator(){
			$res = "?";
			if(!self::hasModRewrite()){
				$res = "&";
			}
			return $res;
		}
	
	}
?>
