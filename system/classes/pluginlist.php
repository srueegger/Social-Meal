<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class PluginList{
    public $plugins;
    
    /**
     *
     * @param PluginInfo $pluginInfo 
     */
    public function Add(PluginInfo $pluginInfo){
      $this->plugins[] = $pluginInfo;
    }
    
    public function loadAll(){
      $pluginpath = Settings::getInstance()->get("root")."system/plugins";
      $oDir = openDir($pluginpath);
      while($item = readDir($oDir)){
        $this->AddInfo($item);
      }

      closeDir($oDir);
    }
    
    private function AddInfo($plugin_name){
      $pluginpath = Settings::getInstance()->get("root")."system/plugins";
      if(is_dir($pluginpath."/".$plugin_name)){
        if(file_exists($pluginpath."/".$plugin_name."/info.php")){
          include($pluginpath."/".$plugin_name."/info.php");
        }
      }
    }
    
    public static function GetInfo($plugin_name){
      $list = new PluginList();
      $list->AddInfo($plugin_name);
      if(isset($list->plugins[0])){
        return $list->plugins[0];
      }
      return null;
    }
  }
?>
