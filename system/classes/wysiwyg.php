<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
	class WYSIWYG extends Editor{
			
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
	
		public function display(){
			$template = new Template();
			$template->load($this->page->getEditorContent($this));
			echo $template->getCode();
			if(!Mobile::isMobileDevice()){
				//Sidebar
				include('./system/skins/socialmeal/sidebar.php');
			}
		}

		function getHeader(){
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$template = new Template();
			$template->load("control_wysiwyg");
			$template->assign_var("CONTENT",$this->page->getEditorContent($this));
			$template->assign_var("HOST",Settings::getInstance()->get("host"));
			$template->assign_var("ALIAS",$this->page->alias);
			$template->assign_var("URL",	UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias));
			$template->assign_var("LANG",strtolower(Settings::getInstance()->get("language")));
			$template->assign_var("PREVIEWURL",$this->page->GetUrl());
			return $template->getCode();
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",$_POST['content']));
			$this->page->save();
		}
	}
?>
