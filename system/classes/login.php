<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

  class Login extends Editor{
      
    /**
     *
     * @param Page $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
        ?>
            <h2>Login</h2>
        <?PHP
        if(isset($_POST['name']) && $_POST['name']){
            if(User::Current()->login($_POST['name'],$_POST['password'])){
                echo Language::DirectTranslate("HELLO")." ".User::Current()->name;
?>
<script language="JavaScript"><!--
var zeit=(new Date()).getTime(); 
var stoppZeit=zeit+4000; 
while((new Date()).getTime()<stoppZeit){}; 
window.location.href="<?PHP echo UrlRewriting::GetUrlByAlias("admin/home"); ?>";
// --></script> 
<?PHP
            }
            else{
                echo Language::DirectTranslate("LOGIN_FAILED");
            }
        }
        if(User::Current()->isGuest()){
            ?>
            <form method="POST">
                <table>
                    <tr>
                        <td><?PHP echo Language::DirectTranslate("USERNAME"); ?>:</td>
                        <td><input name="name" autofocus /></td>
                    </tr>
                    <tr>
                        <td><?PHP echo Language::DirectTranslate("PASSWORD"); ?>:</td>
                        <td> <input name="password" type="password" /></td>
                    </tr>
                </table>
                <input type="submit" value="Login" />
            </form>
            <?PHP
        }
        else{
?>
<script language="JavaScript"><!--
window.location.href="<?PHP echo UrlRewriting::GetUrlByAlias("admin/home"); ?>";
// --></script> 
<?PHP
        }
    }
    function getHeader(){
    }
    
    /**
     *
     * @return string
     */
    public function getEditableCode(){
      $change = htmlentities(utf8_encode(Language::GetGlobal()->getString("CHANGE")));
      return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }    
}
?>
