<?php
//Invite Start
if(!isset($_SESSION['invite'])){
	if(isset($_GET['invitefrom'])){
		$iuserid = DataBase::Current()->EscapeString($_GET['invitefrom']);
		$iusername = DataBase::Current()->ReadField("SELECT name FROM {'dbprefix'}user WHERE id = '".$iuserid."'");
		$_SESSION['invite'] = $iusername;
	}
}
//Invite Ende
?>
<html xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<meta charset="utf-8">
<?PHP
sys::includeHeader();
?>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<script src="<?PHP echo sys::getFullSkinPath(); ?>js/javascript.js"></script>
<link href='https://fonts.googleapis.com/css?family=Noticia+Text:400,400italic,700,700italic|Crete+Round:400,400italic|Lobster|Lobster+Two:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?PHP echo sys::getFullSkinPath(); ?>css/style.min.css">
<link id="color" rel="stylesheet" href="<?PHP echo sys::getFullSkinPath(); ?>css/color1.min.css">
<link rel="stylesheet" type="text/css" href="<?PHP echo sys::getFullSkinPath(); ?>fancybox/jquery.fancybox-1.3.4.min.css" media="screen" />
<meta name="author" content="Samuel Rüegger" >
<meta name="google-site-verification" content="kRJXw-UzserGgFtVaSvZPP8qB4Qwc1i7YdqNM_Yv5Pw" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="geo.region" content="CH-BS" />
<meta name="geo.placename" content="Basel" />
<meta name="geo.position" content="47.6;7.6" />
<meta name="ICBM" content="47.6, 7.6" />
<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/manifest.json">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
<meta name="apple-mobile-web-app-title" content="Social Meal">
<meta name="application-name" content="Social Meal">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<meta name="theme-color" content="#ffffff">
<link rel="alternate" type="application/rss+xml" title="<?PHP echo htmlentities(sys::getTitle()); ?> &raquo; Feed" href="<?php echo Settings::getInstance()->get("host"); ?>rss-feed-neue-events.html" />
<!-- Piwik -->
<script type="text/javascript">
	var _paq = _paq || [];
	_paq.push(['trackPageView']);
	_paq.push(['enableLinkTracking']);
	(function() {
	var u="//socialmeal.ch/piwik/";
	_paq.push(['setTrackerUrl', u+'piwik.php']);
	_paq.push(['setSiteId', 1]);
	var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
	g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
	})();
</script>
<noscript><p><img src="//socialmeal.ch/piwik/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->
</head>
<body>
<!--Header Part Start-->
<header id="header" class="clearfix">
	<div class="headerstrip"><div class="spiral"></div></div>
	
	<div class="container clearfix">
		<a href="/home.html" id="logo">
			<img src="<?PHP echo sys::getFullSkinPath(); ?>images/smneulogo.png" width="325" alt="<?PHP echo htmlentities(sys::getTitle()); ?>">
		</a>
		<?php
			include('./system/skins/socialmeal/menu.php');
		?>
	</div>
</header>
<!--Header Part End-->

<!--Middle Part Start-->
<?php
if(isset($_GET['verifymail'])){
	$verifyid = DataBase::Current()->EscapeString($_GET['verifymail']);
	$verifydata = Plugin_PluginData_Data::getData('newmail_'.$verifyid.'','plugin','profile');
	if($verifydata['code'] == $_GET['verifycode']){
		DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET role = '4' WHERE id = '".$verifyid."' LIMIT 1");
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_plugindata_data WHERE property = 'newmail_".$verifyid."' AND area = 'profile'");
		if(!User::Current()->isGuest()){
			User::Current()->logout();
		}
	}
}
?>
<section id="maincontainer" class="container clearfix">
	<?PHP
	if(Page::Current()->alias != 'home'){
	?>
	<h1 class="heading1"><?PHP echo(htmlentities(Page::Current()->title)); ?></h1>
	<?PHP
	}
	if(Page::Current()->editorText == "wysiwyg" or Page::Current()->editorText == "Plugin_BlogOverview_Editor" or Page::Current()->editorText == "Plugin_BlogEntries_Editor"){
		echo('<section class="mb20 leftpart">');
	}
	if(isset($_POST['content']) && $_POST['content']){
		echo $_POST['content'];
	}else{
		sys::includeContent();
	}
	?>
</section>
<!--Middle container End-->

<!--Middle Part End-->
<!--Footer Start-->
<footer id="footer" class="clearfix">
	<div class="footerbg clearfix">
		<ul class="footersection container">
			<li class="testimonial">
				<h4>Testimonial</h4>
				<p class="testi">
					"Unglaublich lustiger Abend mit netten und sozialen Leuten. Gerne wieder! :-)"
				</p>
		
				<span>Franka<br />
				Studentin</span>
			</li>
			<li class="links">
				<h4>Kategorien</h4>
				<?PHP
					Menu::display(25,'<ul>','</ul>','<li>','</li>','','');
				?>
			</li>
			<li class="flickrsection">
				<h4>Weiteres</h4>
				<?PHP
					Menu::display(24,'<ul>','</ul>','<li>','</li>','','');
				?>
			</li>
			<li class="links">
				<a href="/uber-uns.html"><img src="<?PHP echo sys::getFullSkinPath(); ?>images/partner/logoobnew.png" /></a>
				<br /><br />
				<a href="http://urbanagriculturebasel.ch" target="_blank"><img src="<?PHP echo sys::getFullSkinPath(); ?>images/partner/uanblogo.png" /></a>
			</li>
		</ul>
	</div>
	<!--Social links Start-->
	<section id="social" class="container">
		<div class="leftline">&copy; 2014 - <?php echo(date('Y')); ?></div>
		<div id="footersocial">
			<a class="facebook" title="Facebook" target="_blank" href="https://www.facebook.com/42socialmeal">Facebook</a>
			<a class="twitter" title="Twitter" target="_blank" href="https://twitter.com/infosocialmeal">Twitter</a>
			<a class="googleplus" title="Googleplus" target="_blank" href="https://plus.google.com/u/0/b/102044072731061267569/102044072731061267569/posts">Googleplus</a>
			<a class="rss" title="rss" href="<?php echo Settings::getInstance()->get("host"); ?>rss-feed-neue-events.html">rss</a>
		</div>
	</section>
	<!--Social links End-->
	<!--Go to Top-->
	<a title="Go Top" id="gotop" href="#">&nbsp;</a>
</footer>
<!--Footer End-->

<!--Javascripts-->
<script src="<?PHP echo sys::getFullSkinPath(); ?>js/jquery-latest.js"></script>
<script type="text/javascript" src="<?PHP echo sys::getFullSkinPath(); ?>js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?PHP echo sys::getFullSkinPath(); ?>js/jquery.isotope.min.js"></script>
<script src="<?PHP echo sys::getFullSkinPath(); ?>js/mediaelement-and-player.min.js"></script>
<script	type="text/javascript" src="<?PHP echo sys::getFullSkinPath(); ?>js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="<?PHP echo sys::getFullSkinPath(); ?>js/jquery.elastislide.js"></script>
<script type="text/javascript" src="<?PHP echo sys::getFullSkinPath(); ?>fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="<?PHP echo sys::getFullSkinPath(); ?>fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script	type="text/javascript" src="<?PHP echo sys::getFullSkinPath(); ?>js/jquery.validate.js"></script>
<script src="<?PHP echo sys::getFullSkinPath(); ?>js/jflickrfeed.min.js"></script>
<script type="text/javascript" src="<?PHP echo sys::getFullSkinPath(); ?>js/script.js"></script>
</body>
</html>
<?PHP
if(Page::Current()->alias == 'event-bearbeiten'){
?>
<script src="/system/plugins/events/js/jquery.datetimepicker.js"></script>
<script>/*
window.onerror = function(errorMsg) {
	$('#console').html($('#console').html()+'<br>'+errorMsg)
}*/
$('#datetimepicker').datetimepicker({
dayOfWeekStart : 1,
lang:'de',
disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
startDate:	'1986/01/05'
});
$('#datetimepicker').datetimepicker({value:'2015/04/15 05:03',step:10});

$('.some_class').datetimepicker();

$('#evdate').datetimepicker({
	lang: 'de',
	formatTime:'H:i',
	formatDate:'d.m.Y',
	format:'Y-m-d H:i',
	defaultTime:'18:00',
	inline: true,
	step: 15,
	minDate: 0,
	todayButton: false,
	timepickerScrollbar:true
});

$('#asdate').datetimepicker({
	lang: 'de',
	formatTime:'H:i',
	formatDate:'d.m.Y',
	format:'Y-m-d H:i',
	defaultTime:'18:00',
	inline: true,
	step: 15,
	minDate: 0,
	todayButton: false,
	timepickerScrollbar:true
});
</script>
<?php
	}
?>
