<nav id="topnav">
	<?php
	$counter = Plugin_Profile_Manager::hasUnreadEvents();
	?>
	<br /><br /><br />
	<ul class="menuul">
		<li <?php if(Page::Current()->alias == 'essen-gehen'){ echo('class="active"'); } ?>>
			<a	href="/essen-gehen.html" data-description="<?php echo(Language::DirectTranslate('private_findmeal2')); ?>"><?php echo(Language::DirectTranslate('private_findmeal')); ?>
				<?php
					if($counter > 0){
						echo('<span class="nav-counter nav-counter-blue">'.$counter.'</span>');
					}
				?>
			</a>
			<?php
				if($counter > 0){
					echo('<ul><li><a href="/home.html?readall=true">'.Language::DirectTranslate('plugin_events_markallassee').'</a></li></ul>');
				}
			?>
		</li>
		<?php
		if(!User::Current()->isGuest()){
		?>
			<li <?php if(Page::Current()->alias == 'neuen-event-erstellen' or Page::Current()->alias == 'event-bearbeiten'){ echo('class="active"'); } ?>><a href="/neuen-event-erstellen.html" data-description="Ich will Essen anbieten">Meal erstellen</a>
			</li>
		<?php
		}else{
			?>
			<li>
				<a href="#" data-description="Ich will Essen anbieten">Meal erstellen</a>
				<ul>
					<?php
					echo('<li><a href="/login.html">'.Language::DirectTranslate('plugin_register2_gologin').'</a></li>');
					echo('<li><a href="/registrieren.html">'.Language::DirectTranslate('plugin_register2_goregister').'</a></li>');
					?>
				</ul>
			</li>
			<?php
		}
		?>
		<li>
			<a href="/Blog/uebersicht.html" data-description="<?php echo(Language::DirectTranslate('plugin_bloglanguagepacks_wewrite')); ?>..."><?php echo(Language::DirectTranslate('plugin_bloglanguagepacks_blog')); ?>
				<?php
					$unreadblogs = Plugin_Profile_Manager::hasUnreadBlogs();
					if($unreadblogs > 0){
						echo('<span class="nav-counter nav-counter-green">'.$unreadblogs.'</span>');
					}
				?>
			</a>
			<?php
				if($unreadblogs > 0){
					$alias = DataBase::Current()->ReadField("SELECT alias FROM {'dbprefix'}pages WHERE editor = 'Plugin_BlogEntries_Editor' ORDER BY id DESC LIMIT 1");
					?>
					<ul>
						<li><a href="<?php echo(UrlRewriting::GetUrlByAlias($alias)); ?>">Neusten Blogeintrag anzeigen</a></li>
					</ul>
					<?php
				}
			?>
		</li>
		<li <?php if(Page::Current()->alias == 'mein-profil' or Page::Current()->alias == 'mein-profil-bearbeiten' or Page::Current()->alias == 'passwort-aendern' or Page::Current()->alias == 'ausloggen' or Page::Current()->alias == 'login' or Page::Current()->alias == 'registrieren' or Page::Current()->alias == 'freunde' or Page::Current()->alias == 'meine-sprachen' or Page::Current()->alias == 'posteingang' or Page::Current()->alias == 'postausgang' or Page::Current()->alias == 'nachricht-schreiben' or Page::Current()->alias == 'freunde-einladen' or Page::Current()->alias == 'meine-einstellungen'){ echo('class="active"'); } ?>>
		<?php
			unset($counter);
			$counter = 0;
			$counter = $counter + Plugin_PM_Manager::countUnreadMessages(User::Current()->id);
			$counter = $counter + Plugin_Profile_Manager::countFriendRequests();
			$counter = $counter + Plugin_Profile_Manager::countAdminRequest();
			$muserdata = Plugin_Profile_Manager::getUserData(User::Current()->name);
			if(Plugin_Profile_Manager::hasNewRatings() == true){
				$counter = $counter + 1;
			}
			if($muserdata->wettbewerb == 0){
				$counter = $counter + 1;
			}
			if(User::Current()->role->ID != 1){
				echo ('<a href="/mein-profil.html" data-description="'.Language::DirectTranslate('plugin_profile_myprofile').'">'.Language::DirectTranslate('plugin_profile_profile').'');
				if($counter > 0){
					echo('<span class="nav-counter nav-counter">'.$counter.'</span>');
				}
				echo('</a>');
			}else{
				echo('<a href="/login.html" data-description="'.Language::DirectTranslate('plugin_register2_bemember').'">'.Language::DirectTranslate('plugin_register2_gologin').'</a>');
			}
		?>
		
			<ul>
				<?php
				if(User::Current()->role->ID != 1){
				echo '
				<li><a href="/wettbewerb.html">'.Language::DirectTranslate('plugin_profile_competitionpagetypetitle').'';
				if($muserdata->wettbewerb == 0){
					echo ' (1)';
				}
				echo '</a></li>';
				echo'
				<li><a href="/posteingang.html">'.Language::DirectTranslate('plugin_pm_pagetypetitleinbox').'';
				if(Plugin_PM_Manager::countUnreadMessages(User::Current()->id) > 0){
					echo(' (<span class="bold">'.Plugin_PM_Manager::countUnreadMessages(User::Current()->id).'</span>)');
				}
				echo '</a></li>
				<li><a href="/freunde-einladen.html">'.Language::DirectTranslate('plugin_profile_invitefriends').'</a></li>';
				if(Plugin_Profile_Manager::countFriendRequests() > 0){
					echo('<li><a href="/freunde.html">'.Language::DirectTranslate('plugin_profile_friendrequest').' (<span class="bold">'.Plugin_Profile_Manager::countFriendRequests().')</span></a></li>');
				}
				if(Plugin_Profile_Manager::hasAdminRequest()){
					echo('<li><a href="/mein-profil.html">'.Language::DirectTranslate('plugin_profile_adminrequest').' (<span class="bold">'.Plugin_Profile_Manager::countAdminRequest().'</span>)</a></li>');
				}
				if(Plugin_Profile_Manager::hasNewRatings() == true){
					echo('<li><a href="/bewertungen.html">'.Language::DirectTranslate('plugin_profile_newratings').'</a></li>');
				}
				echo '
				<li><a href="/meine-sprachen.html">'.utf8_encode(Language::DirectTranslate('plugin_profile_addlng2')).'</a></li>
				<li><a href="/mein-profil-bearbeiten.html">'.Language::DirectTranslate('plugin_profile_editprofilepagetypetitle').'</a></li>
				<li><a href="/meine-einstellungen.html">'.utf8_encode(Language::DirectTranslate('plugin_profile_setsettings')).'</a></li>
				<li><a href="/passwort-aendern.html">'.utf8_encode(Language::DirectTranslate('plugin_profile_changemypw')).'</a></li>';
				if(User::Current()->role->ID == 2){
					echo('<li><a target="_blank" href="/admin/home.html">'.Language::DirectTranslate('private_admin').'</a></li>');
				}
				echo '<li><a href="/ausloggen.html">'.Language::DirectTranslate('plugin_logout_logout').'</a></li>';
				}else{
					if(!Mobile::isMobileDevice()){
						echo'<li><a href="/login.html">'.Language::DirectTranslate('plugin_register2_gologin').'</a></li>';
					}
				echo '<li><a href="/registrieren.html">'.Language::DirectTranslate('plugin_register2_goregister').'</a></li><li><a href="/passwort-vergessen.html">'.Language::DirectTranslate('plugin_profile_newpwpagetypetitle').'</a></li>';
				}
				?>
			</ul>
		</li>
	</ul>
</nav>
