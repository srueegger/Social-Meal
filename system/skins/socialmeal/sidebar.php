<?PHP
$template = new Template();
$template->load('plugin_events_sidebar');
$template->show_if('textcontent',false);
if(User::Current()->role->ID == 1){
	$template->show_if('gast',true);
}else{
	$template->show_if('gast',false);
}
if(Page::Current()->editorText == "wysiwyg" or Page::Current()->editorText == "Plugin_BlogOverview_Editor" or Page::Current()->editorText == "Plugin_BlogEntries_Editor"){
	$template->show_if('textcontent',true);
}
foreach(Plugin_Events_Manager::getPopularEvents() as $pevent){
	if(Plugin_Events_Manager::countSubscribers($pevent->id) < $pevent->maxguest or $pevent->eventtype == 3 && $pevent->maxguest == 0){
		$index = $template->add_loop_item("popular");
		$template->assign_loop_var("popular",$index,"id",$pevent->id);
		$template->assign_loop_var("popular",$index,"titel",htmlentities($pevent->eventname));
		$template->assign_loop_var("popular",$index,"datum",Plugin_Events_Manager::formatDate($pevent->eventdate));
		if($pevent->preis == 0){
			$template->assign_loop_var("popular",$index,"preis",'');
		}else{
			$template->assign_loop_var("popular",$index,"preis",htmlentities($pevent->preis).' CHF');
		}
		$image = Plugin_Events_Manager::getFirstImageFromEvent($pevent->id);
		if($image == ''){
			$template->assign_loop_var("popular",$index,"image",'');
		}else{
			$template->assign_loop_var("popular",$index,"image",'<img src="/content/uploads/eat/'.$image.'" />');
		}
	}
}
foreach(Plugin_Events_Manager::getNewEvents() as $nevent){
	if(Plugin_Events_Manager::countSubscribers($nevent->id) < $nevent->maxguest or $nevent->eventtype == 3 && $newvent->maxguest == 0){
		$index = $template->add_loop_item("neu");
		$template->assign_loop_var("neu",$index,"id",$nevent->id);
		$template->assign_loop_var("neu",$index,"titel",htmlentities($nevent->eventname));
		$template->assign_loop_var("neu",$index,"datum",Plugin_Events_Manager::formatDate($nevent->eventdate));
		if($pevent->preis == 0){
			$template->assign_loop_var("neu",$index,"preis",'');
		}else{
			$template->assign_loop_var("neu",$index,"preis",htmlentities($nevent->preis).' CHF');
		}
		$image = Plugin_Events_Manager::getFirstImageFromEvent($nevent->id);
		if($image == ''){
			$template->assign_loop_var("neu",$index,"image",'');
		}else{
			$template->assign_loop_var("neu",$index,"image",'<img src="/content/uploads/eat/'.$image.'" />');
		}
	}
}
echo($template->getCode());
?>
