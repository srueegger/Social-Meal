<!-- /*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */ -->
<?php
if(!empty($_GET['dir'])){
	$_GET['dir'] = '/'.$_GET['dir'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de">
  <head>
	  <meta charset="utf-8">
    <link rel='stylesheet' href="<?PHP echo sys::getFullSkinPath(); ?>style.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<?PHP echo Settings::getInstance()->get("host"); ?>system/css/jquery/base/jquery.ui.all.css">
    <link rel="stylesheet" href="<?PHP echo Settings::getInstance()->get("host"); ?>system/css/jquery/contentlion.css">
    <script type="text/javascript" src="<?PHP echo Settings::getInstance()->get("host"); ?>system/plugins/jquery/jquery-1.4.2.js"></script>
    <script type="text/javascript" src="<?PHP echo Settings::getInstance()->get("host"); ?>system/plugins/jquery/external/jquery.bgiframe-2.1.1.js"></script>
    <script type="text/javascript" src="<?PHP echo Settings::getInstance()->get("host"); ?>system/plugins/jquery/ui/jquery.ui.core.js"></script>
    <script type="text/javascript" src="<?PHP echo Settings::getInstance()->get("host"); ?>system/plugins/jquery/ui/jquery.ui.widget.js"></script>
    <script type="text/javascript" src="<?PHP echo Settings::getInstance()->get("host"); ?>system/plugins/jquery/ui/jquery.ui.mouse.js"></script>
    <script type="text/javascript" src="<?PHP echo Settings::getInstance()->get("host"); ?>system/plugins/jquery/ui/jquery.ui.draggable.js"></script>
    <script type="text/javascript" src="<?PHP echo Settings::getInstance()->get("host"); ?>system/plugins/jquery/ui/jquery.ui.position.js"></script>
    <script type="text/javascript" src="<?PHP echo Settings::getInstance()->get("host"); ?>system/plugins/jquery/ui/jquery.ui.resizable.js"></script>
    <script type="text/javascript" src="<?PHP echo Settings::getInstance()->get("host"); ?>system/plugins/jquery/ui/jquery.ui.sortable.js"></script>
    <script type="text/javascript" src="<?PHP echo Settings::getInstance()->get("host"); ?>system/plugins/jquery/ui/jquery.ui.dialog.js"></script>
    <script type="text/javascript" src="<?PHP echo Settings::getInstance()->get("host"); ?>system/js/dialogs.js"></script>
    <script type="text/javascript" src="<?PHP echo Settings::getInstance()->get("host"); ?>system/thirdparty/wysiwyg/scripts/jHtmlArea-0.8.js"></script>
<link rel="Stylesheet" type="text/css" href="<?PHP echo Settings::getInstance()->get("host"); ?>system/thirdparty/wysiwyg/style/jHtmlArea.css" />
    <?PHP
      sys::includeHeader();
    ?>
	<!--[if IE 7]>
		<link rel="stylesheet" href="<?PHP echo sys::getFullSkinPath(); ?>ie7.css">
	<![endif]-->
  </head>
  <body>
    <div id="header">
      <div id="globalmenu">
        <?PHP
          sys::displayGlobalMenu("<ul>","</ul>","<li>"," </li>",
                                 "");
        ?>
      </div>
    </div>
    <div id="logout">
        <?PHP echo htmlentities(sys::getCurrentUserName()); ?> | <a href="<?PHP echo UrlRewriting::getUrlByAlias("admin/logout"); ?>">Logout</a>
    </div>
    <div id="breadcrumb">
      <?PHP sys::displayBreadcrumb(" -&gt; ","breadcrump","bc"); ?>
    </div>
    <div id="content">
      <h1><?PHP 
      if(Page::Current()->alias == 'admin/translations'){
			echo utf8_encode(Page::Current()->title);
		  }else{
			  echo Page::Current()->title; 
			  }
		?></h1>
      <?PHP
          sys::includeContent();
      ?>
    </div>
    <div id="footer">
	  <div style="padding-right:5px;float:right">
		  <?PHP echo sys::getFooter(); ?> 
		  <?PHP if(DEVELOPMENT){ ?>
			| Q: <?PHP echo $GLOBALS['db']->countQueries(); ?> | M: <?PHP echo memory_get_usage(); ?> | C: <?PHP echo $GLOBALS['loadedClasses']; ?>
		  <?PHP } ?>
	  </div>
	  <div style="padding-left:5px;">
		<a href="https://socialmeal.ch" target="_blank" style="color:#d19340">Social Meal Webseite</a> | 
		<a target="_blank" href="https://github.com/srueegger/Social-Meal/wiki/Dokumentation-Social-Meal" style="color:#d19340">Dokumentation</a>
		<a target="_blank" href="https://github.com/srueegger/Social-Meal/issues" style="color:#d19340">Offene Issues</a> |
		<a target="_blank" href="https://github.com/srueegger/Social-Meal/wiki" style="color:#d19340">Social Meal Wiki</a>
	  </div>
    </div>
    <div id="iframedialog" title="" style="display:none;padding:0">
      <iframe id="iframedialog_frame" style="width:100%;height:100%;border-width:0" />
    </div>
  </body>
</html>
