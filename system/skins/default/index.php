<!-- /*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */ -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de">
  <head>
    <?PHP
      sys::includeHeader();
    ?>
    <link rel='stylesheet' href="<?PHP echo sys::getFullSkinPath(); ?>style.css" type="text/css" media="all" />
  </head>
  <body>
      <div id="container">
        <div id="pagetitle"><?PHP echo htmlentities(sys::getTitle()); ?></div>
        <div id="globalmenu">
        <?PHP
          sys::displayGlobalMenu("<ul id=\"menu\">","</ul>","<li>"," </li>",
                                 "");
        ?>
          <img src="<?PHP echo sys::getFullSkinPath(); ?>images/frontimage.png" />
        </div>
        <div id="localmenu">
        <?PHP
          sys::displayLocalMenu("<ul>","</ul>","<li>","</li>",
                                "localmenuentry");
        ?>
        </div>
        <div id="content">
          <div id="breadcrumb">
            <?PHP
              sys::displayBreadcrumb(" -&gt; ","breadcrumb","bc");
            ?>
          </div>
          <?PHP
            if(isset($_POST['content']) && $_POST['content']){
              echo $_POST['content'];
            }
            else{
              sys::includeContent();
            }
            ?>
        </div>
        <div id="footer">
		  <?PHP if(strtolower(Settings::getValue("language")) == "de"){ ?>
			<?PHP echo htmlentities(sys::getTitle()); ?> is powered by <a href="http://www.contentlion.de">ContentLion CMS</a>
		  <?PHP } else { ?>
			<?PHP echo htmlentities(sys::getTitle()); ?> is powered by <a href="http://www.contentlion.org">ContentLion CMS</a>
		  <?PHP } ?>
        </div>
      </div>
  </body>
</html>
