<div class="mainMenuOuterWrapper mainMenuHidden">
	<ul class="mainMenuWrapper">
		<li data-background="#649ae1" class="menuFood"><a href="/home.html">Meal finden
		<?php
		if(!User::Current()->isGuest()){
			$counter = Plugin_Profile_Manager::hasUnreadEvents();
			if($counter > 0){
				echo(' ('.$counter.')');
			}
		}
		?>
		</a></li>
		<li data-background="#b3cfc1" class="menuDiamond"><a href="/neuen-event-erstellen.html">Meal erstellen</a></li>
		<li data-background="#dc6e6e" class="menuPen"><a href="/Blog/uebersicht.html">Blog
		<?php
		if(!User::Current()->isGuest()){
			$unreadblogs = Plugin_Profile_Manager::hasUnreadBlogs();
			if($unreadblogs > 0){
			 echo (' ('.$unreadblogs.')');
			}
		}
		?>
		</a></li>
		<?php
		if(User::Current()->isGuest()){
			echo('<li data-background="#dbd48b" class="menuUser"><a href="/login.html">Einloggen</a></li>');
		}else{
			echo('<li data-background="#ec6f5a" class="menuUser"><a href="/mein-profil.html">Profil</a></li>');
			if(Plugin_PM_Manager::countUnreadMessages(User::Current()->id)){
				echo('<li data-background="#c47acb" class="menuMail"><a href="/posteingang.html">Posteingang ('.Plugin_PM_Manager::countUnreadMessages(User::Current()->id).')</a></li>');
			}
			if(Plugin_Profile_Manager::countFriendRequests() > 0){
				echo('<li data-background="#f7c65f" class="menuStack"><a href="/freunde.html">'.Language::DirectTranslate('plugin_profile_friendrequest').' ('.Plugin_Profile_Manager::countFriendRequests().')</a></li>');
			}
			echo('<li data-background="#80a697" class="menuLock"><a href="/ausloggen.html">Ausloggen</a></li>');
		}
		?>
		<li data-background="#c47acb" class="menuNote"><a href="/FAQ/faq-ubersicht.html">FAQ</a></li>
		<li data-background="#a3cc9d" class="menuMail"><a href="/kontakt.html">Kontakt</a></li>
	</ul>
</div>
