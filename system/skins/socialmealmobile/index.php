<!DOCTYPE html>
<html>
	<head>
	<?PHP
	sys::includeHeader();
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<meta name="geo.region" content="CH-BS" />
	<meta name="geo.placename" content="Basel" />
	<meta name="geo.position" content="47.6;7.6" />
	<meta name="ICBM" content="47.6, 7.6" />
	<!-- favorite icon starts -->
	<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/manifest.json">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="apple-mobile-web-app-title" content="Social Meal">
	<meta name="application-name" content="Social Meal">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<!-- favorite icon ends -->
	<!-- Google fonts start -->
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600,700' rel='stylesheet' type='text/css'>
	<!-- Google fonts end -->
	<!-- CSS files start -->
	<link href="<?PHP echo sys::getFullSkinPath(); ?>css/framework.min.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?PHP echo sys::getFullSkinPath(); ?>css/colorbox.min.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?PHP echo sys::getFullSkinPath(); ?>css/elements.min.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?PHP echo sys::getFullSkinPath(); ?>css/style.min.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?PHP echo sys::getFullSkinPath(); ?>css/responsive.min.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="<?PHP echo sys::getFullSkinPath(); ?>css/hidpi.min.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="<?PHP echo sys::getFullSkinPath(); ?>css/skin.min.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?PHP echo sys::getFullSkinPath(); ?>css/custom.min.css" rel="stylesheet" type="text/css" media="all" />
	<!-- CSS files end -->
	<!-- JavaScript files start -->
	<script type="text/javascript" src="<?PHP echo sys::getFullSkinPath(); ?>js/jquery.min.js"></script>
	<script type="text/javascript" src="<?PHP echo sys::getFullSkinPath(); ?>js/effects.jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?PHP echo sys::getFullSkinPath(); ?>js/jquery.nivo-slider.min.js"></script>
	<script type="text/javascript" src="<?PHP echo sys::getFullSkinPath(); ?>js/jquery.colorbox.min.js"></script>
	<script type="text/javascript" src="<?PHP echo sys::getFullSkinPath(); ?>js/custom.min.js"></script>
	<!-- JavaScript files end -->
	</head>
	<body>
		<?php
		if(isset($_GET['verifymail'])){
			$verifyid = DataBase::Current()->EscapeString($_GET['verifymail']);
			$verifydata = Plugin_PluginData_Data::getData('newmail_'.$verifyid.'','plugin','profile');
			if($verifydata['code'] == $_GET['verifycode']){
				DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET role = '4' WHERE id = '".$verifyid."' LIMIT 1");
				DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_plugindata_data WHERE property = 'newmail_".$verifyid."' AND area = 'profile'");
				if(!User::Current()->isGuest()){
					User::Current()->logout();
				}
			}
		}
		?>
		<!-- website wrapper starts -->
		<div class="websiteWrapper"> 
			<!-- header outer wrapper starts -->
			<div class="headerOuterWrapper">
				<!-- header outer wrapper starts -->
				<div class="headerOuterWrapper">
					<div class="headerWrapper"> <a href="/home.html" class="mainLogo"><img src="<?PHP echo sys::getFullSkinPath(); ?>images/common/smneulogo.png" alt=""/></a> <a href="" class="mainMenuButton"></a></div>
				</div>
				<!-- header outer wrapper ends --> 
			</div>
			<!-- header outer wrapper ends --> 
			<!-- main menu outer wrapper starts -->
			<?PHP include('./system/skins/socialmealmobile/menu.php'); ?>
			<!-- main menu outer wrapper ends --> 
			<!-- page wrapper starts dynamisch -->
			<?PHP
			if(Page::Current()->editorText == "Plugin_BlogEntries_Editor"){
				echo '<div class="pageWrapper singlePostPageWrapper"><div class="singlePostContentWrapper">';
			}elseif(Page::Current()->editorText == "Plugin_BlogOverview_Editor"){
				echo '<div class="pageWrapper singlePostPageWrapper">';
			}elseif(Page::Current()->alias == "home"){
				echo '<div class="pageWrapper portfolioTwoFilterablePageWrapper">
				<ul class="portfolioMenuWrapper" id="portfolioMenuWrapper">
				<li><a href="" data-type="all" class="currentPortfolioFilter">'.Language::DirectTranslate('plugin_events_home6').'</a></li>
				<li><a href="" data-type="Fleisch">'.Language::DirectTranslate('plugin_events_fleisch').'</a></li>
				<li><a href="" data-type="Vegetarisch">'.Language::DirectTranslate('plugin_events_vegetarisch').'</a></li>
				<li><a href="" data-type="Vegan">'.Language::DirectTranslate('plugin_events_vegan').'</a></li>
				</ul><div class="portfolioTwoFilterableWrapper">';
			}elseif(Page::Current()->alias == 'event-details'){
				echo '<div class="pageWrapper singleProjectPageWrapper">';
				$event = Plugin_Events_Manager::loadEvent($_GET['event']);
				if(Plugin_Events_Manager::countImagesPerEvent($event->id) > 0){
					echo '<div class="singleProjectImageWrapper"><img src="'.Settings::getInstance()->get("host").'content/uploads/eat/'.DataBase::Current()->ReadField("SELECT filename FROM {'dbprefix'}plugin_eventimages WHERE eventid = '".$event->id."' LIMIT 1").'" class="singleProjectImage" alt=""/> </div>';
				}
				echo '<div class="pageContentWrapper">';
			}elseif(Page::Current()->alias == 'kontakt'){
				echo '<div class="pageWrapper contactPageWrapper"><div class="contactFormWrapper">';
			}else{
				echo '<div class="pageWrapper standardPageWrapper"> 
				<div class="pageContentWrapper">';
			}
			?>
					<!-- page title starts -->
					<h3 class="pageTitle"><?PHP echo(htmlentities(Page::Current()->title)); ?></h3>
					<!-- page title ends -->
					<?PHP
					if(isset($_POST['content']) && $_POST['content']){
						echo $_POST['content'];
					}else{
						sys::includeContent();
					}
					?>
				<?PHP
				if(Page::Current()->editorText != "Plugin_BlogEntries_Editor"){
					echo '</div>';
				}
				?>
				<!-- portfolio wrapper ends --> 
			</div>
			<div class="socialIconsWrapper">
				<a target="_blank" href="https://www.facebook.com/42socialmeal" class="socialIcon socialIconFacebookDark"></a> 
				<a target="_blank" href="https://twitter.com/infosocialmeal" class="socialIcon socialIconTwitterDark"></a> 
				<a href="https://socialmeal.ch/rss-feed-neue-events.html" class="socialIcon socialIconRssDark"></a> 
			</div>
			<!-- page wrapper ends -->  
			<!-- footer wrapper starts -->
			<div class="footerWrapper"> 
				<!-- copyright wrapper starts -->
				<div class="copyrightWrapper"> 
					<!-- copyright starts --> 
					<span class="copyright">&copy; 2014 - <?php echo(date('Y')); ?></span> 
					<!-- copyright ends --> 
					<!-- back to top button starts --> 
					<a href="" class="backToTopButton"></a> 
					<!-- back to top button ends --> 
				</div>
				<!-- copyright wrapper ends --> 
			</div>
		<!-- footer wrapper ends -->   
		</div>
		<!-- website wrapper ends -->
		<!-- preloader starts -->
		<div class="preloader"></div>
		<!-- preloader ends -->
	</body>
</html>
<?php
if(Page::Current()->alias == 'event-bearbeiten'){
?>
<script src="/system/plugins/events/js/jquery.datetimepicker.js"></script>
<script>/*
window.onerror = function(errorMsg) {
	$('#console').html($('#console').html()+'<br>'+errorMsg)
}*/
$('#datetimepicker').datetimepicker({
dayOfWeekStart : 1,
lang:'de',
disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
startDate:	'1986/01/05'
});
$('#datetimepicker').datetimepicker({value:'2015/04/15 05:03',step:10});

$('.some_class').datetimepicker();

$('#evdate').datetimepicker({
	lang: 'de',
	formatTime:'H:i',
	formatDate:'d.m.Y',
	format:'Y-m-d H:i',
	defaultTime:'18:00',
	inline: true,
	step: 15,
	minDate: 0,
	todayButton: false,
	timepickerScrollbar:true
});

$('#asdate').datetimepicker({
	lang: 'de',
	formatTime:'H:i',
	formatDate:'d.m.Y',
	format:'Y-m-d H:i',
	defaultTime:'18:00',
	inline: true,
	step: 15,
	minDate: 0,
	todayButton: false,
	timepickerScrollbar:true
});
</script>
<?php
	}
?>
