<?php

/**
 * @package BBCode - Easy BBCode
 * @author Robik
 * @version 0.3
 * @license http://opensource.org/licenses/gpl-2.0.html
 */

class Plugin_BBCode_Translator{
	
	private $tags = array(
		'b','i','u','url','small','big','p','center','color','size','img','quote');
	
	
	/**
	 * Replaces all the BBCodes to HTML Codes
	 * @param string $done: the text with BBCodes
	 * @param array [$usertags] array of tags to replace
	 */
	function replace($done, $usertags = "") {
		
		// If user doesn't specify tags, we'll replace all
		if($usertags == "") {
			$usertags = $this->tags;
		}
		
		// Checking that usertags contains unknown for us BBCode 
		$diff = array_diff($usertags,$this->tags);
		
		// If yes
		if(count($diff))
			throw new Exception("Unknown tag:".join(' ',$diff));
		
		// Deleting spaces from begging and end of string
		$done = trim($done);
		
		// Deleting all html code
		$done = htmlspecialchars($done);
		
		//Auto-URL Detection
		$URLRegex = '/(?:(?<!(\[\/url\]|\[\/url=))(\s|^))';     // No [url]-tag in front and is start of string, or has whitespace in front
		$URLRegex.= '(';                                        // Start capturing URL
		$URLRegex.= '(https?|ftps?|ircs?):\/\/';                // Protocol
		$URLRegex.= '\S+';                                      // Any non-space character
		$URLRegex.= ')';                                        // Stop capturing URL
		$URLRegex.= '(?:(?<![[:punct:]])|(?<=\/))(\s|\.?$)/i';  // Doesn't end with punctuation (excluding /) and is end of string (with a possible dot at the end), or has whitespace after
		$done = preg_replace($URLRegex,"$2[url]$3[/url]$5",$done);
		
		
		if(in_array("url",$usertags)) {
			$done = preg_replace("#\[url\](.*?)?(.*?)\[/url\]#si", "<a href=\"\\1\\2\" target=\"_blank\">\\1\\2</a>", $done);
			$done = preg_replace("#\[url=(.*?)?(.*?)\](.*?)\[/url\]#si", "<a href=\"\\2\" target=\"_blank\">\\3</a>", $done);
		}
		
		if(in_array("b",$usertags))
			$done = preg_replace("#\[b\](.*?)\[/b\]#si", "<span class=\"bold\">\\1</span>", $done);
		if(in_array("i",$usertags))
			$done = preg_replace("#\[i\](.*?)\[/i\]#si", "<span style=\"font-style:italic;\">\\1</span>", $done);
		if(in_array("u",$usertags))
			$done = preg_replace("#\[u\](.*?)\[/u\]#si", "<span style=\"text-decoration:underline;\">\\1</span>", $done);	
		if(in_array("quote",$usertags))
			/*[quote]*/
			while(preg_match('#\[quote\](.*)\[\/quote\]#isU', $done)){
			$done = preg_replace('#\[quote\](.*)\[\/quote\]#isU', '<blockquote style="style1">$1</blockquote>', $done);
			}
		if(in_array("small",$usertags))	
			$done = preg_replace("#\[small\](.*?)\[/small\]#si", "<small>\\1</small>", $done);
		if(in_array("big",$usertags))
			$done = preg_replace("#\[big\](.*?)\[/big\]#si", "<big>\\1</big>", $done);
		if(in_array("p",$usertags))
			$done = preg_replace("#\[p\](.*?)\[\/p\]#si", "<p>\\1</p>", $done);
		if(in_array("center",$usertags))
			$done = preg_replace("#\[center\](.*?)\[\/center\]#si", "<p style=\"text-align:center;\">\\1</p>", $done);
		if(in_array("color",$usertags))
			$done = preg_replace("#\[color=(http://)?(.*?)\](.*?)\[/color\]#si", "<span style=\"color:\\2\">\\3</span>", $done);
		if(in_array("size",$usertags))
			$done = preg_replace("#\[size=(http://)?([0-9]{0,2})\](.*?)\[/size\]#si", "<span style=\"font-size:\\2px\">\\3</span>", $done);
		if(in_array("img",$usertags))	
			$done = preg_replace("#\[img\](.*?)\[/img\]#si", "<img src=\"\\1\" border=\"0\" alt=\"Image\" />", $done);
		$done = Plugin_PM_Manager::formatSmilies($done);
		// Changing [enter] to <br />
		$done = nl2br($done);
		
		return $done;
	}
}

?>
