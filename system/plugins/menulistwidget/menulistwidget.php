<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

class MenuListWidget extends WidgetBase{
	public function load(){
		if(!empty($_GET['dir'])){
			$_GET['dir'] = '/'.$_GET['dir'];
		}
		$this->headline = Language::DirectTranslate("plugin_menulistwidget_menus");
		if(Cache::contains("menu","widget_".isset($_GET['dir']))){
			$this->content = Cache::getData("menu","widget_".isset($_GET['dir']));
		}else{
			if(!isset($_GET['dir']) || substr($_GET['dir'],0,1) == '.') $_GET['dir'] = "";
			$template = new Template();
			$template->load("plugin_menulistwidget_menulist");
			$newmenuurl = UrlRewriting::GetUrlByAlias("admin/newmenu");
			$template->assign_var("NEWMENUURL",$newmenuurl);
			$menus = sys::getMenues($_GET['dir']);
			foreach($menus as $menu){
				$index = $template->add_loop_item("MENUS");
				$template->assign_loop_var("MENUS", $index, "ID", $menu->id);
				$template->assign_loop_var("MENUS", $index, "TITLE", $menu->name);
				$template->assign_loop_var("MENUS", $index, "PAGES", $menu->count);
				$editurl = UrlRewriting::GetUrlByAlias("admin/editmenu","menu=".$menu->id);
				$template->assign_loop_var("MENUS", $index, "EDITURL", $editurl);
				$deleteurl = UrlRewriting::GetUrlByAlias("admin/deletemenu","menu=".$menu->id);
				$template->assign_loop_var("MENUS", $index, "DELETEURL", $deleteurl);
			}
			if(!$menus){
				$template->assign_var("NOMENUS",Language::DirectTranslate("plugin_menulistwidget_no_menus"));
			}else{
				$template->assign_var("NOMENUS","");
			}
			$this->content = $template->getCode();
			Cache::setData("menu","widget_".$_GET['dir'],$this->content);
		}
	}
}
?>
