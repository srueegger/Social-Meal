<?php
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

	DataBase::Current()->Execute("CREATE TABLE IF NOT EXISTS `{'dbprefix'}plugin_logout` (
								 `pageid` int(11) NOT NULL,
								 `redirect_url` varchar(255) NOT NULL,
								  PRIMARY KEY (`pageid`)
								  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
	
	$sql = "SELECT * FROM {'dbprefix'}pages WHERE editor = 'logout'";
	$rows = DataBase::Current()->ReadRows($sql);
	foreach($rows as $row)
	{
		$sql = "INSERT INTO {'dbprefix'}plugin_logout "
			  ."SET pageid = '".DataBase::Current()->EscapeString($row->id)."',"
				  ."redirect_url = '".DataBase::Current()->EscapeString(Settings::getValue("host"))."'";
				  
		DataBase::Current()->Execute($sql);
		
		$sql = "UPDATE {'dbprefix'}pages "
			  ."SET editor = 'Plugin_Logout_Page' "
			  ."WHERE id = '".DataBase::Current()->EscapeString($row->id)."'";
			  
		DataBase::Current()->Execute($sql);
	}
	
	$sql = "INSERT INTO {'dbprefix'}pagetypes "
		  ."SET class = 'Plugin_Logout_Page'"
		  ."AND name = 'logout'";
		
	DataBase::Current()->Execute($sql);
