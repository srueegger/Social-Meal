<?php
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

	class Plugin_Logout_Page extends Editor{
		private $failed = false;
		private $redirect_url = null;
		
		public function __construct(Page $page){
			$this->page = $page;
		}
		
		/**
		 *Display the login form 
		 */
		public function display(){
		}
		
		public function getHeader(){
		}
		
		/**
		 * Force url entering
		 * @return string
		 */
		public function getEditableCode(){
			if(isset($_POST['plugin_logout_redirect_url']) && is_string($_POST['plugin_logout_redirect_url'])){
				$sql = "REPLACE INTO {'dbprefix'}plugin_logout "
					  ."SET redirect_url = '".DataBase::Current()->EscapeString($_POST['plugin_logout_redirect_url'])."', "
						  ."pageid = '".DataBase::Current()->EscapeString($this->page->id)."'";

				DataBase::Current()->Execute($sql);
			}
			$template = new Template();
			$template->load("plugin_logout_editor");
			$template->assign_var("URL", htmlentities($this->redirect_url()));
			return $template->getCode();
		}
		
		public function save(Page $newPage, Page $oldPage){
		}
		
		/**
		 * Url the user will be redirected to
		 * @return string
		 */
		public function redirect_url(){
			if(is_null($this->redirect_url)){
				$sql = "SELECT redirect_url "
					  ."FROM {'dbprefix'}plugin_logout "
					  ."WHERE pageid = '".DataBase::Current()->EscapeString($this->page->id)."'";
				$this->redirect_url = DataBase::Current()->ReadField($sql);
			}
			
			return $this->redirect_url;
		}
		
		public function ExecuteHttpHeader(){
			User::Current()->logout();
			if(isset($_COOKIE['smal'])){
				setcookie('smal','',time()-3600);
			}
			header("Location: ".$this->redirect_url());
			exit;
		}
	}
?>
