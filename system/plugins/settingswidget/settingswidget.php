<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

class SettingsWidget extends WidgetBase{
	public function load(){
		$this->headline = utf8_encode($GLOBALS['language']->getString("SETTINGS"))." (".$GLOBALS['language']->getString("FOLDER").")";
		$settings = new SettingsForm();
		$settings->role = 3;
		$settings->template = "plugin_settingswidget_widget";
		if(isset($_SESSION['dir'])){
			$settings->dir = $_SESSION['dir'];
		}
		if(isset($_GET['dir'])){
			$settings->url = UrlRewriting::GetUrlByAlias($_GET['include'],"dir=".$_GET['dir']);
		}else{
			$settings->url = UrlRewriting::GetUrlByAlias($_GET['include']);
		}
		if(isset($_GET['areatype'])){
			$settings->areaType = $_GET['areatype'];
		}
		if(isset($_GET['area'])){
			$settings->area = $_GET['area'];
		}
		if(isset($_GET['role'])) $settings->role = $_GET['role'];
		$this->content = $settings->getCode();
	}
}
?>
