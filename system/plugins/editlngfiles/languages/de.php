<?PHP
/*
 * de.php
 *
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
	$tokens["plugin_description"]		= utf8_decode("Mit diesem Plugin können alle Sprachdateien (von Plugins) bearbeitet werden.");
	$tokens["pagetypetitle"]			= "Sprachdateien bearbeiten";
	$tokens["editlng"]					= "Sprache bearbeiten";
	$tokens["de"]						= "DE";
	$tokens["fr"]						= "FR";
	$tokens["en"]						= "EN";
?>
