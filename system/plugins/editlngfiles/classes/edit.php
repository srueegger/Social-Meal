<?PHP
/*
 * edit.php
 *
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
	class Plugin_Editlngfiles_Edit extends Editor{

		/**
		 *
		 * @param Page $page
		 */
		function __construct(Page $page){
			$this->page = $page;
		}

		public function display(){
			$template = new Template();
			$template->load('plugin_editlngfiles_edit');
			$template->show_if('saveok',false);
			if(!empty($_GET['lng']) && !empty($_GET['plugin'])){
				$filelink = Settings::getValue("root").'system/plugins/'.$_GET['plugin'].'/languages/'.$_GET['lng'].'.php';
				if(file_exists($filelink)){
					if(isset($_POST['savelngfile'])){
						$fp = @fopen($filelink,"w");
						fwrite($fp,$_POST['lngfile']);
						fclose($fp);
						$template->show_if('saveok',true);
					}
					$template->show_if('showedit',true);
					$template->show_if('nofile',false);
					$template->assign_var('lng',$_GET['lng']);
					$template->assign_var('pluginname',$_GET['plugin']);
					$fp = @fopen($filelink, "r");
					$loadcontent = fread($fp, filesize($filelink));
					$loadcontent = htmlspecialchars($loadcontent);
					fclose($fp);
					$template->assign_var('LNGCONTENT',$loadcontent);
				}else{
					$template->show_if('showedit',false);
					$template->show_if('nofile',true);

				}
			}else{
				$template->show_if('showedit',false);
				$template->show_if('nofile',true);
			}
			echo($template->getCode());
		}

		function getHeader(){
			echo('
			<script language="javascript" type="text/javascript" src="'.Settings::getValue("host").'system/plugins/editlngfiles/js/edit_area_full.js"></script>
			<script language="javascript" type="text/javascript">
editAreaLoader.init({
	id : "code"		// textarea id
	,syntax: "php"			// syntax to be uses for highgliting
	,start_highlight: true		// to display with highlight mode on start-up
});
</script>
			');
		}

		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}

		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
