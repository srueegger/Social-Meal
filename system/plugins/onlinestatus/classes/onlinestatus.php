<?php
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

class Plugin_OnlineStatus_OnlineStatus {
    
    /*
     * 
     * Set the user-flag to online 
    */
    public static function setOnline () {
    
        $userid = User::Current()->id;
        
        if (!User::Current()->isGuest()) {
            DataBase::Current()->Execute("UPDATE `{'dbprefix'}user` SET `plugin_onlinestatus_online` = '1', `plugin_onlinestatus_lastOnline` = NOW() WHERE `id` = '" . (int) $userid . "'");
        }
        
    }
    
    /*
     *
     * update the user-status 
    */
    public static function update () {
		$time = Settings::getInstance()->specify("plugin", "onlinestatus")->get("online_time");
		
		if(!is_numeric($time) || $time < 0)
		{
			$time = 15;
		}
	
        //Bei allen Usern, die in den letzten 5 Minuten nicht online waren, den Status auf "Offline" setzen.
        DataBase::Current()->Execute("UPDATE `{'dbprefix'}user` SET `plugin_onlinestatus_online` = 0 WHERE DATE_SUB(NOW(), INTERVAL " .$time. " MINUTE) > `plugin_onlinestatus_lastOnline`; ");
        
    }
    
    /*
     * 
     * Checks whether the user is logged in
     * @returns boolean true/false
    */
    public static function isOnline ($userid = -1) {
        
        if ($userid == -1) {
            $userid = User::Current()->id;
        } else {
            $userid = (int) $userid;
        }
        
        $online = DataBase::Current()->ReadField("SELECT `plugin_onlinestatus_online` FROM `{'dbprefix'}user` WHERE `id` = '" . $userid . "'; ");
        
        if ($online && $online == "1") {
            return true;
        } else  {
            return false;
        }
        
    }
    
    /*
     * 
     * returns all online user
     * @returns array(User) userlist 
    */
    public static function getAllOnlineUser () {
        $rows = DataBase::Current()->ReadRows("SELECT * FROM `{'dbprefix'}user` WHERE `plugin_onlinestatus_online` = '1'; ");
        $users = array();
        
        foreach ($rows as $row) {
            $user = new User();
            $user->name = $row->name;
            $user->role = new Role();
            $user->role->load($row->role);
            $user->id = $row->id;
            $user->email = $row->email;
            $user->createTimestamp = $row->create_timestamp;
            $user->lastAccessTimestamp = $row->last_access_timestamp;
            
            $users[] = $user;
        }
        
        return $users;
    }
    
}

?>
