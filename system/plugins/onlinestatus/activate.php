<?php
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

Language::ClearCache();
Cache::clear();

//register the event
EventManager::addHandler("system/plugins/onlinestatus/events/load.php", "pre_page_load");

DataBase::Current()->Execute("ALTER TABLE `{'dbprefix'}user` ADD `plugin_onlinestatus_online` BIT NOT NULL AFTER `create_timestamp`");
DataBase::Current()->Execute("ALTER TABLE `{'dbprefix'}user` ADD `plugin_onlinestatus_lastOnline` TIMESTAMP NOT NULL AFTER `plugin_onlinestatus_online`;");

DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}settings` (
`role`, `dir`, `area`, `areaType`, `property`, `value`, `activated`, `description`, `type`
) VALUES (
'3', 'global', 'onlinestatus', 'plugin', 'online_time', '5', '0', 'Online-Time in minutes', 'textbox'
); ");

DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}tasks` (
`script`, `interval`, `lastExecution`
) VALUES (
'system/plugins/onlinestatus/tasks/update.php', '0', '0000-00-00 00:00:00'
); ");

?>
