/*
 * en.contact.js
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

function chkFormular () {
	if (document.contact.name.value == "") {
		alert("Bitte gebe deinen Namen ein!");
		document.contact.name.focus();
		return false;
	}
	if (document.contact.email.value == "") {
		alert("Bitte gebe deine E-Mail Adresse ein!");
		document.contact.email.focus();
		return false;
	}
	if (document.contact.message.value == "") {
		alert("Bitte gebe deine Nachricht ein!");
		document.contact.message.focus();
		return false;
	}
	if (document.contact.email.value.indexOf("@") == -1) {
		alert("Bitte gebe eine gültige E-Mail Adresse an!");
		document.contact.email.focus();
		return false;
	}
}
