<?PHP
/*
 * contact.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_Contact_Contact extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			if(Mobile::isMobileDevice()){
				$template->load('plugin_contact_contact.mobile');
			}else{
				$template->load('plugin_contact_contact');
			}
			$template->show_if('showmsg',false);
			if(isset($_POST['contactsubmit']) && $_POST['url'] == ''){
				if(isset($_POST['name']) && isset($_POST['email']) && isset($_POST['message'])){
					$subject = utf8_decode('Neue Anfrage über das Kontaktformular');
					$body = 'Hallo tolles Social Meal Team<br /><br />Es gab eine neue Kontaktanfrage über das Kontaktformular von '.htmlentities($_POST['name']).' ('.htmlentities($_POST['email']).'). Nachfolgend die Nachricht:<br />'.nl2br(htmlentities($_POST['message'])).'';
					$h2t = new Plugin_Mailer_html2text($body);
					$mail = new Plugin_PHPMailer_PHPMailer;
					$mail->IsSMTP();
					$mail->AddAddress('info@socialmeal.ch');
					$mail->WordWrap = 50;
					$mail->IsHTML(true);
					$mail->Subject = $subject;
					$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$body);
					$mail->AltBody = $h2t->get_text();
					if(!$mail->Send()){
						echo('Mail Error:'.$mail->ErrorInfo);
					}
					$template->show_if('showmsg',true);
				}
			}
			echo($template->getCode());
		}

		function getHeader(){
			echo('<script type="text/javascript" src="/system/plugins/contact/js/contact.js"></script>');
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
