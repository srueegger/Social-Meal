<?php
	class Plugin_Login_Page extends Editor{
		private $failed = false;
		private $redirect_url = null;
		
		public function __construct(Page $page){
			$this->page = $page;
		}
		
		/**
		 *Display the login form 
		 */
		public function display(){
			if(User::Current()->isGuest()){
				$template = new Template();
				if(Mobile::isMobileDevice()){
					$template->load("plugin_login_form.mobile");
				}else{
					$template->load("plugin_login_form");
				}
				$referer = $_SERVER["HTTP_REFERER"];
				$hostlength = strlen(Settings::getInstance()->get("host"));
				$template->assign_var("referer",$_SERVER["HTTP_REFERER"]);
				$template->show_if("FAILED",$this->failed);
				$template->show_if("HARDFAILED",false);
				if(isset($_POST['name'])){
					if(Plugin_Profile_Manager::getLoginCounter($_POST['name']) >= 3){
						$template->show_if("HARDFAILED",true);
					}
				}
				$template->output();
			}
		}
		
		public function getHeader(){
		}
		
		/**
		 * Force url entering
		 * @return string
		 */
		public function getEditableCode(){
			if(isset($_POST['plugin_login_redirect_url']) && is_string($_POST['plugin_login_redirect_url'])){
				$sql = "REPLACE INTO {'dbprefix'}plugin_login "
					  ."SET redirect_url = '".DataBase::Current()->EscapeString($_POST['plugin_login_redirect_url'])."', "
						  ."pageid = '".DataBase::Current()->EscapeString($this->page->id)."'";
				DataBase::Current()->Execute($sql);
			}
			$template = new Template();
			$template->load("plugin_login_editor");
			$template->assign_var("URL", htmlentities($this->redirect_url()));
			return $template->getCode();
		}
		
		public function save(Page $newPage, Page $oldPage){
		}
		
		/**
		 * Url the user will be redirected to
		 * @return string
		 */
		public function redirect_url(){
			if(is_null($this->redirect_url)){
				$sql = "SELECT redirect_url "
					  ."FROM {'dbprefix'}plugin_login "
					  ."WHERE pageid = '".DataBase::Current()->EscapeString($this->page->id)."'";
				$this->redirect_url = DataBase::Current()->ReadField($sql);
			}
			
			return $this->redirect_url;
		}
		
		public function ExecuteHttpHeader(){
			if(isset($_POST['name']) && isset($_POST['password'])){
				$_POST['name'] = DataBase::Current()->EscapeString($_POST['name']);
				if(User::Current()->login($_POST['name'],$_POST['password'])){
					if(Plugin_Profile_Manager::getLoginCounter($_POST['name']) > 0){
						Plugin_Profile_Manager::resetLoginCounter($_POST['name']);
					}
					//Login succeeded, redirect
					if($_POST['referer'] == 'getref'){
						$_POST['referer'] = $_SERVER["HTTP_REFERER"];
					}
					if(substr($_POST['referer'],0,22) == Settings::getInstance()->get("host")){
						$_POST['referer'] = $_POST['referer'];
					}else{
						$_POST['referer'] = Settings::getInstance()->get("host").'mein-profil.html';
					}
					if($_POST['referer'] == Settings::getInstance()->get("host").'passwort-vergessen.html'){
						$_POST['referer'] = Settings::getInstance()->get("host").'mein-profil.html';
					}
					if(count($_GET) == 0){
						$_POST['referer'] .= '';
					}elseif(count($_GET) > 0){
						$_POST['referer'] .= '';
					}
					if(isset($_POST['loginsave'])){
						$autologin = md5(Settings::getValue("salt").$_POST['name']);
						DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET autologin = '".$autologin."' WHERE name = '".$_POST['name']."' LIMIT 1");
						setcookie('smal',$autologin,time()+(3600*24*30));
					}
					header("Location: ".$_POST['referer']);
					exit;
				}
				elseif(User::Current()->login(DataBase::Current()->ReadField("SELECT name FROM {'dbprefix'}user WHERE email = '".$_POST['name']."'"),$_POST['password'])){
					if(Plugin_Profile_Manager::getLoginCounter(DataBase::Current()->ReadField("SELECT name FROM {'dbprefix'}user WHERE email = '".$_POST['name']."'")) > 0){
						Plugin_Profile_Manager::resetLoginCounter(DataBase::Current()->ReadField("SELECT name FROM {'dbprefix'}user WHERE email = '".$_POST['name']."'"));
					}
				}else{
					$this->failed = true;
				}
			}
			if(!User::Current()->isGuest()){
				//Already logged in
				header("Location: ".$this->redirect_url());
				exit;
			}
		}
	}
?>
