<?PHP
/*
 * adminpromo.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_Events_AdminPromo extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			$template->load('plugin_events_adminpromo');
			$template->show_if('edit',false);
			$template->show_if('saveok',false);
			if(isset($_POST['editpromo']) && isset($_GET['edit'])){
				Plugin_Events_Manager::updatePromo($_GET['edit'],$_POST);
				$template->show_if('saveok',true);
			}
			if(isset($_POST['newpromo'])){
				Plugin_Events_Manager::createPromo($_POST['name']);
			}
			if(isset($_GET['del'])){
				Plugin_Events_Manager::deletePromo($_GET['del']);
			}
			if(isset($_GET['edit'])){
				$template->show_if('edit',true);
				$promo = Plugin_Events_Manager::getPromoMeal($_GET['edit']);
				$template->assign_var('restname',htmlentities($promo->name));
				$template->assign_var('promotion',htmlentities($promo->promotion));
				$template->assign_var('mealtitle',htmlentities($promo->mealtitle));
				$template->assign_var('desc',$promo->description);
				$template->assign_var('street',htmlentities($promo->street));
				$template->assign_var('strnr',htmlentities($promo->strnr));
				$template->assign_var('plz',htmlentities($promo->plz));
				$template->assign_var('ort',htmlentities($promo->ort));
				$template->assign_var('kanton',htmlentities($promo->kanton));
				$template->assign_var('weblink',htmlentities($promo->weblink));
				$template->assign_var('restmail',htmlentities($promo->restmail));
				$template->assign_var('ov'.htmlentities($promo->ov),'selected="selected"');
				$template->assign_var('parking'.htmlentities($promo->parking),'selected="selected"');
				$template->assign_var('fleisch'.htmlentities($promo->fleisch),'selected="selected"');
				$template->assign_var('vegetarisch'.htmlentities($promo->vegetarisch),'selected="selected"');
				$template->assign_var('vegan'.htmlentities($promo->vegan),'selected="selected"');
				$template->assign_var('rollchair'.htmlentities($promo->rollchair),'selected="selected"');
				$template->assign_var('createnotice',$promo->createnotice);
				$template->assign_var('speisekarte',htmlentities($promo->speisekarte));
				$template->assign_var('img',htmlentities($promo->img));
				$template->assign_var('ov0','');
				$template->assign_var('ov1','');
				$template->assign_var('ov2','');
				$template->assign_var('parking0','');
				$template->assign_var('parking1','');
				$template->assign_var('parking2','');
				$template->assign_var('parking3','');
				$template->assign_var('fleisch0','');
				$template->assign_var('fleisch1','');
				$template->assign_var('fleisch2','');
				$template->assign_var('vegetarisch0','');
				$template->assign_var('vegetarisch1','');
				$template->assign_var('vegetarisch2','');
				$template->assign_var('vegan0','');
				$template->assign_var('vegan1','');
				$template->assign_var('vegan2','');
				$template->assign_var('rollchair0','');
				$template->assign_var('rollchair1','');
			}
			foreach(Plugin_Events_Manager::getAllPromoMeals() as $promo){
				$index = $template->add_loop_item('promo');
				$template->assign_loop_var("promo",$index,"id",htmlentities($promo->id));
				$template->assign_loop_var("promo",$index,"name",htmlentities($promo->name));
			}
			echo($template->getCode());
		}

		function getHeader(){
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
