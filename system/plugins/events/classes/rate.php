<?PHP
/*
 * rate.php
 *
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
	class Plugin_Events_Rate extends Editor{

		/**
		 *
		 * @param Page $page
		 */
		function __construct(Page $page){
			$this->page = $page;
			$eventdata = Plugin_Events_Manager::loadEvent($_GET['eventid']);
			$userdata = Plugin_Profile_Manager::getUserDataPerId($_GET['rateid']);
			$page->title = Language::DirectTranslate('plugin_events_rate3').' '.$userdata->name.' '.utf8_encode(Language::DirectTranslate('plugin_events_rate4')).' '.$eventdata->eventname;
		}
		public function display(){
			$template = new Template();
			$template->load('plugin_events_rate');
			$template->show_if('showform',true);
			$template->show_if('showmsg',false);
			if($_GET['rateid'] != User::Current()->id && Plugin_Events_Manager::eventHasRating($_GET['eventid'],User::Current()->id,$_GET['rateid']) == 0 && Plugin_Events_Manager::eventExist($_GET['eventid'])){
				$eventdata = Plugin_Events_Manager::loadEvent($_GET['eventid']);
				$counter = 0;
				$selfsubscribe = Plugin_Events_Manager::hasSubscribe($_GET['eventid'],User::Current()->id);
				if($selfsubscribe == 1){
					$counter++;
				}
				if($eventdata->userid == User::Current()->id){
					$counter++;
				}
				if($counter > 0){
					$userdata = Plugin_Profile_Manager::getUserDataPerId($_GET['rateid']);
					$template->assign_var('username',$userdata->name);
					$template->assign_var('rateid',htmlentities($_GET['rateid']));
					$template->assign_var('eventname',htmlentities($eventdata->eventname));
					if($_GET['rateid'] == $eventdata->userid){
						$template->assign_var('ratetype',Language::DirectTranslate('plugin_events_rate1'));
					}else{
						$template->assign_var('ratetype',Language::DirectTranslate('plugin_events_rate2'));
					}
					if(isset($_POST['plugin_events_rate']) && !empty($_POST['text'])){
						if(isset($_POST['newfriend']) == 1){
							Plugin_Profile_Manager::addFriend($_GET['rateid']);
						}
						Plugin_Events_Manager::saveRate($_GET['eventid'],User::Current()->id,$_GET['rateid'],$_POST);
						Plugin_Profile_Manager::updatePunkte(3);
						if(Plugin_Profile_Manager::getUserSetting('rate',$_GET['rateid']) == 1){
							$mailtext = 'Liebe*r '.ucfirst($userdata->firstname).'<br><br>Gerne möchten wir dich darüber informieren, dass du eine neue Bewertung von dem User <a href="'.Settings::getInstance()->get("host").'mein-profil.html?profile='.User::Current()->name.'">'.User::Current()->name.'</a> erhalten hast.<br><br>Du kannst die Bewertung <a href="'.Settings::getInstance()->get("host").'bewertungen.html">hier</a> anschauen und kommentieren.<br><br>Alles was du zum Bewertungssystem wissen musst, erfährst du unter <a href="'.Settings::getInstance()->get("host").'FAQ/so-funktioniert-das-bewertungssystem.html">So funktioniert unser Bewertungssystem</a>.<br><br>Falls du der Meinung bist, dass dieser User die Funktion zur Bewertungs-Abgabe missbräuchlich verwendet, melde uns das bitte über die entsprechende <a href="'.Settings::getInstance()->get("host").'melden.html?source=mail&g=newrater_'.$userdata->id.'">Melde-Seite</a>.<br><br>Wir wünschen dir weiterhin gute Interaktionen in unserer Community!<br>Social Meal<br><br>P.S.: Diesen E-Mail-Typ kannst du in den <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abbestellen, falls er dir überflüssig erscheint.';
							$subject = utf8_decode('Du hast eine neue Bewertung auf Social Meal erhalten');
							$h2t = new Plugin_Mailer_html2text($mailtext);
							$mail = new Plugin_PHPMailer_PHPMailer;
							$mail->IsSMTP();
							$mail->AddAddress($userdata->email);
							$mail->WordWrap = 50;
							$mail->IsHTML(true);
							$mail->Subject = $subject;
							$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
							$mail->AltBody = $h2t->get_text();
							if(!$mail->Send()){
								echo('Mail Error:'.$mail->ErrorInfo);
							}
						}
						$template->show_if('showform',false);
						$template->show_if('showmsg',true);
					}
					if(Plugin_Profile_Manager::isFriend($_GET['rateid'])){
						$template->show_if('newfriend',false);
					}else{
						$template->show_if('newfriend',true);
					}
					echo($template->getCode());
				}
			}
		}

		function getHeader(){
			echo('<script type="text/javascript" src="/system/plugins/events/js/rate.js"></script>');
		}

		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}

		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
