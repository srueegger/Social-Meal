<?php
/*
 * edit.php
 *
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
	class Plugin_Events_Edit extends Editor{

		/**
		 *
		 * @param Page $page
		 */
		function __construct(Page $page){
			$this->page = $page;
			if(isset($_GET['event'])){
				if(Plugin_Events_Manager::eventExist($_GET['event']) != 1){
					$page->title = Language::DirectTranslate('plugin_events_nomealfound');
				}else{
					$eventdata = Plugin_Events_Manager::loadEvent($_GET['event']);
					$page->title = $eventdata->eventname;
				}
			}
		}
		public function display(){
			$template = new Template();
			$event = Plugin_Events_Manager::loadEvent($_GET['event']);
			$eventtype = $event->eventtype;
			$promo = $event->promo;
			if(Plugin_Events_Manager::countSubscribers($event->id) > 0){
				$template->load('plugin_events_noedit');
				echo($template->getCode());
				exit;
			}
			if($event->afterasmail == 1){
				$template->load('plugin_events_noeditasdate');
				echo($template->getCode());
				exit;
			}
			$falseuser = 0;
			if($event->eventtype == 3 && $event->orgid > 0 && $event->userid != User::Current()->id){
				if(Plugin_Profile_Manager::canEditOrgProfile($event->orgid)){
					User::Current()->id = $event->userid;
					$falseuser = 1;
				}
			}
			if($event->userid == User::Current()->id){
			unset($event);
			if($eventtype != 3 && $promo == 0){
				if(Mobile::isMobileDevice()){
					$template->load('plugin_events_editpage.mobile');
				}else{
					$template->load('plugin_events_editpage');
				}
				$template->assign_var('eventid',htmlentities($_GET['event']));
				$template->show_if('showmsg',false);
				$template->show_if('imgmsg',false);
				$template->show_if('mitnehmen',false);
				$template->show_if('kochen',false);
				$template->show_if('norestaurant',true);
				$template->show_if('restaurant',false);
				$template->show_if('options',false);
				if($eventtype == 0){
					$template->show_if('eventtype0',true);
				}else{
					$template->show_if('eventtype0',false);
				}
				if($eventtype == 2){
					$template->show_if('kochen',true);
				}
				if(isset($_POST['plugin_editevent_submit'])){
					$event = Plugin_Events_Manager::loadEvent($_GET['event']);
					if($event->eventtype == 0 or $event->eventtype == 2){
						$userdata = Plugin_Profile_Manager::getUserDataPerId(User::Current()->id);
						$_POST['street'] = ucfirst($userdata->strasse);
						$_POST['strnr'] = $userdata->strnr;
						$_POST['plz'] = $userdata->plz;
						$_POST['ort'] = ucfirst($userdata->ort);
						$_POST['kanton'] = $userdata->kanton;
					}
					Plugin_Events_Manager::updateEvent($event->id,$_POST);
					if(isset($_FILES['files'])){
						Plugin_Events_Manager::uploadFiles($event->id,$_FILES);
					}
					unset($event);
					$event = Plugin_Events_Manager::loadEvent($_GET['event']);
					if($event->eventtype == 1){
						if($event->eventname != '' && $event->beschreibung != '' && $event->maxguest > 0 && $event->dauer != '' && $event->ort != '' && $event->strasse != '' && $event->strnr != '' && $event->plz != '' && $event->kanton != ''){
							Plugin_Events_Manager::activateEvent($event->id);
							Plugin_Events_Manager::setAsRead($event->id);
							if($event->friendmail == 0){
								Plugin_Profile_Manager::updatePunkte(15);
								$url = Settings::getInstance()->get("host").'event-details.html?event='.$event->id;
								if(Plugin_Profile_Manager::getUserSetting('socialmedia',User::Current()->id) == 1){
									if(Plugin_Events_Manager::countImagesPerEvent($event->id) > 0){
										$fbimage = 'content/uploads/eat/'.Plugin_Events_Manager::getFirstImageFromEvent($event->id);
									}else{
										$fbimage = 'system/skins/socialmeal/images/smneulogo.png';
									}
									Plugin_Events_Manager::FBPublishEvent($event->eventname,$url,Plugin_Events_Manager::formatDate($event->eventdate),Settings::getInstance()->get("host").$fbimage);
									Plugin_Events_Manager::tweetEvent($event->eventname,$url,Plugin_Events_Manager::formatDate($event->eventdate),$event->eventtype);
								}
								Plugin_Events_Manager::infoFriends($event->id);
								if(Plugin_Profile_Manager::getUserSetting('newevent',User::Current()->id) == 1){
									$maileventlink = Settings::getInstance()->get("host").'event-details.html?event='.$event->id;
									if($event->eventtype == 0 or $event->eventtype == 2){
										$subject = utf8_decode('Du hast erfolgreich ein Privat-Meal auf Social Meal erstellt');
										$body = 'Liebe*r '.ucfirst(Plugin_Profile_Manager::getUserDataPerId(User::Current()->id)->firstname).'<br><br>Vielen Dank, dass du ein Privat-Meal auf Social Meal erstellt hast, dieses wurde soeben erfolgreich veröffentlicht: <a href="'.$maileventlink.'">'.$event->eventname.'</a>. Wir möchten dich darauf hinweisen, dass dein Angebot verbindlich ist, und erneut auf unsere <a href="'.Settings::getInstance()->get("host").'agb.html">AGBs</a> verweisen.<br><br>Dein Meal wurde bereits in den sozialen Medien veröffentlicht (wenn du dies in deinen <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> nicht abgelehnt hast), teile es doch gleich auf Facebook: <a href="http://www.facebook.com/sharer.php?u='.$maileventlink.'">Dein Meal auf Facebook</a>, oder retweete es auf Twitter: <a href="http://twitter.com/home?status=Mein Privat-Meal über @infosocialmeal : '.$maileventlink.'">Dein Meal auf Twitter</a>.<br><br>Wir werden dich über jede Anmeldung zu deinem Meal informieren (auch das kannst du in deinen <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abstellen). Beachte, dass User definitiv auf deiner Gästeliste stehen, sobald sie sich für dein Meal anmelden. Nach Ablauf der Anmeldefrist erhältst du die definitive Gäste-Liste inklusive den allenfalls gewählten Anmeldeoptionen und den Kontaktinformationen.<br><br>Wenn du noch etwas verbessern willst, kannst du dein <a href="'.Settings::getInstance()->get("host").'event-bearbeiten.html?event='.$event->id.'">Meal bearbeiten</a>. Tipps dazu findest du unter <a href="'.Settings::getInstance()->get("host").'FAQ/erstellung-von-privat-events.html">Erstellung von Privat-Meals</a>. Vielleicht magst du dir auch schon einmal durchlesen <a href="'.Settings::getInstance()->get("host").'FAQ/das-solltest-du-als-gastgeber-beachten.html">Was du als Gastgeber*in beachten solltest</a>.<br><br>Unter Angabe einer Begründung kannst du jemanden von deiner Gästeliste entfernen oder das gesamte <a href="'.Settings::getInstance()->get("host").'event-loeschen.html?event='.$event->id.'">Meal absagen</a>, solange die Anmeldefrist noch nicht abgelaufen ist. Nach Ablauf der Anmeldefrist ist eine Meal-Absage über Social Meal nicht mehr möglich. In Notsituationen empfehlen wir dir die direkte Kontaktaufnahme mit deinen Gästen.<br><br>Wir wünschen dir viel Erfolg mit deinem Meal!<br>Social Meal<br><br>P.S.: Diesen E-Mail-Typ kannst du in den <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abbestellen, falls er dir überflüssig erscheint.';
										$h2t = new Plugin_Mailer_html2text($body);
									}elseif($event->eventtype == 1){
										$subject = utf8_decode('Du hast erfolgreich ein Restaurant-Meal auf Social Meal erstellt');
										$body = 'Liebe*r '.ucfirst(Plugin_Profile_Manager::getUserDataPerId(User::Current()->id)->firstname).'<br><br>Vielen Dank, dass du ein Restaurant-Meal auf Social Meal erstellt hast, dieses wurde soeben erfolgreich veröffentlicht: <a href="'.$maileventlink.'">'.$event->eventname.'</a>. Wir möchten dich darauf hinweisen, dass dein Angebot verbindlich ist, und erneut auf unsere <a href="'.Settings::getInstance()->get("host").'agb.html">AGBs</a> verweisen.<br><br>Dein Meal wurde bereits in den sozialen Medien veröffentlicht (wenn du dies in deinen <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> nicht abgelehnt hast), teile es doch gleich auf Facebook: <a href="http://www.facebook.com/sharer.php?u='.$maileventlink.'">Dein Meal auf Facebook</a>, oder retweete es auf Twitter: <a href="http://twitter.com/home?status=Mein Privat-Meal über @infosocialmeal : '.$maileventlink.'">Dein Meal auf Twitter</a>.<br><br>Wir werden dich über jede Anmeldung zu deinem Meal informieren (auch das kannst du in deinen <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abstellen). Beachte, dass User definitiv auf deiner Gästeliste stehen, sobald sie sich für dein Meal anmelden. Nach Ablauf der Anmeldefrist erhältst du die definitive Gäste-Liste und den Kontaktinformationen.<br><br>Wenn du noch etwas verbessern willst, kannst du dein <a href="'.Settings::getInstance()->get("host").'event-bearbeiten.html?event='.$event->id.'">Meal bearbeiten</a>. Tipps dazu findest du unter <a href="'.Settings::getInstance()->get("host").'FAQ/erstellung-von-restaurant-events.html">Erstellung von Restaurant-Meals</a>. Vielleicht magst du dir auch schon einmal durchlesen <a href="'.Settings::getInstance()->get("host").'FAQ/das-solltest-du-als-gastgeber-beachten.html">Was du als Gastgeber*in beachten solltest</a>.<br><br>Unter Angabe einer Begründung kannst du jemanden von deiner Gästeliste entfernen oder das gesamte <a href="'.Settings::getInstance()->get("host").'event-loeschen.html?event='.$event->id.'">Meal absagen</a>, solange die Anmeldefrist noch nicht abgelaufen ist. Nach Ablauf der Anmeldefrist ist eine Meal-Absage über Social Meal nicht mehr möglich. In Notsituationen empfehlen wir dir die direkte Kontaktaufnahme mit deinen Gästen.<br><br>Wir wünschen dir viel Erfolg mit deinem Meal!<br>Social Meal<br><br>P.S.: Diesen E-Mail-Typ kannst du in den <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abbestellen, falls er dir überflüssig erscheint.';
										$h2t = new Plugin_Mailer_html2text($body);
									}
									$mail = new Plugin_PHPMailer_PHPMailer;
									$mail->IsSMTP();
									$mail->AddAddress(User::Current()->email);
									$mail->WordWrap = 50;
									$mail->IsHTML(true);
									$mail->Subject = $subject;
									$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$body);
									$mail->AltBody = $h2t->get_text();
									if(!$mail->Send()){
										echo('Mail Error:'.$mail->ErrorInfo);
									}
								}
							}
						}
					}else{
						if($event->eventname != '' && $event->beschreibung != '' && $event->drinks != '' && $event->gaenge != '' && $event->maxguest > 0 && $event->dauer != '' && $event->preis != '' && $event->ort != '' && $event->strasse != '' && $event->strnr != '' && $event->plz != '' && $event->kanton != ''){
							Plugin_Events_Manager::activateEvent($event->id);
							Plugin_Events_Manager::setAsRead($event->id);
							if($event->friendmail == 0){
								Plugin_Profile_Manager::updatePunkte(15);
								$url = Settings::getInstance()->get("host").'event-details.html?event='.$event->id;
								if(Plugin_Profile_Manager::getUserSetting('socialmedia',User::Current()->id) == 1){
									if(Plugin_Events_Manager::countImagesPerEvent($event->id) > 0){
										$fbimage = 'content/uploads/eat/'.Plugin_Events_Manager::getFirstImageFromEvent($event->id);
									}else{
										$fbimage = 'system/skins/socialmeal/images/smneulogo.png';
									}
									Plugin_Events_Manager::FBPublishEvent($event->eventname,$url,Plugin_Events_Manager::formatDate($event->eventdate),Settings::getInstance()->get("host").$fbimage);
									Plugin_Events_Manager::tweetEvent($event->eventname,$url,Plugin_Events_Manager::formatDate($event->eventdate),$event->eventtype);
								}
								Plugin_Events_Manager::infoFriends($event->id);
								if(Plugin_Profile_Manager::getUserSetting('newevent',User::Current()->id) == 1){
									$maileventlink = Settings::getInstance()->get("host").'event-details.html?event='.$event->id;
									if($event->eventtype == 0 or $event->eventtype == 2){
										$subject = utf8_decode('Du hast erfolgreich ein Privat-Meal auf Social Meal erstellt');
										$body = 'Liebe*r '.ucfirst(Plugin_Profile_Manager::getUserDataPerId(User::Current()->id)->firstname).'<br><br>Vielen Dank, dass du ein Privat-Meal auf Social Meal erstellt hast, dieses wurde soeben erfolgreich veröffentlicht: <a href="'.$maileventlink.'">'.$event->eventname.'</a>. Wir möchten dich darauf hinweisen, dass dein Angebot verbindlich ist, und erneut auf unsere <a href="'.Settings::getInstance()->get("host").'agb.html">AGBs</a> verweisen.<br><br>Dein Meal wurde bereits in den sozialen Medien veröffentlicht (wenn du dies in deinen <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> nicht abgelehnt hast), teile es doch gleich auf Facebook: <a href="http://www.facebook.com/sharer.php?u='.$maileventlink.'">Dein Meal auf Facebook</a>, oder retweete es auf Twitter: <a href="http://twitter.com/home?status=Mein Privat-Meal über @infosocialmeal : '.$maileventlink.'">Dein Meal auf Twitter</a>.<br><br>Wir werden dich über jede Anmeldung zu deinem Meal informieren (auch das kannst du in deinen <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abstellen). Beachte, dass User definitiv auf deiner Gästeliste stehen, sobald sie sich für dein Meal anmelden. Nach Ablauf der Anmeldefrist erhältst du die definitive Gäste-Liste inklusive den allenfalls gewählten Anmeldeoptionen und den Kontaktinformationen.<br><br>Wenn du noch etwas verbessern willst, kannst du dein <a href="'.Settings::getInstance()->get("host").'event-bearbeiten.html?event='.$event->id.'">Meal bearbeiten</a>. Tipps dazu findest du unter <a href="'.Settings::getInstance()->get("host").'FAQ/erstellung-von-privat-events.html">Erstellung von Privat-Meals</a>. Vielleicht magst du dir auch schon einmal durchlesen <a href="'.Settings::getInstance()->get("host").'FAQ/das-solltest-du-als-gastgeber-beachten.html">Was du als Gastgeber*in beachten solltest</a>.<br><br>Unter Angabe einer Begründung kannst du jemanden von deiner Gästeliste entfernen oder das gesamte <a href="'.Settings::getInstance()->get("host").'event-loeschen.html?event='.$event->id.'">Meal absagen</a>, solange die Anmeldefrist noch nicht abgelaufen ist. Nach Ablauf der Anmeldefrist ist eine Meal-Absage über Social Meal nicht mehr möglich. In Notsituationen empfehlen wir dir die direkte Kontaktaufnahme mit deinen Gästen.<br><br>Wir wünschen dir viel Erfolg mit deinem Meal!<br>Social Meal<br><br>P.S.: Diesen E-Mail-Typ kannst du in den <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abbestellen, falls er dir überflüssig erscheint.';
										$h2t = new Plugin_Mailer_html2text($body);
										$mail = new Plugin_PHPMailer_PHPMailer;
										$mail->IsSMTP();
										$mail->AddAddress(User::Current()->email);
										$mail->WordWrap = 50;
										$mail->IsHTML(true);
										$mail->Subject = $subject;
										$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$body);
										$mail->AltBody = $h2t->get_text();
										if(!$mail->Send()){
											echo('Mail Error:'.$mail->ErrorInfo);
										}
									}
								}
							}
						}
					}
					$template->show_if('showmsg',true);
				}
				if(isset($_GET['event'])){
					$event = Plugin_Events_Manager::loadEvent($_GET['event']);
					if($event->eventtype == 1){
						$template->show_if('norestaurant',false);
						$template->show_if('restaurant',true);
						$template->assign_var('specialpreis'.htmlentities($event->specialprice),'selected="selected"');
					}
					if($event->userid == User::Current()->id){
						if(!empty($_GET['delimg'])){
							Plugin_Events_Manager::deleteImage($_GET['delimg']);
							$template->show_if('imgmsg',true);
						}
						if(Plugin_Events_Manager::countImagesPerEvent($event->id) >= 5){
							$template->show_if('imageform',false);
						}else{
							$template->show_if('imageform',true);
						}
						if($event->eventtype == 0 or $event->eventtype == 2){
							$template->show_if('showadresse',false);
							$template->show_if('options',true);
						}else{
							$template->show_if('showadresse',true);
							$template->assign_var('weblink',htmlentities($event->weblink));
						}
						if($event->eventtype == 2){
							$template->show_if('mitnehmen',true);
						}
						$template->show_if('showimages',false);
						$template->assign_var('eventname',htmlentities($event->eventname));
						$template->assign_var('street',htmlentities(ucfirst($event->strasse)));
						$template->assign_var('strnr',htmlentities($event->strnr));
						$template->assign_var('plz',htmlentities($event->plz));
						$template->assign_var('ort',htmlentities(ucfirst($event->ort)));
						if($event->eventdate == '0000-00-00 00:00:00'){
							$template->assign_var('eventdate',date('Y-m-d H:i'));
						}else{
							$template->assign_var('eventdate',substr($event->eventdate,0,-3));
						}
						if($event->anmeldeschluss == '0000-00-00 00:00:00'){
							$template->assign_var('asdate',date('Y-m-d H:i'));
						}else{
							$template->assign_var('asdate',substr($event->anmeldeschluss,0,-3));
						}
						$makedatehour = mktime(date("H")+1, date("i"), date("s"), date("m") , date("d"), date("Y"));
						$template->assign_var('datehour',date('Y-m-d H:i',$makedatehour));
						$template->assign_var('asdate',substr($event->anmeldeschluss,0,-3));
						$template->assign_var('selected'.htmlentities($event->kanton),'selected="selected"');
						$template->assign_var('eventpreis',htmlentities($event->preis));
						$template->assign_var('bnb',htmlentities($event->bnb));
						$template->assign_var('rest',htmlentities($event->rest));
						$template->assign_var('eventgange',htmlentities($event->gaenge));
						$template->assign_var('maxguest',htmlentities($event->maxguest));
						$template->assign_var('smoke'.htmlentities($event->rauchen),'selected="selected"');
						$template->assign_var('rollchair'.htmlentities($event->rollchair),'selected="selected"');
						$template->assign_var('abouteat',htmlentities($event->beschreibung));
						$template->assign_var('fleisch'.htmlentities($event->fleisch),'checked="checked"');
						$template->assign_var('fisch'.htmlentities($event->fisch),'checked="checked"');
						$template->assign_var('vegi'.htmlentities($event->vegetarisch),'checked="checked"');
						$template->assign_var('seafood'.htmlentities($event->seafood),'checked="checked"');
						$template->assign_var('vegan'.htmlentities($event->vegan),'checked="checked"');
						$template->assign_var('saisonal'.htmlentities($event->saisonal),'checked="checked"');
						$template->assign_var('bio'.htmlentities($event->bio),'checked="checked"');
						$template->assign_var('regional'.htmlentities($event->regional),'checked="checked"');
						$template->assign_var('lactose'.htmlentities($event->lactose),'checked="checked"');
						$template->assign_var('nusse'.htmlentities($event->nusse),'checked="checked"');
						$template->assign_var('gluten'.htmlentities($event->glutenfrei),'checked="checked"');
						$template->assign_var('alkohol'.htmlentities($event->alkohol),'checked="checked"');
						$template->assign_var('scharf'.htmlentities($event->scharf),'checked="checked"');
						$template->assign_var('koscher'.htmlentities($event->koscher),'checked="checked"');
						$template->assign_var('halal'.htmlentities($event->halal),'checked="checked"');
						$template->assign_var('hefe'.htmlentities($event->hefe),'checked="checked"');
						$template->assign_var('ov'.htmlentities($event->ov),'selected="selected"');
						$template->assign_var('parking'.htmlentities($event->parking),'selected="selected"');
						$template->assign_var('drink'.htmlentities($event->drinks),'selected="selected"');
						$template->assign_var('dauer'.htmlentities($event->dauer),'selected="selected"');
						$template->assign_var('option1',htmlentities($event->option1));
						$template->assign_var('option2',htmlentities($event->option2));
						$template->assign_var('option3',htmlentities($event->option3));
						$template->assign_var('mitnehmen',htmlentities($event->mitnehmen));
						if($event->symbolimg == 1){
							$template->assign_var('symbolimg','checked="checked"');
						}else{
							$template->assign_var('symbolimg','');
						}
						if(Plugin_Events_Manager::countImagesPerEvent($event->id) != 0){
							$template->show_if('showimages',true);
							foreach(Plugin_Events_Manager::getImagesPerEvent($event->id) as $image){
								$index = $template->add_loop_item('images');
								$template->assign_loop_var("images",$index,"imgid",htmlentities($image->id));
								$template->assign_loop_var("images",$index,"imgurl",Settings::getInstance()->get("host").'content/uploads/eat/'.htmlentities($image->filename));
								$template->assign_loop_var("images",$index,"eventid",htmlentities($image->eventid));
							}
						}
						$template->assign_var('selectedbs','');
						$template->assign_var('selectedbl','');
						$template->assign_var('selectedag','');
						$template->assign_var('selectedso','');
						$template->assign_var('selectedju','');
						$template->assign_var('selectedar','');
						$template->assign_var('selectedai','');
						$template->assign_var('selectedbe','');
						$template->assign_var('selectedfr','');
						$template->assign_var('selectedge','');
						$template->assign_var('selectedgl','');
						$template->assign_var('selectedgr','');
						$template->assign_var('selectedlu','');
						$template->assign_var('selectedne','');
						$template->assign_var('selectednw','');
						$template->assign_var('selectedow','');
						$template->assign_var('selectedsh','');
						$template->assign_var('selectedsz','');
						$template->assign_var('selectedsg','');
						$template->assign_var('selectedti','');
						$template->assign_var('selectedtg','');
						$template->assign_var('selectedur','');
						$template->assign_var('selectedvd','');
						$template->assign_var('selectedvs','');
						$template->assign_var('selectedzg','');
						$template->assign_var('selectedzh','');
						$template->assign_var('smoke0','');
						$template->assign_var('smoke1','');
						$template->assign_var('smoke2','');
						$template->assign_var('fleisch0','');
						$template->assign_var('fleisch1','');
						$template->assign_var('fleisch2','');
						$template->assign_var('fisch0','');
						$template->assign_var('fisch1','');
						$template->assign_var('fisch2','');
						$template->assign_var('seafood0','');
						$template->assign_var('seafood1','');
						$template->assign_var('seafood2','');
						$template->assign_var('vegi0','');
						$template->assign_var('vegi1','');
						$template->assign_var('vegi2','');
						$template->assign_var('vegan0','');
						$template->assign_var('vegan1','');
						$template->assign_var('vegan2','');
						$template->assign_var('saisonal0','');
						$template->assign_var('saisonal1','');
						$template->assign_var('saisonal2','');
						$template->assign_var('bio0','');
						$template->assign_var('bio1','');
						$template->assign_var('bio2','');
						$template->assign_var('regional0','');
						$template->assign_var('regional1','');
						$template->assign_var('regional2','');
						$template->assign_var('lactose0','');
						$template->assign_var('lactose1','');
						$template->assign_var('lactose2','');
						$template->assign_var('nusse0','');
						$template->assign_var('nusse1','');
						$template->assign_var('nusse2','');
						$template->assign_var('gluten0','');
						$template->assign_var('gluten1','');
						$template->assign_var('gluten2','');
						$template->assign_var('alkohol0','');
						$template->assign_var('alkohol1','');
						$template->assign_var('alkohol2','');
						$template->assign_var('scharf0','');
						$template->assign_var('scharf1','');
						$template->assign_var('scharf2','');
						$template->assign_var('hefe0','');
						$template->assign_var('hefe1','');
						$template->assign_var('hefe2','');
						$template->assign_var('koscher0','');
						$template->assign_var('koscher1','');
						$template->assign_var('koscher2','');
						$template->assign_var('halal0','');
						$template->assign_var('halal1','');
						$template->assign_var('halal2','');
						$template->assign_var('ov0','');
						$template->assign_var('ov1','');
						$template->assign_var('ov2','');
						$template->assign_var('parking0','');
						$template->assign_var('parking1','');
						$template->assign_var('parking2','');
						$template->assign_var('parking3','');
						$template->assign_var('rollchair0','');
						$template->assign_var('rollchair1','');
						$template->assign_var('dauer1','');
						$template->assign_var('dauer2','');
						$template->assign_var('dauer3','');
						$template->assign_var('dauer4','');
						$template->assign_var('drink0','');
						$template->assign_var('drink1','');
						$template->assign_var('drink3','');
						$template->assign_var('specialpreis4','');
						$template->assign_var('specialpreis5','');
						$template->assign_var('specialpreis6','');
					}else{
						$template->load('plugin_events_noevent');
					}
				}else{
					$template->load('plugin_events_noevent');
				}
				}elseif($eventtype == 3 && $promo == 0){
					//Spezial Event
					if(Mobile::isMobileDevice()){
						$template->load('plugin_events_editpagespecial.mobile');
					}else{
						$template->load('plugin_events_editpagespecial');
					}
					$template->show_if('showmsg',false);
					$template->show_if('imgmsg',false);
					$template->show_if('noright',false);
					$template->show_if('falseuser',false);
					$template->assign_var('eventid',htmlentities($_GET['event']));
					if(isset($_POST['plugin_editevent_submit'])){
						//Event Speichern
						if($_POST['asdate'] == ''){
							$_POST['asdate'] = $_POST['evdate'];
						}
						if($_POST['specialpreis'] != 0){
							$_POST['preis'] = 0;
						}
						if(isset($_POST['nolimit'])){
							$_POST['maxguest'] = 0;
						}
						Plugin_Events_Manager::updateEvent($_GET['event'],$_POST);
						if(isset($_FILES['files'])){
							Plugin_Events_Manager::uploadFiles($_GET['event'],$_FILES);
						}
						$event = Plugin_Events_Manager::loadEvent($_GET['event']);
						if($event->eventname != '' && $event->beschreibung != '' && $event->ort != '' && $event->strasse != '' && $event->strnr != '' && $event->plz != '' && $event->kanton != ''){
							Plugin_Events_Manager::activateEvent($event->id);
							Plugin_Events_Manager::setAsRead($event->id);
							if($event->friendmail == 0){
								Plugin_Profile_Manager::updatePunkte(10);
								Plugin_Events_Manager::infoFriends($event->id);
								$url = Settings::getInstance()->get("host").'event-details.html?event='.$event->id;
								if(Plugin_Profile_Manager::getUserSetting('socialmedia',User::Current()->id) == 1){
									if(Plugin_Events_Manager::countImagesPerEvent($event->id) > 0){
										$fbimage = 'content/uploads/eat/'.Plugin_Events_Manager::getFirstImageFromEvent($event->id);
									}else{
										$fbimage = 'system/skins/socialmeal/images/smneulogo.png';
									}
									Plugin_Events_Manager::FBPublishEvent($event->eventname,$url,Plugin_Events_Manager::formatDate($event->eventdate),Settings::getInstance()->get("host").$fbimage);
									Plugin_Events_Manager::tweetEvent($event->eventname,$url,Plugin_Events_Manager::formatDate($event->eventdate),$event->eventtype);
								}
								if(Plugin_Profile_Manager::getUserSetting('newevent',User::Current()->id) == 1){
									$maileventlink = Settings::getInstance()->get("host").'event-details.html?event='.$event->id;
									$subject = utf8_decode('Du hast erfolgreich ein Spezial-Meal auf Social Meal erstellt');
									$body = 'Liebe*r '.ucfirst(Plugin_Profile_Manager::getUserDataPerId(User::Current()->id)->firstname).'<br><br>Vielen Dank, dass du ein Spezial-Meal auf Social Meal erstellt hast, dieses wurde soeben erfolgreich veröffentlicht: <a href="'.$maileventlink.'">'.$event->eventname.'</a>. Wir möchten dich darauf hinweisen, dass dein Angebot verbindlich ist, und erneut auf unsere <a href="'.Settings::getInstance()->get("host").'agb.html">AGBs</a> verweisen.<br><br>Dein Meal wurde bereits in den sozialen Medien veröffentlicht (wenn du dies in deinen <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> nicht abgelehnt hast), teile es doch gleich auf Facebook: <a href="http://www.facebook.com/sharer.php?u='.$maileventlink.'">Dein Meal auf Facebook</a>, oder retweete es auf Twitter: <a href="http://twitter.com/home?status=Mein Spezial-Meal über @infosocialmeal : '.$maileventlink.'">Dein Meal auf Twitter</a>.<br><br>Wir werden dich über jede Anmeldung zu deinem Meal informieren (auch das kannst du in deinen <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abstellen). Beachte, dass User definitiv auf deiner Gästeliste stehen, sobald sie sich für dein Meal anmelden. Nach Ablauf der Anmeldefrist erhältst du die definitive Gäste-Liste und den Kontaktinformationen.<br><br>Wenn du noch etwas verbessern willst, kannst du dein <a href="'.Settings::getInstance()->get("host").'event-bearbeiten.html?event='.$event->id.'">Meal bearbeiten</a>. Tipps dazu findest du unter <a href="'.Settings::getInstance()->get("host").'FAQ/erstellung-von-spezial-events.html">Erstellung von Spezial-Meals</a>. Vielleicht magst du dir auch schon einmal durchlesen <a href="'.Settings::getInstance()->get("host").'FAQ/das-solltest-du-als-gastgeber-beachten.html">Was du als Gastgeber*in beachten solltest</a>.<br><br>Unter Angabe einer Begründung kannst du jemanden von deiner Gästeliste entfernen oder das gesamte <a href="'.Settings::getInstance()->get("host").'event-loeschen.html?event='.$event->id.'">Meal absagen</a>, solange die Anmeldefrist noch nicht abgelaufen ist. Nach Ablauf der Anmeldefrist ist eine Meal-Absage über Social Meal nicht mehr möglich. In Notsituationen empfehlen wir dir die direkte Kontaktaufnahme mit deinen Gästen.<br><br>Wir wünschen dir viel Erfolg mit deinem Meal!<br>Social Meal<br><br>P.S.: Diesen E-Mail-Typ kannst du in den <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abbestellen, falls er dir überflüssig erscheint.';
									$h2t = new Plugin_Mailer_html2text($body);
									$mail = new Plugin_PHPMailer_PHPMailer;
									$mail->IsSMTP();
									$mail->AddAddress(User::Current()->email);
									$mail->WordWrap = 50;
									$mail->IsHTML(true);
									$mail->Subject = $subject;
									$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$body);
									$mail->AltBody = $h2t->get_text();
									if(!$mail->Send()){
										echo('Mail Error:'.$mail->ErrorInfo);
									}
								}
							}
						}
						unset($event);
						$template->assign_var('eventid',htmlentities($_GET['event']));
						$template->show_if('showmsg',true);
					}
					if(!empty($_GET['delimg'])){
						Plugin_Events_Manager::deleteImage($_GET['delimg']);
						$template->show_if('imgmsg',true);
					}
					$event = Plugin_Events_Manager::loadEvent($_GET['event']);
					if($event->userid == User::Current()->id){
						$template->assign_var('eventname',$event->eventname);
						if(Plugin_Profile_Manager::countOrgAdminsFromUser() > 0){
							$template->show_if('showorg',true);
							foreach(Plugin_Profile_Manager::getOrgsByUser() as $orgadmininfo){
								$index = $template->add_loop_item("orgs");
								$orginfo = Plugin_Profile_Manager::loadOrganisation($orgadmininfo->orgid);
								$template->assign_loop_var("orgs",$index,"orgid",htmlentities($orginfo->id));
								$template->assign_loop_var("orgs",$index,"orgname",htmlentities($orginfo->orgname));
								if($orginfo->id == $event->orgid){
									$template->assign_loop_var("orgs",$index,"orgselected",'selected="selected"');
								}else{
									$template->assign_loop_var("orgs",$index,"orgselected",'');
								}
							}
						}else{
							$template->show_if('showorg',false);
						}
						if($falseuser == 1){
							$template->show_if('showorg',false);
							$template->show_if('falseuser',true);
							$template->assign_var('falseorgid',htmlentities($event->orgid));
						}
						if($event->eventdate == '0000-00-00 00:00:00'){
							$template->assign_var('eventdate',date('Y-m-d H:i'));
						}else{
							$template->assign_var('eventdate',substr($event->eventdate,0,-3));
						}
						if($event->anmeldeschluss == '0000-00-00 00:00:00' or $event->anmeldeschluss == $event->eventdate){
							$template->assign_var('asdate','');
						}else{
							$template->assign_var('asdate',substr($event->anmeldeschluss,0,-3));
						}
						$template->assign_var('username',htmlentities(User::Current()->name));
						$template->assign_var('street',htmlentities(ucfirst($event->strasse)));
						$template->assign_var('strnr',htmlentities($event->strnr));
						$template->assign_var('plz',htmlentities($event->plz));
						$template->assign_var('ort',htmlentities(ucfirst($event->ort)));
						$template->assign_var('abouteat',htmlentities($event->beschreibung));
						$template->assign_var('eventpreis',htmlentities($event->preis));
						$template->assign_var('bnb',htmlentities($event->bnb));
						$template->assign_var('ov'.htmlentities($event->ov),'selected="selected"');
						$template->assign_var('parking'.htmlentities($event->parking),'selected="selected"');
						$template->assign_var('drink'.htmlentities($event->drinks),'selected="selected"');
						$template->assign_var('dauer'.htmlentities($event->dauer),'selected="selected"');
						$template->assign_var('rollchair'.htmlentities($event->rollchair),'selected="selected"');
						$template->assign_var('selected'.htmlentities($event->kanton),'selected="selected"');
						$template->assign_var('specialpreis'.htmlentities($event->specialprice),'selected="selected"');
						$template->assign_var('specialbnb'.htmlentities($event->bnbprice),'selected="selected"');
						$makedatehour = mktime(date("H")+1, date("i"), date("s"), date("m") , date("d"), date("Y"));
						$template->assign_var('datehour',date('Y-m-d H:i',$makedatehour));
						$template->assign_var('fleisch'.htmlentities($event->fleisch),'checked="checked"');
						$template->assign_var('vegi'.htmlentities($event->vegetarisch),'checked="checked"');
						$template->assign_var('vegan'.htmlentities($event->vegan),'checked="checked"');
						$template->assign_var('weblink',htmlentities($event->weblink));
						$template->assign_var('rest',htmlentities($event->rest));
						$template->assign_var('maxguest',htmlentities($event->maxguest));
						$template->assign_var('option1',htmlentities($event->option1));
						$template->assign_var('option2',htmlentities($event->option2));
						$template->assign_var('option3',htmlentities($event->option3));
						$template->assign_var('mitnehmen',htmlentities($event->mitnehmen));
						if($event->maxguest == 0){
							$template->assign_var('nolimitchecked','checked="checked"');
						}elseif($event->maxguest > 0){
							$template->assign_var('nolimitchecked','');
						}
						if($event->openaddress == 1){
							$template->assign_var('openaddresschecked','checked="checked"');
						}else{
							$template->assign_var('openaddresschecked','');
						}
						$template->assign_var('selectedbs','');
						$template->assign_var('selectedbl','');
						$template->assign_var('selectedag','');
						$template->assign_var('selectedso','');
						$template->assign_var('selectedju','');
						$template->assign_var('selectedar','');
						$template->assign_var('selectedai','');
						$template->assign_var('selectedbe','');
						$template->assign_var('selectedfr','');
						$template->assign_var('selectedge','');
						$template->assign_var('selectedgl','');
						$template->assign_var('selectedgr','');
						$template->assign_var('selectedlu','');
						$template->assign_var('selectedne','');
						$template->assign_var('selectednw','');
						$template->assign_var('selectedow','');
						$template->assign_var('selectedsh','');
						$template->assign_var('selectedsz','');
						$template->assign_var('selectedsg','');
						$template->assign_var('selectedti','');
						$template->assign_var('selectedtg','');
						$template->assign_var('selectedur','');
						$template->assign_var('selectedvd','');
						$template->assign_var('selectedvs','');
						$template->assign_var('selectedzg','');
						$template->assign_var('selectedzh','');
						$template->assign_var('ov0','');
						$template->assign_var('ov1','');
						$template->assign_var('ov2','');
						$template->assign_var('parking0','');
						$template->assign_var('parking1','');
						$template->assign_var('parking2','');
						$template->assign_var('parking3','');
						$template->assign_var('drink0','');
						$template->assign_var('drink1','');
						$template->assign_var('drink2','');
						$template->assign_var('drink3','');
						$template->assign_var('drink99','');
						$template->assign_var('dauer0','');
						$template->assign_var('dauer1','');
						$template->assign_var('dauer2','');
						$template->assign_var('dauer3','');
						$template->assign_var('dauer4','');
						$template->assign_var('rollchair0','');
						$template->assign_var('rollchair1','');
						$template->assign_var('specialpreis0','');
						$template->assign_var('specialpreis1','');
						$template->assign_var('specialpreis2','');
						$template->assign_var('specialpreis3','');
						$template->assign_var('specialpreis4','');
						$template->assign_var('specialbnb0','');
						$template->assign_var('specialbnb1','');
						$template->assign_var('fleisch0','');
						$template->assign_var('fleisch1','');
						$template->assign_var('fleisch2','');
						$template->assign_var('vegi0','');
						$template->assign_var('vegi1','');
						$template->assign_var('vegi2','');
						$template->assign_var('vegan0','');
						$template->assign_var('vegan1','');
						$template->assign_var('vegan2','');
						if($event->orgid == 0){
							$template->assign_var('orgselected0','selected="selected"');
						}else{
							$template->assign_var('orgselected0','');
						}
						if($event->symbolimg == 1){
							$template->assign_var('symbolimg','checked="checked"');
						}else{
							$template->assign_var('symbolimg','');
						}
						if(Plugin_Events_Manager::countImagesPerEvent($event->id) != 0){
							$template->show_if('showimages',true);
							foreach(Plugin_Events_Manager::getImagesPerEvent($event->id) as $image){
								$index = $template->add_loop_item('images');
								$template->assign_loop_var("images",$index,"imgid",htmlentities($image->id));
								$template->assign_loop_var("images",$index,"imgurl",Settings::getInstance()->get("host").'content/uploads/eat/'.htmlentities($image->filename));
								$template->assign_loop_var("images",$index,"eventid",htmlentities($image->eventid));
							}
						}else{
							$template->show_if('showimages',false);
						}
						if(Plugin_Events_Manager::countImagesPerEvent($event->id) >= 5){
							$template->show_if('imageform',false);
						}else{
							$template->show_if('imageform',true);
						}
					}
				}elseif($eventtype == 1 && $promo > 0){
					//Promo-Meal bearbeiten
					if(Mobile::isMobileDevice()){
						$template->load('plugin_events_editpagepromo.mobile');
					}else{
						$template->load('plugin_events_editpagepromo');
					}
					$template->show_if('showmsg',false);
					if(isset($_POST['plugin_editevent_submit'])){
						//Event Speichern
						$event = Plugin_Events_Manager::loadEvent($_GET['event']);
						$_POST['eventname'] = $event->eventname;
						$_POST['street'] = ucfirst($event->strasse);
						$_POST['strnr'] = $event->strnr;
						$_POST['plz'] = $event->plz;
						$_POST['ort'] = ucfirst($event->ort);
						$_POST['kanton'] = $event->kanton;
						$_POST['rollchair'] = $event->rollchair;
						$_POST['ov'] = $event->ov;
						$_POST['parking'] = $event->parking;
						$_POST['vegi'] = $event->vegetarisch;
						$_POST['vegan'] = $event->vegan;
						$_POST['fleisch'] = $event->fleisch;
						$_POST['symbolimg'] = $event->symbolimg;
						$_POST['rest'] = $event->rest;
						$_POST['weblink'] = $event->weblink;
						unset($event);
						Plugin_Events_Manager::updateEvent($_GET['event'],$_POST);
						$event = Plugin_Events_Manager::loadEvent($_GET['event']);
						if($event->eventname != '' && $event->beschreibung != '' && $event->ort != '' && $event->strasse != '' && $event->strnr != '' && $event->plz != '' && $event->kanton != ''){
							Plugin_Events_Manager::activateEvent($event->id);
							Plugin_Events_Manager::setAsRead($event->id);
							if($event->friendmail == 0){
								Plugin_Events_Manager::infoFriends($event->id);
								$url = Settings::getInstance()->get("host").'event-details.html?event='.$event->id;
								if(Plugin_Profile_Manager::getUserSetting('socialmedia',User::Current()->id) == 1){
									if(Plugin_Events_Manager::countImagesPerEvent($event->id) > 0){
										$fbimage = 'content/uploads/eat/'.Plugin_Events_Manager::getFirstImageFromEvent($event->id);
									}else{
										$fbimage = 'system/skins/socialmeal/images/smneulogo.png';
									}
									Plugin_Events_Manager::FBPublishEvent($event->eventname,$url,Plugin_Events_Manager::formatDate($event->eventdate),Settings::getInstance()->get("host").$fbimage);
									Plugin_Events_Manager::tweetEvent($event->eventname,$url,Plugin_Events_Manager::formatDate($event->eventdate),$event->eventtype);
								}
								if(Plugin_Profile_Manager::getUserSetting('newevent',User::Current()->id) == 1){
									$maileventlink = Settings::getInstance()->get("host").'event-details.html?event='.$event->id;
									$subject = utf8_decode('Du hast erfolgreich ein Promo-Meal auf Social Meal erstellt');
									$body = 'Liebe*r '.ucfirst(Plugin_Profile_Manager::getUserDataPerId(User::Current()->id)->firstname).'<br><br>Vielen Dank, dass du ein Promo-Meal auf Social Meal erstellt hast, dieses wurde soeben erfolgreich veröffentlicht: <a href="'.$maileventlink.'">'.$event->eventname.'</a>. Wir möchten dich darauf hinweisen, dass dein Angebot verbindlich ist, und erneut auf unsere <a href="'.Settings::getInstance()->get("host").'agb.html">AGBs</a> verweisen.<br><br>Dein Meal wurde bereits in den sozialen Medien veröffentlicht (wenn du dies in deinen <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> nicht abgelehnt hast), teile es doch gleich auf Facebook: <a href="http://www.facebook.com/sharer.php?u='.$maileventlink.'">Dein Meal auf Facebook</a>, oder retweete es auf Twitter: <a href="http://twitter.com/home?status=Mein Spezial-Meal über @infosocialmeal : '.$maileventlink.'">Dein Meal auf Twitter</a>.<br><br>Wir werden dich über jede Anmeldung zu deinem Meal informieren (auch das kannst du in deinen <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abstellen). Beachte, dass User definitiv auf deiner Gästeliste stehen, sobald sie sich für dein Meal anmelden. Nach Ablauf der Anmeldefrist erhältst du die definitive Gäste-Liste und den Kontaktinformationen.<br><br>Wenn du noch etwas verbessern willst, kannst du dein <a href="'.Settings::getInstance()->get("host").'event-bearbeiten.html?event='.$event->id.'">Meal bearbeiten</a>. Tipps dazu findest du unter <a href="'.Settings::getInstance()->get("host").'FAQ/erstellung-von-spezial-events.html">Erstellung von Spezial-Meals</a>. Vielleicht magst du dir auch schon einmal durchlesen <a href="'.Settings::getInstance()->get("host").'FAQ/das-solltest-du-als-gastgeber-beachten.html">Was du als Gastgeber*in beachten solltest</a>.<br><br>Unter Angabe einer Begründung kannst du jemanden von deiner Gästeliste entfernen oder das gesamte <a href="'.Settings::getInstance()->get("host").'event-loeschen.html?event='.$event->id.'">Meal absagen</a>, solange die Anmeldefrist noch nicht abgelaufen ist. Nach Ablauf der Anmeldefrist ist eine Meal-Absage über Social Meal nicht mehr möglich. In Notsituationen empfehlen wir dir die direkte Kontaktaufnahme mit deinen Gästen.<br><br>Wir wünschen dir viel Erfolg mit deinem Meal!<br>Social Meal<br><br>P.S.: Diesen E-Mail-Typ kannst du in den <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abbestellen, falls er dir überflüssig erscheint.';
									$h2t = new Plugin_Mailer_html2text($body);
									$mail = new Plugin_PHPMailer_PHPMailer;
									$mail->IsSMTP();
									$mail->AddAddress(User::Current()->email);
									$mail->WordWrap = 50;
									$mail->IsHTML(true);
									$mail->Subject = $subject;
									$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$body);
									$mail->AltBody = $h2t->get_text();
									if(!$mail->Send()){
										echo('Mail Error:'.$mail->ErrorInfo);
									}
								}
							}
						}
						$promo = Plugin_Events_Manager::getPromoMeal($event->promo);
						unset($event);
						$template->assign_var('eventid',htmlentities($_GET['event']));
						$template->assign_var('promomsg',$promo->createnotice);
						$template->show_if('showmsg',true);
					}
					$event = Plugin_Events_Manager::loadEvent($_GET['event']);
					$template->assign_var('eventid',htmlentities($event->id));
					$template->assign_var('eventname',htmlentities($event->eventname));
					$template->assign_var('rest',htmlentities($event->rest));
					$template->assign_var('street',htmlentities(ucfirst($event->strasse)));
					$template->assign_var('strnr',htmlentities($event->strnr));
					$template->assign_var('plz',htmlentities($event->plz));
					$template->assign_var('ort',htmlentities(ucfirst($event->ort)));
					$template->assign_var('kanton',htmlentities($event->kanton));
					$template->assign_var('abouteat',htmlentities($event->beschreibung));
					if($event->eventdate == '0000-00-00 00:00:00'){
						$template->assign_var('eventdate',date('Y-m-d H:i'));
					}else{
						$template->assign_var('eventdate',substr($event->eventdate,0,-3));
					}
					if($event->anmeldeschluss == '0000-00-00 00:00:00'){
						$template->assign_var('asdate',date('Y-m-d H:i'));
					}else{
						$template->assign_var('asdate',substr($event->anmeldeschluss,0,-3));
					}
					$makedatehour = mktime(date("H")+1, date("i"), date("s"), date("m") , date("d"), date("Y"));
					$template->assign_var('datehour',date('Y-m-d H:i',$makedatehour));
					$template->assign_var('specialpreis'.htmlentities($event->specialprice),'selected="selected"');
					$template->assign_var('maxguest',htmlentities($event->maxguest));
					$template->assign_var('dauer'.htmlentities($event->dauer),'selected="selected"');
					$template->assign_var('specialpreis4','');
					$template->assign_var('specialpreis5','');
					$template->assign_var('specialpreis6','');
					$template->assign_var('dauer1','');
					$template->assign_var('dauer2','');
					$template->assign_var('dauer3','');
					$template->assign_var('dauer4','');
				}
			}else{
				$template->load('plugin_events_noevent');
			}
			//User::Current()->id wieder herstellen!
			$userinfos = Plugin_Profile_Manager::getUserData(User::Current()->name);
			User::Current()->id = $userinfos->id;
			echo($template->getCode());
		}

		function getHeader(){
			echo('<link rel="stylesheet" type="text/css" href="'.Settings::getInstance()->get("host").'system/plugins/events/css/jquery.datetimepicker.css"/>');
			if(isset($_GET['event'])){
				$eventid = DataBase::Current()->EscapeString($_GET['event']);
				$eventtype = DataBase::Current()->ReadField("SELECT eventtype FROM {'dbprefix'}plugin_events WHERE id = '".$eventid."'");
				if($eventtype == 0){
					echo('<script type="text/javascript" src="'.Settings::getInstance()->get("host").'system/plugins/events/js/eventtype0.js"></script>');
				}elseif($eventtype == 1){
					echo('<script type="text/javascript" src="'.Settings::getInstance()->get("host").'system/plugins/events/js/eventtype1.js"></script>');
				}elseif($eventtype == 2){
					echo('<script type="text/javascript" src="'.Settings::getInstance()->get("host").'system/plugins/events/js/eventtype2.js"></script>');
				}elseif($eventtype == 3){
					echo('<script type="text/javascript" src="'.Settings::getInstance()->get("host").'system/plugins/events/js/eventtype3.js"></script>');
				}
			}
			echo('<link href="/system/plugins/bbcode/css/editor.css" rel="stylesheet" type="text/css" />');
			echo('<script type="text/javascript" src="/system/plugins/bbcode/js/editor.js"></script>');
		}

		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}

		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
