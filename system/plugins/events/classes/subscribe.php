<?PHP
/*
 * subscribe.php
 *
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
	class Plugin_Events_Subscribe extends Editor{

		/**
		 *
		 * @param Page $page
		 */
		function __construct(Page $page){
			$this->page = $page;
			if(isset($_GET['event'])){
				$event = Plugin_Events_Manager::loadEvent($_GET['event']);
				$page->title = Language::DirectTranslate('plugin_events_subscribeto').' '.$event->eventname;
			}
		}
		public function display(){
			$template = new Template();
			$template->load('plugin_events_subscribe');
			$template->show_if('showsubscribe',false);
			$template->show_if('subscribeok',false);
			$template->show_if('mitnehmen',false);
			$template->show_if('subscriber',false);
			$template->show_if('selfsubscribe',false);
			$template->show_if('maxguest',false);
			//Start - Extra für Rrunning Dinner
			$template->show_if('245mit',false);
			$template->show_if('it245',false);
			//End - Extra für Running Dinner
			$event = Plugin_Events_Manager::loadEvent($_GET['event']);
			if(!empty($_GET['event'])){
				if(empty($_GET['subscribe'])){
					$template->show_if('showsubscribe',true);
					//Start - Extra für Rrunning Dinner
					if($event->id == 245){
						$template->show_if('245mit',true);
					}
					//End - Extra für Running Dinner
					$template->assign_var('eventid',htmlentities($event->id));
					$template->assign_var('eventname',htmlentities($event->eventname));
					if($event->option1 != '' && $event->option2 != ''){
						$template->show_if('options',true);
						$template->assign_var('option1',htmlentities($event->option1));
						$template->assign_var('option2',htmlentities($event->option2));
						if($event->option3 != ''){
							$template->show_if('option3',true);
							$template->assign_var('option3',htmlentities($event->option3));
						}else{
							$template->show_if('option3',false);
						}
					}else{
						$template->show_if('options',false);
					}
					if($event->mitnehmen != ''){
						$template->show_if('mitnehmen',true);
						$template->assign_var('mitnehmen',htmlentities($event->mitnehmen));
					}
					$template->assign_var('eventdatum',Plugin_Events_Manager::formatDate($event->eventdate));
				}else{
					if(Plugin_Events_Manager::hasSubscribe($_GET['event'],User::Current()->id) == 1){
						$template->show_if('subscriber',true);
					}elseif($event->userid == User::Current()->id && $event->orgid == 0){
						$template->show_if('selfsubscribe',true);
					}elseif($event->eventtype != 3 && $event->maxguest == Plugin_Events_Manager::countSubscribers($_GET['event'])){
						$template->show_if('maxguest',true);
					}elseif($event->maxguest == -1){
						$template->show_if('maxguest',true);
					}else{
						$template->show_if('showsubscribe',false);
						$template->show_if('subscribeok',true);
						//Start - Extra für Rrunning Dinner
						if($event->id == 245){
							$template->show_if('it245',true);
						}
						//End - Extra für Rrunning Dinner
						$template->assign_var('eventid',htmlentities($event->id));
						if(isset($_POST['option'])){
							$postoption = $_POST['option'];
						}else{
							$postoption = 0;
						}
						Plugin_Events_Manager::addSubscriber($_GET['event'],User::Current()->id,$postoption);
						$countsubscribers = Plugin_Events_Manager::countSubscribers($_GET['event']);
						if($event->maxguest == $countsubscribers){
							Plugin_Events_Manager::setAllAsReadFromEvent($_GET['event']);
						}
						Plugin_Profile_Manager::updatePunkte(5);
						$usergastinfo = Plugin_Profile_Manager::getUserDataPerId(User::Current()->id);
						$usergeberinfo = Plugin_Profile_Manager::getUserDataPerId($event->userid);
						$mailtext = 'Liebe*r '.ucfirst($usergeberinfo->firstname).'<br><br>Wir freuen uns dir mitteilen zu können, dass dein Meal <a href="'.Settings::getInstance()->get("host").'event-details.html?event='.$event->id.'">'.$event->eventname.'</a> eine neue Anmeldung erhalten hat!<br><br>Der User <a href="'.Settings::getInstance()->get("host").'mein-profil.html?profile='.$usergastinfo->name.'">'.$usergastinfo->name.'</a> hat sich angemeldet. Er steht somit definitiv auf deiner Gästeliste.';
						if(isset($_POST['option'])){
							if($_POST['option'] == 1){
								$option = $event->option1;
							}elseif($_POST['option'] == 2){
								$option = $event->option2;
							}elseif($_POST['option'] == 3){
								$option = $event->option3;
							}
							$mailtext .= '<br>Er hat folgende Anmeldeoption gewählt: '.$option;
						}
						//Start - Extra für Rrunning Dinner
						if($event->id == 245){
							if(isset($_POST['245partner']) != ''){
								$partner245 = $_POST['245partner'];
							}
							if($partner245 == ''){
								$partner245 = "Keine Angabe";
							}
							$mailtext .= '<br><br>Der Teilnehmer nimmt folgende Person mit: <b>'.$partner245.'</b> (Folgende Person soll kochen: <b>'.$_POST['werkocht'].'</b>)';
						}
						//End - Extra für Rrunning Dinner
						$mailtext .= '<br><br>Bitte beachte: Wenn das Meal in mehr als 24 Stunden stattfindet, erhält der User deine vollständige Anschrift 24 Stunden vor dem Meal. Ansonsten erhält er deine Anschrift direkt bei seiner Anmeldung. Du erhältst eine Liste mit allen Gästen, sowie ihren Kontaktinformationen nach Ablauf der Anmeldefrist deines Meals.<br><br>Unter Angabe eines Grundes kannst du diese*n <a href="'.Settings::getInstance()->get("host").'teilnehmer-ausladen.html?event='.$event->id.'&user='.User::Current()->id.'">Teilnehmer*in ausladen</a> solange die Anmeldefrist noch nicht abgelaufen ist. Wir hoffen jedoch, dass du diese Funktion nur zurückhaltend gebrauchst.<br><br>Wir wünschen dir ein spannendes Meal mit diesem Gast!<br>Social Meal<br><br>P.S.: Diesen E-Mail-Typ kannst du in den <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abbestellen, falls er dir überflüssig erscheint.';
						$h2t1 = new Plugin_Mailer_html2text($mailtext);
						if($event->orgid > 0){
							$orginfo = Plugin_Profile_Manager::loadOrganisation($event->orgid);
							$fromuser = 'von der Organisation <a href="'.Settings::getInstance()->get("host").'/organisationsprofil.html?profile='.$orginfo->id.'">'.$orginfo->orgname.'</a>';
						}else{
							$fromuser = 'vom User <a href="'.Settings::getInstance()->get("host").'mein-profil.html?profile='.$usergeberinfo->name.'">'.$usergeberinfo->name.'</a>';
						}
						$mailtext2 = 'Liebe*r '.ucfirst($usergastinfo->firstname).'<br><br>Du hast dich soeben erfolgreich zum Meal <a href="'.Settings::getInstance()->get("host").'event-details.html?event='.$event->id.'">'.$event->eventname.'</a> '.$fromuser.' angemeldet.<br><br>Falls das Meal in mehr als 24 Stunden stattfindet, erhältst du 24 Stunden vor dem Meal ein weiteres E-Mail mit der vollständigen Anschrift von deinem*r Gastgeber*in. Falls das Meal während den nächsten 24 Stunden stattfindet, hast du soeben ein weiteres E-Mail mit dieser Adresse erhalten.<br><br>Für ein gutes Erlebnis für dich und alle anderen User möchten wir dir die Befolgung unserer Tipps, <a href="'.Settings::getInstance()->get("host").'FAQ/das-solltest-du-als-gast-beachten.html">Das solltest du als Gast beachten</a>, nahelegen.<br><br>Wir möchten dich darauf hinweisen, dass deine Teilnahme an dem Meal einer verbindlichen Zusage entspricht und auf unsere <a href="'.Settings::getInstance()->get("host").'agb.html">AGBs</a> verweisen. Unter Angabe eines Grundes kannst du deine <a href="'.Settings::getInstance()->get("host").'event-abmelden.html?event='.$event->id.'">Teilnahme absagen</a>, falls die Anmeldefrist für das Meal noch nicht abgelaufen ist. Wir hoffen jedoch, dass du diese Funktion nur zurückhaltend gebrauchst. Nach Ablauf der Anmeldefrist kannst du dich nicht mehr über Social Meal abmelden, in Notsituationen empfehlen wir dir die direkte Kontaktaufnahme mit deinem*r Gastgeber*in. Umgekehrt kann dich der*die Gastgeber*in unter Angabe eines Grundes bis zum Anmeldeschluss ausladen oder das Meal komplett absagen.<br><br>Wir wünschen dir ein spannendes Meal mit den anderen Usern!<br>Social Meal<br><br>P.S.: Diesen E-Mail-Typ kannst du in den <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abbestellen, falls er dir überflüssig erscheint.';
						$h2t2 = new Plugin_Mailer_html2text($mailtext2);
						$subject1 = utf8_decode('Dein Meal "'.$event->eventname.'" auf Social Meal hat eine neue Anmeldung erhalten');
						$subject2 = utf8_decode('Du hast dich erfolgreich zu dem Meal "'.$event->eventname.'" auf Social Meal angemeldet');
						if(Plugin_Profile_Manager::getUserSetting('newsubscribe',$event->userid) == 1){
							$mail1 = new Plugin_PHPMailer_PHPMailer;
							$mail1->IsSMTP();
							//Start - Extra für Rrunning Dinner
							if($event->id == "245"){
								$mail1->AddAddress("info@socialmeal.ch");
							}
							//End - Extra für Rrunning Dinner
							$mail1->AddAddress($usergeberinfo->email);
							$mail1->WordWrap = 50;
							$mail1->IsHTML(true);
							$mail1->Subject = $subject1;
							$mail1->Body = Plugin_Mailer_Mailer::makeHtml($subject1,$mailtext);
							$mail1->AltBody = $h2t1->get_text();
							if(!$mail1->Send()){
								echo('Mail Error:'.$mail1->ErrorInfo);
							}
						}
						if(Plugin_Profile_Manager::getUserSetting('tb',$usergastinfo->id) == 1){
							$mail2 = new Plugin_PHPMailer_PHPMailer;
							$mail2->IsSMTP();
							$mail2->AddAddress($usergastinfo->email);
							$mail2->WordWrap = 50;
							$mail2->IsHTML(true);
							$mail2->Subject = $subject2;
							$mail2->Body = Plugin_Mailer_Mailer::makeHtml($subject2,$mailtext2);
							$mail2->AltBody = $h2t2->get_text();
							if(!$mail2->Send()){
								echo('Mail Error:'.$mail2->ErrorInfo);
							}
						}
						if($event->sendmail == 1){
							Plugin_Events_Manager::sendAdress(User::Current()->id,$event->id);
						}
					}
				}
			}
			echo($template->getCode());
		}

		function getHeader(){
		}

		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}

		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
