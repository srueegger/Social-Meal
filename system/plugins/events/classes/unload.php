<?PHP
/*
 * unload.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_Events_Unload extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
			if(isset($_GET['event'])){
				$eventdata = Plugin_Events_Manager::loadEvent($_GET['event']);
				$userdata = Plugin_Profile_Manager::getUserDataPerId($_GET['user']);
				$page->title = Language::DirectTranslate('plugin_events_subscriber').' '.htmlentities($userdata->name).' '.Language::DirectTranslate('plugin_events_frommeal').' '.htmlentities($eventdata->eventname).' '.Language::DirectTranslate('plugin_events_unload');
			}
		}
		public function display(){
			$template = new Template();
			$template->load('plugin_events_unload');
			$eventdata = Plugin_Events_Manager::loadEvent($_GET['event']);
			$userdata = Plugin_Profile_Manager::getUserDataPerId($_GET['user']);
			if(date('Y-m-d H:i') > $eventdata->anmeldeschluss){
				$template->show_if('showform',false);
				$template->show_if('showok',false);
				$template->show_if('nosubscriber',false);
				$template->show_if('notown',false);
				$template->show_if('asdate',true);
				echo($template->getCode());
				exit;
			}
			$template->assign_var('eventname',$eventdata->eventname);
			$template->assign_var('username',$userdata->name);
			if($eventdata->userid == User::Current()->id && Plugin_Events_Manager::hasSubscribe($_GET['event'],$_GET['user']) == 1){
				if(!isset($_POST['plugin_events_unloadsubmit']) && !isset($_POST['grund'])){
					$template->show_if('showform',true);
					$template->show_if('showok',false);
					$template->show_if('nosubscriber',false);
					$template->show_if('notown',false);
					$template->show_if('asdate',false);
				}elseif(isset($_POST['plugin_events_unloadsubmit']) && isset($_POST['grund']) != ''){
					$template->show_if('showform',false);
					$template->show_if('showok',true);
					$template->show_if('nosubscriber',false);
					$template->show_if('notown',false);
					$template->show_if('asdate',false);
					Plugin_Events_Manager::removeSubscriber($_GET['event'],$userdata->id);
					$subject = 'Du wurdest von dem Meal "'.$eventdata->eventname.'" auf Social Meal ausgeladen';
					$mailtext = 'Liebe*r '.ucfirst($userdata->firstname).'<br><br>Es tut uns leid dir mitteilen zu müssen, dass dich der User <a href="'.Settings::getInstance()->get("host").'mein-profil.html?profile='.User::Current()->name.'">'.User::Current()->name.'</a> von seinem Meal "<a href="'.Settings::getInstance()->get("host").'event-details.html?event='.$eventdata->id.'">'.$eventdata->eventname.'</a>", für welchen du angemeldet warst, ausgeladen hat.<br><br>Das ist seine Begründung:<br>'.nl2br(htmlentities($_POST['grund'])).'<br><br>Wir hoffen, dass dir ein alternatives Meal unter <a href="'.Settings::getInstance()->get("host").'essen-gehen.html">Meal finden</a> genauso gut gefällt, vielleicht hilft dir auch ein Blick in <a href="'.Settings::getInstance()->get("host").'FAQ/so-erstellst-du-ein-ansprechendes-profil.html">So erstellst du ein ansprechendes Profil</a> oder <a href="'.Settings::getInstance()->get("host").'FAQ/so-findest-du-das-richtige-angebot.html">So findest du das richtige Angebot</a>. Ansonsten können wir dir auch empfehlen, selber ein <a href="'.Settings::getInstance()->get("host").'neuen-event-erstellen.html">Meal anzubieten</a>.<br><br>Falls du der Meinung bist, dass dieser User die Funktion zur Gäste-Ausladung missbräuchlich verwendet, melde uns das bitte über die entsprechende <a href="'.Settings::getInstance()->get("host").'melden.html?source=mail&g=unloaduser_'.$userdata->id.'">Melde-Seite</a>.<br><br>Das nächste Mal klappts bestimmt!<br>Social Meal<br><br>P.S.: Dieser E-Mail-Typ ist nicht abbestellbar, da er für die Funktion der Plattform essentielle Inhalte übermittelt.';
					$h2t = new Plugin_Mailer_html2text($mailtext);
					$mail = new Plugin_PHPMailer_PHPMailer;
					$mail->IsSMTP();
					$mail->AddAddress($userdata->email);
					$mail->WordWrap = 50;
					$mail->IsHTML(true);
					$mail->Subject = $subject;
					$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
					$mail->AltBody = $h2t->get_text();
					if(!$mail->Send()){
						echo('Mail Error:'.$mail->ErrorInfo);
					}
				}
			}else{
				if($eventdata->userid != User::Current()->id){
					$template->show_if('showform',false);
					$template->show_if('showok',false);
					$template->show_if('nosubscriber',false);
					$template->show_if('notown',true);
					$template->show_if('asdate',false);
				}elseif(Plugin_Events_Manager::hasSubscribe($_GET['event'],$_GET['user']) == 0){
					$template->show_if('showform',false);
					$template->show_if('showok',false);
					$template->show_if('nosubscriber',true);
					$template->show_if('notown',false);
					$template->show_if('asdate',false);
				}
			}
			echo($template->getCode());
		}

		function getHeader(){
			echo('<script type="text/javascript" src="'.Settings::getInstance()->get("host").'system/plugins/events/js/unload.js"></script>');
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
