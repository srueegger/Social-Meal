<?PHP
/*
 * filter.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_Events_Filter extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			$template->load('plugin_events_filter');
			if(Plugin_Events_Manager::countAllActiveEvents() > 0){
				$template->show_if('events',true);
				$template->show_if('noevent',false);
				if(!isset($_POST['getfilter']) && !isset($_GET['fleisch']) && !isset($_GET['vegi']) && !isset($_GET['bio']) && !isset($_GET['saison']) && !isset($_GET['vegan']) && !isset($_GET['regional']) && !isset($_GET['fisch']) && !isset($_GET['seafood'])){
					$filter = Plugin_Events_Manager::getAllActiveEvents();
				}else{
					$sql = "SELECT * FROM {'dbprefix'}plugin_events WHERE anmeldeschluss > NOW() AND active = '1'";
					$counter = 0;
					if(isset($_POST['fleisch']) or isset($_GET['fleisch'])){
						$template->assign_var('fleischchecked','checked="checked"');
						$sql .= ' AND fleisch = "1"';
						$counter++;
					}
					if(isset($_POST['fisch']) or isset($_GET['fisch'])){
						$template->assign_var('fischchecked','checked="checked"');
						$sql .= ' AND fisch = "1"';
						$counter++;
					}
					if(isset($_POST['seafood']) or isset($_GET['seafood'])){
						$template->assign_var('seafoodchecked','checked="checked"');
						$sql .= ' AND seafood = "1"';
						$counter++;
					}
					if(isset($_POST['vegi']) or isset($_GET['vegi'])){
						$template->assign_var('vegichecked','checked="checked"');
						$sql .= ' AND vegetarisch = "1"';
						$counter++;
					}
					if(isset($_POST['vegan']) or isset($_GET['vegan'])){
						$template->assign_var('veganchecked','checked="checked"');
						$sql .= ' AND vegan = "1"';
						$counter++;
					}
					if(isset($_POST['bio']) or isset($_GET['bio'])){
						$template->assign_var('biochecked','checked="checked"');
						$sql .= ' AND bio = "1"';
						$counter++;
					}
					if(isset($_POST['saison']) or isset($_GET['saison'])){
						$template->assign_var('saisonchecked','checked="checked"');
						$sql .= ' AND saisonal = "1"';
						$counter++;
					}
					if(isset($_POST['regional']) or isset($_GET['regional'])){
						$template->assign_var('regionalchecked','checked="checked"');
						$sql .= ' AND regional = "1"';
						$counter++;
					}
					if(isset($_POST['eventtype0'])){
						$template->assign_var('eventtype0','checked="checked"');
						$sql .= ' AND eventtype = "0"';
						$counter++;
					}
					if(isset($_POST['eventtype1'])){
						$template->assign_var('eventtype1','checked="checked"');
						$sql .= ' AND eventtype = "1"';
						$counter++;
					}
					if(isset($_POST['eventtype2'])){
						$template->assign_var('eventtype2','checked="checked"');
						$sql .= ' AND eventtype = "2"';
						$counter++;
					}
					if(isset($_POST['eventtype3'])){
						$template->assign_var('eventtype3','checked="checked"');
						$sql .= ' AND eventtype = "3"';
						$counter++;
					}
					$sql .= ' ORDER BY eventdate ASC';
					if($counter > 0){
						$filter = DataBase::Current()->ReadRows($sql);
					}else{
						$filter = Plugin_Events_Manager::getAllActiveEvents();
					}
				}
				foreach($filter as $event){
					if(Plugin_Events_Manager::countSubscribers($event->id) < $event->maxguest or $event->maxguest != -1){
						$index = $template->add_loop_item("events");
						$beschreibung = Plugin_Events_Manager::shortText2($event->beschreibung,$event->id,125);
						$title = Plugin_Events_Manager::shortText($event->eventname,$event->id,28);
						$template->assign_loop_var("events",$index,"eventid",htmlentities($event->id));
						$template->assign_loop_var("events",$index,"eventtitel",htmlentities($event->eventname));
						$template->assign_loop_var("events",$index,"eventbeschreibung",$beschreibung);
						if(User::Current()->role->ID > 1){
							if(Plugin_Events_Manager::hasEventRead($event->id) == true){
								$template->assign_loop_var("events",$index,"unread","");
							}else{
								$template->assign_loop_var("events",$index,"unread",'style="background-color: #B2F0FF;"');
							}
						}else{
							$template->assign_loop_var("events",$index,"unread","");
						}
						if($event->eventtype == 0){
							$template->assign_loop_var("events",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype0')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_green').'" /></span></a>');
						}elseif($event->eventtype == 1){
							$template->assign_loop_var("events",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype1')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_red').'" /></span></a>');
						}elseif($event->eventtype == 2){
							$template->assign_loop_var("events",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype2')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_yellow').'" /></span></a>');
						}elseif($event->eventtype == 3){
							$template->assign_loop_var("events",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype3')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('asterisk_orange').'" /></span></a>');
						}
						if($event->eventtype == 1){
							$template->assign_loop_var("events",$index,"preis",'');
						}elseif($event->eventtype == 3){
							if($event->preis != 0){
								$template->assign_loop_var("events",$index,"preis",Language::directTranslate('plugin_events_preis').':<br />'.htmlentities($event->preis).' '.Language::DirectTranslate('plugin_events_chf'));
							}elseif($event->specialprice > 0){
								$template->assign_loop_var("events",$index,"preis",Language::directTranslate('plugin_events_preis').':<br />'.Language::directTranslate('plugin_events_price'.$event->specialprice));
							}else{
								$template->assign_loop_var("events",$index,"preis",'');
							}
						}else{
							$template->assign_loop_var("events",$index,"preis",Language::directTranslate('plugin_events_preis').':<br />'.htmlentities($event->preis).' '.Language::DirectTranslate('plugin_events_chf'));
						}
						if($event->eventtype == 3){
							if($event->maxguest == 0){
								$contentfreeplace = '<span class="bold">'.utf8_encode(Language::DirectTranslate('plugin_events_yes')).'</span>';
								if(Plugin_Events_Manager::countSubscribers($event->id) > 0){
									$contentfreeplace .= ' ('.Language::DirectTranslate(plugin_events_voteyes2).': '.Plugin_Events_Manager::countSubscribers($event->id).')';
								}
								$template->assign_loop_var("events",$index,'freeplace',$contentfreeplace);
							}elseif($event->maxguest > 0){
								$freeplace = $event->maxguest - Plugin_Events_Manager::countSubscribers($event->id);
								$template->assign_loop_var("events",$index,'freeplace','<span class="bold">'.htmlentities($freeplace).'</span> '.Language::DirectTranslate('plugin_events_by').' '.htmlentities($event->maxguest));
							}
						}else{
							$freeplace = $event->maxguest - Plugin_Events_Manager::countSubscribers($event->id);
							$template->assign_loop_var("events",$index,'freeplace','<span class="bold">'.htmlentities($freeplace).'</span> '.Language::DirectTranslate('plugin_events_by').' '.htmlentities($event->maxguest));
						}
						$template->assign_loop_var("events",$index,"datum",Plugin_Events_Manager::formatDate($event->eventdate));
						if($event->bnb != 0){
							$template->assign_loop_var("events",$index,"bnb",' <small>('.Language::DirectTranslate('plugin_events_or').' '.htmlentities($event->bnb).' <a href="/Partner/bnb-bonnetzbon.html" target="_blank">BNB</a>)</small>');
						}else{
							$template->assign_loop_var("events",$index,"bnb",'');
						}
						if(Plugin_Events_Manager::countImagesPerEvent($event->id) > 0){
							$template->assign_loop_var("events",$index,"eventimg",'<div class="recipegridimage"><img width="275" height="190" alt="'.htmlentities($event->eventname).'" src="/content/uploads/eat/thumbs/'.Plugin_Events_Manager::getFirstImageFromEvent($event->id).'"></div>');
						}else{
							$template->assign_loop_var("events",$index,"eventimg",'');
						}
					}
				}
				$template->assign_var('fleischchecked','');
				$template->assign_var('fischchecked','');
				$template->assign_var('seafoodchecked','');
				$template->assign_var('vegichecked','');
				$template->assign_var('veganchecked','');
				$template->assign_var('biochecked','');
				$template->assign_var('saisonchecked','');
				$template->assign_var('regionalchecked','');
				$template->assign_var('eventtype0','');
				$template->assign_var('eventtype1','');
				$template->assign_var('eventtype2','');
				$template->assign_var('eventtype3','');
			}else{
				$template->show_if('events',false);
				$template->show_if('noevent',true);
			}
			echo($template->getCode());
			//Sidebar
			include('./system/skins/socialmeal/sidebar.php');
		}

		function getHeader(){
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
