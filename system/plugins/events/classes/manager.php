<?php
/*
 * manager.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
class Plugin_Events_Manager{


	public static function createEvent($data){
		$eventname = DataBase::Current()->EscapeString($data['eventname']);
		$eventtype = DataBase::Current()->EscapeString($data['eventtype']);
		if(isset($data['promo'])){
			$promo = DataBase::Current()->EscapeString($data['promo']);
		}else{
			$promo = 0;
		}
		DataBase::Current()->Execute("INSERT INTO {'dbprefix'}plugin_events SET userid='".User::Current()->id."', eventtype='".$eventtype."', eventname='".$eventname."', promo='".$promo."'");
		SessionCache::clear();
		return DataBase::Current()->InsertID();
	}

	public static function loadEvent($id,$option = 'none'){
		$id = DataBase::Current()->EscapeString($id);
		if($option == 'none'){
			return DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}plugin_events WHERE id = '".$id."'");
		}elseif($option == 'old'){
			return DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}plugin_events WHERE id = '".$id."' AND eventdate < NOW()");
		}elseif($option == 'new'){
			return DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}plugin_events WHERE id = '".$id."' AND eventdate > NOW()");
		}
	}

	public static function updateEvent($id,$data){
		$id						= DataBase::Current()->EscapeString($id);
		$eventname				= DataBase::Current()->EscapeString($data['eventname']);
		$street					= DataBase::Current()->EscapeString($data['street']);
		$strnr					= DataBase::Current()->EscapeString($data['strnr']);
		$plz					= DataBase::Current()->EscapeString($data['plz']);
		$ort					= DataBase::Current()->EscapeString($data['ort']);
		$kanton					= DataBase::Current()->EscapeString($data['kanton']);
		$eventdate				= DataBase::Current()->EscapeString($data['evdate']);
		$date					= new DateTime($date);
		$date->modify('+16 hour');
		$aftereventmaildate		= $date->format('Y-m-d H:i');
		$finishdate				= DataBase::Current()->EscapeString($data['asdate']);
		$abouteat				= DataBase::Current()->EscapeString($data['abouteat']);
		$parking				= DataBase::Current()->EscapeString($data['parking']);
		if(isset($data['specialpreis'])){
			$specialprice = DataBase::Current()->EscapeString($data['specialpreis']);
		}else{
			$specialprice = 0;
		}
		if(!empty($data['weblink'])){
			$weblink = Plugin_Profile_Manager::ifhttp($data['weblink']);
		}else{
			$weblink = '';
		}
		if(isset($data['specialbnb'])){
			$specialbnb 			= DataBase::Current()->EscapeString($data['specialbnb']);
		}else{
			$specialbnb = '';
		}
		if(isset($data['preis'])){
			$preis					= DataBase::Current()->EscapeString($data['preis']);
		}else{
			$preis = '';
		}
		if(isset($data['bnb'])){
			$bnb					= DataBase::Current()->EscapeString($data['bnb']);
		}else{
			$bnb = '';
		}
		if(isset($data['gange'])){
			$gange					= DataBase::Current()->EscapeString($data['gange']);
		}else{
			$gange = '';
		}
		if(isset($data['maxguest'])){
			$maxguest					= DataBase::Current()->EscapeString($data['maxguest']);
		}else{
			$maxguest = '';
		}
		if(isset($data['drinks'])){
			$drinks					= DataBase::Current()->EscapeString($data['drinks']);
		}else{
			$drinks = '';
		}
		if(isset($data['rauchen'])){
			$rauchen					= DataBase::Current()->EscapeString($data['rauchen']);
		}else{
			$rauchen = '';
		}
		if(isset($data['rollchair'])){
			$rollchair					= DataBase::Current()->EscapeString($data['rollchair']);
		}else{
			$rollchair = '';
		}
		if(isset($data['fleisch'])){
			$fleisch					= DataBase::Current()->EscapeString($data['fleisch']);
		}else{
			$fleisch = 0;
		}
		if(isset($data['fisch'])){
			$fisch					= DataBase::Current()->EscapeString($data['fisch']);
		}else{
			$fisch = 0;
		}
		if(isset($data['seafood'])){
			$seafood					= DataBase::Current()->EscapeString($data['seafood']);
		}else{
			$seafood = 0;
		}
		if(isset($data['vegi'])){
			$vegi					= DataBase::Current()->EscapeString($data['vegi']);
		}else{
			$vegi = 0;
		}
		if(isset($data['vegan'])){
			$vegan					= DataBase::Current()->EscapeString($data['vegan']);
		}else{
			$vegan = 0;
		}
		if(isset($data['saisonal'])){
			$saisonal					= DataBase::Current()->EscapeString($data['saisonal']);
		}else{
			$saisonal = 0;
		}
		if(isset($data['bio'])){
			$bio					= DataBase::Current()->EscapeString($data['bio']);
		}else{
			$bio = 0;
		}
		if(isset($data['regional'])){
			$regional					= DataBase::Current()->EscapeString($data['regional']);
		}else{
			$regional = 0;
		}
		if(isset($data['lactose'])){
			$lactose					= DataBase::Current()->EscapeString($data['lactose']);
		}else{
			$lactose = 0;
		}
		if(isset($data['nusse'])){
			$nusse					= DataBase::Current()->EscapeString($data['nusse']);
		}else{
			$nusse = 0;
		}
		if(isset($data['gluten'])){
			$gluten					= DataBase::Current()->EscapeString($data['gluten']);
		}else{
			$gluten = 0;
		}
		if(isset($data['alkohol'])){
			$alkohol					= DataBase::Current()->EscapeString($data['alkohol']);
		}else{
			$alkohol = 0;
		}
		if(isset($data['scharf'])){
			$scharf						= DataBase::Current()->EscapeString($data['scharf']);
		}else{
			$scharf = 0;
		}
		if(isset($data['hefe'])){
			$hefe					= DataBase::Current()->EscapeString($data['hefe']);
		}else{
			$hefe = 0;
		}
		if(isset($data['koscher'])){
			$koscher					= DataBase::Current()->EscapeString($data['koscher']);
		}else{
			$koscher = 0;
		}
		if(isset($data['halal'])){
			$halal					= DataBase::Current()->EscapeString($data['halal']);
		}else{
			$halal = 0;
		}
		if(isset($data['symbolimg'])){
			$symbolimg					= DataBase::Current()->EscapeString($data['symbolimg']);
		}else{
			$symbolimg = 0;
		}
		if(isset($data['openaddress'])){
			$openaddress					= DataBase::Current()->EscapeString($data['openaddress']);
		}else{
			$openaddress = 0;
		}
		if(isset($data['ov'])){
			$ov					= DataBase::Current()->EscapeString($data['ov']);
		}else{
			$ov = 0;
		}
		if(isset($data['dauer'])){
			$dauer					= DataBase::Current()->EscapeString($data['dauer']);
		}else{
			$dauer = 0;
		}
		if(isset($data['option1'])){
			$option1				= DataBase::Current()->EscapeString($data['option1']);
		}else{
			$option1 = '';
		}
		if(isset($data['option2'])){
			$option2				= DataBase::Current()->EscapeString($data['option2']);
		}else{
			$option2 = '';
		}
		if(isset($data['option3'])){
			$option3				= DataBase::Current()->EscapeString($data['option3']);
		}else{
			$option3 = '';
		}
		if(isset($data['mitnehmen'])){
			$mitnehmen				= DataBase::Current()->EscapeString($data['mitnehmen']);
		}else{
			$mitnehmen = '';
		}
		if(isset($data['org'])){
			if($data['org'] == 'user'){
				$orgid = 0;
			}else{
				$orgid			= DataBase::Current()->EscapeString($data['org']);
			}
		}else{
			$orgid = 0;
		}
		if(isset($data['rest'])){
			$rest = DataBase::Current()->EscapeString($data['rest']);
		}else{
			$rest = '';
		}
		$eventdate				= $eventdate.':00';
		$finishdate				= $finishdate.':00';
		$query					= "UPDATE {'dbprefix'}plugin_events SET eventname='".$eventname."', beschreibung='".$abouteat."', eventdate='".$eventdate."', rauchen='".$rauchen."', drinks='".$drinks."', anmeldeschluss='".$finishdate."', gaenge='".$gange."', maxguest='".$maxguest."', ov='".$ov."', parking = '".$parking."', dauer='".$dauer."', preis='".$preis."', bnb='".$bnb."', rest='".$rest."', ort='".$ort."', strasse='".$street."', strnr='".$strnr."', plz='".$plz."', kanton='".$kanton."', rollchair='".$rollchair."', mitnehmen = '".$mitnehmen."', fleisch='".$fleisch."', fisch='".$fisch."', seafood='".$seafood."', vegetarisch='".$vegi."', vegan='".$vegan."', saisonal='".$saisonal."', bio='".$bio."', lactose='".$lactose."', nusse='".$nusse."', glutenfrei='".$gluten."', regional='".$regional."', alkohol='".$alkohol."', scharf = '".$scharf."', hefe = '".$hefe."', koscher = '".$koscher."', halal = '".$halal."', option1 = '".$option1."', option2 = '".$option2."', option3 = '".$option3."', orgid = '".$orgid."', specialprice = '".$specialprice."', bnbprice = '".$specialbnb."', symbolimg = '".$symbolimg."', openaddress = '".$openaddress."', weblink = '".$weblink."', aftereventmaildate = '".$aftereventmaildate."' WHERE id = '".$id."'";
		DataBase::Current()->Execute($query);
		SessionCache::clear();
	}

	public static function updateAdminEvent($data){
		$id							= DataBase::Current()->EscapeString($data['mealid']);
		$eventname					= DataBase::Current()->EscapeString($data['eventname']);
		$street						= DataBase::Current()->EscapeString($data['strasse']);
		$strnr						= DataBase::Current()->EscapeString($data['strnr']);
		$plz						= DataBase::Current()->EscapeString($data['plz']);
		$ort						= DataBase::Current()->EscapeString($data['ort']);
		$kanton						= DataBase::Current()->EscapeString($data['kanton']);
		$openaddress				= DataBase::Current()->EscapeString($data['openaddress']);
		$eventdate					= DataBase::Current()->EscapeString($data['evdate']);
		$finishdate					= DataBase::Current()->EscapeString($data['asdate']);
		$abouteat					= DataBase::Current()->EscapeString($data['beschreibung']);
		$parking					= DataBase::Current()->EscapeString($data['parking']);
		$ov							= DataBase::Current()->EscapeString($data['ov']);
		$rauchen					= DataBase::Current()->EscapeString($data['rauchen']);
		$parking					= DataBase::Current()->EscapeString($data['parking']);
		$fleisch					= DataBase::Current()->EscapeString($data['fleisch']);
		$fisch						= DataBase::Current()->EscapeString($data['fisch']);
		$seafood					= DataBase::Current()->EscapeString($data['seafood']);
		$vegi						= DataBase::Current()->EscapeString($data['vegetarisch']);
		$vegan						= DataBase::Current()->EscapeString($data['vegan']);
		$saisonal					= DataBase::Current()->EscapeString($data['saisonal']);
		$bio						= DataBase::Current()->EscapeString($data['bio']);
		$lactose					= DataBase::Current()->EscapeString($data['lactose']);
		$nusse						= DataBase::Current()->EscapeString($data['nusse']);
		$glutenfrei					= DataBase::Current()->EscapeString($data['glutenfrei']);
		$regional					= DataBase::Current()->EscapeString($data['regional']);
		$alkohol					= DataBase::Current()->EscapeString($data['alkohol']);
		$scharf						= DataBase::Current()->EscapeString($data['scharf']);
		$hefe						= DataBase::Current()->EscapeString($data['hefe']);
		$koscher					= DataBase::Current()->EscapeString($data['koscher']);
		$halal						= DataBase::Current()->EscapeString($data['halal']);
		$option1					= DataBase::Current()->EscapeString($data['option1']);
		$option2					= DataBase::Current()->EscapeString($data['option2']);
		$option3					= DataBase::Current()->EscapeString($data['option3']);
		$mitnehmen					= DataBase::Current()->EscapeString($data['mitnehmen']);
		$rest						= DataBase::Current()->EscapeString($data['rest']);
		$drinks						= DataBase::Current()->EscapeString($data['drinks']);
		$rollchair					= DataBase::Current()->EscapeString($data['rollchair']);
		$gange						= DataBase::Current()->EscapeString($data['gange']);
		$maxguest					= DataBase::Current()->EscapeString($data['maxguest']);
		$dauer						= DataBase::Current()->EscapeString($data['dauer']);
		$comment					= DataBase::Current()->EscapeString($data['comment']);
		$commentdate				= DataBase::Current()->EscapeString($data['commentdate']);
		$preis						= DataBase::Current()->EscapeString($data['preis']);
		$bnb						= DataBase::Current()->EscapeString($data['bnb']);
		$specialprice				= DataBase::Current()->EscapeString($data['specialprice']);
		$specialbnb 				= DataBase::Current()->EscapeString($data['bnbprice']);
		$counter		 			= DataBase::Current()->EscapeString($data['counter']);
		if($data['weblink'] == ''){
			$weblink				= '';
		}else{
			$weblink					= Plugin_Profile_Manager::ifhttp($data['weblink']);
		}
		if(isset($data['symbolimg'])){
			$symbolimg				= DataBase::Current()->EscapeString($data['symbolimg']);
		}else{
			$symbolimg				= 0;
		}
		$query						= "UPDATE {'dbprefix'}plugin_events SET eventname='".$eventname."', beschreibung='".$abouteat."', eventdate='".$eventdate."', rauchen='".$rauchen."', drinks='".$drinks."', anmeldeschluss='".$finishdate."', gaenge='".$gange."', maxguest='".$maxguest."', ov='".$ov."', parking = '".$parking."', dauer='".$dauer."', preis='".$preis."', bnb='".$bnb."', rest='".$rest."', ort='".$ort."', strasse='".$street."', strnr='".$strnr."', plz='".$plz."', kanton='".$kanton."', rollchair='".$rollchair."', mitnehmen = '".$mitnehmen."', fleisch='".$fleisch."', fisch='".$fisch."', seafood='".$seafood."', vegetarisch='".$vegi."', vegan='".$vegan."', saisonal='".$saisonal."', bio='".$bio."', lactose='".$lactose."', nusse='".$nusse."', glutenfrei='".$gluten."', regional='".$regional."', alkohol='".$alkohol."', scharf = '".$scharf."', hefe = '".$hefe."', koscher = '".$koscher."', halal = '".$halal."', counter = '".$counter."', option1 = '".$option1."', option2 = '".$option2."', option3 = '".$option3."', specialprice = '".$specialprice."', bnbprice = '".$specialbnb."', symbolimg = '".$symbolimg."', openaddress = '".$openaddress."', weblink = '".$weblink."', commentdate = '".$commentdate."', comment = '".$comment."', aftereventmaildate = '".$aftereventmaildate."' WHERE id = '".$id."'";
		DataBase::Current()->Execute($query);
	}

	public static function activateEvent($id){
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_events SET active='1' WHERE id = '".$id."' LIMIT 1");
	}

	public static function uploadFiles($id,$data){
		$errors = array();
		$id = DataBase::Current()->EscapeString($id);
		$counter = self::countImagesPerEvent($id);
		if($counter == 0){
			$i = 1;
		}else{
			$i = $counter;
		}
		foreach($data['files']['tmp_name'] as $key => $tmp_name){
			$file_name		= time().$key.$data['files']['name'][$key];
			$file_end 		= substr($file_name, -3);
			if($file_end == 'peg'){
				$file_name = mt_rand().time().'.jpeg';
			}else{
				$file_name = mt_rand().time().'.'.$file_end;
			}
			$file_size		= $data['files']['size'][$key];
			$file_tmp		= $data['files']['tmp_name'][$key];
			$file_type		= $data['files']['type'][$key];
			if($file_size > 2097152){
				$errors[] = 'Das Bild darf nicht grösser als 2MB sein';
			}
			if($file_size == 0){
				$errors[] = 'Bildgrösse darf nicht 0 sein';
			}
			if($i < 5){
				$query = "INSERT INTO {'dbprefix'}plugin_eventimages (`eventid`,`filename`) VALUES ('$id','$file_name')";
				if(empty($errors) == true){
					move_uploaded_file($file_tmp,'./content/uploads/eat/'.$file_name);
					DataBase::Current()->Execute($query);
					self::mkThumb($file_name);
				}
			}
			$i++;
		}
		SessionCache::clear();
	}

	public static function mkThumb($imgsrc,$imgwidth = '400',$imgheight = '250',$folder_src = './content/uploads/eat',$dessrc = './content/uploads/eat/thumbs'){
		list($src_width,$src_height,$src_typ) = getimagesize($folder_src.'/'.$imgsrc);
		if($src_width >= $src_height){
			$new_image_width = $imgwidth;
			$new_image_height = $src_height * $imgwidth / $src_width;
		}
		if($src_width < $src_height){
			$new_image_height = $imgwidth;
			$new_image_width = $src_width * $imgheight / $src_height;
		}
		if($src_typ == 1){
			//GIF
			$image = imagecreatefromgif($folder_src.'/'.$imgsrc);
			$new_image = imagecreate($new_image_width, $new_image_height);
			imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_image_width, $new_image_height, $src_width, $src_height);
			imagegif($new_image, $dessrc.'/'.$imgsrc,100);
			imagedestroy($image);
			imagedestroy($new_image);
			return true;
		}elseif($src_typ == 2){
			//JPG
			$image = imagecreatefromjpeg($folder_src.'/'.$imgsrc);
			$new_image = imagecreatetruecolor($new_image_width, $new_image_height);
			imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_image_width, $new_image_height, $src_width, $src_height);
			imagegif($new_image, $dessrc.'/'.$imgsrc,100);
			imagedestroy($image);
			imagedestroy($new_image);
			return true;
		}elseif($src_typ == 3){
			//PNG
			$image = imagecreatefrompng($folder_src.'/'.$imgsrc);
			$new_image = imagecreatetruecolor($new_image_width, $new_image_height);
			imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_image_width, $new_image_height, $src_width, $src_height);
			imagegif($new_image, $dessrc.'/'.$imgsrc,100);
			imagedestroy($image);
			imagedestroy($new_image);
			return true;
		}
	}

	public static function deleteImage($imgid){
		$imgid = DataBase::Current()->EscapeString($imgid);
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_eventimages WHERE id = '".$imgid."'");
	}
	
	public static function insertImage($id,$filename){
		$id = DataBase::Current()->EscapeString($id);
		$filename = DataBase::Current()->EscapeString($filename);
		DataBase::Current()->Execute("INSERT INTO {'dbprefix'}plugin_eventimages (`eventid`, `filename`) VALUES ('$id','".$filename."')");
	}

	public static function saveQuestion($eventid,$userid,$data){
		$eventid				= DataBase::Current()->EscapeString($eventid);
		$titel					= DataBase::Current()->EscapeString($data['titel']);
		$question				= DataBase::Current()->EscapeString($data['question']);
		DataBase::Current()->Execute("INSERT INTO {'dbprefix'}plugin_eventquestions (`eventid`, `userid`,`titel`, `question`, `datum`) VALUES ('$eventid','$userid', '$titel','$question',NOW())");
		SessionCache::clear();
	}

	public static function saveAnswer($data){
		$id						= DataBase::Current()->EscapeString($data['questid']);
		$titel					= DataBase::Current()->EscapeString($data['atitel']);
		$answer					= DataBase::Current()->EscapeString($data['answer']);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_eventquestions SET atitel = '".$titel."', antwort = '".$answer."', adatum = NOW() WHERE id = '".$id."' LIMIT 1");
		SessionCache::clear();
	}

	public static function deleteQuestions($eventid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_eventquestions WHERE eventid = '".$eventid."'");
	}

	public static function countQuestions($eventid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_eventquestions WHERE eventid = '".$eventid."'");
	}

	public static function getQuestions($eventid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_eventquestions WHERE eventid = '".$eventid."' ORDER BY datum DESC");
	}
	
	public static function getQuestUserId($id){
		$id = DataBase::Current()->EscapeString($id);
		return DataBase::Current()->ReadField("SELECT userid FROM {'dbprefix'}plugin_eventquestions WHERE id = '".$id."'");
	}

	public static function getEventsByUser($id){
		$id 					= DataBase::Current()->EscapeString($id);
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_events WHERE userid = '".$id."' AND eventdate > NOW() AND active = '1' ORDER BY eventdate DESC");
	}
	
	public static function countEventsByUser($id){
		$id 					= DataBase::Current()->EscapeString($id);
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_events WHERE userid = '".$id."' AND eventdate > NOW() AND active = '1'");
	}

	public static function getOldEventsByUser($id){
		$id 					= DataBase::Current()->EscapeString($id);
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_events WHERE userid = '".$id."' AND eventdate < NOW() AND active = '1' ORDER BY eventdate DESC");
	}
	
	public static function countOldEventsByUser($id){
		$id 					= DataBase::Current()->EscapeString($id);
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_events WHERE userid = '".$id."' AND eventdate < NOW() AND active = '1'");
	}

	public static function getAllAfterEventMail(){
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_events WHERE aftereventmail = '0' AND DATE_ADD(eventdate, INTERVAL 16 HOUR) < NOW() AND active = '1'");
	}

	public static function getAllAsEventMail(){
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_events WHERE afterasmail = '0' AND anmeldeschluss < NOW() AND active = '1'");
	}

	public static function getAllEventsbyUser($id){
		$id 					= DataBase::Current()->EscapeString($id);
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_events WHERE userid = '".$id."' ORDER BY eventdate DESC");
	}

	public static function getAllActiveEventsbyUser($id){
		$id 					= DataBase::Current()->EscapeString($id);
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_events WHERE userid = '".$id."' AND active = '1' ORDER BY eventdate DESC");
	}

	public static function getAllInactiveEventsbyUser($id){
		$id 					= DataBase::Current()->EscapeString($id);
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_events WHERE userid = '".$id."' AND active = '0' ORDER BY eventdate DESC");
	}
	
	public static function countAllInactiveEventsbyUser($id){
		$id 					= DataBase::Current()->EscapeString($id);
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_events WHERE userid = '".$id."' AND active = '0'");
	}

	public static function countEventsFromOrg($orgid){
		$orgid = DataBase::Current()->EscapeString($orgid);
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_events WHERE orgid = '".$orgid."' AND active = '1' AND eventdate > NOW()");
	}

	public static function countOldEventsFromOrg($orgid){
		$orgid = DataBase::Current()->EscapeString($orgid);
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_events WHERE orgid = '".$orgid."' AND active = '1' AND eventdate < NOW()");
	}

	public static function getEventsFromOrg($orgid){
		$orgid = DataBase::Current()->EscapeString($orgid);
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_events WHERE orgid = '".$orgid."' AND active = '1' AND eventdate > NOW() ORDER BY id DESC");
	}

	public static function getOldEventsFromOrg($orgid){
		$orgid = DataBase::Current()->EscapeString($orgid);
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_events WHERE orgid = '".$orgid."' AND active = '1' AND eventdate < NOW() ORDER BY id DESC");
	}

	public static function getAllEvents(){
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_events WHERE anmeldeschluss > NOW() ORDER BY eventdate DESC");
	}

	public static function getAllActiveEvents(){
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_events WHERE anmeldeschluss > NOW() AND active = '1' ORDER BY eventdate ASC");
	}
	
	public static function getAllActiveEvents_3(){
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_events WHERE anmeldeschluss > NOW() AND active = '1' ORDER BY eventdate ASC LIMIT 3");
	}
	
	public static function getAllSpecialLongTime(){
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_events WHERE eventtype = '3' AND anmeldeschluss = '0000-00-00 00:00:00' AND eventdate > NOW() AND active = 1");
	}

	public static function countAllActiveEvents(){
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_events WHERE anmeldeschluss > NOW() AND active = '1'");
	}

	public static function getAll24Events(){
		$date1 = date("Y-m-d H:i");
		$date1 .= ':00';
		$date2 = date("Y-m-d H:i:s", mktime(date("H")+24, date("i"),'00', date("n"), date("d"), date("Y")));
		return DataBase::Current()->ReadRows("SELECT id FROM {'dbprefix'}plugin_events WHERE sendmail = '0' AND eventdate BETWEEN '".$date1."' AND '".$date2."'");
	}

	public static function getFirstImageFromEvent($id){
		$id						= DataBase::Current()->EscapeString($id);
		$res = DataBase::Current()->ReadField("SELECT filename FROM {'dbprefix'}plugin_eventimages WHERE eventid = '".$id."' LIMIT 1");
		return $res;
	}

	public static function addSubscriber($eventid,$userid,$option){
		$eventid = DataBase::Current()->EscapeString($eventid);
		$userid = DataBase::Current()->EscapeString($userid);
		$option = DataBase::Current()->EscapeString($option);
		DataBase::Current()->Execute("INSERT INTO {'dbprefix'}plugin_eventsubscribe (`eventid`, `userid`, `eventoption`) VALUES ('$eventid','$userid','$option')");
		SessionCache::clear();
	}

	public static function removeSubscriber($eventid,$userid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		$userid = DataBase::Current()->EscapeString($userid);
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_eventsubscribe WHERE eventid = '".$eventid."' AND userid = '".$userid."'");
		SessionCache::clear();
	}

	public static function removeSubscribersPerEvent($eventid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_eventsubscribe WHERE eventid = '".$eventid."'");
	}

	public static function countSubscribers($eventid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_eventsubscribe WHERE eventid = '".$eventid."'");
	}

	public static function hasSubscribe($eventid,$userid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		$userid = DataBase::Current()->EscapeString($userid);
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_eventsubscribe WHERE eventid = '".$eventid."' AND userid = '".$userid."'");
	}

	public static function getSubscribesByUser($userid){
		$userid = DataBase::Current()->EscapeString($userid);
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_eventsubscribe WHERE userid = '".$userid."' ORDER BY id DESC");
	}
	
	public static function countSubscribesByUser($userid){
		$userid = DataBase::Current()->EscapeString($userid);
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_eventsubscribe WHERE userid = '".$userid."'");
	}

	public static function getSubscribesByEvent($eventid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_eventsubscribe WHERE eventid = '".$eventid."'");
	}

	public static function countImagesPerEvent($eventid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_eventimages WHERE eventid = '".$eventid."'");
	}

	public static function getImagesPerEvent($eventid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_eventimages WHERE eventid = '".$eventid."'");
	}

	public static function addCounter($eventid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		$counter = DataBase::Current()->ReadField("SELECT counter FROM {'dbprefix'}plugin_events WHERE id = '".$eventid."'");
		$counter++;
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_events SET counter = '".$counter."' WHERE id = '".$eventid."' LIMIT 1");
	}

	public static function infoFriends($eventid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		$userids = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_friends WHERE userid1 = '".User::Current()->id."' OR userid2 = '".User::Current()->id."' AND statusid = '1'");
		$eventdata = self::loadEvent($eventid);
		if($eventdata->orgid == 0){
			foreach($userids as $userid){
				if($userid->userid1 == User::Current()->id){
					$id = $userid->userid2;
				}
				if($userid->userid2 == User::Current()->id){
					$id = $userid->userid1;
				}
				$userdata = Plugin_Profile_Manager::getUserDataPerId($id);
				$userfromdata = Plugin_Profile_Manager::getUserDataPerId(User::Current()->id);
				$subject = 'Dein*e Freund*in '.ucfirst($userfromdata->firstname).' hat das neue Meal '.$eventdata->eventname.' auf Social Meal erstellt';
				$mailtext = 'Liebe*r '.$userdata->firstname.'<br><br>Wir freuen uns dir mitteilen zu können, dass dein*e Freund*in <a href="'.Settings::getInstance()->get("host").'mein-profil.html?profile='.$userfromdata->name.'">'.$userfromdata->firstname.'</a> soeben das neue Meal <a href="'.Settings::getInstance()->get("host").'event-details.html?event='.$eventid.'">'.$eventdata->eventname.'</a> auf Social Meal erstellt hat!<br><br>Bestimmt würde er*sie sich freuen, dich auch dieses Mal wieder bei seinem*ihrem Meal begrüssen zu dürfen.<br><br>Wir wünschen dir ein spannendes Meal!<br>Social Meal<br><br>P.S.: Diesen E-Mail-Typ kannst du in den <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abbestellen, falls er dir überflüssig erscheint.';
				$h2t = new Plugin_Mailer_html2text($mailtext);
				if(Plugin_Profile_Manager::getUserSetting('friendevent',$userdata->id) == 1){
					$mail = new Plugin_PHPMailer_PHPMailer;
					$mail->IsSMTP();
					$mail->AddAddress($userdata->email);
					$mail->WordWrap = 50;
					$mail->IsHTML(true);
					$mail->Subject = $subject;
					$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
					$mail->AltBody = $h2t->get_text();
					if(!$mail->Send()){
						echo('Mail Error:'.$mail->ErrorInfo);
					}
				}
			}
			foreach(Plugin_Profile_Manager::getUserFollowers() as $ufollow){
				$userdata = Plugin_Profile_Manager::getUserDataPerId($ufollow->uid);
				$userfromdata = Plugin_Profile_Manager::getUserDataPerId(User::Current()->id);
				$subject = 'Der*Die Benutzer*in '.$userfromdata->name.' hat das neue Meal '.$eventdata->eventname.' auf Social Meal erstellt';
				$mailtext = 'Liebe*r '.$userdata->firstname.'<br><br>Wir freuen uns dir mitteilen zu können, dass der*die Benutzer*in <a href="'.Settings::getInstance()->get("host").'mein-profil.html?profile='.$userfromdata->name.'">'.$userfromdata->name.'</a> soeben das neue Meal <a href="'.Settings::getInstance()->get("host").'event-details.html?event='.$eventid.'">'.$eventdata->eventname.'</a> auf Social Meal erstellt hat!<br><br>Bestimmt würde er*sie sich freuen, dich auch dieses Mal wieder bei seinem*ihrem Meal begrüssen zu dürfen.<br><br>Wir wünschen dir ein spannendes Meal!<br>Social Meal';
				$h2t = new Plugin_Mailer_html2text($mailtext);
				$mail = new Plugin_PHPMailer_PHPMailer;
				$mail->IsSMTP();
				$mail->AddAddress($userdata->email);
				$mail->WordWrap = 50;
				$mail->IsHTML(true);
				$mail->Subject = $subject;
				$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
				$mail->AltBody = $h2t->get_text();
				if(!$mail->Send()){
					echo('Mail Error:'.$mail->ErrorInfo);
				}
			}
		}else{
			$orgdata = Plugin_Profile_Manager::loadOrganisation($eventdata->orgid);
			foreach(Plugin_Profile_Manager::getOrgFollowers($orgdata->id) as $ofollow){
				$userdata = Plugin_Profile_Manager::getUserDataPerId($ofollow->uid);
				$subject = 'Die Organisation '.$orgdata->orgname.' hat das neue Meal '.$eventdata->eventname.' auf Social Meal erstellt';
				$mailtext = 'Liebe*r '.$userdata->firstname.'<br><br>Wir freuen uns dir mitteilen zu können, dass die Organisation <a href="'.Settings::getInstance()->get("host").'organisationsprofil.html?profile='.$orgdata->id.'">'.$orgdata->orgname.'</a> soeben das neue Meal <a href="'.Settings::getInstance()->get("host").'event-details.html?event='.$eventid.'">'.$eventdata->eventname.'</a> auf Social Meal erstellt hat!<br><br>Bestimmt würden sich die Veranstalter freuen, dich auch dieses Mal wieder bei seinem*ihrem Meal begrüssen zu dürfen.<br><br>Wir wünschen dir ein spannendes Meal!<br>Social Meal';
				$h2t = new Plugin_Mailer_html2text($mailtext);
				$mail = new Plugin_PHPMailer_PHPMailer;
				$mail->IsSMTP();
				$mail->AddAddress($userdata->email);
				$mail->WordWrap = 50;
				$mail->IsHTML(true);
				$mail->Subject = $subject;
				$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
				$mail->AltBody = $h2t->get_text();
				if(!$mail->Send()){
					echo('Mail Error:'.$mail->ErrorInfo);
				}
			}
		}
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_events SET friendmail = '1' WHERE id = '".$eventid."' LIMIT 1");
	}

	public static function deleteEvent($eventid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_events WHERE id = '".$eventid."' LIMIT 1");
	}

	public static function getOrgEvents($orgid){
		$orgid = DataBase::Current()->EscapeString($orgid);
		if($orgid > 0){
			return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_events WHERE orgid = '".$orgid."'");
		}
	}

	public static function formatDate($datum){
		$date = new DateTime($datum);
		$datum = $date->format('l, d.m.Y, H:i');
		$datum = self::translateDate($datum);
		return $datum;
	}
	
	public static function translateDate($datum){
		$eng = array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
		$deu = array('Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag','Sonntag');
		$datum = str_replace($eng,$deu,$datum);
		return $datum;
	}

	public static function shortText($text,$id,$maxlen = 1000){
		$bb = new Plugin_BBCode_Translator();
		$text = $bb->replace($text);
		if(strlen($text) > $maxlen){
			$text = substr($text,0, $maxlen);
			$text = preg_replace('#[^\s]*$#s', '', $text) . ' <a href="/event-details.html?event='.$id.'&readmore=true">Mehr lesen</a>';
		}
		return $text;
	}

	public static function shortText2($text,$id,$maxlen = 1000){
		$bb = new Plugin_BBCode_Translator();
		$text = $bb->replace($text);
		if(strlen($text) > $maxlen){
			$text = substr($text,0, $maxlen);
			$text = preg_replace('#[^\s]*$#s', '', $text) . ' ...';
		}
		return $text;
	}


	public static function eventHasRating($eventid,$userid,$rateid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		$userid = DataBase::Current()->EscapeString($userid);
		$rateid = DataBase::Current()->EscapeString($rateid);
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_ratings WHERE eventid = '".$eventid."' AND userid = '".$userid."' AND rateid = '".$rateid."'");
	}

	public static function saveRate($eventid,$userid,$rateid,$data){
		$eventid = DataBase::Current()->EscapeString($eventid);
		$stars = DataBase::Current()->EscapeString($data['stars']);
		$userid = DataBase::Current()->EscapeString($userid);
		$rateid = DataBase::Current()->EscapeString($rateid);
		$text = DataBase::Current()->EscapeString($data['text']);
		DataBase::Current()->Execute("INSERT INTO {'dbprefix'}plugin_ratings (`datum`, `stars`,`userid`, `rateid`, `eventid`, `text`) VALUES (NOW(), '$stars','$userid', '$rateid', '$eventid', '$text')");
	}

	public static function deleteRatingsPerEvent($eventid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_ratings WHERE eventid = '".$eventid."'");
	}

	public static function countRatings($userid){
		$userid = DataBase::Current()->EscapeString($userid);
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_ratings WHERE rateid = '".$userid."'");
	}

	public static function saveReplyRate($rateid,$text){
		$rateid = DataBase::Current()->EscapeString($rateid);
		$text = DataBase::Current()->EscapeString($text);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_ratings SET replydatum = NOW(), ratereply = '".$text."' WHERE id = '".$rateid."'");
	}

	public static function eventExist($eventid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_events WHERE id = '".$eventid."'");
	}

	public static function getRatings($userid){
		$userid = DataBase::Current()->EscapeString($userid);
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_ratings WHERE rateid = '".$userid."' ORDER BY id DESC");
	}

	public static function getRating($id){
		$id = DataBase::Current()->EscapeString($id);
		return DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}plugin_ratings WHERE id = '".$id."'");
	}

	public static function updateRate($data,$id){
		$id = DataBase::Current()->EscapeString($id);
		$stars = DataBase::Current()->EscapeString($data['stars']);
		$datum = DataBase::Current()->EscapeString($data['datum']);
		$text = DataBase::Current()->EscapeString($data['text']);
		$replydatum = DataBase::Current()->EscapeString($data['replydatum']);
		$ratereply = DataBase::Current()->EscapeString($data['ratereply']);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_ratings SET stars = '".$stars."', datum = '".$datum."', text = '".$text."', replydatum = '".$replydatum."', ratereply = '".$ratereply."' WHERE id = '".$id."' LIMIT 1");
	}

	public static function updateSendmail($eventid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_events SET sendmail = '1' WHERE id = '".$eventid."' LIMIT 1");
	}

	public static function sendAdress($userid,$eventid){
		$userdata = Plugin_Profile_Manager::getUserDataPerId($userid);
		$eventdata = self::loadEvent($eventid);
		$usergeberdata = Plugin_Profile_Manager::getUserDataPerId($eventdata->userid);
		if($eventdata->eventtype == 3 && $eventdata->orgid != 0){
			$orgdata = Plugin_Profile_Manager::loadOrganisation($eventdata->orgid);
			$from = 'vom Organisationsprofil <a href="'.Settings::getInstance()->get("host").'organisationsprofil.html?profile='.$orgdata->id.'">'.$orgdata->orgname.'</a>';
			$orgname = $orgdata->orgname.'<br>';
			if($orgdata->asmailtel == 1 && $orgdata->orgtel != ''){
				$tel = $orgdata->orgtel;
			}else{
				$tel = $usergeberdata->tel;
			}
		}else{
			$from = 'vom User <a href="'.Settings::getInstance()->get("host").'mein-profil.html?profile='.$usergeberdata->name.'">'.$usergeberdata->name.'</a>';
			$orgname = '';
			$tel = $usergeberdata->tel;
		}
		if($eventdata->preis > 0){
			$mealpreis = $eventdata->preis.' CHF';
		}else{
			$mealpreis = Language::DirectTranslate('plugin_events_price'.$eventdata->specialprice);
		}
		if($eventdata->rest != ''){
			$placeholder = '<br>'.$eventdata->rest.'<br>'; 
		}else{
			$placeholder = '';
		}
		$subject = utf8_decode('Das Meal '.$eventdata->eventname.', zu welchem du dich auf Social Meal angemeldet hast, findet bald statt');
		$mailtext = 'Liebe*r '.ucfirst($userdata->firstname).'<br><br>Das Meal <a href="'.Settings::getInstance()->get("host").'event-details.html?event='.$eventdata->id.'">'.$eventdata->eventname.'</a> '.$from.', für welches du dich angemeldet hast, findet am <b>'.self::formatDate($eventdata->eventdate).'</b> statt!<br><br>Deshalb erhältst du mit diesem E-Mail die vollständige Anschrift von deinem*r Gastgeber*in:'.$orgname.'<br>'.$placeholder.''.ucfirst($usergeberdata->firstname).' '.ucfirst($usergeberdata->lastname).'<br>'.ucfirst($eventdata->strasse).' '.$eventdata->strnr.'<br>'.$eventdata->plz.' '.ucfirst($eventdata->ort).' ('.$eventdata->kanton.')<br>Tel: '.$tel.'<br><br>Preis für das Meal: '.$mealpreis.'.';
		if($eventdata->bnb != ''){
			$mailtext .= ' (oder '.$eventdata->bnb.' <a href="'.Settings::getInstance()->get("host").'Partner/bnb-bonnetzbon.html">BNB</a>)';
		}
		$mailtext .= ' Achte bitte darauf, dass du passend bezahlen kannst.';
		if($eventdata->mitnehmen != ''){
			$mailtext .= '<br><br>Der*die Gastgeber*in bittet dich folgendes mitzubringen: '.$eventdata->mitnehmen.'';
		}
		$mailtext .= '<br><br>Für ein gutes Erlebnis für dich und alle anderen User möchten wir dir die Befolgung unserer Tipps, <a href="'.Settings::getInstance()->get("host").'FAQ/das-solltest-du-als-gast-beachten.html">Das solltest du als Gast beachten</a>, nahelegen.<br><br>Wir möchten dich darauf hinweisen, dass deine Teilnahme an dem Meal einer verbindlichen Zusage entspricht und auf unsere <a href="'.Settings::getInstance()->get("host").'agb.html">AGBs</a> verweisen. Unter Angabe eines Grundes kannst du deine <a href="'.Settings::getInstance()->get("host").'event-abmelden.html?event='.$eventdata->id.'">Teilnahme absagen</a>, falls die Anmeldefrist für das Meal noch nicht abgelaufen ist. Wir hoffen jedoch, dass du diese Funktion nur zurückhaltend gebrauchst. Nach Ablauf der Anmeldefrist kannst du dich nicht mehr über Social Meal abmelden, in Notsituationen empfehlen wir dir die direkte Kontaktaufnahme mit deinem*r Gastgeber*in. Umgekehrt kann dich der*die Gastgeber*in unter Angabe eines Grundes bis zum Anmeldeschluss ausladen oder das Meal komplett absagen.<br><br>Wir wünschen dir ein spannendes Meal mit den anderen Usern!<br>Social Meal<br><br>P.S.: Dieser E-Mail-Typ ist nicht abbestellbar, da er für die Funktion der Plattform essentielle Inhalte übermittelt.';
		$h2t = new Plugin_Mailer_html2text($mailtext);
		$mail = new Plugin_PHPMailer_PHPMailer;
		$mail->IsSMTP();
		$mail->AddAddress($userdata->email);
		$mail->WordWrap = 50;
		$mail->IsHTML(true);
		$mail->Subject = $subject;
		$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
		$mail->AltBody = $h2t->get_text();
		if(!$mail->Send()){
			echo('Mail Error:'.$mail->ErrorInfo);
		}
	}
	
	public static function FBPublishEvent($title,$url,$datum,$image){
		$fb = new Facebook\Facebook([
			'app_id' => Settings::getInstance()->get("fbAppID"),
			'app_secret' => Settings::getInstance()->get("fbAppSecret"),
			'default_graph_version' => Settings::getInstance()->get("fbAppGraphVersion"),
		]);
		$page_id = Settings::getInstance()->get("fbPageID");
		$params["message"] = 'Neues Angebot auf Social Meal: '.$title.', am '.$datum.'. Anmeldung: '.$url.'';
		$params["link"] = $url;
		$params["picture"] = $image;
		$params["description"] = 'Weitere tolle Angebote findest du auf unserer Webseite. Melde dich gleich an um an einem leckeren Essen teilzunehmen!';
		#$params["description"] = 'Neues '.utf8_encode(Language::DirectTranslate('plugin_events_eventtype'.$eventtype)).': '.$title.' am '.$datum.'';
		$page_access_token = Settings::getInstance()->get("fbPageToken");
		// post to Facebook Page
		try {
			$response = $fb->post('/'.$page_id.'/feed', $params, $page_access_token);
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}
	}
	
	public static function tweetEvent($title,$url,$datum,$eventtype){
		if(strlen($title) >= 50){
			$title = substr($title,0,47);
		}
		$newEventText = 'Neues '.utf8_encode(Language::DirectTranslate('plugin_events_eventtype'.$eventtype)).': '.$title.' am '.$datum.' #SocialMeal Anmeldung: '.$url;
		$tmh0Auth = new Plugin_Twitter_tmhAuth;
		$response = $tmh0Auth->request('POST', $tmh0Auth->url('1.1/statuses/update'), array('status' => $newEventText));
		if($response != 200){
			echo('FEHLER BEI TWITTER');
		}
	}

	public static function getPopularEvents(){
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_events WHERE anmeldeschluss > NOW() AND active = '1' ORDER BY counter DESC LIMIT 3");
	}

	public static function getNewEvents($anzahl = 3,$cat = 'none'){
		$anzahl = DataBase::Current()->EscapeString($anzahl);
		if($cat == 'none'){
			$ret = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_events WHERE anmeldeschluss > NOW() AND active = '1' ORDER BY id DESC LIMIT ".$anzahl."");
		}elseif($cat == 'fleisch'){
			$ret = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_events WHERE anmeldeschluss > NOW() AND active = '1' AND fleisch = '1' ORDER BY id DESC LIMIT ".$anzahl."");
		}elseif($cat == 'vegan'){
			$ret = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_events WHERE anmeldeschluss > NOW() AND active = '1' AND vegan = '1' ORDER BY id DESC LIMIT ".$anzahl."");
		}elseif($cat == 'vegi'){
			$ret = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_events WHERE anmeldeschluss > NOW() AND active = '1' AND vegetarisch = '1' ORDER BY id DESC LIMIT ".$anzahl."");
		}
		return $ret;
	}

	public static function newstEvent(){
		return DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}plugin_events WHERE anmeldeschluss > NOW() AND active = '1' ORDER BY id DESC LIMIT 1");
	}

	public static function setAfterMailDone($eventid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_events SET aftereventmail = '1' WHERE id = '".$eventid."' LIMIT 1");
	}

	public static function setAsMailDone($eventid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_events SET afterasmail = '1' WHERE id = '".$eventid."' LIMIT 1");
	}

	public static function setAsRead($eventid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		$event = self::loadEvent($eventid);
		if($event->active == 1){
			$count = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_eventread WHERE userid = '".User::Current()->id."' AND eventid = '".$eventid."'");
			if($count == 0){
				DataBase::Current()->Execute("INSERT INTO {'dbprefix'}plugin_eventread SET userid = '".User::Current()->id."', eventid = '".$eventid."'");
			}
		}
	}

	public static function setAllAsReadFromUser($userid){
		$userid = DataBase::Current()->EscapeString($userid);
		foreach(DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_events WHERE active = '1'") as $upevent){
			$count = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_eventread WHERE userid = '".$userid."' AND eventid = '".$upevent->id."'");
			if($count == 0){
				DataBase::Current()->Execute("INSERT INTO {'dbprefix'}plugin_eventread SET userid = '".$userid."', eventid = '".$upevent->id."'");
			}
		}
	}

	public static function setAllAsReadFromEvent($eventid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		foreach(DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}user") as $user){
			$count = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_eventread WHERE userid = '".$user->id."' AND eventid = '".$eventid."'");
			if($count == 0){
				DataBase::Current()->Execute("INSERT INTO {'dbprefix'}plugin_eventread SET userid = '".$user->id."', eventid = '".$eventid."'");
			}
		}
	}

	public static function hasEventRead($eventid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		$count = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_eventread WHERE userid = '".User::Current()->id."' AND eventid = '".$eventid."'");
		if($count == 1){
			return true;
		}else{
			return false;
		}
	}

	public static function delEventRead($eventid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_eventread WHERE eventid = '".$eventid."'");
	}

	public static function updateComment($eventid,$comment){
		$eventid = DataBase::Current()->EscapeString($eventid);
		$comment = DataBase::Current()->EscapeString($comment);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_events SET comment = '".$comment."', commentdate = NOW() WHERE id = '".$eventid."' LIMIT 1");
	}

	public static function getAllPromoMeals(){
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_promopartner ORDER BY id DESC");
	}

	public static function getPromoMeal($id){
		$id = DataBase::Current()->EscapeString($id);
		return DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}plugin_promopartner WHERE id = '".$id."'");
	}

	public static function createPromo($name){
		$name = DataBase::Current()->EscapeString($name);
		DataBase::Current()->Execute("INSERT INTO {'dbprefix'}plugin_promopartner SET name = '".$name."'");
	}

	public static function deletePromo($id){
		$id = DataBase::Current()->EscapeString($id);
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_promopartner WHERE id = '".$id."' LIMIT 1");
	}

	public static function updatePromo($id,$data){
		$id				= DataBase::Current()->EscapeString($id);
		$name			= DataBase::Current()->EscapeString($data['name']);
		$promotion		= DataBase::Current()->EscapeString($data['promotion']);
		$mealtitle		= DataBase::Current()->EscapeString($data['mealtitle']);
		$desc			= DataBase::Current()->EscapeString($data['desc']);
		$street			= DataBase::Current()->EscapeString($data['street']);
		$strnr			= DataBase::Current()->EscapeString($data['strnr']);
		$plz			= DataBase::Current()->EscapeString($data['plz']);
		$ort			= DataBase::Current()->EscapeString($data['ort']);
		$kanton			= DataBase::Current()->EscapeString($data['kanton']);
		$weblink		= DataBase::Current()->EscapeString($data['weblink']);
		$restmail		= DataBase::Current()->EscapeString($data['restmail']);
		$img			= DataBase::Current()->EscapeString($data['img']);
		$speisekarte	= DataBase::Current()->EscapeString($data['speisekarte']);
		$ov				= DataBase::Current()->EscapeString($data['ov']);
		$parking		= DataBase::Current()->EscapeString($data['parking']);
		$fleisch		= DataBase::Current()->EscapeString($data['fleisch']);
		$vegetarisch	= DataBase::Current()->EscapeString($data['vegetarisch']);
		$vegan			= DataBase::Current()->EscapeString($data['vegan']);
		$rollchair		= DataBase::Current()->EscapeString($data['rollchair']);
		$createnotice	= DataBase::Current()->EscapeString($data['createnotice']);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_promopartner SET name = '".$name."', promotion = '".$promotion."', mealtitle = '".$mealtitle."', description = '".$desc."', street = '".$street."', strnr = '".$strnr."', plz = '".$plz."', ort = '".$ort."', kanton = '".$kanton."', weblink = '".$weblink."', img = '".$img."', speisekarte = '".$speisekarte."', ov = '".$ov."', parking = '".$parking."', fleisch = '".$fleisch."', vegetarisch = '".$vegetarisch."', vegan = '".$vegan."', rollchair = '".$rollchair."', createnotice = '".$createnotice."', restmail = '".$restmail."' WHERE id = '".$id."' LIMIT 1");
	}
	
	public static function closeSubscribe($eventid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_events SET maxguest = '-1' WHERE id = '".$eventid."' LIMIT 1");
	}
	
	public static function openSubscribe($eventid){
		$eventid = DataBase::Current()->EscapeString($eventid);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_events SET maxguest = '0' WHERE id = '".$eventid."' LIMIT 1");
	}
}
?>
