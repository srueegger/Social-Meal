<?PHP
/*
 * new.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_Events_New extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			if(Mobile::isMobileDevice()){
				$template->load('plugin_events_newpage.mobile');
			}else{
				$template->load('plugin_events_newpage');
			}
			$template->show_if('showform',true);
			$template->show_if('showok',false);
			if(isset($_POST['plugin_events_new']) && $_POST['eventname'] != ''){
				$id = Plugin_Events_Manager::createEvent($_POST);
				$template->show_if('showform',false);
				$template->show_if('showok',true);
				$template->assign_var('eventid',$id);
			}
			if(isset($_POST['plugin_events_promo']) && $_POST['promomeal'] != 0){
				$promo = Plugin_Events_Manager::getPromoMeal($_POST['promomeal']);
				$promomeal = array();
				$promomeal['promo']				= $_POST['promomeal'];
				$promomeal['eventname']			= $promo->mealtitle;
				$promomeal['eventtype']			= 1;
				$promomeal['street']			= ucfirst($promo->street);
				$promomeal['strnr']				= $promo->strnr;
				$promomeal['plz']				= $promo->plz;
				$promomeal['ort']				= ucfirst($promo->ort);
				$promomeal['kanton']			= $promo->kanton;
				$promomeal['rollchair']			= $promo->rollchair;
				$promomeal['fleisch']			= $promo->fleisch;
				$promomeal['vegi']				= $promo->vegetarisch;
				$promomeal['vegan']				= $promo->vegan;
				$promomeal['ov']				= $promo->ov;
				$promomeal['parking']			= $promo->parking;
				$promomeal['rest']				= $promo->name;
				$promomeal['symbolimg']			= 1;
				$promomeal['weblink']			= $promo->weblink;
				$neweventid = Plugin_Events_Manager::createEvent($promomeal);
				$promomeal['id']				= $neweventid;
				Plugin_Events_Manager::insertImage($neweventid,$promo->img);
				Plugin_Events_Manager::updateEvent($neweventid,$promomeal);
				$template->show_if('showform',false);
				$template->show_if('showok',true);
				$template->assign_var('eventid',htmlentities($neweventid));
			}
			if(isset($_POST['plugin_events_oldevent']) && $_POST['oldevent'] != 0){
				$oldevent = Plugin_Events_Manager::loadEvent($_POST['oldevent']);
				$oldeventarray = array();
				$oldeventarray['eventname']		= $oldevent->eventname;
				$oldeventarray['eventtype']		= $oldevent->eventtype;
				$oldeventarray['street']		= ucfirst($oldevent->strasse);
				$oldeventarray['strnr']			= $oldevent->strnr;
				$oldeventarray['plz']			= $oldevent->plz;
				$oldeventarray['ort']			= ucfirst($oldevent->ort);
				$oldeventarray['kanton']		= $oldevent->kanton;
				$oldeventarray['preis']			= $oldevent->preis;
				$oldeventarray['bnb']			= $oldevent->bnb;
				$oldeventarray['gange']			= $oldevent->gaenge;
				$oldeventarray['maxguest']		= $oldevent->maxguest;
				$oldeventarray['drinks']		= $oldevent->drinks;
				$oldeventarray['rauchen']		= $oldevent->rauchen;
				$oldeventarray['rollchair']		= $oldevent->rollchair;
				$oldeventarray['abouteat']		= $oldevent->beschreibung;
				$oldeventarray['fleisch']		= $oldevent->fleisch;
				$oldeventarray['fisch']			= $oldevent->fisch;
				$oldeventarray['seafood']		= $oldevent->seafood;
				$oldeventarray['vegi']			= $oldevent->vegetarisch;
				$oldeventarray['vegan']			= $oldevent->vegan;
				$oldeventarray['saisonal']		= $oldevent->saisonal;
				$oldeventarray['bio']			= $oldevent->bio;
				$oldeventarray['regional']		= $oldevent->regional;
				$oldeventarray['lactose']		= $oldevent->lactose;
				$oldeventarray['nusse']			= $oldevent->nusse;
				$oldeventarray['gluten']		= $oldevent->glutenfrei;
				$oldeventarray['alkohol']		= $oldevent->alkohol;
				$oldeventarray['ov']			= $oldevent->ov;
				$oldeventarray['parking']		= $oldevent->parking;
				$oldeventarray['dauer']			= $oldevent->dauer;
				$oldeventarray['rest']			= $oldevent->rest;
				$oldeventarray['org']			= $oldevent->orgid;
				$oldeventarray['option1']		= $oldevent->option1;
				$oldeventarray['option2']		= $oldevent->option2;
				$oldeventarray['option3']		= $oldevent->option3;
				$oldeventarray['mitnehmen']		= $oldevent->mitnehmen;
				$oldeventarray['symbolimg']		= $oldevent->symbolimg;
				$oldeventarray['openaddress']	= $oldevent->openaddress;
				$oldeventarray['weblink']		= $oldevent->weblink;
				$oldeventarray['specialpreis']	= $oldevent->specialprice;
				$oldeventarray['scharf']		= $oldevent->scharf;
				$oldeventarray['hefe']			= $oldevent->hefe;
				$neweventid = Plugin_Events_Manager::createEvent($oldeventarray);
				$oldeventarray['id']			= $neweventid;
				Plugin_Events_Manager::updateEvent($neweventid,$oldeventarray);
				if(Plugin_Events_Manager::countImagesPerEvent($_POST['oldevent']) != 0){
					foreach(Plugin_Events_Manager::getImagesPerEvent($_POST['oldevent']) as $image){
						Plugin_Events_Manager::insertImage($neweventid,$image->filename);
					}
				}
				$template->show_if('showform',false);
				$template->show_if('showok',true);
				$template->assign_var('eventid',htmlentities($neweventid));
			}
			if(isset($_GET['eventtype']) && !empty($_GET['eventtype'])){
				$template->assign_var('selected'.$_GET['eventtype'],'selected="selected"');
			}
			$template->assign_var('selected0','');
			$template->assign_var('selected1','');
			$template->assign_var('selected2','');
			$template->assign_var('selected3','');
			foreach(Plugin_Events_Manager::getAllPromoMeals() as $promo){
				$index = $template->add_loop_item('promo');
				$template->assign_loop_var("promo",$index,"id",htmlentities($promo->id));
				$template->assign_loop_var("promo",$index,"name",htmlentities($promo->name));
				$template->assign_loop_var("promo",$index,"promotion",htmlentities($promo->promotion));
			}
			foreach(Plugin_Events_Manager::getOldEventsByUser(User::Current()->id) as $oldevent){
				$index = $template->add_loop_item('oldevents');
				$template->assign_loop_var("oldevents",$index,"oldeventid",htmlentities($oldevent->id));
				$template->assign_loop_var("oldevents",$index,"oldeventname",htmlentities($oldevent->eventname));
				$template->assign_loop_var("oldevents",$index,"oldeventdatum",Plugin_Events_Manager::formatDate($oldevent->eventdate));
			}
			echo($template->getCode());
		}

		function getHeader(){
			echo('<script type="text/javascript" src="/system/plugins/events/js/new.js"></script>');
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
