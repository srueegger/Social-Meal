<?PHP
/*
 * adminedit.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_Events_AdminEdit extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			$template->load('plugin_events_adminedit');
			$template->show_if('edit',false);
			$template->show_if('noevent',false);
			$template->show_if('saveok',false);
			$template->show_if('images',false);
			$template->show_if('delok',false);
			$template->show_if('imageform',false);
			$template->show_if('mealdelok',false);
			if(isset($_POST['mealid']) or isset($_GET['mealid'])){
				if(empty($_POST['mealid'])){
					$mealid = $_GET['mealid'];
				}else{
					$mealid = $_POST['mealid'];
				}
				if(isset($_GET['mealid']) && isset($_GET['delmeal'])){
					foreach(Plugin_Events_Manager::getSubscribesByEvent($mealid) as $subscriber){
						Plugin_Events_Manager::removeSubscriber($_GET['mealid'],$subscriber->userid);
					}
					foreach(Plugin_Events_Manager::getImagesPerEvent($_GET['mealid']) as $image){
						Plugin_Events_Manager::deleteImage($image->id);
					}
					Plugin_Events_Manager::deleteQuestions($_GET['mealid']);
					Plugin_Events_Manager::deleteEvent($_GET['mealid']);
					Plugin_Events_Manager::delEventRead($_GET['mealid']);
					$template->show_if('mealdelok',true);
				}
				$template->assign_var('eventid',htmlentities($mealid));
				if(Plugin_Events_Manager::eventExist($mealid) == 1){
					$template->show_if('edit',true);
					if(isset($_POST['editsave'])){
						Plugin_Events_Manager::updateAdminEvent($_POST);
						if(isset($_FILES['files'])){
							Plugin_Events_Manager::uploadFiles($mealid,$_FILES);
						}
						$template->show_if('saveok',true);
					}
					if(isset($_GET['delimg'])){
						Plugin_Events_Manager::deleteImage($_GET['delimg']);
						$template->show_if('delok',true);
					}
					$event = Plugin_Events_Manager::loadEvent($mealid);
					if($event->active == 1){
						$template->assign_var('active','Ja');
					}elseif($event->active == 0){
						$template->assign_var('active','Nein');
					}
					$template->assign_var('eventtype',utf8_encode(Language::DirectTranslate('plugin_events_eventtype'.htmlentities($event->eventtype))));
					if($event->orgid == 0){
						$template->assign_var('orgname','Kein Organisationsevent');
					}else{
						$template->assign_var('orgname',htmlentities(Plugin_Profile_Manager::loadOrganisation($event->orgid)->orgname));
					}
					if($event->promo == 1){
						$template->assign_var('promo','Ja');
					}elseif($event->promo == 0){
						$template->assign_var('promo','Nein');
					}
					if($event->friendmail == 1){
						$template->assign_var('friendmail','Ja');
					}elseif($event->friendmail == 0){
						$template->assign_var('friendmail','Nein');
					}
					if($event->afterasmail == 1){
						$template->assign_var('afterasmail','Ja');
					}elseif($event->afterasmail == 0){
						$template->assign_var('afterasmail','Nein');
					}
					if($event->aftereventmail == 1){
						$template->assign_var('aftereventmail','Ja');
					}elseif($event->aftereventmail == 0){
						$template->assign_var('aftereventmail','Nein');
					}
					if($event->symbolimg == 1){
						$template->assign_var('symbolimg','checked="checked"');
					}else{
						$template->assign_var('symbolimg','');
					}
					$template->assign_var('user',htmlentities(Plugin_Profile_Manager::getUserDataPerId($event->userid)->name));
					$template->assign_var('eventname',htmlentities($event->eventname));
					$template->assign_var('beschreibung',htmlentities($event->beschreibung));
					$template->assign_var('eventdate',htmlentities($event->eventdate));
					$template->assign_var('anmeldeschluss',htmlentities($event->anmeldeschluss));
					$template->assign_var('preis',htmlentities($event->preis));
					$template->assign_var('bnb',htmlentities($event->bnb));
					$template->assign_var('rest',htmlentities($event->rest));
					$template->assign_var('weblink',htmlentities($event->weblink));
					$template->assign_var('counter',htmlentities($event->counter));
					$template->assign_var('strasse',htmlentities($event->strasse));
					$template->assign_var('strnr',htmlentities($event->strnr));
					$template->assign_var('plz',htmlentities($event->plz));
					$template->assign_var('ort',htmlentities($event->ort));
					$template->assign_var('kanton',htmlentities($event->kanton));
					$template->assign_var('mitnehmen',htmlentities($event->mitnehmen));
					$template->assign_var('gaenge',htmlentities($event->gaenge));
					$template->assign_var('maxguest',htmlentities($event->maxguest));
					$template->assign_var('option1',htmlentities($event->option1));
					$template->assign_var('option2',htmlentities($event->option2));
					$template->assign_var('option3',htmlentities($event->option3));
					$template->assign_var('comment',htmlentities($event->comment));
					$template->assign_var('commentdate',htmlentities($event->commentdate));
					$template->assign_var('smoke'.htmlentities($event->rauchen),'selected="selected"');
					$template->assign_var('drink'.htmlentities($event->drinks),'selected="selected"');
					$template->assign_var('ov'.htmlentities($event->ov),'selected="selected"');
					$template->assign_var('parking'.htmlentities($event->parking),'selected="selected"');
					$template->assign_var('rollchair'.htmlentities($event->rollchair),'selected="selected"');
					$template->assign_var('dauer'.htmlentities($event->dauer),'selected="selected"');
					$template->assign_var('specialpreis'.htmlentities($event->specialprice),'selected="selected"');
					$template->assign_var('specialbnb'.htmlentities($event->bnbprice),'selected="selected"');
					$template->assign_var('fleisch'.htmlentities($event->fleisch),'selected="selected"');
					$template->assign_var('fisch'.htmlentities($event->fisch),'selected="selected"');
					$template->assign_var('seafood'.htmlentities($event->seafood),'selected="selected"');
					$template->assign_var('vegetarisch'.htmlentities($event->vegetarisch),'selected="selected"');
					$template->assign_var('vegan'.htmlentities($event->vegan),'selected="selected"');
					$template->assign_var('saisonal'.htmlentities($event->saisonal),'selected="selected"');
					$template->assign_var('bio'.htmlentities($event->bio),'selected="selected"');
					$template->assign_var('lactose'.htmlentities($event->lactose),'selected="selected"');
					$template->assign_var('nusse'.htmlentities($event->nusse),'selected="selected"');
					$template->assign_var('glutenfrei'.htmlentities($event->glutenfrei),'selected="selected"');
					$template->assign_var('regional'.htmlentities($event->regional),'selected="selected"');
					$template->assign_var('alkohol'.htmlentities($event->alkohol),'selected="selected"');
					$template->assign_var('scharf'.htmlentities($event->scharf),'selected="selected"');
					$template->assign_var('hefe'.htmlentities($event->hefe),'selected="selected"');
					$template->assign_var('koscher'.htmlentities($event->koscher),'selected="selected"');
					$template->assign_var('halal'.htmlentities($event->halal),'selected="selected"');
					$template->assign_var('openaddress'.htmlentities($event->openaddress),'selected="selected"');
					$template->assign_var('smoke0','');
					$template->assign_var('smoke1','');
					$template->assign_var('smoke2','');
					$template->assign_var('drink0','');
					$template->assign_var('drink1','');
					$template->assign_var('drink2','');
					$template->assign_var('ov0','');
					$template->assign_var('ov1','');
					$template->assign_var('ov2','');
					$template->assign_var('parking0','');
					$template->assign_var('parking1','');
					$template->assign_var('parking2','');
					$template->assign_var('parking3','');
					$template->assign_var('dauer1','');
					$template->assign_var('dauer2','');
					$template->assign_var('dauer3','');
					$template->assign_var('dauer4','');
					$template->assign_var('rollchair0','');
					$template->assign_var('rollchair1','');
					$template->assign_var('specialpreis0','');
					$template->assign_var('specialpreis1','');
					$template->assign_var('specialpreis2','');
					$template->assign_var('specialpreis3','');
					$template->assign_var('specialpreis4','');
					$template->assign_var('specialpreis5','');
					$template->assign_var('specialpreis6','');
					$template->assign_var('specialbnb0','');
					$template->assign_var('specialbnb1','');
					$template->assign_var('fleisch0','');
					$template->assign_var('fleisch1','');
					$template->assign_var('fleisch2','');
					$template->assign_var('fisch0','');
					$template->assign_var('fisch1','');
					$template->assign_var('fisch2','');
					$template->assign_var('seafood0','');
					$template->assign_var('seafood1','');
					$template->assign_var('seafood2','');
					$template->assign_var('vegetarisch0','');
					$template->assign_var('vegetarisch1','');
					$template->assign_var('vegetarisch2','');
					$template->assign_var('vegan0','');
					$template->assign_var('vegan1','');
					$template->assign_var('vegan2','');
					$template->assign_var('saisonal0','');
					$template->assign_var('saisonal1','');
					$template->assign_var('saisonal2','');
					$template->assign_var('bio0','');
					$template->assign_var('bio1','');
					$template->assign_var('bio2','');
					$template->assign_var('lactose0','');
					$template->assign_var('lactose1','');
					$template->assign_var('lactose2','');
					$template->assign_var('nusse0','');
					$template->assign_var('nusse1','');
					$template->assign_var('nusse2','');
					$template->assign_var('glutenfrei0','');
					$template->assign_var('glutenfrei1','');
					$template->assign_var('glutenfrei2','');
					$template->assign_var('regional0','');
					$template->assign_var('regional1','');
					$template->assign_var('regional2','');
					$template->assign_var('alkohol0','');
					$template->assign_var('alkohol1','');
					$template->assign_var('alkohol2','');
					$template->assign_var('scharf0','');
					$template->assign_var('scharf1','');
					$template->assign_var('scharf2','');
					$template->assign_var('hefe0','');
					$template->assign_var('hefe1','');
					$template->assign_var('hefe2','');
					$template->assign_var('koscher0','');
					$template->assign_var('koscher1','');
					$template->assign_var('koscher2','');
					$template->assign_var('halal0','');
					$template->assign_var('halal1','');
					$template->assign_var('halal2','');
					$template->assign_var('openaddress0','');
					$template->assign_var('openaddress1','');
					if(Plugin_Events_Manager::countImagesPerEvent($mealid) > 0){
						$template->show_if('images',true);
						foreach(Plugin_Events_Manager::getImagesPerEvent($mealid) as $image){
							$index = $template->add_loop_item('images');
							$template->assign_loop_var("images",$index,"imgid",htmlentities($image->id));
							$template->assign_loop_var("images",$index,"imgurl",Settings::getInstance()->get("host").'content/uploads/eat/thumbs/'.htmlentities($image->filename));
							$template->assign_loop_var("images",$index,"eventid",htmlentities($image->eventid));
						}
					}
					if(Plugin_Events_Manager::countImagesPerEvent($mealid) < 5){
						$template->show_if('imageform',true);
					}
				}else{
					$template->show_if('noevent',true);
				}
			}else{
				$template->assign_var('eventid','');
			}
			echo($template->getCode());
		}

		function getHeader(){
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
