<?php 
/*
 * widget.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
class Plugin_Events_Widget extends Plugin_BlankPage_Page{
	public function display(){
		$template = new Template();
		$template->load('plugin_events_widget');
		$template->assign_var('skinpath',sys::getFullSkinPath());
		$template->assign_var('host',Settings::getInstance()->get("host"));
		if(isset($_GET['anzahl'])){
			$anzahl = $_GET['anzahl'];
		}else{
			$anzahl = 3;
		}
		if(isset($_GET['category'])){
			$cat = DataBase::Current()->EscapeString($_GET['category']);
			if($cat == 'fleisch' or $cat == 'vegan' or $cat == 'vegi'){
				$foreach = Plugin_Events_Manager::getNewEvents($anzahl,$cat);
			}else{
				$foreach = Plugin_Events_Manager::getNewEvents($anzahl);
			}
		}else{
			$foreach = Plugin_Events_Manager::getNewEvents($anzahl);
		}
		foreach($foreach as $nevent){
			$index = $template->add_loop_item("content");
			$template->assign_loop_var("content",$index,"host",htmlentities(Settings::getInstance()->get("host")));
			$template->assign_loop_var("content",$index,"id",htmlentities($nevent->id));
			$template->assign_loop_var("content",$index,"name",htmlentities($nevent->eventname));
			$template->assign_loop_var("content",$index,"datum",Plugin_Events_Manager::formatDate($nevent->eventdate));
		}
		echo $template->getCode();
	}
		
	public function getEditableCode(){
		$change = 'Speichern';
		return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
	}
}
?>
