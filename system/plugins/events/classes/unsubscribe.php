<?PHP
/*
 * unsubscribe.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_Events_UnSubscribe extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
			if(isset($_GET['event'])){
				$event = Plugin_Events_Manager::loadEvent($_GET['event']);
				$page->title = Language::DirectTranslate('plugin_events_unsubscribefrommeal').' '.$event->eventname;
			}
		}
		public function display(){
			$template = new Template();
			if(isset($_GET['event']) && Plugin_Events_Manager::hasSubscribe($_GET['event'],User::Current()->id) == 1){
				$template->load('plugin_events_unsubscribe');
				$template->show_if('showunsubscribe',false);
				$template->show_if('unsubscribeok',false);
				$template->show_if('asdate',false);
				$event = Plugin_Events_Manager::loadEvent($_GET['event']);
				if(date('Y-m-d H:i') > $event->anmeldeschluss){
					$template->show_if('asdate',true);
					echo($template->getCode());
					exit;
				}
				$usergeberdata = Plugin_Profile_Manager::getUserDataPerId($event->userid);
				$usergastdata = Plugin_Profile_Manager::getUserDataPerId(User::Current()->id);
				if(!isset($_POST['grund']) && is_object($event)){
					$template->show_if('showunsubscribe',true);
					$template->assign_var('eventname',htmlentities($event->eventname));
					$template->assign_var('eventid',htmlentities($event->id));
				}elseif(isset($_POST['grund']) && is_object($event)){
					$template->show_if('unsubscribeok',true);
					Plugin_Events_Manager::removeSubscriber($_GET['event'],User::Current()->id);
					$mailtext = 'Liebe*r '.ucfirst($usergeberdata->firstname).'<br><br>Es tut uns leid dir mitteilen zu müssen, dass der User <a href="'.Settings::getInstance()->get("host").'mein-profil.html?profile='.$usergastdata->name.'">'.$usergastdata->name.'</a> die Teilnahme an deinem Meal "<a href="'.Settings::getInstance()->get("host").'event-details.html?event='.$event->id.'">'.$event->eventname.'</a>", für welchen er sich angemeldet hatte, abgesagt hat.<br><br>Das ist seine Begründung:<br>'.nl2br(htmlentities($_POST['grund'])).'<br><br>Wir hoffen, dass du eine alternative Zusage zu deinem Meal erhältst. Ansonsten können wir dir empfehlen, unsere Tipps für das erfolgreiche Erstellen von <a href="'.Settings::getInstance()->get("host").'FAQ/erstellung-von-privat-events.html">Privat-Meals</a>, <a href="'.Settings::getInstance()->get("host").'FAQ/erstellung-von-restaurant-events.html">Restaurant-Meals</a> oder <a href="'.Settings::getInstance()->get("host").'FAQ/erstellung-von-spezial-events.html">Spezial-Meals</a> zu berücksichtigen.<br><br>Falls du der Meinung bist, dass dieser User die Funktion zur Teilnahme-Absagung missbräuchlich verwendet, melde uns das bitte über die entsprechende <a href="'.Settings::getInstance()->get("host").'melden.html?source=mail&g=unsubscribeuser_'.$usergastdata->id.'">Melde-Seite</a>.<br><br>Bestimmt erhältst du bald einen anderen Gast!<br>Social Meal<br><br>P.S.: Dieser E-Mail-Typ ist nicht abbestellbar, da er für die Funktion der Plattform essentielle Inhalte übermittelt.';
					$h2t1 = new Plugin_Mailer_html2text($mailtext);
					$mailtext2 = 'Liebe*r '.ucfirst($usergastdata->firstname).'<br><br>Hiermit bestätigen wir dir die Abmeldung vom Meal "<a href="'.Settings::getInstance()->get("host").'event-details.html?event='.$event->id.'">'.$event->eventname.'</a>". Ein E-Mail mit deiner Begründung wurde an den*die Gastgeber*in geschickt.<br><br>Wir hoffen, dass du ein alternatives Meal unter <a href="'.Settings::getInstance()->get("host").'essen-gehen.html">An einem Meal teilnehmen</a> findest und empfehlen dir einen Blick auf <a href="'.Settings::getInstance()->get("host").'FAQ/so-findest-du-das-richtige-angebot.html">So findest du das richtige Angebot</a>. Ansonsten können wir dir auch empfehlen, selber ein <a href="'.Settings::getInstance()->get("host").'neuen-event-erstellen.html">Meal zu erstellen</a>.<br><br>Wir möchten dich darauf hinweisen, dass Anmeldungen zu Meals bindend sind und die Abmeldefunktion nur in besonderen Fällen genutzt werden sollte.<br><br>Das nächste Meal passt bestimmt!<br>Social Meal<br><br>P.S.: Diesen E-Mail-Typ kannst du in den <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abbestellen, falls er dir überflüssig erscheint.';
					$h2t2 = new Plugin_Mailer_html2text($mailtext2);
					$subject1 = utf8_decode('Ein Gast hat seine Teilnahme an deinem Meal "'.htmlentities($event->eventname).'" auf Social Meal abgesagt');
					$subject2 = utf8_decode('Du hast dich von dem Meal "'.htmlentities($event->eventname).'" auf Social Meal abgemeldet');
					$mail1 = new Plugin_PHPMailer_PHPMailer;
					$mail1->IsSMTP();
					$mail1->AddAddress($usergeberdata->email);
					$mail1->WordWrap = 50;
					$mail1->IsHTML(true);
					$mail1->Subject = $subject1;
					$mail1->Body = Plugin_Mailer_Mailer::makeHtml($subject1,$mailtext);
					$mail1->AltBody = $h2t1->get_text();
					if(!$mail1->Send()){
						echo('Mail Error:'.$mail1->ErrorInfo);
					}
					if(Plugin_Profile_Manager::getUserSetting('unsubscribe',User::Current()->id) == 1){
						$mail2 = new Plugin_PHPMailer_PHPMailer;
						$mail2->IsSMTP();
						$mail2->AddAddress($usergastdata->email);
						$mail2->WordWrap = 50;
						$mail2->IsHTML(true);
						$mail2->Subject = $subject2;
						$mail2->Body = Plugin_Mailer_Mailer::makeHtml($subject2,$mailtext2);
						$mail2->AltBody = $h2t2->get_text();
						if(!$mail2->Send()){
							echo('Mail Error:'.$mail2->ErrorInfo);
						}
					}
				}
			}else{
				//User hat sich gar nicht angemeldet.
			}
			echo($template->getCode());
		}

		function getHeader(){
			echo('<script type="text/javascript" src="'.Settings::getInstance()->get("host").'system/plugins/events/js/unsubscribe.js"></script>');
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
