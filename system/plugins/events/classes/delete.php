<?PHP
/*
 * delete.php
 *
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
	class Plugin_Events_Delete extends Editor{

		/**
		 *
		 * @param Page $page
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			$template->load('plugin_events_delete');
			if(isset($_GET['event']) && !isset($_POST['grund'])){
				$eventdata = Plugin_Events_Manager::loadEvent($_GET['event']);
				if(date('Y-m-d H:i') > $eventdata->anmeldeschluss){
					$template->show_if('showdelete',false);
					$template->show_if('deleteok',false);
					$template->show_if('noright',false);
					$template->show_if('asdate',true);
					echo($template->getCode());
					exit;
				}
				if($eventdata->userid == User::Current()->id){
					$template->show_if('showdelete',true);
					$template->show_if('deleteok',false);
					$template->show_if('noright',false);
					$template->show_if('asdate',false);
					$template->assign_var('eventid',htmlentities($eventdata->id));
					$template->assign_var('eventname',htmlentities($eventdata->eventname));
				}else{
					$template->show_if('showdelete',false);
					$template->show_if('deleteok',false);
					$template->show_if('noright',true);
					$template->show_if('asdate',false);
				}
			}elseif(isset($_GET['event']) && !empty($_POST['grund'])){
				$eventdata = Plugin_Events_Manager::loadEvent($_GET['event']);
				if($eventdata->userid == User::Current()->id){
					$template->show_if('showdelete',false);
					$template->show_if('deleteok',true);
					$template->show_if('noright',false);
					$template->show_if('asdate',false);
					foreach(Plugin_Events_Manager::getSubscribesByEvent($_GET['event']) as $id){
						$userinfo = Plugin_Profile_Manager::getUserDataPerId($id->userid);
						$subject = utf8_decode('Das Meal "'.$eventdata->eventname.'" auf Social Meal, an welchem du teilnehmen wolltest, wurde abgesagt');
						$mailtext = 'Liebe*r '.ucfirst($userinfo->firstname).'<br><br>Es tut uns leid dir mitteilen zu müssen, dass der User <a href="'.Settings::getInstance()->get("host").'mein-profil.html?profile='.User::Current()->name.'">'.User::Current()->name.'</a> sein Meal "'.$eventdata->eventname.'", für welchen du angemeldet warst, abgesagt hat.<br><br>Das ist seine Begründung:<br>'.nl2br(htmlentities($_POST['grund'])).'<br><br>Wir hoffen, dass du ein alternatives Meal unter <a href="'.Settings::getInstance()->get("host").'essen-gehen.html">An einem Meal teilnehmen</a> findest, ansonsten können wir dir auch empfehlen, selber ein <a href="'.Settings::getInstance()->get("host").'neuen-event-erstellen.html">Meal anzubieten</a>.<br><br>Falls du der Meinung bist, dass dieser User die Funktion zur Meal-Absagung missbräuchlich verwendet, melde uns das bitte über die entsprechende <a href="'.Settings::getInstance()->get("host").'melden.html?source=mail&g=delevent_'.$eventdata->id.'">Melde-Seite</a>.<br><br>Bestimmt hast du beim nächsten Meal mehr Glück!<br>Social Meal<br><br>P.S.: Dieser E-Mail-Typ ist nicht abbestellbar, da er für die Funktion der Plattform essentielle Inhalte übermittelt.';
						$h2t = new Plugin_Mailer_html2text($mailtext);
						$mail = new Plugin_PHPMailer_PHPMailer;
						$mail->IsSMTP();
						$mail->AddAddress($userinfo->email);
						$mail->WordWrap = 50;
						$mail->IsHTML(true);
						$mail->Subject = $subject;
						$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
						$mail->AltBody = $h2t->get_text();
						if(!$mail->Send()){
							echo('Mail Error:'.$mail->ErrorInfo);
						}
						Plugin_Events_Manager::removeSubscriber($_GET['event'],$id->userid);
					}
					foreach(Plugin_Events_Manager::getImagesPerEvent($_GET['event']) as $image){
						Plugin_Events_Manager::deleteImage($image->id);
					}
					Plugin_Events_Manager::deleteQuestions($_GET['event']);
					Plugin_Events_Manager::deleteEvent($_GET['event']);
					Plugin_Events_Manager::delEventRead($_GET['event']);
				}else{
					$template->show_if('showdelete',false);
					$template->show_if('deleteok',false);
					$template->show_if('noright',true);
					$template->show_if('asdate',false);
				}
			}
			echo($template->getCode());
		}

		function getHeader(){
			echo('<script type="text/javascript" src="'.Settings::getInstance()->get("host").'system/plugins/events/js/delete.js"></script>');
		}

		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}

		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
