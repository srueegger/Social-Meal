<?php
/*
 * home.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_Events_Home extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			if(Mobile::isMobileDevice()){
				$template->load('plugin_events_home.mobile');
			}else{
				$template->load('plugin_events_home');
			}
			$template->show_if('showallmeals',false);
			if(isset($_GET['readall']) && User::Current()->role->ID > 1){
				Plugin_Events_Manager::setAllAsReadFromUser(User::Current()->id);
			}
			if(User::Current()->isGuest()){
				$template->show_if('notloggedin',true);
				$template->show_if('loggedin',false);
			}else{
				$template->show_if('notloggedin',false);
				$template->show_if('loggedin',true);
			}
			if(Plugin_Events_Manager::countAllActiveEvents() > 0){
				$template->show_if('events',true);
				$template->show_if('noevent',false);
				if(Plugin_Events_Manager::countAllActiveEvents() > 9){
					$template->show_if('showallmeals',true);
				}
				//NEU
				foreach(Plugin_Events_Manager::getAllSpecialLongTime() as $event){
					if(Plugin_Events_Manager::countSubscribers($event->id) < $event->maxguest or $event->maxguest != -1){
						$index = $template->add_loop_item("allevents");
						$template->assign_loop_var("allevents",$index,"id",htmlentities($event->id));
						$template->assign_loop_var("allevents",$index,"name",htmlentities($event->eventname));
						$template->assign_loop_var("allevents",$index,"beschreibung",Plugin_Events_Manager::shortText2($event->beschreibung,$event->id,100));
						if(User::Current()->role->ID > 1){
							if(Plugin_Events_Manager::hasEventRead($event->id) == true){
								$template->assign_loop_var("allevents",$index,"unread","");
							}else{
								$template->assign_loop_var("allevents",$index,"unread",'style="background-color: #B2F0FF;"');
							}
						}else{
							$template->assign_loop_var("allevents",$index,"unread","");
						}
						if(!Mobile::isMobileDevice()){
							if($event->eventtype == 0){
								$template->assign_loop_var("allevents",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype0')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_green').'" /></span></a>');
							}elseif($event->eventtype == 1){
								$template->assign_loop_var("allevents",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype1')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_red').'" /></span></a>');
							}elseif($event->eventtype == 2){
								$template->assign_loop_var("allevents",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype2')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_yellow').'" /></span></a>');
							}elseif($event->eventtype == 3){
								$template->assign_loop_var("allevents",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype3')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('asterisk_orange').'" /></span></a>');
							}
						}else{
							$template->assign_loop_var("allevents",$index,"eventtype",'');
						}
						if(!Mobile::isMobileDevice()){
							if(Plugin_Events_Manager::getFirstImageFromEvent($event->id) != ''){
								$imgurl = '<a href="./content/uploads/eat/'.Plugin_Events_Manager::getFirstImageFromEvent($event->id).'" class="fancyboxpopup"><img style="max-height: 250px; max-width: 450px;" max-width="450px" max-height="250px" src="/content/uploads/eat/thumbs/'.Plugin_Events_Manager::getFirstImageFromEvent($event->id).'" alt="" /></a>';
								$template->assign_loop_var("allevents",$index,"img",$imgurl);
							}else{
								$template->assign_loop_var("allevents",$index,"img",'');
							}
						}else{
							if(Plugin_Events_Manager::getFirstImageFromEvent($event->id) != ''){
								$imgurl = './content/uploads/eat/thumbs/'.Plugin_Events_Manager::getFirstImageFromEvent($event->id).'';
							}else{
								$imgurl = './system/skins/socialmealmobile/images/content/small-1-1.jpg';
							}
							$template->assign_loop_var("allevents",$index,"img",$imgurl);
						}
						if(!Mobile::isMobileDevice()){
							$categorys = ' ';
							if($event->fleisch == 1 or $event->fisch == 1 or $event->seafood == 1){
								$categorys .= 'Fleisch ';
							}
							if($event->vegetarisch == 1){
								$categorys .= 'Vegetarisch ';
							}
							if($event->vegan == 1){
								$categorys .= 'Vegan';
							}
						}else{
							$categorys = '';
							if($event->fleisch == 1 or $event->fisch == 1 or $event->seafood == 1){
								$categorys .= 'Fleisch,';
							}
							if($event->vegetarisch == 1){
								$categorys .= 'Vegetarisch,';
							}
							if($event->vegan == 1){
								$categorys .= 'Vegan';
							}
						}
						if($event->eventtype == 3){
							if($event->maxguest == 0){
								$contentfreeplace = '<span class="bold">'.utf8_encode(Language::DirectTranslate('plugin_events_yes')).'</span>';
								if(Plugin_Events_Manager::countSubscribers($event->id) > 0){
									$contentfreeplace .= ' ('.Language::DirectTranslate(plugin_events_voteyes2).': '.Plugin_Events_Manager::countSubscribers($event->id).')';
								}
								$template->assign_loop_var("allevents",$index,'freeplace',$contentfreeplace);
							}else{
								$freeplace = $event->maxguest - Plugin_Events_Manager::countSubscribers($event->id);
								$template->assign_loop_var("allevents",$index,'freeplace','<span class="bold">'.htmlentities($freeplace).'</span> '.Language::DirectTranslate('plugin_events_by').' '.htmlentities($event->maxguest));
							}
						}else{
							$freeplace = $event->maxguest - Plugin_Events_Manager::countSubscribers($event->id);
							$template->assign_loop_var("allevents",$index,'freeplace','<span class="bold">'.htmlentities($freeplace).'</span> '.Language::DirectTranslate('plugin_events_by').' '.htmlentities($event->maxguest));
						}
						$template->assign_loop_var("allevents",$index,"categorys",$categorys);
						if($event->eventtype == 1){
							$template->assign_loop_var("allevents",$index,"preis",'');
						}elseif($event->eventtype == 3){
							if($event->preis != 0){
								$template->assign_loop_var("allevents",$index,"preis",Language::directTranslate('plugin_events_preis').':<br />'.htmlentities($event->preis).' CHF');
							}elseif($event->specialprice > 0){
								$template->assign_loop_var("allevents",$index,"preis",Language::directTranslate('plugin_events_preis').':<br />'.Language::directTranslate('plugin_events_price'.$event->specialprice));
							}else{
								$template->assign_loop_var("allevents",$index,"preis",'');
							}
						}else{
							$template->assign_loop_var("allevents",$index,"preis",Language::directTranslate('plugin_events_preis').':<br />'.htmlentities($event->preis).' CHF');
						}
						$template->assign_loop_var("allevents",$index,"datum",Plugin_Events_Manager::formatDate($event->eventdate));
						if($event->bnb != 0){
							$template->assign_loop_var("allevents",$index,"bnb",' <small>('.Language::DirectTranslate('plugin_events_or').' '.htmlentities($event->bnb).' <a href="/Partner/bnb-bonnetzbon.html" target="_blank">'.Language::DirectTranslate('plugin_events_bnb').'</a>)</small> ');
						}else{
							$template->assign_loop_var("allevents",$index,"bnb",'');
						}
					}
				}
				//NEU
				foreach(Plugin_Events_Manager::getAllActiveEvents_3() as $event){
					if(Plugin_Events_Manager::countSubscribers($event->id) < $event->maxguest or $event->maxguest != -1){
						$index = $template->add_loop_item("allevents");
						$template->assign_loop_var("allevents",$index,"id",htmlentities($event->id));
						$template->assign_loop_var("allevents",$index,"name",htmlentities($event->eventname));
						$template->assign_loop_var("allevents",$index,"beschreibung",Plugin_Events_Manager::shortText2($event->beschreibung,$event->id,100));
						if(User::Current()->role->ID > 1){
							if(Plugin_Events_Manager::hasEventRead($event->id) == true){
								$template->assign_loop_var("allevents",$index,"unread","");
							}else{
								$template->assign_loop_var("allevents",$index,"unread",'style="background-color: #B2F0FF;"');
							}
						}else{
							$template->assign_loop_var("allevents",$index,"unread","");
						}
						if(!Mobile::isMobileDevice()){
							if($event->eventtype == 0){
								$template->assign_loop_var("allevents",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype0')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_green').'" /></span></a>');
							}elseif($event->eventtype == 1){
								$template->assign_loop_var("allevents",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype1')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_red').'" /></span></a>');
							}elseif($event->eventtype == 2){
								$template->assign_loop_var("allevents",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype2')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_yellow').'" /></span></a>');
							}elseif($event->eventtype == 3){
								$template->assign_loop_var("allevents",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype3')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('asterisk_orange').'" /></span></a>');
							}
						}else{
							$template->assign_loop_var("allevents",$index,"eventtype",'');
						}
						if(!Mobile::isMobileDevice()){
							if(Plugin_Events_Manager::getFirstImageFromEvent($event->id) != ''){
								$imgurl = '<a href="./content/uploads/eat/'.Plugin_Events_Manager::getFirstImageFromEvent($event->id).'" class="fancyboxpopup"><img style="max-height: 250px; max-width: 450px;" max-width="450px" max-height="250px" src="/content/uploads/eat/thumbs/'.Plugin_Events_Manager::getFirstImageFromEvent($event->id).'" alt="" /></a>';
								$template->assign_loop_var("allevents",$index,"img",$imgurl);
							}else{
								$template->assign_loop_var("allevents",$index,"img",'');
							}
						}else{
							if(Plugin_Events_Manager::getFirstImageFromEvent($event->id) != ''){
								$imgurl = './content/uploads/eat/thumbs/'.Plugin_Events_Manager::getFirstImageFromEvent($event->id).'';
							}else{
								$imgurl = './system/skins/socialmealmobile/images/content/small-1-1.jpg';
							}
							$template->assign_loop_var("allevents",$index,"img",$imgurl);
						}
						if(!Mobile::isMobileDevice()){
							$categorys = ' ';
							if($event->fleisch == 1 or $event->fisch == 1 or $event->seafood == 1){
								$categorys .= 'Fleisch ';
							}
							if($event->vegetarisch == 1){
								$categorys .= 'Vegetarisch ';
							}
							if($event->vegan == 1){
								$categorys .= 'Vegan';
							}
						}else{
							$categorys = '';
							if($event->fleisch == 1 or $event->fisch == 1 or $event->seafood == 1){
								$categorys .= 'Fleisch,';
							}
							if($event->vegetarisch == 1){
								$categorys .= 'Vegetarisch,';
							}
							if($event->vegan == 1){
								$categorys .= 'Vegan';
							}
						}
						if($event->eventtype == 3){
							if($event->maxguest == 0){
								$contentfreeplace = '<span class="bold">'.utf8_encode(Language::DirectTranslate('plugin_events_yes')).'</span>';
								if(Plugin_Events_Manager::countSubscribers($event->id) > 0){
									$contentfreeplace .= ' ('.Language::DirectTranslate(plugin_events_voteyes2).': '.Plugin_Events_Manager::countSubscribers($event->id).')';
								}
								$template->assign_loop_var("allevents",$index,'freeplace',$contentfreeplace);
							}else{
								$freeplace = $event->maxguest - Plugin_Events_Manager::countSubscribers($event->id);
								$template->assign_loop_var("allevents",$index,'freeplace','<span class="bold">'.htmlentities($freeplace).'</span> '.Language::DirectTranslate('plugin_events_by').' '.htmlentities($event->maxguest));
							}
						}else{
							$freeplace = $event->maxguest - Plugin_Events_Manager::countSubscribers($event->id);
							$template->assign_loop_var("allevents",$index,'freeplace','<span class="bold">'.htmlentities($freeplace).'</span> '.Language::DirectTranslate('plugin_events_by').' '.htmlentities($event->maxguest));
						}
						$template->assign_loop_var("allevents",$index,"categorys",$categorys);
						if($event->eventtype == 1){
							$template->assign_loop_var("allevents",$index,"preis",'');
						}elseif($event->eventtype == 3){
							if($event->preis != 0){
								$template->assign_loop_var("allevents",$index,"preis",Language::directTranslate('plugin_events_preis').':<br />'.htmlentities($event->preis).' CHF');
							}elseif($event->specialprice > 0){
								$template->assign_loop_var("allevents",$index,"preis",Language::directTranslate('plugin_events_preis').':<br />'.Language::directTranslate('plugin_events_price'.$event->specialprice));
							}else{
								$template->assign_loop_var("allevents",$index,"preis",'');
							}
						}else{
							$template->assign_loop_var("allevents",$index,"preis",Language::directTranslate('plugin_events_preis').':<br />'.htmlentities($event->preis).' CHF');
						}
						$template->assign_loop_var("allevents",$index,"datum",Plugin_Events_Manager::formatDate($event->eventdate));
						if($event->bnb != 0){
							$template->assign_loop_var("allevents",$index,"bnb",' <small>('.Language::DirectTranslate('plugin_events_or').' '.htmlentities($event->bnb).' <a href="/Partner/bnb-bonnetzbon.html" target="_blank">'.Language::DirectTranslate('plugin_events_bnb').'</a>)</small> ');
						}else{
							$template->assign_loop_var("allevents",$index,"bnb",'');
						}
					}
				}
				if(!Mobile::isMobileDevice()){
					//Neuster Event Balken
					$eventdata = Plugin_Events_Manager::newstEvent();
					$bb = new Plugin_BBCode_Translator();
					$template->assign_var('datum',Plugin_Events_Manager::formatDate($eventdata->eventdate));
					$beschreibungstext = $bb->replace($eventdata->beschreibung);
					$beschreibungstext = substr($beschreibungstext, 0, 120);
					$template->assign_var('beschreibung',$beschreibungstext);
					$template->assign_var('title',htmlentities($eventdata->eventname));
					$template->assign_var('id',htmlentities($eventdata->id));
					if($eventdata->preis > 0){
						$template->assign_var('preis',''.Language::DirectTranslate('plugin_events_preis').': '.htmlentities($eventdata->preis).' '.Language::DirectTranslate('plugin_events_chf'));
					}else{
						$template->assign_var('preis','');
					}
				}
			}else{
				$template->show_if('events',false);
				$template->show_if('noevent',true);
			}
			//Neuster Blog Beitrag
			$template->show_if('authorinfo',true);
			$template->show_if('readmore',false);
			$entryData = Plugin_BlogEntries_Manager::getNewBlog();
			$entryMetaData = Plugin_PluginData_Data::getData('blogentrymeta_'.$entryData->id.'','plugin','blogentries');
			$template->assign_var('ENTRYTITLE',htmlentities($entryData->title));
			$template->assign_var('ENTRYURL',UrlRewriting::GetUrlByAlias($entryData->alias));
			$template->assign_var('ENTRYCONTENT',$entryData->content);
			$template->assign_var('DATETIME',Plugin_Events_Manager::formatDate($entryMetaData['lastchange']));
			$template->assign_var('AUFRUFE',htmlentities($entryData->aufrufe));
			$template->assign_var('AUTHOR',htmlentities($entryMetaData['author']));
			$authordata = Plugin_Profile_Manager::getUserData($entryMetaData['author']);
			if(Mobile::isMobileDevice()){
				$template->assign_var('showauthor',Plugin_Profile_Manager::showUsername($entryMetaData['author'].'singleIconWrapper singleIconText iconEditDark postInfo postAuthor postInfoNoMargin'));
			}else{
				$template->assign_var('showauthor',Plugin_Profile_Manager::showUsername($entryMetaData['author'],'author'));
			}
			if(isset($entryMetaData['image']) != ''){
				$template->show_if('blogimage',true);
				$template->assign_var('imgpath',htmlentities($entryMetaData['image']));
				$imgdata = ImageServer::getImageData($entryMetaData['image']);
				if($imgdata->description != ''){
					$template->show_if('imgdesc',true);
					$template->assign_var('imgdesc',htmlentities($imgdata->description));
				}else{
					$template->show_if('imgdesc',false);
				}
			}else{
				$template->show_if('blogimage',false);
				$template->show_if('imgdesc',false);
			}
			$template->assign_var('AUTHORURL','/Blog/uebersicht.html?author='.htmlentities($entryMetaData['author']));
			$template->assign_var('CATID',htmlentities($entryMetaData['category']));
			$template->assign_var('CATNAME',Plugin_BlogEntries_Manager::getCatName($entryMetaData['category']));
			if(Plugin_Blogcomments_Manager::countComments($entryData->id) == 0){
				$template->assign_var('comments',Language::DirectTranslate('plugin_bloglanguagepacks_nocomment'));
			}elseif(Plugin_Blogcomments_Manager::countComments($entryData->id) == 1){
				$template->assign_var('comments',Plugin_Blogcomments_Manager::countComments($entryData->id).' '.Language::DirectTranslate('plugin_bloglanguagepacks_comment'));
			}else{
				$template->assign_var('comments',Plugin_Blogcomments_Manager::countComments($entryData->id).' '.Language::DirectTranslate('plugin_bloglanguagepacks_comments'));
			}
			if(User::Current()->role->ID == 1 && Plugin_Profile_Manager::openImg($entryMetaData['author']) == 0){
				$authorimage = Sys::getFullSkinPath().'images/avtar.png';
			}else{
				$authorimage = '/content/uploads/profile/thumbs/'.Plugin_Profile_Manager::getUserImage($entryMetaData['author']);
				if($authorimage == '/content/uploads/profile/thumbs/'){
					$authorimage = Sys::getFullSkinPath().'images/avtar.png';
				}
			}
			$template->assign_var('authorimg',htmlentities($authorimage));
			$authorinfo = substr($authordata->aboutme,0,250);
			$template->assign_var('authorinfo',htmlentities($authorinfo));
			echo($template->getCode());
			if(!Mobile::isMobileDevice()){
				//Sidebar
				include('./system/skins/socialmeal/sidebar.php');
			}
		}

		function getHeader(){
			echo('
				<meta property="og:site_name" content="Social Meal - Meal Sharing für die Region Basel" />
				<meta property="og:title" content="Startseite" />
				<meta property="og:description" content="Du kochst gerne? Du isst nicht gerne alleine? Du lernst gerne neue Leute kennen?
Wir verbinden euch gerne! Ab dem 28. März in Basel!" />
				<meta property="og:type" content="website" />
				<meta property="og:url" content="'.Settings::getInstance()->get("host").'home.html" />
				<meta property="og:image" content="'.Settings::getInstance()->get("host").'system/skins/socialmeal/images/logofb.jpg" />
				<link rel="image_src" href="'.Settings::getInstance()->get("host").'system/skins/socialmeal/images/logofb.jpg" />
				');
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
