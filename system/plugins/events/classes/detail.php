<?PHP
/*
 * detail.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_Events_Detail extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
			if(isset($_GET['event'])){
				if(Plugin_Events_Manager::eventExist($_GET['event']) != 1){
					$page->title = Language::DirectTranslate('plugin_events_nomealfound');
				}else{
					$mealdata = Plugin_Events_Manager::loadEvent($_GET['event']);
					if(strlen($mealdata->eventname) > 35){
						$mealdata->eventname = substr($mealdata->eventname,0,32);
						$mealdata->eventname = $mealdata->eventname.'...';
					}
					$page->title = $mealdata->eventname;
				}
			}
		}
		public function display(){
			$template = new Template();
			if(Plugin_Events_Manager::eventExist($_GET['event']) != 1){
				$template->load('plugin_events_noevent');
				echo($template->getCode());
				exit();
			}
			if(Mobile::isMobileDevice()){
				$template->load('plugin_events_detailpage.mobile');
			}else{
				$template->load('plugin_events_detailpage');
			}
			$template->show_if('showquestmsg',false);
			$template->show_if('showansmsg',false);
			$template->show_if('norestaurant',true);
			$template->show_if('restaurant',false);
			$template->show_if('mitnehmen',false);
			$template->show_if('personal',true);
			$template->show_if('org',false);
			$template->show_if('showlogin',false);
			$template->show_if('full',false);
			$template->show_if('openaddress',false);
			$template->show_if('showcomment',false);
			$template->show_if('commentform',false);
			$template->show_if('commentsave',false);
			$template->show_if('promo',false);
			$template->show_if('showorgedit',false);
			$template->show_if('completeaddress',true);
			$template->show_if('rest',false);
			$template->show_if('noorg',true);
			$template->show_if('showopenclose',false);
			$template->show_if('openok',false);
			$template->show_if('closeok',false);
			$template->show_if('minimalinfo',false);
			if(User::Current()->isGuest()){
				$template->show_if('showlogin',true);
			}else{
				Plugin_Events_Manager::setAsRead($_GET['event']);
			}
			if(isset($_POST['savecomment'])){
				Plugin_Events_Manager::updateComment($_GET['event'],$_POST['upcomment']);
				$template->show_if('commentsave',true);
			}
			if(isset($_GET['close']) && Plugin_Profile_Manager::canEditOrgProfile($_GET['orgid'])){
				Plugin_Events_Manager::closeSubscribe($_GET['event']);
				$template->show_if('closeok',true);
			}
			if(isset($_GET['open']) && Plugin_Profile_Manager::canEditOrgProfile($_GET['orgid'])){
				Plugin_Events_Manager::openSubscribe($_GET['event']);
				$template->show_if('openok',true);
			}
			if(isset($_GET['event'])){
				$event = Plugin_Events_Manager::loadEvent($_GET['event']);
				$map = htmlentities($event->plz);
				if($event->rest != ''){
					$template->show_if('rest',true);
					$template->assign_var('rest',htmlentities($event->rest));
				}
				if($event->eventtype == 1){
					$template->show_if('norestaurant',false);
					$template->show_if('restaurant',true);
					$template->show_if('drinks',false);
					if($event->promo > 0){
						$template->show_if('promo',true);
						$promo = Plugin_Events_Manager::getPromoMeal($event->promo);
						$template->assign_var('promotion',htmlentities($promo->promotion));
						$template->assign_var('restdesc',$promo->description);
						$template->assign_var('speisekarte',htmlentities($promo->speisekarte));
					}
					$template->assign_var('strasse',htmlentities(ucfirst($event->strasse)));
					$template->assign_var('strnr',htmlentities($event->strnr));
					$template->assign_var('plz',htmlentities($event->plz));
					$map = htmlentities($event->strasse).'+'.htmlentities($event->strnr).',+'.htmlentities($event->plz).'+'.htmlentities($event->ort);
					$template->assign_var('preis',Language::directTranslate('plugin_events_price'.$event->specialprice));
				}else{
					if($event->drinks != 99){
						$template->show_if('drinks',true);
						$template->assign_var('drinks',utf8_encode(Language::directTranslate('plugin_events_drink'.$event->drinks)));
					}else{
						$template->show_if('drinks',false);
					}
				}
				if(!empty($_POST['question']) && !empty($_POST['titel'])){
					Plugin_Events_Manager::saveQuestion($event->id,User::Current()->id,$_POST);
					if(Plugin_Profile_Manager::getUserSetting('newquestion',$event->userid) == 1){
						$geberinfo = Plugin_Profile_Manager::getUserDataPerId($event->userid);
						$mailtext = 'Liebe*r '.htmlentities(ucfirst($geberinfo->firstname)).'<br><br>Zu deinem Meal "<a href="'.Settings::getInstance()->get("host").'event-details.html?event='.htmlentities($event->id).'">'.htmlentities($event->eventname).'</a>" wurde eine Frage gestellt!<br><br>Unter <a href="'.Settings::getInstance()->get("host").'event-details.html?event='.htmlentities($event->id).'#questions">Frage beantworten</a> kannst du die Unklarheit beseitigen, eine rasche Beantwortung verhilft dir möglicherweise zu mehr zufriedenen Gästen.<br><br>Falls du der Meinung bist, dass dieser User die Funktion der Frage-Stellung missbräuchlich verwendet, melde uns das bitte über die entsprechende <a href="'.Settings::getInstance()->get("host").'melden.html?source=mail&g=newquestion_'.htmlentities($event->id).'">Melde-Seite</a>.<br><br>Danke, dass du die Neugier der Community stillst!<br>Social Meal<br><br>P.S.: Diesen E-Mail-Typ kannst du in den <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abbestellen, falls er dir überflüssig erscheint.';
						$subject = utf8_decode('Zu deinem Meal "'.htmlentities($event->eventname).'" auf Social Meal wurde eine Frage gestellt');
						$h2t = new Plugin_Mailer_html2text($mailtext);
						$mail = new Plugin_PHPMailer_PHPMailer;
						$mail->IsSMTP();
						$mail->AddAddress($geberinfo->email);
						$mail->WordWrap = 50;
						$mail->IsHTML(true);
						$mail->Subject = $subject;
						$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
						$mail->AltBody = $h2t->get_text();
						if(!$mail->Send()){
							echo('Mail Error:'.$mail->ErrorInfo);
						}
					}
					$template->show_if('showquestmsg',true);
				}
				if(!empty($_POST['answer']) && !empty($_POST['atitel'])){
					Plugin_Events_Manager::saveAnswer($_POST);
					$questuserid = Plugin_Events_Manager::getQuestUserId($_POST['questid']);
					$nehmerinfo = Plugin_Profile_Manager::getUserDataPerId($questuserid);
					if(Plugin_Profile_Manager::getUserSetting('newanswer',$questuserid) == 1){
						$mailtext = 'Liebe*r '.htmlentities(ucfirst($nehmerinfo->firstname)).'<br><br>Deine Frage zum Meal <a href="'.Settings::getInstance()->get("host").'event-details.html?event='.htmlentities($event->id).'">'.$event->eventname.'</a> wurde beantwortet!<br><br>Wir hoffen, dass die <a href="'.Settings::getInstance()->get("host").'event-details.html?event='.htmlentities($event->id).'#questions">Antwort auf deine Frage</a> deine Unsicherheiten beseitigt hat, ansonsten empfehlen wir dir noch einmal nachzuhaken.<br><br>Falls du der Meinung bist, dass dieser User die Funktion der Frage-Beantwortung missbräuchlich verwendet, melde uns das bitte über die entsprechende <a href="'.Settings::getInstance()->get("host").'melden.html?source=mail&g=newanswer">Melde-Seite</a>.<br><br>Danke für deine Neugier!<br>Social Meal<br><br>P.S.: Diesen E-Mail-Typ kannst du in den <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abbestellen, falls er dir überflüssig erscheint.';
						$subject = utf8_decode('Deine Frage zu einem Meal auf Social Meal wurde beantwortet');
						$h2t = new Plugin_Mailer_html2text($mailtext);
						$mail = new Plugin_PHPMailer_PHPMailer;
						$mail->AddAddress($nehmerinfo->email);
						$mail->WordWrap = 50;
						$mail->IsHTML(true);
						$mail->Subject = $subject;
						$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
						$mail->AltBody = $h2t->get_text();
						if(!$mail->Send()){
							echo('Mail Error:'.$mail->ErrorInfo);
						}
					}
					$template->show_if('showansmsg',true);
				}
				if(isset($_GET['readmore'])){
					$bb = new Plugin_BBCode_Translator();
					$beschreibung = $bb->replace($event->beschreibung);
				}else{
					$beschreibung = Plugin_Events_Manager::shortText($event->beschreibung,$event->id);
				}
				if($event->eventtype == 3){
					$template->show_if('gangguest',false);
					$template->show_if('smoke',false);
					$template->show_if('sideinfo',false);
					$template->show_if('noorg',false);
					if($event->openaddress == 1){
						$template->show_if('openaddress',true);
						$template->show_if('completeaddress',false);
						$template->assign_var('strasse',htmlentities(ucfirst($event->strasse)));
						$template->assign_var('strnr',htmlentities($event->strnr));
						$map = htmlentities($event->strasse).'+'.htmlentities($event->strnr).',+'.htmlentities($event->plz).'+'.htmlentities($event->ort);
					}else{
						$template->show_if('rest',false);
					}
				}else{
					$template->show_if('gangguest',true);
					$template->show_if('smoke',true);
					$template->show_if('sideinfo',true);
				}
				$template->show_if('showedit',false);
				$template->show_if('showimages',false);
				$template->show_if('showquestions',false);
				$template->show_if('shownewquestion',false);
				$template->show_if('showsubscribe',false);
				$template->show_if('showmsg',false);
				$template->show_if('melden',false);
				if($event->eventtype == 3 or $event->eventtype == 1){
					$template->show_if('minimalinfo',true);
				}
				if(Mobile::isMobileDevice()){
					$template->show_if('mobile',true);
				}else{
					$template->show_if('mobile',false);
				}
				$userdatas = Plugin_Profile_Manager::getUserDataPerId($event->userid);
				$template->assign_var('title',htmlentities($event->eventname));
				if(Plugin_Profile_Manager::isFriend($event->userid) or $event->userid == User::Current()->id){
					$template->assign_var('username',ucfirst($userdatas->firstname).' '.ucfirst($userdatas->lastname));
				}else{
					$template->assign_var('username',htmlentities($userdatas->name));
				}
				if($event->parking == 0){
					$template->assign_var('parking',Language::DirectTranslate('plugin_events_ka'));
				}elseif($event->parking == 1){
					$template->assign_var('parking',Language::DirectTranslate('plugin_events_no'));
				}elseif($event->parking == 2){
					$template->assign_var('parking',Language::DirectTranslate('plugin_events_yes'));
				}elseif($event->parking == 3){
					$template->assign_Var('parking',Language::DirectTranslate('plugin_events_parking5min'));
				}
				if(!empty($event->weblink)){
					$template->show_if('weblink',true);
					$template->assign_var('weblink',htmlentities($event->weblink));
					if($event->eventtype == 1){
						$template->assign_var('weblinkname',Language::DirectTranslate('plugin_events_weblink1'));
					}elseif($event->eventtype == 3){
						$template->assign_var('weblinkname',Language::DirectTranslate('plugin_events_weblink2'));
					}
				}else{
					$template->show_if('weblink',false);
				}
				$template->assign_var('usernamelink',htmlentities($userdatas->name));
				$template->assign_var('showauthor',Plugin_Profile_Manager::showUsername($userdatas->name,'author'));
				$template->assign_var('eventdate',Plugin_Events_Manager::formatDate($event->eventdate));
				$template->assign_var('beschreibung',$beschreibung);
				if($event->eventtype == 0){
					$template->assign_var("eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype0')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_green').'" /></span></a>');
				}elseif($event->eventtype == 1){
					$template->assign_var("eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype1')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_red').'" /></span></a>');
				}elseif($event->eventtype == 2){
					$template->assign_var("eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype2')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_yellow').'" /></span></a>');
				}elseif($event->eventtype == 3){
					$template->assign_var("eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype3')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('asterisk_orange').'" /></span></a>');
				}
				if(!empty($event->comment)){
					$template->show_if('showcomment',true);
					$bb = new Plugin_BBCode_Translator();
					$template->assign_var('comment',$bb->replace($event->comment));
					$template->assign_var('commentdate',Plugin_Events_Manager::formatDate($event->commentdate));
				}
				if(User::Current()->id == $event->userid && Plugin_Events_Manager::countSubscribers($event->id) >= 1){
					$template->show_if('commentform',true);
					$template->assign_var('commentof',$event->comment);
				}
				if($event->eventtype != 3){
					$template->show_if('bnbspecial',false);
					$template->assign_var('preis',$event->preis.' '.Language::DirectTranslate('plugin_events_chf'));
					$freeplace = $event->maxguest - Plugin_Events_Manager::countSubscribers($event->id);
					if($freeplace > 0){
						$template->assign_var('freeplace','<span class="bold">'.$freeplace.'</span> '.Language::DirectTranslate('plugin_events_by').' '.htmlentities($event->maxguest));
					}else{
						$template->assign_var('freeplace','<span class="bold">'.Language::DirectTranslate('plugin_events_keine').'</span>');
					}
				}else{
					if($event->preis == 0 && $event->specialprice == 0){
						$template->assign_var('preis',Language::DirectTranslate('plugin_events_noprice').'.');
					}elseif($event->preis > 0 && $event->specialprice == 0){
						$template->assign_var('preis',htmlentities($event->preis).' '.Language::DirectTranslate('plugin_events_chf'));
					}elseif($event->preis == 0 && $event->specialprice > 0){
						$template->assign_var('preis',Language::directTranslate('plugin_events_price'.$event->specialprice));
					}
					if($event->bnbprice > 0 && $event->bnb == '' or $event->bnb == 0){
						$template->show_if('bnbspecial',true);
						$template->assign_var('bnbspecial',Language::DirectTranslate('plugin_events_bnbprice'.$event->bnbprice));
					}else{
						$template->show_if('bnbspecial',false);
					}
					if($event->maxguest == 0){
						$template->assign_var('freeplace','<span class="bold">'.utf8_encode(Language::DirectTranslate('plugin_events_yes')).'</span>');
					}else{
						$freeplace = $event->maxguest - Plugin_Events_Manager::countSubscribers($event->id);
						if($freeplace > 0){
							$template->assign_var('freeplace','<span class="bold">'.$freeplace.'</span> '.Language::DirectTranslate('plugin_events_by').' '.htmlentities($event->maxguest));
						}else{
							$template->assign_var('freeplace','<span class="bold">'.Language::DirectTranslate('plugin_events_keine').'</span>');
						}
					}
				}
				$template->assign_var('gange',htmlentities($event->gaenge));
				if($event->maxguest > 0){
					$template->assign_var('guest',htmlentities($event->maxguest));
				}elseif($event->maxguest == 0){
					$template->assign_var('guest',utf8_encode(Language::directTranslate('plugin_events_unlimited')));
				}elseif($event->maxguest == -1){
					$template->assign_var('guest',Plugin_Events_Manager::countSubscribers($event->id));
				}
				$template->assign_var('aufrufe',htmlentities($event->counter));
				if($event->mitnehmen != ''){
					$template->show_if('mitnehmen',true);
					$template->assign_var('mitnehmen',htmlentities($event->mitnehmen));
				}
				if($event->eventtype == 3 && $event->orgid > 0){
					$template->show_if('personal',false);
					$template->show_if('org',true);
					$orginfo = Plugin_Profile_Manager::loadOrganisation($event->orgid);
					$template->assign_var('orgname',htmlentities($orginfo->orgname));
					$template->assign_var('orgid',htmlentities($orginfo->id));
				}
				if($event->option1 != '' && $event->option2 != ''){
					$template->show_if('options',true);
					$template->assign_var('option1',htmlentities($event->option1));
					$template->assign_var('option2',htmlentities($event->option2));
					if($event->option3 != ''){
						$template->show_if('option3',true);
						$template->assign_var('option3',htmlentities($event->option3));
					}else{
						$template->show_if('option3',false);
					}
				}else{
					$template->show_if('options',false);
				}
				if($event->bnb != '' && $event->bnb != 0){
					$template->show_if('bnb',true);
					$template->assign_var('bnb',htmlentities($event->bnb));
				}else{
					$template->show_if('bnb',false);
				}
				if($event->dauer > 0){
					$template->show_if('dauer',true);
					if($event->dauer == 1){
						$template->assign_var('dauer',htmlentities($event->dauer).' '.Language::DirectTranslate('plugin_events_hour'));
					}else{
						$template->assign_var('dauer',htmlentities($event->dauer).' '.Language::DirectTranslate('plugin_events_hours'));
					}
				}else{
					$template->show_if('dauer',false);
				}
				$template->assign_var('plz',htmlentities($event->plz));
				$template->assign_var('ort',htmlentities($event->ort));
				$template->assign_var('eventid',htmlentities($event->id));
				if(Mobile::isMobileDevice()){
					if($event->fleisch == 0){
						$template->assign_var('fleisch','Gear');
					}elseif($event->fleisch == 1){
						$template->assign_var('fleisch','Checkmark');
					}elseif($event->fleisch == 2){
						$template->assign_var('fleisch','No');
					}
					if($event->fisch == 0){
						$template->assign_var('fisch','Gear');
					}elseif($event->fisch == 1){
						$template->assign_var('fisch','Checkmark');
					}elseif($event->fisch == 2){
						$template->assign_var('fisch','No');
					}
					if($event->seafood == 0){
						$template->assign_var('seafood','Gear');
					}elseif($event->seafood == 1){
						$template->assign_var('seafood','Checkmark');
					}elseif($event->seafood == 2){
						$template->assign_var('seafood','No');
					}
					if($event->vegetarisch == 0){
						$template->assign_var('vegi','Gear');
					}elseif($event->vegetarisch == 1){
						$template->assign_var('vegi','Checkmark');
					}elseif($event->vegetarisch == 2){
						$template->assign_var('vegi','No');
					}
					if($event->vegan == 0){
						$template->assign_var('vegan','Gear');
					}elseif($event->vegan == 1){
						$template->assign_var('vegan','Checkmark');
					}elseif($event->vegan == 2){
						$template->assign_var('vegan','No');
					}
					if($event->scharf == 0){
						$template->assign_var('scharf','Gear');
					}elseif($event->scharf == 1){
						$template->assign_var('scharf','Checkmark');
					}elseif($event->scharf == 2){
						$template->assign_var('scharf','No');
					}
					if($event->bio == 0){
						$template->assign_var('bio','Gear');
					}elseif($event->bio == 1){
						$template->assign_var('bio','Checkmark');
					}elseif($event->bio == 2){
						$template->assign_var('bio','No');
					}
					if($event->saisonal == 0){
						$template->assign_var('saisonal','Gear');
					}elseif($event->saisonal == 1){
						$template->assign_var('saisonal','Checkmark');
					}elseif($event->saisonal == 2){
						$template->assign_var('saisonal','No');
					}
					if($event->regional == 0){
						$template->assign_var('regional','Gear');
					}elseif($event->regional == 1){
						$template->assign_var('regional','Checkmark');
					}elseif($event->regional == 2){
						$template->assign_var('regional','No');
					}
					if($event->lactose == 0){
						$template->assign_var('lactose','Gear');
					}elseif($event->lactose == 1){
						$template->assign_var('lactose','Checkmark');
					}elseif($event->lactose == 2){
						$template->assign_var('lactose','No');
					}
					if($event->glutenfrei == 0){
						$template->assign_var('gluten','Gear');
					}elseif($event->glutenfrei == 1){
						$template->assign_var('gluten','Checkmark');
					}elseif($event->glutenfrei == 2){
						$template->assign_var('gluten','No');
					}
					if($event->nusse == 0){
						$template->assign_var('nusse','Gear');
					}elseif($event->nusse == 1){
						$template->assign_var('nusse','Checkmark');
					}elseif($event->nusse == 2){
						$template->assign_var('nusse','No');
					}
					if($event->alkohol == 0){
						$template->assign_var('alkohol','Gear');
					}elseif($event->alkohol == 1){
						$template->assign_var('alkohol','Checkmark');
					}elseif($event->alkohol == 2){
						$template->assign_var('alkohol','No');
					}
					if($event->hefe == 0){
						$template->assign_var('hefe','Gear');
					}elseif($event->hefe == 1){
						$template->assign_var('hefe','Checkmark');
					}elseif($event->hefe == 2){
						$template->assign_var('hefe','No');
					}
					if($event->koscher == 0){
						$template->assign_var('koscher','Gear');
					}elseif($event->koscher == 1){
						$template->assign_var('koscher','Checkmark');
					}elseif($event->koscher == 2){
						$template->assign_var('koscher','No');
					}
					if($event->halal == 0){
						$template->assign_var('halal','Gear');
					}elseif($event->halal == 1){
						$template->assign_var('halal','Checkmark');
					}elseif($event->halal == 2){
						$template->assign_var('halal','No');
					}
				}else{
					if($event->fleisch == 0){
						$template->assign_var('fleisch',Icons::getIcon('FRAGE'));
					}elseif($event->fleisch == 1){
						$template->assign_var('fleisch',Icons::getIcon('ACCEPT'));
					}elseif($event->fleisch == 2){
						$template->assign_var('fleisch',Icons::getIcon('CROSS'));
					}
					if($event->fisch == 0){
						$template->assign_var('fisch',Icons::getIcon('FRAGE'));
					}elseif($event->fisch == 1){
						$template->assign_var('fisch',Icons::getIcon('ACCEPT'));
					}elseif($event->fisch == 2){
						$template->assign_var('fisch',Icons::getIcon('CROSS'));
					}
					if($event->seafood == 0){
						$template->assign_var('seafood',Icons::getIcon('FRAGE'));
					}elseif($event->seafood == 1){
						$template->assign_var('seafood',Icons::getIcon('ACCEPT'));
					}elseif($event->seafood == 2){
						$template->assign_var('seafood',Icons::getIcon('CROSS'));
					}
					if($event->vegetarisch == 0){
						$template->assign_var('vegi',Icons::getIcon('FRAGE'));
					}elseif($event->vegetarisch == 1){
						$template->assign_var('vegi',Icons::getIcon('ACCEPT'));
					}elseif($event->vegetarisch == 2){
						$template->assign_var('vegi',Icons::getIcon('CROSS'));
					}
					if($event->vegan == 0){
						$template->assign_var('vegan',Icons::getIcon('FRAGE'));
					}elseif($event->vegan == 1){
						$template->assign_var('vegan',Icons::getIcon('ACCEPT'));
					}elseif($event->vegan == 2){
						$template->assign_var('vegan',Icons::getIcon('CROSS'));
					}
					if($event->scharf == 0){
						$template->assign_var('scharf',Icons::getIcon('FRAGE'));
					}elseif($event->scharf == 1){
						$template->assign_var('scharf',Icons::getIcon('ACCEPT'));
					}elseif($event->scharf == 2){
						$template->assign_var('scharf',Icons::getIcon('CROSS'));
					}
					if($event->bio == 0){
						$template->assign_var('bio',Icons::getIcon('FRAGE'));
					}elseif($event->bio == 1){
						$template->assign_var('bio',Icons::getIcon('ACCEPT'));
					}elseif($event->bio == 2){
						$template->assign_var('bio',Icons::getIcon('CROSS'));
					}
					if($event->saisonal == 0){
						$template->assign_var('saisonal',Icons::getIcon('FRAGE'));
					}elseif($event->saisonal == 1){
						$template->assign_var('saisonal',Icons::getIcon('ACCEPT'));
					}elseif($event->saisonal == 2){
						$template->assign_var('saisonal',Icons::getIcon('CROSS'));
					}
					if($event->regional == 0){
						$template->assign_var('regional',Icons::getIcon('FRAGE'));
					}elseif($event->regional == 1){
						$template->assign_var('regional',Icons::getIcon('ACCEPT'));
					}elseif($event->regional == 2){
						$template->assign_var('regional',Icons::getIcon('CROSS'));
					}
					if($event->lactose == 0){
						$template->assign_var('lactose',Icons::getIcon('FRAGE'));
					}elseif($event->lactose == 1){
						$template->assign_var('lactose',Icons::getIcon('ACCEPT'));
					}elseif($event->lactose == 2){
						$template->assign_var('lactose',Icons::getIcon('CROSS'));
					}
					if($event->glutenfrei == 0){
						$template->assign_var('gluten',Icons::getIcon('FRAGE'));
					}elseif($event->glutenfrei == 1){
						$template->assign_var('gluten',Icons::getIcon('ACCEPT'));
					}elseif($event->glutenfrei == 2){
						$template->assign_var('gluten',Icons::getIcon('CROSS'));
					}
					if($event->nusse == 0){
						$template->assign_var('nusse',Icons::getIcon('FRAGE'));
					}elseif($event->nusse == 1){
						$template->assign_var('nusse',Icons::getIcon('ACCEPT'));
					}elseif($event->nusse == 2){
						$template->assign_var('nusse',Icons::getIcon('CROSS'));
					}
					if($event->alkohol == 0){
						$template->assign_var('alkohol',Icons::getIcon('FRAGE'));
					}elseif($event->alkohol == 1){
						$template->assign_var('alkohol',Icons::getIcon('ACCEPT'));
					}elseif($event->alkohol == 2){
						$template->assign_var('alkohol',Icons::getIcon('CROSS'));
					}
					if($event->hefe == 0){
						$template->assign_var('hefe',Icons::getIcon('FRAGE'));
					}elseif($event->hefe == 1){
						$template->assign_var('hefe',Icons::getIcon('ACCEPT'));
					}elseif($event->hefe == 2){
						$template->assign_var('hefe',Icons::getIcon('CROSS'));
					}
					if($event->koscher == 0){
						$template->assign_var('koscher',Icons::getIcon('FRAGE'));
					}elseif($event->koscher == 1){
						$template->assign_var('koscher',Icons::getIcon('ACCEPT'));
					}elseif($event->koscher == 2){
						$template->assign_var('koscher',Icons::getIcon('CROSS'));
					}
					if($event->halal == 0){
						$template->assign_var('halal',Icons::getIcon('FRAGE'));
					}elseif($event->halal == 1){
						$template->assign_var('halal',Icons::getIcon('ACCEPT'));
					}elseif($event->halal == 2){
						$template->assign_var('halal',Icons::getIcon('CROSS'));
					}
				}
				if($event->anmeldeschluss < date("Y-m-d H:i:s") && Plugin_Events_Manager::hasSubscribe($event->id,User::Current()->id) != 1){
					$template->show_if('showmsg',true);
				}
				if($event->eventtype != 3 && $event->maxguest == Plugin_Events_Manager::countSubscribers($event->id)){
					$template->show_if('full',true);
				}
				if($event->eventtype == 3 && $event->maxguest == -1){
					$template->show_if('full',true);
				}
				if($event->eventtype == 3 && $event->maxguest > 0 && $event->maxguest == Plugin_Events_Manager::countSubscribers($event->id)){
					$template->show_if('full',true);
				}
				if(Plugin_Events_Manager::countQuestions($event->id) == 0){
					$template->assign_var('countquestions','');
				}elseif(Plugin_Events_Manager::countQuestions($event->id) == 1){
					$template->assign_var('countquestions',Plugin_Events_Manager::countQuestions($event->id).' '.Language::DirectTranslate('plugin_events_question'));
				}else{
					$template->assign_var('countquestions',Plugin_Events_Manager::countQuestions($event->id).' '.Language::DirectTranslate('plugin_events_questions'));
				}
				if(Plugin_Events_Manager::countSubscribers($event->id) == 0){
					$template->assign_var('subscribers','Keine');
					$template->show_if('showteilnehmer',false);
				}else{
					$template->assign_var('subscribers',Plugin_Events_Manager::countSubscribers($event->id));
					$template->show_if('showteilnehmer',true);
					foreach(Plugin_Events_Manager::getSubscribesByEvent($event->id) as $subuser){
						$index = $template->add_loop_item('teilnehmer');
						$teiluserdata = Plugin_Profile_Manager::getUserDataPerId($subuser->userid);
						$template->assign_loop_var("teilnehmer",$index,"teilshowname",Plugin_Profile_Manager::showUsername($teiluserdata->name));
						if($subuser->eventoption > 0){
							if(User::Current()->id == $event->userid){
								if($subuser->eventoption == 1){
									$option = $event->option1;
								}
								if($subuser->eventoption == 2){
									$option = $event->option2;
								}
								if($subuser->eventoption == 3){
									$option = $event->option3;
								}
								$template->assign_loop_var("teilnehmer",$index,"option",'('.Language::DirectTranslate('plugin_events_subscribeoption').': '.htmlentities($option).')');
							}else{
								$template->assign_loop_var("teilnehmer",$index,"option",'');
							}
						}else{
							$template->assign_loop_var("teilnehmer",$index,"option",'');
						}
					}
				}
				if(Plugin_Events_Manager::countImagesPerEvent($event->id) > 0){
					$template->show_if('showimages',true);
					if($event->symbolimg == 1){
						$template->show_if('symbolimg',true);
					}else{
						$template->show_if('symbolimg',false);
					}
					foreach(Plugin_Events_Manager::getImagesPerEvent($event->id) as $image){
						$index = $template->add_loop_item("images");
						$template->assign_loop_var("images",$index,"imgurl",Settings::getInstance()->get("host").'content/uploads/eat/'.htmlentities($image->filename));
					}
				}
				if($event->rauchen == '1'){
					$template->assign_var('smoke',Language::DirectTranslate("PLUGIN_EVENTS_SMOKEYES"));
				}elseif($event->rauchen == '0'){
					$template->assign_var('smoke',Language::DirectTranslate("PLUGIN_EVENTS_SMOKENO"));
				}elseif($event->rauchen == '2'){
					$template->assign_var('smoke',Language::DirectTranslate("PLUGIN_EVENTS_SMOKEBALK"));
				}
				if($event->rollchair < 99){
					$template->show_if('rollchair',true);
					if($event->rollchair == '0'){
						$template->assign_var('rollchair',Language::DirectTranslate("PLUGIN_EVENTS_NO"));
					}elseif($event->rollchair == '1'){
						$template->assign_var('rollchair',Language::DirectTranslate("PLUGIN_EVENTS_YES"));
					}
				}else{
					$template->show_if('rollchair',false);
				}
				if($event->ov < 99){
					$template->show_if('ov',true);
					if($event->ov == '1'){
						$template->assign_var('ov',utf8_encode(Language::DirectTranslate("PLUGIN_EVENTS_OVYES")));
					}elseif($event->ov == '0'){
						$template->assign_var('ov',utf8_encode(Language::DirectTranslate("PLUGIN_EVENTS_OVNO")));
					}elseif($event->ov == '2'){
						$template->assign_var('ov',utf8_encode(Language::DirectTranslate("PLUGIN_EVENTS_OVMAYBE")));
					}
				}else{
					$template->show_if('ov',false);
				}
				if($event->eventtype == 3 && $event->anmeldeschluss == $event->eventdate){
					$template->assign_var('finish',Language::DirectTranslate('plugin_events_noas'));
				}else{
					$template->assign_var('finish',Plugin_Events_Manager::formatDate($event->anmeldeschluss));
				}
				if(User::Current()->id == $event->userid){
					$template->show_if('showedit',true);
				}
				if(Plugin_Profile_Manager::canEditOrgProfile($event->orgid)){
					$template->show_if('showorgedit',true);
					$template->show_if('showedit',false);
					if($event->maxguest == 0 or $event->maxguest == -1){
						$template->show_if('showopenclose',true);
						if($event->maxguest == 0){
							$template->assign_var('opencloselink','close');
							$template->assign_var('opencloselinkname',htmlentities(Language::directTranslate('plugin_events_closesubscribe')));
						}elseif($event->maxguest == -1){
							$template->assign_var('opencloselink','open');
							$template->assign_var('opencloselinkname',htmlentities(Language::directTranslate('plugin_events_opensubscribe')));
						}
					}
				}
				if(User::Current()->id != $event->userid && User::Current()->role->ID != 1 && !Plugin_Profile_Manager::canEditOrgProfile($event->orgid)){
					$template->show_if('shownewquestion',true);
					if(Plugin_Events_Manager::hasSubscribe($event->id,User::Current()->id) != 1 && $event->anmeldeschluss > date("Y-m-d H:i:s")){
						if(Plugin_Events_Manager::countSubscribers($event->id) < $event->maxguest or $event->eventtype == 3){
							if($event->maxguest >= 0){
								$template->show_if('showsubscribe',true);
							}
						}
					}
				}
				if(User::Current()->id != $event->userid){
					$template->show_if('melden',true);
				}
				if(Plugin_Events_Manager::countQuestions($event->id) > 0){
					$template->show_if('showquestions',true);
					foreach(Plugin_Events_Manager::getQuestions($event->id) as $question){
						$index = $template->add_loop_item("questions");
						$questuserdata = Plugin_Profile_Manager::getUserDataPerId($question->userid);
						$template->assign_loop_var("questions",$index,"questshowauthor",Plugin_Profile_Manager::showUsername($questuserdata->name,'author'));
						$template->assign_loop_var("questions",$index,"questusernamelink",$questuserdata->name);
						$template->assign_loop_var("questions",$index,"questtitel",htmlentities($question->titel));
						$template->assign_loop_var("questions",$index,"question",nl2br(htmlentities($question->question)));
						$template->assign_loop_var("questions",$index,"questdatum",Plugin_Events_Manager::formatDate($question->datum));
						if(User::Current()->role->ID == 1 && Plugin_Profile_Manager::openImgPerId($question->userid) == 0){
							$profilimage = Sys::getFullSkinPath().'images/avtar.png';
						}else{
							$profilimage = '/content/uploads/profile/thumbs/'.htmlentities($questuserdata->profilimg);
							if($profilimage == '/content/uploads/profile/thumbs/'){
								$profilimage = Sys::getFullSkinPath().'images/avtar.png';
							}
						}
						$template->assign_loop_var("questions",$index,"questprofilimg",htmlentities($profilimage));
						if($question->antwort == ''){
							if($event->userid == User::Current()->id){
								if(Mobile::isMobileDevice()){
									//Nothing
								}else{
									$answer = '<ul class="childcomment"><li><h3 class="heading3">{LANG:PLUGIN_EVENTS_NEWANSWER}</h3>
									<form name="newreply" class="commentform" method="post" onsubmit="return chkFormular()"><p><label for="atitel">{LANG:PLUGIN_EVENTS_SUBJECT} <span class="required">*</span></label><br />
									<input type="text" name="atitel" id="atitel" class="required" required="required" /></p><p><label for="answer">{LANG:PLUGIN_EVENTS_YOURANSWER}
									<span class="required">*</span></label><br /><textarea required="required" class="required" rows="6" cols="40" id="answer" name="answer"></textarea><input type="hidden" name="questid" value="'.htmlentities($question->id).'" /></p><p><input type="submit" value="{LANG:PLUGIN_EVENTS_NEWANSWER}" /></p><br /><br /></form></li></ul>';
								}
								$template->assign_loop_var("questions",$index,"answer",$answer);
							}else{
								$template->assign_loop_var("questions",$index,"answer",'');
							}
						}else{
							$auserdata = Plugin_Profile_Manager::getUserDataPerId($event->userid);
							if(User::Current()->role->ID == 1 && Plugin_Profile_Manager::openImgPerId($event->userid) == 0){
								$aprofilimage = Sys::getFullSkinPath().'images/avtar.png';
							}else{
								$aprofilimage = '/content/uploads/profile/thumbs/'.htmlentities($auserdata->profilimg);
								if($profilimage == '/content/uploads/profile/thumbs/'){
									$aprofilimage = Sys::getFullSkinPath().'images/avtar.png';
								}
							}
							$answer = '<ul class="childcomment"><li><img class="authorimg" src="'.$aprofilimage.'" alt=""><h5 class="user">'.htmlentities($question->atitel).'</h5><div class="entry-meta">'.Language::DirectTranslate('plugin_events_by').' '.Plugin_Profile_Manager::showUsername($auserdata->name,'author').' '.Language::DirectTranslate('plugin_events_am').' <span class="published">'.Plugin_Events_Manager::formatDate($question->adatum).'</span> <span><a href="/melden.html?source=mealanswer">'.Language::DirectTranslate('plugin_melden_meldanswer').'</a></span></div><div class="commentdetail">'.nl2br(htmlentities($question->antwort)).'</div></li></ul>';
							$template->assign_loop_var("questions",$index,"answer",$answer);
						}
					}
				}
			}
			if(User::Current()->id != $event->userid){
				Plugin_Events_Manager::addCounter($event->id);
			}
			$template->assign_var('map',$map);
			echo($template->getCode());
			if(!Mobile::isMobileDevice()){
				//Sidebar
				include('./system/skins/socialmeal/sidebar.php');
			}
		}

		function getHeader(){
			if(isset($_GET['event'])){
				$event = Plugin_Events_Manager::loadEvent($_GET['event']);
				$bb = new Plugin_BBCode_Translator();
				echo('
				<meta property="og:title" content="'.htmlentities($event->eventname).'" />
				<meta property="og:description" content="'.strip_tags($bb->replace($event->beschreibung)).'" />
				<meta property="og:type" content="website" />
				<meta property="og:url" content="'.Settings::getInstance()->get("host").'event-details.html?event='.htmlentities($event->id).'" />
				<meta property="og:image" content="'.Settings::getInstance()->get("host").'content/uploads/eat/'.Plugin_Events_Manager::getFirstImageFromEvent($event->id).'" />
				<link rel="image_src" href="'.Settings::getInstance()->get("host").'content/uploads/eat/'.DataBase::Current()->ReadField("SELECT filename FROM {'dbprefix'}plugin_eventimages WHERE eventid = '".Plugin_Events_Manager::getFirstImageFromEvent($event->id)."' LIMIT 1").'" />
				');
				echo('<script type="text/javascript" src="/system/plugins/events/js/newquestion.js"></script>');
				echo('<script type="text/javascript" src="/system/plugins/events/js/newreply.js"></script>');
				echo('<script type="text/javascript" src="/system/plugins/bbcode/js/editor.js"></script>');
			}
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
