<?php
/*
 * ratings.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_Events_Ratings extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
			if(!isset($_GET['user'])){
				$page->title = Language::DirectTranslate('plugin_events_ratings1');
			}else{
				if($_GET['user'] == User::Current()->id){
					$page->title = Language::DirectTranslate('plugin_events_ratings1');
				}else{
					$page->title = utf8_encode(Language::DirectTranslate('plugin_events_ratings2'))." ".Plugin_Profile_Manager::getUserDataPerId($_GET['user'])->name;
				}
			}
		}
		public function display(){
			$template = new Template();
			$template->load('plugin_events_ratings');
			$template->show_if('savemsg',false);
			$template->show_if('userisdel',false);
			if(isset($_GET['user'])){
				$userid = DataBase::Current()->EscapeString($_GET['user']);
			}else{
				$userid = User::Current()->id;
			}
			if($userid == User::Current()->id){
				Plugin_Profile_Manager::updateLatestRating();
			}
			if(isset($_POST['sendrate'])){
				//Antwort speichern
				$rateinfo = Plugin_Events_Manager::getRating($_POST['rateid']);
				if(User::Current()->id == $rateinfo->rateid && $rateinfo->ratereply == ''){
					Plugin_Events_Manager::saveReplyRate($_POST['rateid'],$_POST['reply']);
					if(Plugin_Profile_Manager::getUserSetting('ratereply',$rateinfo->userid)){
						$userdata = Plugin_Profile_Manager::getUserDataPerId($rateinfo->userid);
						$rateuserinfo = Plugin_Profile_Manager::getUserDataPerId($rateinfo->rateid);
						$subject = utf8_decode("Du hast eine Antwort auf eine deiner Bewertungen auf Social Meal erhalten");
						$mailtext = 'Liebe*r '.ucfirst($userdata->firstname).'<br><br>Gerne möchten wir dich darüber informieren, dass du eine Antwort von dem User <a href="'.Settings::getInstance()->get("host").'mein-profil.html?profile='.$rateuserinfo->name.'">'.$rateuserinfo->name.'</a> auf deine Bewertung erhalten hast.<br><br>Du kannst die Antwort <a href="'.Settings::getInstance()->get("host").'bewertungen.html?user='.$rateuserinfo->id.'">hier</a> lesen.<br><br>Alles was du zum Bewertungssystem wissen musst, erfährst du unter <a href="'.Settings::getInstance()->get("host").'FAQ/so-funktioniert-das-bewertungssystem.html">So funktioniert unser Bewertungssystem</a>.<br><br>Falls du der Meinung bist, dass dieser User die Funktion zur Bewertungs-Beantwortung missbräuchlich verwendet, melde uns das bitte über die entsprechende <a href="'.Settings::getInstance()->get("host").'melden.html?source=mail&g=ratereply_'.$rateinfo->id.'">Melde-Seite</a>.<br><br>Wir wünschen dir weiterhin gute Interaktionen in unserer Community!<br>Social Meal<br><br>P.S.: Diesen E-Mail-Typ kannst du in den <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abbestellen, falls er dir überflüssig erscheint.';
						$h2t = new Plugin_Mailer_html2text($mailtext);
						$mail = new Plugin_PHPMailer_PHPMailer;
						$mail->IsSMTP();
						$mail->AddAddress($userdata->email);
						$mail->WordWrap = 50;
						$mail->IsHTML(true);
						$mail->Subject = $subject;
						$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
						$mail->AltBody = $h2t->get_text();
						if(!$mail->Send()){
							echo('Mail Error:'.$mail->ErrorInfo);
						}
					}
					$template->show_if('savemsg',true);
				}
			}
			$template->assign_Var('username',htmlentities(Plugin_Profile_Manager::getUserDataPerId($userid)->name));
			if(Plugin_Profile_Manager::isFriend($userid) or $userid == User::Current()->id){
				$replyname = Plugin_Profile_Manager::getUserDataPerId($userid)->firstname.' '.Plugin_Profile_Manager::getUserDataPerId($userid)->lastname;
			}else{
				$replyname = Plugin_Profile_Manager::getUserDataPerId($userid)->name;
			}
			$template->assign_var('showusername',htmlentities($replyname));
			if(Plugin_Profile_Manager::profileIsDel(Plugin_Profile_Manager::getUserDataPerId($userid)->name)){
				$template->show_if('userisdel',true);
				$template->show_if('ratings',false);
				$template->show_if('noratings',false);
				echo($template->getCode());
				exit;
			}
			if(Plugin_Events_Manager::countRatings($userid) == 0){
				$template->show_if('ratings',false);
				$template->show_if('noratings',true);
			}else{
				$template->show_if('ratings',true);
				$template->show_if('noratings',false);
				foreach(Plugin_Events_Manager::getRatings($userid) as $rating){
					$userinfo = Plugin_Profile_Manager::getUserDataPerId($rating->userid);
					$eventinfo = Plugin_Events_Manager::loadEvent($rating->eventid);
					$index = $template->add_loop_item("ratings");
					$template->assign_loop_var("ratings",$index,"rateid",htmlentities($rating->id));
					$template->assign_loop_var("ratings",$index,"text",nl2br(htmlentities($rating->text)));
					$template->assign_loop_var("ratings",$index,"date",Plugin_Events_Manager::formatDate($rating->datum));
					$template->assign_loop_var("ratings",$index,"usernamelink",htmlentities($userinfo->name));
					$template->assign_loop_var("ratings",$index,"eventid",htmlentities($eventinfo->id));
					$template->assign_loop_var("ratings",$index,"eventname",htmlentities($eventinfo->eventname));
					if($rating->userid == $eventinfo->userid){
						$template->assign_loop_var("ratings",$index,'ratetype','Gastgeber*in');
					}else{
						$template->assign_loop_var("ratings",$index,'ratetype','Gast');
					}
					if(Plugin_Profile_Manager::isFriend($rating->userid) or User::Current()->id == $userinfo->id){
						$template->assign_loop_var("ratings",$index,"username",htmlentities(ucfirst($userinfo->firstname).' '.ucfirst($userinfo->lastname)));
					}else{
						$template->assign_loop_var("ratings",$index,"username",htmlentities($userinfo->name));
					}
					if(Plugin_Profile_Manager::isVerified($userinfo->name) == 2){
						$template->assign_loop_var("ratings",$index,'verify','<a infotext="'.Language::DirectTranslate('plugin_profile_isverify').'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('tick').'" /></span></a>');
					}else{
						$template->assign_loop_var("ratings",$index,'verify','<a infotext="'.Language::DirectTranslate('plugin_profile_noverify').'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('cancel').'" /></span></a>');
					}
					if($rating->stars >= 1){
						$template->assign_loop_var("ratings",$index,"star1","on");
					}else{
						$template->assign_loop_var("ratings",$index,"star1","off");
					}
					if($rating->stars >= 2){
						$template->assign_loop_var("ratings",$index,"star2","on");
					}else{
						$template->assign_loop_var("ratings",$index,"star2","off");
					}
					if($rating->stars >= 3){
						$template->assign_loop_var("ratings",$index,"star3","on");
					}else{
						$template->assign_loop_var("ratings",$index,"star3","off");
					}
					if($rating->stars >= 4){
						$template->assign_loop_var("ratings",$index,"star4","on");
					}else{
						$template->assign_loop_var("ratings",$index,"star4","off");
					}
					if($rating->stars == 5){
						$template->assign_loop_var("ratings",$index,"star5","on");
					}else{
						$template->assign_loop_var("ratings",$index,"star5","off");
					}
					if($rating->ratereply == ''){
						if(User::Current()->id == $userid){
							$template->assign_loop_var("ratings",$index,"ratereply",'<form name="newanswer" method="post" action="/bewertungen.html" onsubmit="return chkFormular()"><input type="hidden" name="rateid" value="'.htmlentities($rating->id).'" /><blockquote class="style1">'.Language::DirectTranslate('plugin_events_ratereply').':<br /><textarea name="reply" rows="5" cols="45" required="required" class="required"></textarea><br /><input type="submit" name="sendrate" value="'.Language::directTranslate('save').'" /></blockquote></form>');
						}else{
							$template->assign_loop_var("ratings",$index,"ratereply",'');
						}
					}else{
						if(Plugin_Profile_Manager::isFriend($rating->rateid) or $rating->rateid == User::Current()->id){
							$replyname = ucfirst(Plugin_Profile_Manager::getUserDataPerId($rating->rateid)->firstname).' '.ucfirst(Plugin_Profile_Manager::getUserDataPerId($rating->rateid)->lastname);
						}else{
							$replyname = Plugin_Profile_Manager::getUserDataPerId($rating->rateid)->name;
						}
						if(Plugin_Profile_Manager::isVerified(Plugin_Profile_Manager::getUserDataPerId($rating->rateid)->name) == 2){
							$verify = '<a infotext="'.Language::DirectTranslate('plugin_profile_isverify').'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('tick').'" /></span></a>';
						}else{
							$verify = '<a infotext="'.Language::DirectTranslate('plugin_profile_noverify').'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('cancel').'" /></span></a>';
						}
						$template->assign_loop_var("ratings",$index,"ratereply",'<hr /><blockquote class="style1">'.Language::DirectTranslate('plugin_events_answerfrom').' <a href="/mein-profil.html?profile='.Plugin_Profile_Manager::getUserDataPerId($rating->rateid)->name.'">'.htmlentities($replyname).'</a> '.$verify.' '.Language::DirectTranslate('plugin_events_am').' '.Plugin_Events_Manager::formatDate($rating->replydatum).':<br />'.nl2br(htmlentities($rating->ratereply)).' <a href="/melden.html?source=ratereply">'.Language::DirectTranslate("plugin_melden_meldanswer").'</a></blockquote>');
					}
				}
			}
			echo($template->getCode());
		}

		function getHeader(){
			echo('<script type="text/javascript" src="/system/plugins/events/js/ratereply.js"></script>');
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
