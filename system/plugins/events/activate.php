<?PHP
/*
 * activate.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	Language::ClearCache();

	DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Events_Detail', '{LANG:PLUGIN_EVENTS_PAGETYPETITLE}'); ");
	DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Events_Edit', '{LANG:PLUGIN_EVENTS_PAGETYPETITLEEDIT}'); ");
	DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Events_New', '{LANG:PLUGIN_EVENTS_PAGETYPETITLENEW}'); ");
	DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Events_Home', '{LANG:PLUGIN_EVENTS_PAGETYPETITLEHOME}'); ");
	DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Events_Subscribe', '{LANG:PLUGIN_EVENTS_PAGETYPETITLESUBSCRIBE}'); ");
	DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Events_UnSubscribe', '{LANG:PLUGIN_EVENTS_PAGETYPETITLEUNSUBSCRIBE}'); ");
	DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Events_Delete', '{LANG:PLUGIN_EVENTS_PAGETYPETITLEDELETE}'); ");
	DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Events_Unload', '{LANG:PLUGIN_EVENTS_PAGETYPETITLEUNLOAD}'); ");
	DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Events_Filter', '{LANG:PLUGIN_EVENTS_PAGETYPETITLEFILTER}'); ");
	DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Events_Rate', '{LANG:PLUGIN_EVENTS_PAGETYPETITLERATE}'); ");
	DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Events_Ratings', '{LANG:PLUGIN_EVENTS_PAGETYPETITLERATINGS}'); ");
	DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Events_AdminPromo', '{LANG:PLUGIN_EVENTS_PAGETYPETITLEADMINPROMO}'); ");
	DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Events_AdminEdit', '{LANG:PLUGIN_EVENTS_PAGETYPETITLEADMINEDIT}'); ");
	DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Events_Widget', '{LANG:PLUGIN_EVENTS_PAGETYPETITLEWIDGET}'); ");
?>
