/*
 * en.eventtype1.js
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
function chkFormular () {
	if (document.editevent.eventname.value == "") {
		alert("Du musst einen Namen für dein Meal eingeben!");
		document.editevent.eventname.focus();
		return false;
	}
	if (document.editevent.rest.value == "") {
		alert("Du musst den Namen des Restaurant ausfüllen.");
		document.editevent.rest.focus();
		return false;
	}
	if (document.editevent.street.value == "") {
		alert("Bitte gebe den Strassennamen des Restaurant ein.");
		document.editevent.street.focus();
		return false;
	}
	if (document.editevent.strnr.value == "") {
		alert("Bitte gib eine Hausnummer ein");
		document.editevent.strnr.focus();
		return false;
	}
	if (document.editevent.evdate.value == "") {
		alert("Du musst ein Mealdatum eingeben");
		document.editevent.evdate.focus();
		return false;
	}
	if (document.editevent.asdate.value == "") {
		alert("Du musst ein Anmeldeschluss eingeben");
		document.editevent.asdate.focus();
		return false;
	}
	if (document.editevent.asdate.value > document.editevent.evdate.value) {
		alert("Der Anmeldeschluss muss vor dem Mealdatum sein!");
		document.editevent.asdate.focus();
		return false;
	}
	if (document.editevent.asdate.value < document.editevent.datehour.value){
		alert('Dein Anmeldeschluss ist zu kurzfristig angelegt!');
		document.editevent.asdate.focus();
		return false;
	}
	if (document.editevent.evdate.value < document.editevent.datehour.value){
		alert('Dein Mealdatum ist zu kurzfristig angelegt!');
		document.editevent.evdate.focus();
		return false;
	}
	if (document.editevent.maxguest.value == "") {
		alert("Du musst angeben wie viele Gäste du maximal empfangen kannst.");
		document.editevent.maxguest.focus();
		return false;
	}
	if (document.editevent.maxguest.value <= 0) {
		alert("Die Anzahl Gäste darf nicht 0 oder negativ sein. Du musst min. einen Gast empfangen!");
		document.editevent.maxguest.focus();
		return false;
	}
	if (document.editevent.abouteat.value == "") {
		alert("Du musst dein Meal beschreiben. Bitte gib eine Mealbeschreibung ein.");
		document.editevent.abouteat.focus();
		return false;
	}
	if (document.editevent.abouteat.value.length < 10) {
		alert("Bitte beschreibe dein Meal mit min. 10 Zeichen!");
		document.editevent.abouteat.value.focus();
		return false;
	}
	if (document.editevent.plz.value == "") {
		alert("Du musst eine PLZ eingeben.");
		document.editevent.plz.focus();
		return false;
	}
	if (document.editevent.plz.value <= 0) {
		alert("Die PLZ darf keine negative Zahl sein");
		document.editevent.plz.focus();
		return false;
	}
	if (document.editevent.plz.value < 1000) {
		alert("Die PLZ muss grösser als 1000 sein!");
		document.editevent.plz.focus();
		return false;
	}
	if (document.editevent.ort.value == "") {
		alert("Bitte gib dein Ort des Restaurant an.");
		document.editevent.ort.focus();
		return false;
	}
}
