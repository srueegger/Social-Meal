<?PHP
/*
 * info.php
 * 
 * Copyright 2016 Samuel Rüegger <rueegger@posteo.de>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

  $pluginInfo = new PluginInfo();
  $pluginInfo->path               = "errorlistwidget";
  $pluginInfo->name               = "ErrorList Widget";
  $pluginInfo->description        = "Listet die Error Logs auf";
  $pluginInfo->authorName         = utf8_decode("Samuel Rüegger");
  $pluginInfo->authorLink         = "https://2lounge.ch/";
  $pluginInfo->version            = "1.0.0";
  $pluginInfo->license            = "GPL 3";
  $pluginInfo->licenseUrl         = "http://www.gnu.org/licenses/gpl.html";
  $pluginInfo->supportedLanguages = array("de","en");
  $this->Add($pluginInfo);
?>
