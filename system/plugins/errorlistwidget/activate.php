<?PHP
/*
 * activate.php
 * 
 * Copyright 2016 Samuel Rüegger <rueegger@posteo.de>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

	WidgetController::register("ErrorListWidget","ErrorList","errorlistwidget/errorlistwidget.php");
	$row = DataBase::Current()->ReadField("SELECT IFNULL(MAX(row),0) + 1 FROM {'dbprefix'}dashboards WHERE col = 1");
	DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}dashboards` (alias,col,row,path) VALUES ('admin/home', 2, ".$row.", 'errorlistwidget/errorlistwidget.php');");
?>
