<?PHP
/*
 * de.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	$tokens["plugin_description"]		= utf8_decode("Dieses Plugin ermöglicht die Benutzerregistrierung");
	$tokens["pagetypetitle"]			= "Benutzerregistrierungsformular - neu";
	$tokens["captcha"]					= "Bitte Captcha eingeben";
	$tokens["agb"]						= 'Ich habe die AGB gelesen und bin damit einverstanden.';
	$tokens["submittext"]				= "Registrierung abschliessen";
	$tokens["go"]						= "Weiter";
	$tokens["nlsm"]						= "Newsletter von Social Meal abonnieren";
	$tokens["nlob"]						= "Newsletter von Occupy Basel abonnieren";
	$tokens["iname"]					= utf8_decode("Mit diesem Namen kannst du dich später einloggen. Der Benutzername ist öffentlich sichtbar.");
	$tokens["imail"]					= utf8_decode("Bitte gib eine gültige E-Mail Adresse ein. Wir schicken dir nach der Registrierung einen Bestätigungslink an diese E-Mail Adresse. Deine E-Mail Adresse wird vertraulich behandelt und ist auf deinem Profil nicht sichtbar.");
	$tokens["ipasswort"]				= utf8_decode("Wir empfehlen dir ein sicheres Passwort zu verwenden. Sichere Passwörter bestehen aus einer Kombination von Klein- und Grossbuchstaben, Zahlen und Sonderzeichen. Das Passwort sollte eine Mindestlänge von 8 Zeichen haben.");
	$tokens["icpasswort"]				= "Gib das identische Passwort nochmals ein.";
	$tokens["inlsm"]					= utf8_decode("Aktuelle Informationen über Social Meal. Noch keine Häufigkeit/Regelmässigkeit festgelegt.");
	$tokens["inlob"]					= utf8_decode("Occupy Basel ist die Trägerorganisation von Social Meal. Newsletter erscheint jede zweite Woche mit aktuellen Aktivitäten.");
	$tokens["ihave"]					= "Ich habe die";
	$tokens["haveread"]					= "gelesen und bin damit einverstanden";
	$tokens["datasec"]					= "Datenschutzbestimmungen";
	$tokens["agb"]						= "AGB";
	$tokens["und"]						= "und";
	$tokens["registerok"]				= "Du hast eben eine E-Mail von uns erhalten. Bitte klicke auf den dortigen Aktivierungslink um deine Registrierung abzuschliessen. Falls du keine E-Mail von uns findest schaue bitte in deinem Spam Ordner nach.";
	$tokens["userexist"]				= "Dein Benutzername ist bereits bei uns im System vorhanden. Wenn du dein Passwort vergessen hast, kannst du";
	$tokens["userexist2"]				= "Deine E-Mail Adresse ist bereits bei uns im System vorhanden. Wenn du dein Passwort vergessen hast, kannst du";
	$tokens["hier"]						= "hier";
	$tokens["new"]						= "ein neues anfordern";
	$tokens["registerfinish"]			= utf8_decode("Herzlichen Glückwunsch, du hast dich erfolgreich bei uns angemeldet.");
	$tokens["clickhere"]				= "Klicke hier";
	$tokens["tologin"]					= "um dich einzuloggen";
	$tokens["ilastname"]				= utf8_decode("Vor- und Nachname werden deinen Gästen oder Gastgeber*innen per Mail mitgeteilt. Auch bestätigte Freunde auf Social Meal sowie du selber sehen deinen richtigen Namen, anstelle deines Benutzernamens.");
	$tokens["titlename"]				= "Der Benutzername muss mindestens 3 Zeichen enthalten.";
	$tokens["gologin"]					= "Einloggen";
	$tokens["goregister"]				= "Neues Profil erstellen";
	$tokens["goregister2"]				= "Du bist noch nicht Mitglied von Social Meal?";
	$tokens["goregister3"]				= "Erstelle hier ein neues Profil!";
	$tokens["confirm"]					= utf8_decode("bestätigen");
	$tokens["invite"]					= "Empfohlen von";
	$tokens["bemember"]					= "Mitglied werden";
	$tokens["falsestring"]				= utf8_decode("Der Benutzername enthält ungültige Zeichen");
?>
