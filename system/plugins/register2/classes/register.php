<?PHP
/*
 * register.php
 *
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
	class Plugin_Register2_Register extends Editor{

		/**
		 *
		 * @param Page $page
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			if(Mobile::isMobileDevice()){
				$template->load('plugin_register2_registerform.mobile');
			}else{
				$template->load('plugin_register2_registerform');
			}
			$template->show_if('falsename',false);
			$template->show_if('falsemail',false);
			$template->show_if('falsestring',false);
			$template->show_if('step1',false);
			$template->show_if('step2',false);
			$template->show_if('step3',false);
			$template->show_if('step4',false);
			$template->show_if('redirect',false);
			if(User::Current()->role->ID != 1){
				$template->show_if('redirect',true);
				echo($template->getCode());
				exit;
			}
			if(isset($_GET['user']) && isset($_GET['acode'])){
				$md5mail = DataBase::Current()->EscapeString($_GET['user']);
				$userdata = Plugin_PluginData_Data::getData('user_'.$md5mail.'','plugin','register2');
				if($_GET['acode'] == $userdata['acode']){
					$register = new User();
					$register->name = $userdata['name'];
					$register->email = $userdata['email'];
					$register->role = 4;
					$register->insert($userdata['passwort']);
					$userinfo = Plugin_Profile_Manager::getUserData($userdata['name']);
					$subject = 'Neue Anmeldung';
					$mail2 = new Plugin_PHPMailer_PHPMailer;
					$mail2->IsSMTP();
					$mail2->AddAddress('info+newsletter@socialmeal.ch');
					$mail2->WordWrap = 50;
					$mail2->IsHTML(true);
					$mail2->Subject = $subject;
					$mail2->Body = Plugin_Mailer_Mailer::makeHtml('Neue Anmeldung','Neue Anmeldung von: <a href="'.Settings::getInstance()->get("host").'mein-profil.html?profile='.$userdata['name'].'">'.$userdata['name'].'</a> ('.ucfirst($userdata['firstname']).' '.ucfirst($userdata['lastname']).')<br>ID: '.$userinfo->id.'<br>E-Mail: '.$userdata['email'].'<br><br>Empfohlen von: '.$userdata['invite'].'');
					if(!$mail2->Send()){
						echo('Mail Error:'.$mail2->ErrorInfo);
					}
					DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_plugindata_data WHERE property = 'user_".$md5mail."' AND area = 'register2'");
					Plugin_Profile_Manager::setAllSettings($userinfo->id);
					Plugin_Profile_Manager::setAGBVersions($userinfo->id);
					Plugin_Profile_Manager::setNLSettings($userinfo->id,$userdata);
					Plugin_Profile_Manager::setProfileInfos($userinfo->id,$userdata);
					Plugin_Profile_Manager::setAllBlogsRead($userinfo->id);
					Plugin_Events_Manager::setAllAsReadFromUser($userinfo->id);
					$template->show_if('step4',true);
					echo($template->getCode());
					exit;
				}
			}
			if(!isset($_POST['plugin_register_go'])){
				$template->show_if('step1',true);
				$template->show_if('redirect',false);
				$template->assign_var('username','');
				$template->assign_var('email','');
				$template->assign_var('passwort','');
				$template->assign_var('nlsm','checked="checked"');
				$template->assign_var('nlob','checked="checked"');
			}else{
				if(Plugin_Profile_Manager::userExist($_POST['name'])){
					//Step1 Benutzername bereits vorhanden
					$template->show_if('step1',true);
					$template->show_if('falsename',true);
					$template->show_if('redirect',false);
					$template->assign_var('username',htmlentities($_POST['name']));
					$template->assign_var('email',htmlentities($_POST['email']));
					$template->assign_var('passwort',htmlentities($_POST['passwort']));
					if(isset($_POST['nlsm'])){
						$template->assign_var('nlsm','checked="checked"');
					}else{
						$template->assign_var('nlsm','');
					}
					if(isset($_POST['nlob'])){
						$template->assign_var('nlob','checked="checked"');
					}else{
						$template->assign_var('nlob','');
					}
				}elseif(Plugin_Profile_Manager::mailExist($_POST['email'])){
					//E-Mail Adresse bereits vorhanden
					$template->show_if('step1',true);
					$template->show_if('falsemail',true);
					$template->show_if('redirect',false);
					$template->assign_var('username',htmlentities($_POST['name']));
					$template->assign_var('email',htmlentities($_POST['email']));
					$template->assign_var('passwort',htmlentities($_POST['passwort']));
					if(isset($_POST['nlsm'])){
						$template->assign_var('nlsm','checked="checked"');
					}else{
						$template->assign_var('nlsm','');
					}
					if(isset($_POST['nlob'])){
						$template->assign_var('nlob','checked="checked"');
					}else{
						$template->assign_var('nlob','');
					}
				}elseif(!preg_match("#^[a-zA-Z0-9äöüß _-]+$#si",$_POST['name'])){
					//Step 1
					$template->show_if('step1',true);
					$template->show_if('falsestring',true);
					$template->assign_var('username',htmlentities($_POST['name']));
					$template->assign_var('email',htmlentities($_POST['email']));
					$template->assign_var('passwort',htmlentities($_POST['passwort']));
					if(isset($_POST['nlsm'])){
						$template->assign_var('nlsm','checked="checked"');
					}else{
						$template->assign_var('nlsm','');
					}
					if(isset($_POST['nlob'])){
						$template->assign_var('nlob','checked="checked"');
					}else{
						$template->assign_var('nlob','');
					}
				}else{
					//Step 2
					if(empty($_POST['url'])){
						$template->show_if('step2',true);
						$range = 100;
						$limit = 14;
						$current = date('Y');
						$eldest = $current - $range;
						$recent = $current - $limit;
						foreach(range($recent,$eldest) as $year){
							$index = $template->add_loop_item('gbdyears');
							$template->assign_loop_var("gbdyears",$index,"year",$year);
							if($year != $gbdatum[0]){
								$template->assign_loop_var("gbdyears",$index,"selected",'');
							}else{
								$template->assign_loop_var("gbdyears",$index,"selected",'selected="selected"');
							}
						}
						$template->assign_var('username',htmlentities($_POST['name']));
						$template->assign_var('email',htmlentities($_POST['email']));
						$template->assign_var('passwort',htmlentities($_POST['passwort']));
						if(isset($_SESSION['invite'])){
							$template->assign_var('invite',$_SESSION['invite']);
						}else{
							$template->assign_var('invite','');
						}
						if(isset($_POST['nlsm'])){
							$template->assign_var('nlsm',1);
						}else{
							$template->assign_var('nlsm',0);
						}
						if(isset($_POST['nlob'])){
							$template->assign_var('nlob',1);
						}else{
							$template->assign_var('nlob',0);
						}
					}
				}
			}
			if(isset($_POST['plugin_register_submit'])){
				$template->show_if('step2',false);
				$template->show_if('step1',false);
				$template->show_if('step4',false);
				$template->show_if('step3',true);
				$template->show_if('redirect',false);
				$userdata = array();
				$userdata['name'] = DataBase::Current()->EscapeString($_POST['name']);
				$userdata['email'] = DataBase::Current()->EscapeString($_POST['email']);
				$userdata['passwort'] = DataBase::Current()->EscapeString($_POST['passwort']);
				$userdata['acode'] = md5($userdata['name'].$userdata['email']);
				$userdata['firstname'] = DataBase::Current()->EscapeString($_POST['firstname']);
				$userdata['lastname'] = DataBase::Current()->EscapeString($_POST['lastname']);
				$userdata['street']	= DataBase::Current()->EscapeString($_POST['street']);
				$userdata['strnr'] = DataBase::Current()->EscapeString($_POST['strnr']);
				$userdata['plz'] = DataBase::Current()->EscapeString($_POST['plz']);
				$userdata['ort'] = DataBase::Current()->EscapeString($_POST['ort']);
				$userdata['kanton'] = DataBase::Current()->EscapeString($_POST['kanton']);
				$userdata['tel'] = DataBase::Current()->EscapeString($_POST['tel']);
				$userdata['nlsm'] = DataBase::Current()->EscapeString($_POST['nlsm']);
				$userdata['nlob'] = DataBase::Current()->EscapeString($_POST['nlob']);
				$userdata['gender'] = DataBase::Current()->EscapeString($_POST['gender']);
				$userdata['invite'] = DataBase::Current()->EscapeString($_POST['invite']);
				$userdata['gbdatum'] = $_POST['gbdyear'].'-'.$_POST['gbmonth'].'-'.$_POST['gbday'];
				$md5mail = md5($userdata['email'].time());
				Plugin_PluginData_Data::setData('user_'.$md5mail.'',$userdata,'register2','plugin');
				$activationlink = UrlRewriting::GetUrlByAlias($this->page->alias).'?user='.$md5mail.'&acode='.$userdata['acode'].'';
				$subject = utf8_decode('Bestätigung deiner Registrierung auf Social Meal');
				$body = 'Liebe*r '.$userdata['name'].'<br><br>Um deine Registrierung abzuschliessen, klicke bitte auf diesen Link:<br><a href="'.$activationlink.'">'.$activationlink.'</a><br>Mit der Registrierung bestätigst du die Befolgung unserer <a href="'.Settings::getInstance()->get("host").'agb.html">AGBs</a>.<br><br>Falls du dich nicht mit dieser E-Mail-Adresse auf <a href="'.Settings::getInstance()->get("host").'">socialmeal.ch</a> registriert hast, ignoriere bitte diese Nachricht.<br><br>Nach der Bestätigung deiner Registrierung gehst du am besten gleich zum <a href="'.Settings::getInstance()->get("host").'login.html">Login</a>. Im Anschluss würde es uns freuen, wenn du dein Profil möglichst ausführlich ausfüllst. Tipps dazu findest du auf <a href="'.Settings::getInstance()->get("host").'FAQ/so-erstellst-du-ein-ansprechendes-profil.html">So erstellst du ein ansprechendes Profil</a>. Alle Antworten auf allfällige Fragen zum Datenschutz findest du unter <a href="'.Settings::getInstance()->get("host").'FAQ/sichtbarkeit-und-verwendung-der-personlichen-daten.html">Sichtbarkeit und Verwendung der persönlichen Daten</a>.<br><br>Wir wünschen dir eine gute Zeit in unserer Community!<br>Social Meal';
				$h2t = new Plugin_Mailer_html2text($body);
				$mail = new Plugin_PHPMailer_PHPMailer;
				$mail->IsSMTP();
				$mail->AddAddress($userdata['email']);
				$mail->WordWrap = 50;
				$mail->IsHTML(true);
				$mail->Subject = $subject;
				$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$body);
				$mail->AltBody = $h2t->get_text();
				if(!$mail->Send()){
					echo('Mail Error:'.$mail->ErrorInfo);
				}
				if($userdata['nlsm'] == 1){
					Plugin_Profile_Manager::sendNLSMMail($userdata);
				}
				if($userdata['nlob'] == 1){
					Plugin_Profile_Manager::sendNLOBMail($userdata);
				}
			}
			echo($template->getCode());
		}

		function getHeader(){
			echo('<script type="text/javascript" src="/system/plugins/register2/js/registerform.js"></script>');
			echo('<script type="text/javascript" src="/system/plugins/register2/js/registerform2.js"></script>');
		}

		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}

		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage
		 */
		public function save(Page $newPage,Page $oldPage){
				$this->page = $newPage;
				$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
				$this->page->save();
		}

	}
?>
