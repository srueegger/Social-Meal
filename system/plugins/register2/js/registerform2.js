/*
 * registerform2.js
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
function chkFormular2 () {
	if (document.registerform2.firstname.value == "") {
		alert("Bitte gib einen Vornamen ein");
		document.registerform2.firstname.focus();
		return false;
	}
	if (document.registerform2.lastname.value == "") {
		alert("Bitte gib einen Nachnamen ein");
		document.registerform2.lastname.focus();
		return false;
	}
	if (document.registerform2.street.value == "") {
		alert("Bitte gib eine Strasse ein");
		document.registerform2.street.focus();
		return false;
	}
	if (document.registerform2.strnr.value == "") {
		alert("Bitte gib eine Strassennummer ein");
		document.registerform2.street.focus();
		return false;
	}
	if (document.registerform2.plz.value == "") {
		alert("Bitte gebe deine PLZ ein!");
		document.registerform2.plz.focus();
		return false;
	}
	var chkZplz = 1;
	for (i = 0; i < document.registerform2.plz.value.length; ++i)
		if (document.registerform2.plz.value.charAt(i) < "0" || 
		document.registerform2.plz.value.charAt(i) > "9")
		chkZplz = -1;
	if(chkZplz == -1) {
		alert("PLZ ist keine Zahl!");
		document.registerform2.plz.focus();
		return false;
	}
	if (document.registerform2.plz.value < 1000) {
		alert("Die PLZ darf nicht kleiner als 1000 sein!");
		document.registerform2.plz.focus();
		return false;
	}
	if (document.registerform2.ort.value == "") {
		alert("Bitte gebe deinen Wohnort ein!");
		document.registerform2.ort.focus();
		return false;
	}
	if (document.registerform2.tel.value == "") {
		alert("Bitte gebe eine Telefonnummer ein!");
		document.registerform2.tel.focus();
		return false;
	}
	if (document.registerform2.tel.value.length < 9) {
		alert("Deine Telefonnummer muss mindestens 9 Zeichen haben!");
		document.registerform2.tel.focus();
		return false;
	}
}
