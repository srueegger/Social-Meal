/*
 * registerform.js
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
function chkFormular () {
	if (document.registerform.name.value == "") {
		alert("Bitte gib einen Benutzername ein");
		document.registerform.name.focus();
		return false;
	}
	if (document.registerform.email.value == "") {
		alert("Bitte gib einen E-Mail Adresse ein");
		document.registerform.email.focus();
		return false;
	}
	if (document.registerform.passwort.value == "") {
		alert("Bitte gib ein Passwort ein");
		document.registerform.passwort.focus();
		return false;
	}
	if (!document.registerform.agb.checked) {
		alert("Du musst die AGB akzeptieren!");
		document.registerform.agb.focus();
		return false;
	}
	if (document.registerform.email.value.indexOf("@") == -1) {
		alert("Bitte gebe eine gültige E-Mail Adresse an!");
		document.registerform.email.focus();
		return false;
	}
	if (document.registerform.confirm_passwort.value == "") {
		alert("Bitte gebe das PAsswort nochmal ein.");
		document.registerform.confirm_passwort.focus();
		return false;
	}
	if (document.registerform.confirm_passwort.value != document.registerform.passwort.value) {
		alert("Beide Passwörter müssen identtisch sein!");
		document.registerform.confirm_passwort.focus();
		return false;
	}
}
