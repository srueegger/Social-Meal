<?php
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

	class Plugin_Log_Message{
		private $type           = null;
		private $area           = null;
		private $areaType       = null;
		private $level          = null;
		private $details        = array();
		
		const   LEVEL_DEBUG     = "DEBUG";
		const   LEVEL_NOTICE    = "NOTICE";
		const   LEVEL_WARNING   = "WARNING";
		const   LEVEL_FATAL     = "FATAL";
		const   LEVEL_EMERGENCY = "EMERGENCY";
		
		public function __construct(){
			$this->add_detail("url", $this->url());
			$this->add_detail("stacktrace", $this->stacktrace());
			$this->add_detail("post", $_POST);
			$this->add_detail("get", $_GET);
			$this->add_detail("session", $_SESSION);
			$this->add_detail("page_alias", $this->page_alias());
			$this->add_detail("loaded_classes", $this->loaded_classes());
			$this->add_detail("user_agent", $this->user_agent());
		}
		
		/**
		 * Type of the error.
		 * Examples: database_crash
		 *			 invalid_license
		 * 
		 * @param string $new_value New value, optional
		 * @param boolean $allow_reset Can $new_value be null?
		 * @return string
		 */
		public function type($new_value = null, $allow_reset = false){
			if(is_string($new_value)){
				//Assign value
				$this->type = $new_value;
			}elseif(is_null($new_value) && $allow_reset){
				//Revert value
				$this->type = null;
			}
			//Return value
			return $this->type;
		}
		
		/**
		 * The name of the area, where the error accours.
		 * If you use this in a Plugin, here must be the data name of your plugin.
		 * In this case you have to fill areaType with "plugin"
		 * 
		 * @param type $new_value
		 * @param type $allow_reset
		 * @return type 
		 */
		public function area($new_value = null, $allow_reset = false){
			if(is_string($new_value)){
				//Assign value
				$this->area = $new_value;
			}elseif(is_null($new_value) && $allow_reset){
				//Revert value
				$this->area = null;
			}
			//Return value;
			return $this->area;
		}
		
		/**
		 * Type of the area (core|plugin|skin)
		 * 
		 * @param string $new_value
		 * @param type $allow_reset
		 * @return type 
		 */
		public function areaType($new_value = null, $allow_reset = false){
			if(is_string($new_value) && in_array(strtolower($new_value), array("core","plugin","skin"))){
				//Assign value
				$this->areaType = $new_value;
			}elseif(is_null($new_value) && $allow_reset){
				//Revert value
				$this->areaType = null;
			}
			//Return value
			return $this->areaType;
		}
		
		/**
		 * All supported log levels
		 * @return array(of string)
		 */
		public static function supported_levels(){
			return array(self::LEVEL_DEBUG,
						 self::LEVEL_NOTICE,
						 self::LEVEL_WARNING,
						 self::LEVEL_FATAL,
						 self::LEVEL_EMERGENCY);
		}
		
		/**
		 * The level of your message.
		 * Use the LEVEL-constants of this class
		 * @param string $new_value New value, optional
		 * @return string
		 */
		public function level($new_value = null){
			if(is_string($new_value) && in_array(strtoupper($new_value), array())){
				//Assign value
				$this->level = strtoupper($new_value);
			}
			//Return value
			return $this->level;
		}
		
		/**
		 * Returns the details of the message
		 * @return array
		 */
		public function details(){
			return $this->details;
		}
		
		/**
		 * Adds a new detail (parameter) to this message
		 * 
		 * @param string $type Name of the type. 
		 * @param mixed $value 
		 */
		public function add_detail($type, $value){
			if(is_string($type)){
				$this->details[$type] = $value;
			}
		}
		
		/**
		 * Url
		 * @return string
		 */
		public function url(){
			return "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		}
		
		/**
		 * User-Agent (Browser)
		 * @return string
		 */
		public function user_agent(){
			return $_SERVER['HTTP_USER_AGENT'];
		}
		
		/**
		 * Alias of the current page
		 * @return type 
		 */
		public function page_alias(){
			$res = null;
			if(Page::Current() != null){
				$res = Page::Current()->alias;
			}
			return $res;
		}
		
		/**
		 * Where did we come tohere?
		 * @return string 
		 */
		private function stacktrace(){
			$res = array();
			$first = true;
			foreach(debug_backtrace() as $line){
				//Remove the last line, needed for writing this
				if(!$first){
					$res[] = $line['file'].":".$line['line'];
				}
				$first = false;
			}
			return $res;
		}
		
		/**
		 * All loaded classes
		 * @return array
		 */
		public function loaded_classes(){
			return get_declared_classes();
		}
		
		/**
		 * Schickt die Meldung ab. 
		 */
		public function send(){
			EventManager::RaiseEvent("plugin_log_event",$this);
		}
	}
?>
