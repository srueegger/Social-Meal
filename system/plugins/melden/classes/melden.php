<?PHP
/*
 * melden.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_Melden_Melden extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			$template->load('plugin_melden_melden');
			$template->show_if('showmsg',false);
			$template->show_if('gast',false);
			if(isset($_POST['meldung'])){
				$mailtext = 'Daten zur Meldung:<br><br>Meldeinfo: "'.htmlentities($_POST['meldeinfo']).'"<br><br>URL der Meldung: <a href="'.htmlentities($_POST['meldeurl']).'">'.htmlentities($_POST['meldeurl']).'</a><br><br>Nachricht vom Melder:<br>'.nl2br(htmlentities($_POST['meldung'])).'<br><br>Kontaktinfos vom Melder:<br>';
				if(isset($_POST['email'])){
					$mailtext .= 'Die Meldung stammt von einem Gast. Seine E-Mail Adresse ist: <a href="mailto:'.htmlentities($_POST['email']).'">'.htmlentities($_POST['email']).'</a>';
				}else{
					$mailtext .= 'Die Meldung stamm vom User: <a href="'.Settings::getInstance()->get("host").'mein-profil.html?profile='.htmlentities(User::Current()->name).'">'.htmlentities(User::Current()->name).'</a><br>Seine E-Mail Adresse lautet: <a href="mailto:'.htmlentities(User::Current()->email).'">'.htmlentities(User::Current()->email).'</a>';
				}
				$h2t = new Plugin_Mailer_html2text($mailtext);
				$subject = utf8_decode('Neue Meldung!');
				$mail = new Plugin_PHPMailer_PHPMailer;
				$mail->IsSMTP();
				$mail->AddAddress('info+meldungen@socialmeal.ch');
				$mail->WordWrap = 50;
				$mail->IsHTML(true);
				$mail->Subject = $subject;
				$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
				$mail->AltBody = $h2t->get_text();
				if(!$mail->Send()){
					echo('Mail Error:'.$mail->ErrorInfo);
				}
				$template->show_if('showmsg',true);
			}
			if(User::Current()->role->ID == 1){
				$template->show_if('gast',true);
				$template->assign_var('gastmelden','1');
			}else{
				$template->assign_var('gastmelden','0');
			}
			if(!isset($_GET['source'])){
				$template->assign_var('meldeinfos',Language::DirectTranslate('plugin_melden_allg'));
				$template->assign_var('meldeurl',Language::DirectTranslate('plugin_events_ka'));
			}else{
				if($_GET['source'] == 'mail'){
					$getvars = explode('_',$_GET['g']);
				}
				if($_GET['source'] == "profile" && $_GET['g']!= ''){
					$userinfo = Plugin_Profile_Manager::getUserDataPerId($_GET['g']);
					$template->assign_var('meldeinfos',Language::DirectTranslate('plugin_melden_profilefrom').' '.htmlentities($userinfo->name).'');
					$template->assign_var('meldeurl',Settings::getInstance()->get("host").'mein-profil.html?profile='.htmlentities($userinfo->name));
				}elseif($_GET['source'] == "eventdetail" && $_GET['g'] != ''){
					$eventinfo = Plugin_Events_Manager::loadEvent($_GET['g']);
					$template->assign_var('meldeinfos',Language::DirectTranslate('plugin_melden_meal').' '.htmlentities($eventinfo->eventname).'');
					$template->assign_var('meldeurl',Settings::getInstance()->get("host").'event-details.html?event='.htmlentities($eventinfo->id));
				}elseif($_GET['source'] == 'mail' && $getvars[0] == 'delete'){
					$template->assign_var('meldeinfos',utf8_encode(Language::DirectTranslate('plugin_melden_del')));
					$template->assign_var('meldeurl',Language::DirectTranslate('plugin_events_ka'));
				}elseif($_GET['source'] == 'mail' && $getvars[0] == 'newquestion'){
					$template->assign_var('meldeinfos',Language::DirectTranslate('plugin_melden_quest'));
					$template->assign_var('meldeurl',Settings::getInstance()->get("host").'event-details.html?event='.htmlentities($getvars[1]));
				}elseif($_GET['source'] == 'mail' && $getvars[0] == 'newanswer'){
					$template->assign_var('meldeinfos',Language::DirectTranslate('plugin_melden_ans'));
					$template->assign_var('meldeurl',Settings::getInstance()->get("host").'event-details.html?event='.htmlentities($getvars[1]));
				}elseif($_GET['source'] == 'mail' && $getvars[0] == 'unloaduser'){
					$template->assign_var('meldeinfos',Language::DirectTranslate('plugin_melden_unload'));
					$template->assign_var('meldeurl',Settings::getInstance()->get("host").'event-details.html?event='.htmlentities($getvars[1]));
				}elseif($_GET['source'] == 'mail' && $getvars[0] == 'unsubscribeuser'){
					$template->assign_var('meldeinfos',Language::DirectTranslate('plugin_melden_unsubscribe'));
					$template->assign_var('meldeurl',Settings::getInstance()->get("host").'event-details.html?event='.htmlentities($getvars[1]));
				}elseif($_GET['source'] == 'mail' && $getvars[0] == 'newrater'){
					$template->assign_var('meldeinfos',utf8_encode(Language::DirectTranslate('plugin_melden_rate')));
					$template->assign_var('meldeurl',Settings::getInstance()->get("host").'event-details.html?event='.htmlentities($getvars[1]));
				}elseif($_GET['source'] == 'mail' && $getvars[0] == 'rareteply'){
					$template->assign_var('meldeinfos',utf8_encode(Language::DirectTranslate('plugin_melden_ratereply')));
					$template->assign_var('meldeurl',Settings::getInstance()->get("host").'event-details.html?event='.htmlentities($getvars[1]));
				}elseif($_GET['source'] == 'mail' && $getvars[0] == 'newpm'){
					$template->assign_var('meldeinfos',utf8_encode(Language::DirectTranslate('plugin_melden_pm')));
					$template->assign_var('meldeurl',Settings::getInstance()->get("host").'event-details.html?event='.htmlentities($getvars[1]));
				}elseif($_GET['source'] == 'mail' && $getvars[0] == 'aftereventmail'){
					$eventinfo = Plugin_Events_Manager::loadEvent($getvars[1]);
					$template->assign_var('meldeinfos',Language::DirectTranslate('plugin_melden_meal').' '.htmlentities($eventinfo->eventname).'');
					$template->assign_var('meldeurl',Settings::getInstance()->get("host").'event-details.html?event='.htmlentities($eventinfo->id));
				}elseif($_GET['source'] == 'mail' && $getvars[0] == 'blogcomment'){
					$template->assign_var('meldeinfos','Blog Kommentar');
					$template->assign_var('meldeurl',Settings::getInstance()->get("host").'event-details.html?event='.htmlentities($eventinfo->id));
				}else{
					$template->assign_var('meldeinfos',Language::DirectTranslate('plugin_melden_allg'));
					$template->assign_var('meldeurl',Language::DirectTranslate('plugin_events_ka'));
				}
			}
			echo($template->getCode());
		}

		function getHeader(){
			echo('<script type="text/javascript" src="'.Settings::getInstance()->get("host").'system/plugins/melden/js/checkall.js"></script>');
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
