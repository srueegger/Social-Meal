<?PHP
/*
 * de.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	$tokens["plugin_description"]		= "Mit diesem Plugin wird die komplette Meldenfunktion von Social Meal umgesetzt";
	$tokens["pagetypetitle"]			= "Melde-Seite";
	$tokens["msg"]						= utf8_decode("Vielen Dank für deine Meldung. Wir werden diese schnellstmöglich bearbeiten und dich über den Verlauf informieren.");
	$tokens["melden1"]					= utf8_decode("Bitte gib an, weshalb du diese Meldung machen möchtest.");
	$tokens["melden2"]					= utf8_decode("Folgendes möchtest du melden:");
	$tokens["submit"]					= "Meldung senden";
	$tokens["profilefrom"]				= "Profil von";
	$tokens["meal"]						= "Meal";
	$tokens["allg"]						= "Allgemeine Meldung";
	$tokens["del"]						= utf8_decode("Meal wurde gelöscht");
	$tokens["quest"]					= "Eine Frage";
	$tokens["ans"]						= "Eine Antwort";
	$tokens["unload"]					= "Ich wurde bei einem Meal ausgeladen.";
	$tokens["unsubscribe"]				= "Ein Gast hat sich von meinem Meal abgemeldet.";
	$tokens["rate"]						= utf8_decode("Missbräuchliche Bewertung erhalten.");
	$tokens["ratereply"]				= utf8_decode("Missbräuchliche Antwort auf eine Bewertung erhalten");
	$tokens["pm"]						= utf8_decode("Missbräuchliche Nachricht erhalten");
	$tokens["meldmeal"]					= "Meal melden";
	$tokens["meldblogcomment"]			= "Kommentar melden";
	$tokens["meldquest"]				= "Frage melden";
	$tokens["meldanswer"]				= "Antwort melden";
	$tokens["meldrating"]				= "Bewertung melden";
?>
