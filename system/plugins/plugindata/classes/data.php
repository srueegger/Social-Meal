<?php
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

class Plugin_PluginData_Data {

	protected static $data = array();
	public static $instance = null;
	
	protected static $hasInit = false;
	
	/**
	 * Loads the Data into the static area 
	*/
	public static function load () {
		
		$rows = DataBase::Current()->ReadRows("SELECT * FROM `{'dbprefix'}plugin_plugindata_data`");
		
		foreach ($rows as $row) {
			
			$value = $row->value;
			
			if ($value == "{LAST_UPDATE}") {
				$value = $row->last_update;
			}
		
			self::$data[$row->area][$row->property] = unserialize($value);
		}
		
	}
	
	/**
	* checks, if the plugin was initialized
	*/
	public static function hasInit () {
		if (!self::$hasInit) {
			self::load();
		}
	}
	
	/**
	* Gets the data by a specific key
	*
	* @param string $key Identifier
	* @param string $area area
	* @return mixed
	*/
	public static function getData ($key, $areaType = "core", $area = "global") {
	
		self::hasInit();
		
		if ($areaType == "" || $areaType == "core") {
			$area = "global";
		}
	
		if (isset(self::$data[$area][$key])) {
			return self::$data[$area][$key];
		} else {
			return null;
		}
	}
	
	/**
	* Saves custom data
	*
	* @param string $key identifier
	* @param mixed $value Value
	* @param string $area the name of plugin/skin
	* @param string $areaType plugin/skin
	* @param string $last_update Format: Y-m-d H:i:s
	*/
	public static function setData ($key, $value, $area = "global", $areaType = "core", $last_update = null) {
		
		$key_escape = DataBase::Current()->EscapeString($key);
		$value_escape = DataBase::Current()->EscapeString(serialize($value));
		$area_escape = DataBase::Current()->EscapeString($area);
		$areaType_escape = DataBase::Current()->EscapeString($areaType);
		
		self::hasInit();
		
		if(is_null($last_update)){
			$last_update = "NOW()";
		} else{
			$last_update = "'".DataBase::Current()->EscapeString($area)."'";
		}
		
		DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}plugin_plugindata_data` (
									`property`, `value`, `last_update`, `area`, `areaType`
									) VALUES (
									 '" . $key_escape . "', '" . $value_escape . "', " . $last_update . ", '" . $area_escape . "', '" . $areaType_escape . "'
									) ON DUPLICATE KEY UPDATE `value` = '" . $value_escape . "', `last_update` = " . $last_update . "; ");
		
		self::$data[$area][$key] = $value;
		
	}

		
}
?>
