<?php
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

/*
 * PluginData - Installationsscript 
 * Copyright JuKu
 * 
 * Package plugindata
*/

Language::ClearCache();

//Tabelle "plugin_plugindata_data" erstellen
DataBase::Current()->Execute("CREATE TABLE IF NOT EXISTS `{'dbprefix'}plugin_plugindata_data` (
  `property` varchar(600) NOT NULL,
  `value` TEXT NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `area` varchar(600) NOT NULL DEFAULT 'global',
  `areaType` varchar(600) NOT NULL DEFAULT 'core'
) DEFAULT CHARSET=latin1;");

//{'dbprefix'}

?>
