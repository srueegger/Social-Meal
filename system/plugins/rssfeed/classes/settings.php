<?php
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

	class Plugin_RssFeed_Settings
	{
		private $pageid       = null;
		private $beginns_with = "";
		private $description  = "";
		
		/**
		 * Returns the setting object from a pageid
		 * @param int $pageid id of a page
		 * @param boolean $use_dummy Empty object, if nothing was found?
		 * @return Plugin_RssFeed_Settings
		 */
		public static function get_by_pageid($pageid, $use_dummy = true)
		{
			$res = null;
			
			if(is_numeric($pageid))
			{
				$sql = "SELECT * "
					  ."FROM {'dbprefix'}plugin_rssfeed_settings "
					  ."WHERE pageid = '".DataBase::Current()->EscapeString($pageid)."'";
				
				$rows = self::get_by_sql($sql);
				if(sizeof($rows) > 0)
				{
					$res = $rows[0];
				}
			}
			
			if(!isset($res) && $use_dummy)
			{
				//Dummy
				$res = new Plugin_RssFeed_Settings();
			}
			
			return $res;
		}
		
		/**
		 * Returns an array of Plugin_RssFeed_Settings from a sql query
		 * @param String $sql query to cl_plugin_rssfeed_pages
		 * @return array(of Plugin_RssFeed_Settings)
		 */
		public static function get_by_sql($sql)
		{
			$res = array();
			
			$rows = DataBase::Current()->ReadRows($sql);
			foreach($rows as $row)
			{
				$res[] = self::get_by_array($row);
			}
			
			return $res;
		}


		/**
		 * Converts anstdClass to a Plugin_RssFeed_Settings object
		 * @param stdClass $data structure like cl_plugin_rssfeed_pages
		 * @return Plugin_RssFeed_Settings 
		 */
		public static function get_by_array(stdClass $data)
		{
			$res = new Plugin_RssFeed_Settings();
			
			$res->pageid($data->pageid);
			$res->beginns_with($data->beginns_with);
			$res->description($data->description);
			
			return $res;
		}
		
		/**
		 * ID of the page
		 * @param int $new_value optional
		 * @param boolean $allow_reset Can $new_value be null?
		 * @return int
		 */
		public function pageid($new_value = null, $allow_reset = false)
		{
			if(is_numeric($new_value))
			{
				$this->pageid = $new_value;
			}
			else if(is_null($new_value) && $allow_reset)
			{
				$this->pageid = null;
			}
			
			return $this->pageid;
		}
		
		/**
		 * Only pages starting with this value will be included
		 * inside the feed
		 * 
		 * @param string $new_value optional
		 * @param boolean $allow_reset Can $new_value be null?
		 * @return string
		 */
		public function beginns_with($new_value = null, $allow_reset = false)
		{
			if(is_string($new_value))
			{
				$this->beginns_with = $new_value;
			}
			else if(is_null($new_value) && $allow_reset)
			{
				$this->beginns_with = null;
			}
			
			return $this->beginns_with;
		}
		
		/**
		 * Description of the feed
		 * @param string $new_value optional
		 * @param boolean $allow_reset Can $new_value be null?
		 * @return string
		 */
		public function description($new_value = null, $allow_reset = false)
		{
			if(is_string($new_value))
			{
				$this->description = $new_value;
			}
			else if(is_null($new_value) && $allow_reset)
			{
				$this->description = null;
			}
			
			return $this->description;
		}
		
		public function save()
		{
			$sql = "REPLACE INTO {'dbprefix'}plugin_rssfeed_settings "
				  ."SET beginns_with = '".DataBase::Current()->EscapeString($this->beginns_with())."',"
					  ."description = '".DataBase::Current()->EscapeString($this->description())."', "
					  ."pageid = '".DataBase::Current()->EscapeString($this->pageid())."' ";
					  
			DataBase::Current()->Execute($sql);
		}
	}
?>
