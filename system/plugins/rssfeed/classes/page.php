<?php 
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

	class Plugin_RssFeed_Page extends Plugin_BlankPage_Page
	{
		public function display()
		{
			
			$settings = Plugin_RssFeed_Settings::get_by_pageid($this->page->id);
			
			$sql = "SELECT * "
				  ."FROM {'dbprefix'}plugin_events "
				  ."WHERE active = 1 "
				  ."ORDER BY id DESC";
				  
			$pages = DataBase::Current()->ReadRows($sql);
			header('Content-Type: application/rss+xml; charset=UTF-8');
				echo '<?xml version="1.0" encoding="UTF-8"?>';
			?>
				<rss version="2.0">
					<channel>
						<title><?php echo htmlentities($this->page->title); ?></title>
						<link><?php echo htmlentities(UrlRewriting::GetUrlByAlias($this->page->alias)); ?></link>
						<description><?php echo htmlentities($settings->description()); ?></description>
						<lastBuildDate><?php echo date("D, d M Y H:i:s O"); ?></lastBuildDate>
						<generator>ContentLion RssFeed Plugin</generator>
						<?php foreach($pages as $page){ ?>
							<item>
								<title><?php echo htmlentities($page->eventname); ?></title>
								<link><?php echo UrlRewriting::GetUrlByAlias("event-details","event=".$page->id); ?></link>
								<pubDate><?php echo date("D, d M Y H:i:s O",strtotime($page->eventdate)); ?></pubDate>
								<description><![CDATA[<?php echo $page->beschreibung; ?>]]></description>
							</item>
						<?php } ?>

							</channel>
				</rss>
			<?php
		}
		public function getEditableCode()
		{
			$settings = Plugin_RssFeed_Settings::get_by_pageid($this->page->id);
				
			if(isset($_POST['content']))
			{
				$settings->description($_POST['plugin_rssfeed_description']);
				$settings->beginns_with($_POST['plugin_rssfeed_beginns_with']);
				$settings->pageid($this->page->id);
				$settings->save();
			}
			
			$template = new Template();
			$template->load("plugin_rssfeed_form");
			$template->assign_var("DESCRIPTION",htmlentities($settings->description()));
			$template->assign_var("BEGINNS_WITH",htmlentities($settings->beginns_with()));
		
			return $template->getCode();
		}
		
	}
?>
