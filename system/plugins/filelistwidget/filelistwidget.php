<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

class FileListWidget extends WidgetBase{
	protected $template = null;
	public function load(){
		$this->headline = $GLOBALS['language']->getString("FILES");
		if(!isset($_GET['dir']) || substr($_GET['dir'],0,1) == '.'){
			$_GET['dir'] = "";
		}
		$uploadpath = Settings::getInstance()->get("root")."content/uploads/";
		$this->template = new Template();
		$this->template->load("plugin_filelistwidget_filelist");
		$this->template->show_if("FILES_EXIST",false);
		$this->template->show_if("NOFILES",false);
		$this->template->assign_var("DIR",htmlentities($_GET['dir']));
		$actions = ActionList::get("plugin_filelistwidget");
		$dir = $_GET['dir'];
		if(file_exists($uploadpath.$dir)){
			$this->delete($uploadpath);
			$this->rename($dir, $uploadpath);
			$verzeichnis = openDir($uploadpath.$dir);
			$pre = "";
			$path = "";
			foreach(explode("/",$dir) as $cDir){
				$index = $this->template->add_loop_item("PATH");
				$path .= "/".$cDir;
				if($path == "/"){
					$this->template->assign_loop_var("PATH", $index, "URL","/admin/index.php?page=files");
					$this->template->assign_loop_var("PATH", $index, "LABEL","/");
					$path = "";
				}else{
					$this->template->assign_loop_var("PATH", $index, "URL","/admin/index.php?page=files&dir=".$path);
					$this->template->assign_loop_var("PATH", $index, "LABEL",$cDir);
				}
			}
			if(trim($_GET['dir']) != "" & trim($_GET['dir']) != "/"){
				$this->template->assign_var("DELETEFOLDERLINK","<a href=\"/admin/index.php?page=files&rmdir=".$_GET['dir']."\">Ordner l&ouml;schen</a>");
			}else{
				$this->template->assign_var("DELETEFOLDERLINK","");
			}
			$files = FileServer::getFiles($uploadpath.$dir);
		}
		if(isset($files) && sizeOf($files) > 0){
			$this->template->show_if("FILES_EXIST",true);
			$this->template->show_if("NOFILES",false);
			foreach($files as $file){
				$index = $this->template->add_loop_item("FILES");
				$this->template->assign_loop_var("FILES",$index,"IMAGE",$this->getImageCode($uploadpath,$dir,$file));
				$this->template->assign_loop_var("FILES",$index,"FILELINK","<a href=\"../content/uploads".$dir."/".$file."\">".$file."</a>");
				$this->template->assign_loop_var("FILES",$index,"DIR",$_GET['dir']);
				$this->template->assign_loop_var("FILES",$index,"FILE",$file);
				$params = array();
				$params['dir'] = $_GET['dir'];
				$params['file'] = $file;
				$this->template->assign_loop_var("FILES",$index,"ACTIONS",$actions->getCode($params));
			}
		}else{
			$this->template->show_if("FILES_EXIST",false);
			$this->template->show_if("NOFILES",true);
		}
		$this->template->assign_var("MESSAGE","");
		$this->content = $this->template->getCode();
	}

	protected function getImageCode($uploadpath,$dir,$file){
		$res = "";
		$path_info = pathinfo($uploadpath.$dir."/".$file);
		if(strtolower($path_info['extension'] == 'jpg') or
			strtolower($path_info['extension'] == 'jpeg') or
			strtolower($path_info['extension'] == 'gif') or
			strtolower($path_info['extension'] == 'png') or
			strtolower($path_info['extension'] == 'bmp')){
			$imagePath = $this->getImagePath($dir, $file);
			$imageData = ImageServer::getImageData($imagePath);
			$res = "<img src=\"".$imagePath."\" style=\"max-width:100px;max-height:100px;\"";
			if($imageData){
				$res .= " title=\"".htmlentities($imageData->name)."\"";
			}else{
				$res .= " />";
			}
		}
		return $res;
	}

	protected function delete($uploadpath){
		if(isset($_GET['deletefile']) && file_exists($uploadpath.$_GET['dir']."/".$_GET['deletefile'])){
			if($_GET['dir'] == "" || $_GET['dir'] == "/"){
				$imagePath = Settings::getInstance()->get("host")."content/uploads/".$_GET['deletefile'];
			}else{
				$imagePath = Settings::getInstance()->get("host")."content/uploads".$_GET['dir']."/".$_GET['deletefile'];
			}
			ImageServer::delete($imagePath);
			unlink($uploadpath.$_GET['dir']."/".$_GET['deletefile']);
			$this->template->assign_var("MESSAGE",$GLOBALS['language']->getString("FILE")." ".htmlentities($_GET['deletefile'])." ".$GLOBALS['language']->getString("WAS_DELETED"));
		}
	}

	private function getImagePath($dir, $file){
		$res = "";
		if($dir == "" || $dir == "/"){
			$res = Settings::getInstance()->get("host")."content/uploads/".$file;
		}else{
			$res = Settings::getInstance()->get("host")."content/uploads".$dir."/".$file;
		}
		return $res;
	}

	private function rename($dir, $uploadpath){
		if(isset($_POST['plugin_filelistwidget_action_rename'])){
		$oldpath   = $uploadpath.$dir."/".$_POST['plugin_filelistwidget_rename_oldname'];
		$newpath   = $uploadpath.$dir."/".$_POST['plugin_filelistwidget_rename_newname'];
		if(file_exists($oldpath) && !file_exists($newpath)){
			$imagePathOld = $this->getImagePath($dir, $_POST['plugin_filelistwidget_rename_oldname']);
			$imagePathNew = $this->getImagePath($dir, $_POST['plugin_filelistwidget_rename_newname']);
			if(ImageServer::contains($imagePathOld)){
				ImageServer::move($imagePathOld,$imagePathNew);
			}
			rename($oldpath,$newpath);
		}
	}
}
}
?>
