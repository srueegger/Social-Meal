<?PHP
/*
 * deactivate.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	EventManager::removeHandler("system/plugins/blogcomments/events/ovcc.php", "plugin_blogoverview_bottom_content");
	EventManager::removeHandler("system/plugins/blogcomments/events/listform.php","plugin_blogentry_bottom_content");
	EventManager::removeHandler("system/plugins/blogcomments/events/header.php","plugin_blogentries_header");
	DataBase::Current()->Execute("DROP TABLE {'dbprefix'}plugin_comments");
	DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_plugindata_data WHERE area = 'blogcomments' AND property = 'settings'");
?>
