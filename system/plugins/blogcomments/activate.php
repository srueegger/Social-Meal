<?PHP
/*
 * activate.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	EventManager::addHandler("system/plugins/blogcomments/events/ovcc.php", "plugin_blogoverview_bottom_content");
	EventManager::addHandler("system/plugins/blogcomments/events/listform.php","plugin_blogentry_bottom_content");
	EventManager::addHandler("system/plugins/blogcomments/events/header.php","plugin_blogentries_header");
	
	$plugin_blogcomments_settings = array();
	$plugin_blogcomments_settings['gravatar'] = 1;
	Plugin_PluginData_Data::setData('settings',$plugin_blogcomments_settings,'blogcomments','plugin');
	
	DataBase::Current()->Execute("CREATE TABLE IF NOT EXISTS {'dbprefix'}plugin_comments (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  pageid INT NOT NULL,
  authorname VARCHAR(100),
  authormail VARCHAR(100),
  authorcomment TEXT,
  datetime DATETIME NOT NULL 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
?>
