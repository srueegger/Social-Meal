<?php
/*
 * settings.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	Cache::clear();
	
	$template = new Template();
	$template->load('plugin_blogcomments_settings');
	
	if(isset($_POST['plugin_blogcomments_save'])){
		$plugin_blogcomments_gravatar = DataBase::Current()->EscapeString($_POST['plugin_blogcomments_gravatar']);
		$plugin_blogcomments_settings = array();
		$plugin_blogcomments_settings['gravatar'] = $plugin_blogcomments_gravatar;
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_plugindata_data WHERE area = 'blogcomments' AND property = 'settings'");
		Plugin_PluginData_Data::setData('settings',$plugin_blogcomments_settings,'blogcomments','plugin');
	}
	
	if(isset($_GET['deletecomment'])){
		Plugin_Blogcomments_Manager::deleteComment($_GET['deletecomment']);
	}
	
	if(isset($_POST['editcommentsave'])){
		Plugin_Blogcomments_Manager::updateComment($_GET['editcomment'],$_POST);
	}
	
	$plugin_blogcomments_settings = Plugin_PluginData_Data::getData('settings','plugin','blogcomments');
	if($plugin_blogcomments_settings['gravatar'] == 1){
		$template->assign_var('SELECTEDYES',' selected="selected"');
	}else{
		$template->assign_var('SELECTEDYES','');
	}
	if($plugin_blogcomments_settings['gravatar'] == 0){
		$template->assign_var('SELECTEDNO',' selected="selected"');
	}else{
		$template->assign_var('SELECTEDNO','');
	}
	
	$plugin_blogcomments_datas = DataBase::Current()->ReadRows("SELECT id,title FROM {'dbprefix'}pages WHERE editor = 'Plugin_BlogEntries_Editor' ORDER BY update_timestamp DESC");
	foreach($plugin_blogcomments_datas as $plugin_blogcomments_data){
		$index = $template->add_loop_item('ENTRIES');
		$template->assign_loop_var('ENTRIES',$index,'ENTRYTITLE',$plugin_blogcomments_data->title);
		$template->assign_loop_var('ENTRIES',$index,'ENTRYURL',Settings::getInstance()->get("host").'admin/pluginsettings.html?plugin=blogcomments&editcomments='.$plugin_blogcomments_data->id);
	}
	
	if(isset($_GET['editcomments'])){
		$template->show_if('EDITCOMMENTS',true);
		foreach(Plugin_Blogcomments_Manager::getComments($_GET['editcomments']) as $comment){
			$index = $template->add_loop_item('COMMENTS');
			$template->assign_loop_var('COMMENTS',$index,'COMMENTAUTHOR',$comment->authorname);
			$template->assign_loop_var('COMMENTS',$index,'COMMENTCONTENT',$comment->authorcomment);
			$template->assign_loop_var('COMMENTS',$index,'COMMENTEDITURL',Settings::getInstance()->get("host").'admin/pluginsettings.html?plugin=blogcomments&editcomments='.$_GET['editcomments'].'&editcomment='.$comment->id);
			$template->assign_loop_var('COMMENTS',$index,'COMMENTDELETEURL',Settings::getInstance()->get("host").'admin/pluginsettings.html?plugin=blogcomments&editcomments='.$_GET['editcomments'].'&deletecomment='.$comment->id);
		}
	}else{
		$template->show_if('EDITCOMMENTS',false);
	}
	
	if(isset($_GET['editcomment'])){
		$template->show_if('EDITCOMMENT',true);
		$plugin_blogcomments_data = Plugin_Blogcomments_Manager::getCommentDatabyId($_GET['editcomment']);
		$template->assign_var('EDITCOMMENTAUTHOR',$plugin_blogcomments_data->authorname);
		$template->assign_var('EDITCOMMENTCONTENT',$plugin_blogcomments_data->authorcomment);
	}else{
		$template->show_if('EDITCOMMENT',false);
	}
	
	$template->output();
?>
