<?php
/*
 * manager.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
class Plugin_Blogcomments_Manager{
	
	public static function saveComment($data){
		$plugin_blogcomments_authorname = User::Current()->name;
		$plugin_blogcomments_authorcomment = DataBase::Current()->EscapeString($data['plugin_blogcomments_authorcomment']);
		$plugin_blogcomments_pageid = DataBase::Current()->EscapeString($data['plugin_blogcomments_pageid']);
		if(DataBase::Current()->Execute("INSERT INTO
	{'dbprefix'}plugin_comments 
	(`pageid`,`authorname`,`authorcomment`,`datetime`)
	VALUES
	('".$plugin_blogcomments_pageid."','".$plugin_blogcomments_authorname."','".$plugin_blogcomments_authorcomment."',NOW())")){
			return true;
		}else{
			return false;
		}
	}
	
	public static function getComments($pageid){
		$pageid = DataBase::Current()->EscapeString($pageid);
		$commentDatas = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_comments WHERE pageid = '".$pageid."' ORDER BY datetime ASC");
		return $commentDatas;
	}
	
	public static function getGravatarHash($mail){
		$mail = trim($mail);
		$mail = strtolower($mail);
		$mail = md5($mail);
		return $mail;
	}
	
	public static function countComments($pageid){
		$pageid = DataBase::Current()->EscapeString($pageid);
		$countComments = DataBase::Current()->ReadRows("SELECT id FROM {'dbprefix'}plugin_comments WHERE pageid = '".$pageid."'");
		return count($countComments);
	}
	
	public static function deleteComment($id){
		$id = DataBase::Current()->EscapeString($id);
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_comments WHERE id = '".$id."'");
	}
	
	public static function getCommentDatabyId($id){
		$id = DataBase::Current()->EscapeString($id);
		return DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}plugin_comments WHERE id = '".$id."'");
	}
	
	public static function updateComment($id,$data){
		$id = DataBase::Current()->EscapeString($id);
		$name = DataBase::Current()->EscapeString($data['editauthorname']);
		$content = DataBase::Current()->EscapeString($data['editauthorcomment']);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_comments SET authorname='".$name."', authorcomment='".$content."' WHERE id = '".$id."'");
	}
}
?>
