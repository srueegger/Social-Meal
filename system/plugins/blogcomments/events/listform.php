<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
$template = new Template();
$plugin_blogcomments_settings = Plugin_PluginData_Data::getData('settings','plugin','blogcomments');
if($plugin_blogcomments_settings['gravatar'] == 1){
	$template->load('plugin_blogcomments_listformgravatar');
}else{
	if(Mobile::isMobileDevice()){
		$template->load('plugin_blogcomments_listform.mobile');
	}else{
		$template->load('plugin_blogcomments_listform');
	}
}
if(isset($_POST['plugin_blogcomments_newcommentsave']) && User::Current()->role->ID != 1 && $_POST['plugin_blogcomments_authorcomment'] != ''){
	if(Plugin_Blogcomments_Manager::saveComment($_POST)){
		$entryMetaData = Plugin_PluginData_Data::getData('blogentrymeta_'.$args->id.'','plugin','blogentries');
		$autohrinfo = Plugin_Profile_Manager::getUserData($entryMetaData['author']);
		if(Plugin_Profile_Manager::getUserSetting('blogcomment',$authorinfo->id) == 1){
			$mailtext = 'Liebe*r '.ucfirst($authorinfo->firstname).'<br><br>Zu deinem Blogeintrag <a href="'.Settings::getInstance()->get("host").'Blog/'.$args->alias.'.html">'.$args->title.'</a> wurde ein neuer Kommentar abgegeben<br><br>Unter <a href="'.Settings::getInstance()->get("host").'Blog/'.$args->alias.'.html#comments">Neuer Kommentar</a> kannst du darauf antworten und die Diskussion am Leben halten.<br><br>Falls du der Meinung bist, dass dieser User die Kommentar-Funktion missbräuchlich verwendet, melde uns das bitte über die entsprechende <a href="'.Settings::getInstance()->get("host").'melden.html?source=mail&g=blogcomment_'.$args->id.'">Melde-Seite</a>.<br><br>Vielen Dank für deinen Beitrag zu einer lebhaften Community!<br>Social Meal<br><br>P.S.: Diesen E-Mail-Typ kannst du in den <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abbestellen, falls er dir überflüssig erscheint.';
			$subject = utf8_decode('Zu einem Blogeintrag von dir auf Social Meal wurde ein neuer Kommentar abgegeben');
			$h2t = new Plugin_Mailer_html2text($mailtext);
			$mail = new Plugin_PHPMailer_PHPMailer;
			$mail->IsSMTP();
			$mail->AddAddress($authorinfo->email);
			$mail->WordWrap = 50;
			$mail->IsHTML(true);
			$mail->Subject = $subject;
			$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
			$mail->AltBody = $h2t->get_text();
			if(!$mail->Send()){
				echo('Mail Error:'.$mail->ErrorInfo);
			}
		}
	}
}
foreach(Plugin_Blogcomments_Manager::getComments($args->id) as $comment){
	$index = $template->add_loop_item('COMMENTS');
	$template->assign_loop_var('COMMENTS',$index,'SHOWAUTHOR',Plugin_Profile_Manager::showUsername($comment->authorname,'author'));
	$template->assign_loop_var('COMMENTS',$index,'AUTHORNAME',htmlentities($comment->authorname));
	$template->assign_loop_var('COMMENTS',$index,'DATETIME',htmlentities(Plugin_Events_Manager::formatDate($comment->datetime)));
	$bb = new Plugin_BBCode_Translator();
	$template->assign_loop_var('COMMENTS',$index,'AUTHORCOMMENT',$bb->replace($comment->authorcomment));
	$authorinfo = Plugin_Profile_Manager::getUserData($comment->authorname);
	if(User::Current()->role->ID == 1 && Plugin_Profile_Manager::openImg($comment->authorname) == 0){
		$profilimg = Sys::getFullSkinPath().'images/avtar.png';
	}else{
		$profilimg = '/content/uploads/profile/thumbs/'.Plugin_Profile_Manager::getUserImage($comment->authorname);
		if($profilimg == '/content/uploads/profile/thumbs/'){
			$profilimg = Sys::getFullSkinPath().'images/avtar.png';
		}
	}
	$template->assign_loop_var('COMMENTS',$index,'AUTHORIMG',$profilimg);
}
if(User::Current()->role->ID != 1){
	$template->show_if('form',true);
	$template->show_if('noform',false);
}else{
	$template->show_if('form',false);
	$template->show_if('noform',true);
}

$template->assign_var('PAGEID',$args->id);
$template->assign_var('NEWCOMMENTFORMURL',UrlRewriting::GetUrlByAlias($args->alias));
Cache::clear();
echo $template->getCode();
?>
