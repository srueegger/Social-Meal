<?php
/*
 * mailer.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
class Plugin_Mailer_Mailer

{

    private $to ;
    private $from ;
    private $return  ;
    private $subject ;
    private $header ;

    // }}}



    // {{{ __construct()



    /**

     * Pass Variable

     * @param   string  $from    Content from email address

     * @param   string  $to      Content to email address

     * @param   string  $subject Content subject email

     * @param   string  $return  Content return path email address

     * No return value. Pass variable into this class 

     */

    function __construct($from, $to, $subject, $return)

    {

        $this->from     = $from ;

        $this->to       = $to   ;

        $this->subject  = $subject ;        

        $this->return   = $return  ;

    }



    // }}}



    // {{{ simpleMail()



    /**

     * Send simple email

     * @param   string  $message    Content for this email 

     * No return value. Send email 

     * @access  public

     * @access  static

     */



    public function simpleMail($message) 

    {

        $this->header   = "From: "      .$this->from. "\n" ;

        $this->header  .= "Reply-To: "  .$this->from. "\n" ;

        $this->header  .= "Return-Path: " .$this->return. "\n" ;

        $this->header  .= "X-Mailer: PHP/"  .phpversion(). "\n" ;

        $this->send($message) ;

    }

    // }}}



    // {{{ htmlMail()



    /**

     * Send html email

     * @param   string  $html       Message with html tag 

     * @param   string  $plainText  Message in plain text format

     * No return value. Send html email 

     * @access  public

     * @access  static

     */



    public function htmlMail($html, $plainText='') 

    {

        $boundary = md5(uniqid(time()));

        $this->header    = "From: "      .$this->from. "\r\n" ;

        $this->header   .= "To: "        .$this->to. "\n" ;

        $this->header   .= "Return-Path: " .$this->return. "\n" ;

        $this->header   .= "MIME-Version: 1.0\n" ;

        $this->header   .= "Content-Type: multipart/alternative; boundary=\"".$boundary."\"\n";



        // Text Version

        $msgPlainText    = "--" . $boundary . "\n" ;

        $msgPlainText   .= "Content-Type: text/plain; charset=iso-8589-1\n" ;

        $msgPlainText   .= "Content-Transfer-Encoding: 8bit\n" ;

        $msgPlainText   .= "If you are seeing this is because you may need to change your\n" ;

        $msgPlainText   .= "preferred message format from HTML to plain text.\n\n" ;

        if ($plainText == '') {

            // $msgPlainText .= $plainText . "\n" ;

            $plainText   = strip_tags($html) ; 

        }

        $msgPlainText   .= $plainText . "\n" ;



        // HTML Version

        $msgHtml     = "--" . $boundary . "\n" ;

        $msgHtml    .= "Content-Type: text/html; charset=iso-8589-1\n" ;

        $msgHtml    .= "Content-Transfer-Encoding: 8bit\n" ;

        $msgHtml    .= $html ;

        

        $message    = $msgPlainText . $msgHtml ."\n\n" ;

        // Send Email

        $this->send($message) ;

    }

    // }}}



    // {{{ simpleAttachment()



    /**

     * Send simple email with attachment

     * @param   string  $file       File for attachment (file name or path of file)

     * @param   string  $plainText  Message in plain text format

     * No return value. Send simple email with attachment

     * @access  public

     * @access  static

     */



    public function simpleAttachment($file, $plainText='')

    {

        $handle      = fopen($file, 'rb') ;    

        $data        = fread($handle,filesize($file)) ;

        $data        = chunk_split(base64_encode($data))  ;

        $filetype    = mime_content_type($file) ;

        

        $boundary    = md5(uniqid(time()));

        $this->header    = "From: "      .$this->from. "\n" ;

        $this->header   .= "To: "        .$this->to. "\n" ;

        $this->header   .= "Return-Path: " .$this->return. "\n" ;

        $this->header   .= "MIME-Version: 1.0\n" ;

        $this->header   .= "Content-Type: multipart/mixed; boundary=\"".$boundary."\"";



        // Text Version

        $msgPlainText    = "--" . $boundary . "\n" ;

        $msgPlainText   .= "Content-Type: text/plain; charset=iso-8589-1\n" ;

        $msgPlainText   .= "Content-Transfer-Encoding: 8bit\n" ;

        if ($plainText != '') {

            $msgPlainText .= $plainText . "\n" ;

        }



        // Attachment Version

        $attach      = "--" . $boundary ."\n" ;

        $attach     .= "Content-Type: " . $filetype . "; name=\"" . $file . "\"\n" ;

        $attach     .= "Content-Transfer-Encoding: base64 \n" ;

        // Need two end of lines

        $attach     .= "Content-Disposition: attachment; filename=\"" .$file. "\"\n\n" ;

        $attach     .= $data   . "\n\n" ;



        $message     = $msgPlainText . $attach ;

        // Send Email

        $this->send($message) ;

    }

    // }}}



    // {{{ htmlAttachment()



    /**

     * Send html email with attachment

     * @param   string  $file       File for attachment (file name or path of file)

     * @param   string  $html       Message in HTML format

     * @param   string  $plainText  Message in plain text format

     * No return value. Send html email with attachment

     * @access  public

     * @access  static

     */



    public function htmlAttachment($file, $html, $plainText='') 

    {

        $handle      = fopen($file, 'rb') ;    

        $data        = fread($handle,filesize($file)) ;

        $data        = chunk_split(base64_encode($data))  ;

        $filetype    = mime_content_type($file) ;

        

        $boundary    = md5(uniqid(time()));

        $this->header    = "From: "      .$this->from. "\n" ;

        $this->header   .= "To: "        .$this->to. "\n" ;

        $this->header   .= "Return-Path: " .$this->return. "\n" ;

        $this->header   .= "MIME-Version: 1.0\n" ;

        $this->header   .= "Content-Type: multipart/related; boundary=\"".$boundary."\"\n";



        // Text Version

        $msgPlainText    = "--" . $boundary . "\n" ;

        $msgPlainText   .= "Content-Type: text/plain; charset=iso-8589-1\n" ;

        $msgPlainText   .= "Content-Transfer-Encoding: 8bit\n" ;

        $msgPlainText   .= "If you are seeing this is because you may need to change your\n" ;

        $msgPlainText   .= "preferred message format from HTML to plain text.\n\n" ;

        if ($plainText == '') {

            $plainText   = strip_tags($html) ; 

        }

        $msgPlainText   .= $plainText . "\n" ;



        // HTML Version

        $msgHtml     = "--" . $boundary . "\n" ;

        $msgHtml    .= "Content-Type: text/html; charset=iso-8589-1\n" ;

        $msgHtml    .= "Content-Transfer-Encoding: 8bit\n" ;

        $msgHtml    .= $html ."\n"  ;



        // Attachment Version

        $attach      = "--" . $boundary ."\n" ;

        $attach     .= "Content-Type: " . $filetype . "; name=\"" . $file . "\"\n" ;

        $attach     .= "Content-Transfer-Encoding: base64 \n" ;

        // Need two end of lines

        $attach     .= "Content-Disposition: attachment; filename=\"" .$file. "\"\n\n" ;

        $attach     .= $data   . "\n\n" ;



        $message     = "Content-Type: multipart/alternative; boundary=\"".$boundary."\"";

        $message    .= $msgPlainText . $msgHtml . $attach ;

        // Send Email

        $this->send($message) ;

    }

    // }}}



    // {{{ send()



    /**

     * return function mail() 

     * @param   string  $message    Content for this email 

     * return function mail() 

     * @access  private

     * @access  static

     */



    private function send($message)

    {

        return @mail($this->to, $this->subject, $message, $this->header) ;

    }
    
    public static function makeHtml($subject,$body){
	    $subject = htmlentities($subject);
	    $htmltext = utf8_decode('
	    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>'.$subject.'</title>
        
    <style type="text/css">
		#outlook a{
			padding:0;
		}
		.ReadMsgBody{
			width:100%;
		}
		.ExternalClass{
			width:100%;
		}
		.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{
			line-height:100%;
		}
		body,table,td,p,a,li,blockquote{
			-webkit-text-size-adjust:100%;
			-ms-text-size-adjust:100%;
		}
		table,td{
			mso-table-lspace:0pt;
			mso-table-rspace:0pt;
		}
		img{
			-ms-interpolation-mode:bicubic;
		}
		body{
			margin:0;
			padding:0;
		}
		img{
			border:0;
			height:auto;
			line-height:100%;
			outline:none;
			text-decoration:none;
		}
		table{
			border-collapse:collapse !important;
		}
		body,#bodyTable,#bodyCell{
			height:100% !important;
			margin:0;
			padding:0;
			width:100% !important;
		}
		#bodyCell{
			padding:7px;
		}
		#templateContainer{
			width:500px;
		}
	/*
	@tab Page
	@section background style
	@tip Set the background color and top border for your email. You may want to choose colors that match your companys branding.
	@theme page
	*/
		body,#bodyTable{
			/*@editable*/background-color:#ffffff;
		}
	/*
	@tab Page
	@section background style
	@tip Set the background color and top border for your email. You may want to choose colors that match your companys branding.
	@theme page
	*/
		#bodyCell{
			/*@editable*/border-top:4px solid #ffffff;
		}
	/*
	@tab Page
	@section email border
	@tip Set the border for your email.
	*/
		#templateContainer{
			/*@editable*/border:1px solid #ffffff;
		}
	/*
	@tab Page
	@section heading 1
	@tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
	@style heading 1
	*/
		h1{
			/*@editable*/color:#202020 !important;
			display:block;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:26px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:100%;
			/*@editable*/letter-spacing:normal;
			margin-top:0;
			margin-right:0;
			margin-bottom:10px;
			margin-left:0;
			/*@editable*/text-align:left;
		}
	/*
	@tab Page
	@section heading 2
	@tip Set the styling for all second-level headings in your emails.
	@style heading 2
	*/
		h2{
			/*@editable*/color:#404040 !important;
			display:block;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:20px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:100%;
			/*@editable*/letter-spacing:normal;
			margin-top:0;
			margin-right:0;
			margin-bottom:10px;
			margin-left:0;
			/*@editable*/text-align:left;
		}
	/*
	@tab Page
	@section heading 3
	@tip Set the styling for all third-level headings in your emails.
	@style heading 3
	*/
		h3{
			/*@editable*/color:#606060 !important;
			display:block;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:16px;
			/*@editable*/font-style:italic;
			/*@editable*/font-weight:normal;
			/*@editable*/line-height:100%;
			/*@editable*/letter-spacing:normal;
			margin-top:0;
			margin-right:0;
			margin-bottom:10px;
			margin-left:0;
			/*@editable*/text-align:left;
		}
	/*
	@tab Page
	@section heading 4
	@tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
	@style heading 4
	*/
		h4{
			/*@editable*/color:#808080 !important;
			display:block;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:14px;
			/*@editable*/font-style:italic;
			/*@editable*/font-weight:normal;
			/*@editable*/line-height:100%;
			/*@editable*/letter-spacing:normal;
			margin-top:0;
			margin-right:0;
			margin-bottom:10px;
			margin-left:0;
			/*@editable*/text-align:left;
		}
		li{
			margin-bottom:9px;
		}
	/*
	@tab Header
	@section preheader style
	@tip Set the background color and bottom border for your emails preheader area.
	@theme header
	*/
		#templatePreheader{
			/*@editable*/background-color:#ffffff;
			/*@editable*/border-bottom:1px solid #ffffff;
		}
	/*
	@tab Header
	@section preheader text
	@tip Set the styling for your emails preheader text. Choose a size and color that is easy to read.
	*/
		.preheaderContent{
			/*@editable*/color:#808080;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:10px;
			/*@editable*/line-height:125%;
			/*@editable*/text-align:left;
		}
	/*
	@tab Header
	@section preheader link
	@tip Set the styling for your emails preheader links. Choose a color that helps them stand out from your text.
	*/
		.preheaderContent a:link,.preheaderContent a:visited,.preheaderContent a .yshortcuts {
			/*@editable*/color:#606060;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	/*
	@tab Header
	@section header style
	@tip Set the background color and borders for your emails header area.
	@theme header
	*/
		#templateHeader{
			/*@editable*/background-color:#ffffff;
			/*@editable*/border-top:1px solid #FFFFFF;
			/*@editable*/border-bottom:1px solid #ffffff;
		}
	/*
	@tab Header
	@section header text
	@tip Set the styling for your emails header text. Choose a size and color that is easy to read.
	*/
		.headerContent{
			/*@editable*/color:#505050;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:20px;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:100%;
			/*@editable*/padding-top:0;
			/*@editable*/padding-right:0;
			/*@editable*/padding-bottom:0;
			/*@editable*/padding-left:0;
			/*@editable*/text-align:left;
			/*@editable*/vertical-align:middle;
		}
	/*
	@tab Header
	@section header link
	@tip Set the styling for your emails header links. Choose a color that helps them stand out from your text.
	*/
		.headerContent a:link,.headerContent a:visited,.headerContent a .yshortcuts {
			/*@editable*/color:#EB4102;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
		#headerImage{
			height:auto;
			max-width:600px;
		}
	/*
	@tab Body
	@section body style
	@tip Set the background color and borders for your emails body area.
	*/
		#templateBody{
			/*@editable*/background-color:#ffffff;
			/*@editable*/border-top:1px solid #FFFFFF;
			/*@editable*/border-bottom:1px solid #ffffff;
		}
	/*
	@tab Body
	@section body text
	@tip Set the styling for your emails main content text. Choose a size and color that is easy to read.
	@theme main
	*/
		.bodyContent{
			/*@editable*/color:#505050;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:14px;
			/*@editable*/line-height:150%;
			padding-top:6px;
			padding-right:6px;
			padding-bottom:7px;
			padding-left:6px;
			/*@editable*/text-align:left;
		}
	/*
	@tab Body
	@section body link
	@tip Set the styling for your emails main content links. Choose a color that helps them stand out from your text.
	*/
		.bodyContent a:link,.bodyContent a:visited,.bodyContent a .yshortcuts {
			/*@editable*/color:#EB4102;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
		.bodyContent img{
			display:inline;
			height:auto;
			max-width:560px;
		}
	/*
	@tab Footer
	@section footer style
	@tip Set the background color and borders for your emails footer area.
	@theme footer
	*/
		#templateFooter{
			/*@editable*/background-color:#F4F4F4;
			/*@editable*/border-top:1px solid #FFFFFF;
		}
	/*
	@tab Footer
	@section footer text
	@tip Set the styling for your emails footer text. Choose a size and color that is easy to read.
	@theme footer
	*/
		.footerContent{
			/*@editable*/color:#808080;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:10px;
			/*@editable*/line-height:150%;
			padding-top:20px;
			padding-right:10px;
			padding-bottom:20px;
			padding-left:10px;
			/*@editable*/text-align:left;
		}
	/*
	@tab Footer
	@section footer link
	@tip Set the styling for your emails footer links. Choose a color that helps them stand out from your text.
	*/
		.footerContent a:link,.footerContent a:visited,.footerContent a .yshortcuts,.footerContent a span {
			/*@editable*/color:#606060;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	@media only screen and (max-width: 480px){
		body,table,td,p,a,li,blockquote{
			-webkit-text-size-adjust:none !important;
		}
}	@media only screen and (max-width: 480px){
		body{
			width:100% !important;
			min-width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		#bodyCell{
			padding:4px !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section template width
	@tip Make the template fluid for portrait or landscape view adaptability. If a fluid layout doesnt work for you, set the width to 300px instead.
	*/
		#templateContainer{
			/*@tab Mobile Styles
@section template width
@tip Make the template fluid for portrait or landscape view adaptability. If a fluid layout doesnt work for you, set the width to 300px instead.*/max-width:600px !important;
			/*@editable*/width:100% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section heading 1
	@tip Make the first-level headings larger in size for better readability on small screens.
	*/
		h1{
			/*@editable*/font-size:24px !important;
			/*@editable*/line-height:100% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section heading 2
	@tip Make the second-level headings larger in size for better readability on small screens.
	*/
		h2{
			/*@editable*/font-size:20px !important;
			/*@editable*/line-height:100% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section heading 3
	@tip Make the third-level headings larger in size for better readability on small screens.
	*/
		h3{
			/*@editable*/font-size:18px !important;
			/*@editable*/line-height:100% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section heading 4
	@tip Make the fourth-level headings larger in size for better readability on small screens.
	*/
		h4{
			/*@editable*/font-size:16px !important;
			/*@editable*/line-height:100% !important;
		}
}	@media only screen and (max-width: 480px){
		#templatePreheader{
			display:none !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section header image
	@tip Make the main header image fluid for portrait or landscape view adaptability, and set the images original width as the max-width. If a fluid setting doesnt work, set the image width to half its original size instead.
	*/
		#headerImage{
			/*@tab Mobile Styles
@section header image
@tip Make the main header image fluid for portrait or landscape view adaptability, and set the images original width as the max-width. If a fluid setting doesnt work, set the image width to half its original size instead.*/height:auto !important;
			/*@editable*/max-width:500px !important;
			/*@editable*/width:100% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section header text
	@tip Make the header content text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
		.headerContent{
			/*@editable*/font-size:20px !important;
			/*@editable*/line-height:125% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section body text
	@tip Make the body content text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
		.bodyContent{
			/*@editable*/font-size:16px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section footer text
	@tip Make the body content text larger in size for better readability on small screens.
	*/
		.footerContent{
			/*@editable*/font-size:13px !important;
			/*@editable*/line-height:115% !important;
		}
}	@media only screen and (max-width: 480px){
		.footerContent a{
			display:block !important;
		}
}</style></head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<!-- <center> -->
        	<table align="left" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
            	<tr>
                	<td align="left" valign="top" id="bodyCell">
                    	<!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	
                    
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                        <tr>
                                            <td valign="top" class="bodyContent" mc:edit="body_content">
												'.$body.'
                                                <br>
                                                <br>
                                                -- <br>
                                                --- Signatur ---<br>
                                                Social Meal - Meal-Sharing für die Region Basel - Entwickelt von Occupy Basel und Helfer*innen
                                                <br><br>
                                                <a href="'.Settings::getInstance()->get("host").'">Homepage Social Meal</a> - <a href="'.Settings::getInstance()->get("host").'agb.html">AGBs</a> - <a href="'.Settings::getInstance()->get("host").'FAQ/sichtbarkeit-und-verwendung-der-personlichen-daten.html">Datenschutz</a> - <a href="'.Settings::getInstance()->get("host").'uber-uns.html">Über uns</a> - <a href="'.Settings::getInstance()->get("host").'kontakt.html">Kontakt</a> - <a href="https://www.facebook.com/42socialmeal?fref=ts">Facebook</a> - <a href="https://twitter.com/infosocialmeal">Twitter</a> - <a href="https://plus.google.com/u/0/b/102044072731061267569/102044072731061267569/posts">Google+</a> - <a href="http://www.occupybasel.ch">Homepage Occupy Basel</a>
                                                <br><br>
                                                Wir sind noch auf der Suche nach Mithilfe unterschiedlicher Art und freuen uns über jede Rückmeldung!
                                                <br><br>
                                                Finanzielle Unterstützung zuhanden unseres zinsfreien Kontos bei der Alternativen Bank Schweiz, Verein von Occupy Basel, Betreff "Social Meal" Konto-Nr: 318.763.100-07, IBAN-Nr: CH77 0839 0031 8763 1000 7
<br>Herzlichen Dank!
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END BODY -->
                                </td>
                            </tr>
                        	
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        <!-- </center> -->
    </body>
</html>
	    ');
	    return $htmltext;
    }

    

    // }}}

}



// }}}

?>

