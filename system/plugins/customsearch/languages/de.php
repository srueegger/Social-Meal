<?PHP
/*
 * de.php
 * 
 * Copyright 2015 Samuel R�egger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	$tokens["plugin_description"]		= "Dieses Plugin erm�glicht die Durchsuchung von Seiten, Bildern und Benutzern.";
	$tokens["pagetypetitle"]			= "Suchseite";
	$tokens["edittitle"]				= "Welche Inhalte soll man durchsuchen k�nnen";
	$tokens["showform"]					= "Suchformular auf Seite zeigen";
	$tokens["placeholder"]				= "Suchen ...";
	$tokens["searchstart"]				= "Suche starten";
	$tokens["internpages"]				= "Interne (Admin) Seiten auch anzeigen?";
	$tokens["noresult"]					= "Text, falls keine Resultate gefunden werden:";
	$tokens["skincode"]					= "Code f�r deinen Skin:";
	$tokens["nosearchquery"]			= "Kein Suchbegriff definiert";
	$tokens["newsearch"]				= "Neue Suche starten";
	$tokens["inputtitle"]				= "Der Suchbegriff muss mindestestens 3 Zeichen enthalten";
	$tokens["search"]					= "Suchen";
?>
