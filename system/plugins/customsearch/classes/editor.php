<?PHP
/*
 * editor.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_CustomSearch_Editor extends Editor {

		function __construct(Page $page) {
			$this->page = $page;
		}

		public function display () {
		
			Cache::clear();
			$template = new Template();
			$template->load("plugin_customsearch_searchoutput");
			$template->show_if('showsearchorgs',false);
			$plugin_customsearch_datas = Plugin_PluginData_Data::getData("plugin_customsearch_datas".$this->page->alias,"plugin","customsearch");
			$plugin_customsearch_result = 0;
			if(isset($_POST["q"])){
				if($_POST["q"] == ''){
					$_POST["q"] = Language::DirectTranslate('plugin_customsearch_nosearchquery');
				} 
				if(isset($plugin_customsearch_datas["form"]) == 1){
					$template->assign_var("SEARCHTOKEN",htmlentities($_POST["q"]));
					$template->assign_var("PLACEHOLDER",Language::DirectTranslate("plugin_customsearch_placeholder"));
					$template->show_if("SHOWSEARCHFORM", true);
				}else{
					$template->show_if("SHOWSEARCHFORM",false);
				}
				if(isset($plugin_customsearch_datas["pages"]) == 1){
					$plugin_customsearch_userrole = User::Current()->role->ID;
					$i = 0;
					$counter = array();
					foreach(Plugin_CustomSearch_Manager::searchPages($_POST['q']) as $plugin_customsearch_pagesearchdata){
						$plugin_customsearch_meta_description = Plugin_CustomSearch_Manager::getMetaData($plugin_customsearch_pagesearchdata->id);
						if($plugin_customsearch_meta_description != ""){
							$plugin_customsearch_meta_description = htmlentities($plugin_customsearch_meta_description);
						}
						if(Plugin_CustomSearch_Manager::verifyRoleRight($plugin_customsearch_userrole,$plugin_customsearch_pagesearchdata->id)){
							if(isset($plugin_customsearch_datas["internpages"]) != 1){
								if(substr($plugin_customsearch_pagesearchdata->alias, 0,5) != "admin"){
									$index = $template->add_loop_item("SEARCHPAGES");
									$template->assign_loop_var("SEARCHPAGES",$index,"TITLE",htmlentities($plugin_customsearch_pagesearchdata->title));
									$template->assign_loop_var("SEARCHPAGES",$index,"URL",UrlRewriting::GetUrlByAlias($plugin_customsearch_pagesearchdata->alias));
									$template->assign_loop_var("SEARCHPAGES",$index,"DESCRIPTION",$plugin_customsearch_meta_description);
								}
							}else{
									$index = $template->add_loop_item("SEARCHPAGES");
									$template->assign_loop_var("SEARCHPAGES",$index,"TITLE",htmlentities(utf8_encode($plugin_customsearch_pagesearchdata->title)));
									$template->assign_loop_var("SEARCHPAGES",$index,"URL",UrlRewriting::GetUrlByAlias($plugin_customsearch_pagesearchdata->alias));
									$template->assign_loop_var("SEARCHPAGES",$index,"DESCRIPTION",htmlentities($plugin_customsearch_meta_description));
							}
						}
						$i++;
						$counter[] = $i;
					}
					if(sizeof($counter) == 0){
						$template->show_if("SHOWSEARCHPAGES", false);
					}else{
						$template->show_if("SHOWSEARCHPAGES", true);
						$plugin_customsearch_result = 1;
					}
				}else{
					$template->show_if("SHOWSEARCHPAGES",false);
				}
				if(isset($plugin_customsearch_datas["users"]) == 1){
					$i = 0;
					$counter = array();
					foreach(Plugin_CustomSearch_Manager::searchUsers($_POST['q']) as $plugin_customsearch_usersearchdata){
						$index = $template->add_loop_item("SEARCHUSERS");
						if(Plugin_Profile_Manager::isFriend($plugin_customsearch_usersearchdata->id) == true or User::Current()->id == $plugin_customsearch_usersearchdata->id){
							$plugin_customsearch_showusername = ucfirst($plugin_customsearch_usersearchdata->firstname).' '.ucfirst($plugin_customsearch_usersearchdata->lastname);
						}else{
							$plugin_customsearch_showusername = $plugin_customsearch_usersearchdata->name;
						}
						$template->assign_loop_var("SEARCHUSERS",$index,"USERNAME",htmlentities($plugin_customsearch_usersearchdata->name));
						$template->assign_loop_var("SEARCHUSERS",$index,"SHOWUSERNAME",htmlentities($plugin_customsearch_showusername));
						if(Plugin_Profile_Manager::isVerified($plugin_customsearch_usersearchdata->name) == 2){
							$template->assign_loop_var("SEARCHUSERS",$index,"VERIFY",'<a infotext="'.Language::DirectTranslate('plugin_profile_isverify').'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('tick').'" /></span></a>');
						}else{
							$template->assign_loop_var("SEARCHUSERS",$index,"VERIFY",'<a infotext="'.Language::DirectTranslate('plugin_profile_noverify').'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('cancel').'" /></span></a>');
						}
						$i++;
						$counter[] = $i;
					}if(sizeof($counter) == 0){
						$template->show_if("SHOWSEARCHUSERS", false);
					}else{
						$template->show_if("SHOWSEARCHUSERS", true);
						$plugin_customsearch_result = 1;
					}
				}else{
					$template->show_if("SHOWSEARCHUSERS",false);
				}
				if(isset($plugin_customsearch_datas["orgs"]) == 1){
					$i = 0;
					$counter = array();
					foreach(Plugin_CustomSearch_Manager::searchOrgs($_POST['q']) as $plugin_customsearch_orgsearchdata){
						$index = $template->add_loop_item("SEARCHORGS");
						$template->assign_loop_var("SEARCHORGS",$index,"ORGNAME",htmlentities(utf8_encode($plugin_customsearch_orgsearchdata->orgname)));
						$template->assign_loop_var("SEARCHORGS",$index,"ORGID",htmlentities($plugin_customsearch_orgsearchdata->id));
						$i++;
						$counter[] = $i;
					}if(sizeof($counter) == 0){
						$template->show_if("SHOWSEARCHORGS",false);
					}else{
						$template->show_if("SHOWSEARCHORGS",true);
						$plugin_customsearch_result = 1;
					}
				}else{
					$template->show_if("SHOWSEARCHORGS",false);
				}
				
				
				if(isset($plugin_customsearch_datas["images"]) == 1){
					$i = 0; 
					$counter = array();
					foreach(Plugin_CustomSearch_Manager::searchImgs($_POST['q']) as $plugin_customsearch_imagesearchdata){
						$index = $template->add_loop_item("SEARCHIMAGES");
						$template->assign_loop_var("SEARCHIMAGES",$index,"TITLE",htmlentities(utf8_encode($plugin_customsearch_imagesearchdata->name)));
						$template->assign_loop_var("SEARCHIMAGES",$index,"URL",htmlentities(utf8_encode($plugin_customsearch_imagesearchdata->path)));
						$template->assign_loop_var("SEARCHIMAGES",$index,"IMAGEDESCRIPTION","<br />".htmlentities(utf8_encode($plugin_customsearch_imagesearchdata->description)));
						$i++;
						$counter[] = $i;
					}
					if(sizeof($counter) == 0){
						$template->show_if("SHOWSEARCHIMAGES", false);
					}else{
						$template->show_if("SHOWSEARCHIMAGES", true);
						$plugin_customsearch_result = 1;
					}
				}else{
					$template->show_if("SHOWSEARCHIMAGES",false);
				}
				if($plugin_customsearch_result == 0){
					$template->assign_var("NORESULT",htmlentities(utf8_encode($plugin_customsearch_datas["noresult"])));
					$template->show_if("SHOWNORESULT", true);
				}else{
					$template->show_if("SHOWNORESULT", false);
				}
			}else{
				$template->show_if("SHOWSEARCHPAGES",false);
				$template->show_if("SHOWSEARCHUSERS",false);
				$template->show_if("SHOWSEARCHIMAGES",false);
				$template->show_if("SHOWNORESULT",false);
				if(isset($plugin_customsearch_datas["form"]) == 1){
					$template->assign_var("SEARCHTOKEN","");
					$template->assign_var("PLACEHOLDER",Language::DirectTranslate("plugin_customsearch_placeholder"));
					$template->show_if("SHOWSEARCHFORM", true);
				}else{
					$template->show_if("SHOWSEARCHFORM", false);
				}
			}
			echo $template->getCode();
		}

		public function getHeader () {
		}

		public function getEditableCode() {
			Cache::clear();
			if(isset($_POST["save"])){
				$plugin_customsearch_datas = array();
				if(isset($_POST["plugin_customsearch_pages"]) == 1){
					$plugin_customsearch_datas["pages"] = 1;
					if(isset($_POST["plugin_customsearch_internpages"]) == 1){
						$plugin_customsearch_datas["internpages"] = 1;
					}
				}
				if(isset($_POST["plugin_customsearch_users"]) == 1){
					$plugin_customsearch_datas["users"] = 1;
				}
				if(isset($_POST["plugin_customsearch_orgs"]) == 1){
					$plugin_customsearch_datas["orgs"] = 1;
				}
				if(isset($_POST["plugin_customsearch_images"]) == 1){
					$plugin_customsearch_datas["images"] = 1;
				}
				if(isset($_POST["plugin_customsearch_form"]) == 1){
					$plugin_customsearch_datas["form"] = 1;
				}
				if(isset($_POST["plugin_customsearch_noresulttext"]) != ""){
					$plugin_customsearch_datas["noresult"] = DataBase::Current()->EscapeString($_POST["plugin_customsearch_noresulttext"]);
				}
				Plugin_CustomSearch_Manager::deleteSearchSettings($this->page->alias);
				Plugin_PluginData_Data::setData("plugin_customsearch_datas".$this->page->alias,$plugin_customsearch_datas,"customsearch","plugin");
			}
			unset($plugin_customsearch_datas);
			$plugin_customsearch_datas = Plugin_PluginData_Data::getData("plugin_customsearch_datas".$this->page->alias,"plugin","customsearch");
			$template = new Template();
			$template->load("plugin_customsearch_searchsettings");
			$template->assign_var("FORMURL",UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias));
			if(isset($plugin_customsearch_datas["pages"]) == 1){
				$template->assign_var("PAGES_CHECKED"," checked=\checked\"");
			}else{
				$template->assign_var("PAGES_CHECKED","");
			}
			if(isset($plugin_customsearch_datas["users"]) == 1){
				$template->assign_var("USERS_CHECKED"," checked=\checked\"");
			}else{
				$template->assign_var("USERS_CHECKED","");
			}
			if(isset($plugin_customsearch_datas["orgs"]) == 1){
				$template->assign_var("ORGS_CHECKED"," checked=\checked\"");
			}else{
				$template->assign_var("ORGS_CHECKED","");
			}
			if(isset($plugin_customsearch_datas["images"]) == 1){
				$template->assign_var("IMAGES_CHECKED"," checked=\checked\"");
			}else{
				$template->assign_var("IMAGES_CHECKED","");
			}
			if(isset($plugin_customsearch_datas["form"]) == 1){
				$template->assign_var("FORM_CHECKED"," checked=\checked\"");
			}else{
				$template->assign_var("FORM_CHECKED","");
			}
			if(isset($plugin_customsearch_datas["internpages"]) == 1){
				$template->assign_var("INTERNPAGES_CHECKED"," checked=\checked\"");
			}else{
				$template->assign_var("INTERNPAGES_CHECKED","");
			}
			if(isset($plugin_customsearch_datas["noresult"]) != ""){
				$template->assign_var("NORESULTTEXT",htmlentities(utf8_encode($plugin_customsearch_datas["noresult"])));
			}else{
				$template->assign_var("NORESULTTEXT","");
			}
			$template->assign_var("FORMACTION",UrlRewriting::GetUrlByAlias($this->page->alias));
			$template->assign_var("NORESULT",Language::DirectTranslate("plugin_customsearch_noresult"));
			return $template->getCode();
		}

		public function save(Page $newPage, Page $oldPage) {
		}
	}
?>
