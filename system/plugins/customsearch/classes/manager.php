<?php
/*
 * manager.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
class Plugin_CustomSearch_Manager{

	public static function searchPages($token){
		$token = DataBase::Current()->EscapeString($token);
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}pages WHERE title LIKE '%".$token."%' OR content LIKE '%".$token."%'");
	}
	
	public static function getMetaData($pageid){
		$pageid = DataBase::Current()->EscapeString($pageid);
		return DataBase::Current()->ReadField("SELECT content FROM {'dbprefix'}meta_local WHERE page = '".$pageid."' AND name = 'description'");
	}
	
	public static function verifyRoleRight($role,$id){
		$role = DataBase::Current()->EscapeString($role);
		$id = DataBase::Current()->EscapeString($id);
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}role_rights WHERE role = '".$role."' AND page = '".$id."'");
	}
	
	public static function searchUsers($token){
		$token = DataBase::Current()->EscapeString($token);
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}user WHERE role != '5' AND name LIKE '%".$token."%'");
	}
	
	public static function searchOrgs($token){
		$token = DataBase::Current()->EscapeString($token);
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_orgprofile WHERE orgname LIKE '%".$token."%'");
	}
	
	public static function searchImgs($token){
		$token = DataBase::Current()->EscapeString($token);
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}images WHERE MATCH (name, description) AGAINST ('".$token."' IN BOOLEAN MODE)");
	}
	
	public static function deleteSearchSettings($alias){
		$alias = DataBase::Current()->EscapeString($alias);
		DataBase::Current()->Execute("DELETE FROM `{'dbprefix'}plugin_plugindata_data` WHERE `property` = 'plugin_customsearch_datas".$alias."'; ");
	}
}
?>
