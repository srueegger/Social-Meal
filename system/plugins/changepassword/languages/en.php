<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

	$tokens['PLUGIN_DESCRIPTION'] = "Adds a page for change your password.";
	$tokens['CHANGEPASSWORD'] = "Change Password";
	$tokens['WRONG-CONFIRM-PASSWORD'] = "Confirm Password is wrong!";
	$tokens['OLD-PASSWORD-WRONG'] = "The current password is incorrect!";
	$tokens['SUCCESSFUL'] = "Password changed succesful!";
	$tokens['CURRENT-PASSWORD'] = "Current Password";
	$tokens['NEW-PASSWORD'] = "New Password";
	$tokens['CONFIRM-PASSWORD'] = "Confirm Password";
	$tokens['TITLE'] = "Change Password";
?>
