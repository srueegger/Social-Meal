/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

function chkFormular () {
	if (document.changepw.old_password.value == "") {
		alert("Bitte gib dein altes Passwort ein!");
		document.changepw.old_password.focus();
		return false;
	}
	if (document.changepw.new_password.value == "") {
		alert("Bitte gib dein neues Passwort ein!");
		document.changepw.new_password.focus();
		return false;
	}
	if (document.changepw.confirm_password.value == "") {
		alert("Bitte bestätige dein neues Passwort");
		document.changepw.confirm_password.focus();
		return false;
	}
	if (document.changepw.new_password.value != document.changepw.confirm_password.value) {
		alert("Die neuen Passwörter stimmen nicht überein!");
		document.changepw.confirm_password.focus();
		return false;
	}
}
