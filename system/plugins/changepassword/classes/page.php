<?php
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

class Plugin_ChangePassword_Page extends Editor {

	function __construct(Page $page) {
			$this->page = $page;
		}

	public function display () {

		$template = new Template();
		$template->load("plugin_changepassword_changepassword");
		$template->show_if('PASSWORD_WRONG', false);
		$template->show_if('SUCCESSFUL', false);
		$template->show_if('OLD_PASSWORD_WRONG', false);
		if(isset($_REQUEST['old_password']) && !empty($_REQUEST['old_password']) && is_string($_REQUEST['old_password']) && isset($_REQUEST['new_password']) && !empty($_REQUEST['new_password']) && is_string($_REQUEST['new_password']) && isset($_REQUEST['confirm_password']) && !empty($_REQUEST['confirm_password']) && is_string($_REQUEST['confirm_password'])) {
			$old_password = DataBase::Current()->EscapeString($_REQUEST['old_password']);
			$new_password = DataBase::Current()->EscapeString($_REQUEST['new_password']);
			$confirm_password = DataBase::Current()->EscapeString($_REQUEST['confirm_password']);
			if($new_password != $confirm_password) {
				$template->show_if('PASSWORD_WRONG', true);
			}else{
				$password = DataBase::Current()->EscapeString(md5($new_password . Settings::getInstance()->get("salt")));
				$old_password = DataBase::Current()->EscapeString(md5($old_password . Settings::getInstance()->get("salt")));
				$userdata = Plugin_Profile_Manager::getUserDataPerId(User::Current()->id);
				$db_password = $userdata->password;
				if ($db_password && $db_password != null) {
					if ($db_password != $old_password) {
						$template->show_if('OLD_PASSWORD_WRONG', true);
					}else{
						DataBase::Current()->Execute("UPDATE `{'dbprefix'}user` SET `password` = '" . $password . "' WHERE `id` = '" . User::Current()->id . "'; ");
						$template->show_if('SUCCESSFUL', true);
						EventManager::raiseEvent("plugin_changepassword_change", array('old_password' => $old_password, 'new_password' => $password, 'userid' => User::Current()->id));
						Cache::clear("tables","userlist");
					}
				}
			}
		}
		$template->assign_var('ACTION', UrlRewriting::GetUrlByAlias($this->page->alias));
		echo $template->getCode();
	}

	public function getHeader () {
		echo "<link rel='stylesheet' href=\"" . Settings::getInstance()->get("host") . "system/plugins/changepassword/style.css\" type=\"text/css\" media=\"all\" />";
		echo('<script type="text/javascript" src="/system/plugins/changepassword/js/changepw.js"></script>');
	}

	public function getEditableCode() {
		$change = htmlentities(Language::GetGlobal()->getString("CHANGE"));
		return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
	}

	public function save(Page $newPage, Page $oldPage) {}
}

?>
