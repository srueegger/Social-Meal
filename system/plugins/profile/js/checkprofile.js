/*
 * checkprofile.js
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
function chkFormular () {
	if (document.editprofile.firstname.value == "") {
		alert("Bitte gebe deinen Vornamen ein!");
		document.editprofile.firstname.focus();
		return false;
	}
	if (document.editprofile.tel.value == "") {
		alert("Bitte gebe eine Telefonnummer ein!");
		document.editprofile.tel.focus();
		return false;
	}
	if (document.editprofile.tel.value.length < 9) {
		alert("Deine Telefonnummer muss mindestens 9 Zeichen haben!");
		document.editprofile.tel.focus();
		return false;
	}
	if (document.editprofile.lastname.value == "") {
		alert("Bitte gebe deinen Nachnamen ein!");
		document.editprofile.lastname.focus();
		return false;
	}
	if (document.editprofile.street.value == "") {
		alert("Bitte gebe deine Strasse ein!");
		document.editprofile.street.focus();
		return false;
	}
	if (document.editprofile.strnr.value == "") {
		alert("Bitte gebe deine Hausnummer ein!");
		document.editprofile.strnr.focus();
		return false;
	}
	if (document.editprofile.plz.value == "") {
		alert("Bitte gebe deine PLZ ein!");
		document.editprofile.plz.focus();
		return false;
	}
	var chkZplz = 1;
	for (i = 0; i < document.editprofile.plz.value.length; ++i)
		if (document.editprofile.plz.value.charAt(i) < "0" || 
		document.editprofile.plz.value.charAt(i) > "9")
		chkZplz = -1;
	if(chkZplz == -1) {
		alert("PLZ ist keine Zahl!");
		document.editprofile.plz.focus();
		return false;
	}
	if (document.editprofile.plz.value < 1000) {
		alert("Die PLZ darf nicht kleiner als 1000 sein!");
		document.editprofile.plz.focus();
		return false;
	}
	if (document.editprofile.ort.value == "") {
		alert("Bitte gebe deinen Wohnort ein!");
		document.editprofile.ort.focus();
		return false;
	}
	if (document.editprofile.gbdyear.value == "") {
		alert("Bitte gib deinen Jahrgang ein");
		document.editprofile.gbdyear.focus();
		return false;
	}
	if (document.editprofile.gbdyear.value < 0) {
		alert("Dein Jahrgang muss eine positivie Zahl sein!");
		document.editprofile.gbdyear.focus();
		return false;
	}
	if (document.editprofile.plz.value < 0) {
		alert("Deine PLZ muss eine positivie Zahl sein!");
		document.editprofile.plz.focus();
		return false;
	}
}
function confirm_alert(node) {
	return confirm("Möchtest du dein Profilbild wirklich löschen?");
}
