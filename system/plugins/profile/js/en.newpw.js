/*
 * en.newpw.js
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
function chkFormular () {
	if (document.newpw.email.value == "") {
		alert("Bitte gebe deine E-Mail Adresse ein!");
		document.newpw.email.focus();
		return false;
	}
	if (document.newpw.captcha.value == "") {
		alert("Bitte gebe das Captcha ein!");
		document.newpw.captcha.focus();
		return false;
	}
	if (document.newpw.email.value.indexOf("@") == -1) {
		alert("Bitte gebe eine gültige E-Mail Adresse an!");
		document.newpw.email.focus();
		return false;
	}
}
