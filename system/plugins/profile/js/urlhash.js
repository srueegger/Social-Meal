/*
 * urlhash.js
 * 
 * Copyright 2016 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	if ("onhashchange" in window) {

		var hash = location.hash;
		
		if (hash == "")
			return false;
		
		var pages_array = [
			'1',
			'2',
			'3',
			'4',
			'5'
		];
		
		var hash = hash.replace("#", ""); 

		if (!($.inArray(hash, pages_array) > -1)){
			return false;
		}else{
			$(".tabdata" + hash).trigger('click');
		}
		
	}

});
