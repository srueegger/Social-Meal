<?php
/*
 * newagb.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_Profile_newAGB extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			$template->load('plugin_profile_newagb');
			if(isset($_POST['newagb'])){
				if(isset($_POST['agb']) == 1){
					if($_POST['agb'] == 1){
						Plugin_Profile_Manager::updateAGBVersion();
					}
				}
				if(isset($_POST['datenschutz'])){
					if($_POST['datenschutz'] == 1){
						Plugin_Profile_Manager::updateDatenschutzVersion();
					}
				}
			}
			$user = Plugin_Profile_Manager::getUserDataPerId(User::Current()->id);
			$agb = Settings::getInstance()->get("agb");
			$datenschutz = Settings::getInstance()->get("datenschutz");
			$template->show_if('redirect',false);
			$template->show_if('agb',false);
			$template->show_if('datenschutz',false);
			if($user->agb == $agb && $user->datenschutz == $datenschutz){
				$template->show_if('redirect',true);
				echo($template->getCode());
				exit;
			}
			if($user->agb < $agb){
				$template->show_if('agb',true);
			}
			if($user->datenschutz < $datenschutz){
				$template->show_if('datenschutz',true);
			}
			echo($template->getCode());
		}

		function getHeader(){
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
