<?php
/*
 * editprofile.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_Profile_EditProfile extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			if(Mobile::isMobileDevice()){
				$template->load('plugin_profile_editprofile.mobile');
			}else{
				$template->load('plugin_profile_editprofile');
			}
			$template->show_if('showmsg',false);
			$template->show_if('imgmsg',false);
			$template->show_if('redirect',false);
			if(isset($_POST['plugin_editprofile_submit'])){
				Plugin_Profile_Manager::updateUser($_POST,$_FILES);
				$template->show_if('showmsg',true);
				if(Plugin_Profile_Manager::completeProfilEdit() == true){
					$template->show_if('redirect',true);
				}
			}
			$user = Plugin_Profile_Manager::getUserData(User::Current()->name);
			if(isset($_GET['deleteprofilimg']) && !isset($_FILES['profilimg'])){
				Plugin_Profile_Manager::deleteUserImage($user->profilimg);
				$user = Plugin_Profile_Manager::getUserData(User::Current()->name);
				$template->show_if('imgmsg',true);
			}
			if($user->profilimg == ''){
				$template->show_if('showupload',true);
				$template->show_if('showimage',false);
			}else{
				$template->show_if('showupload',false);
				$template->assign_var('profilimg',Settings::getInstance()->get("host").'content/uploads/profile/thumbs/'.$user->profilimg);
				$template->show_if('showimage',true);
			}
			$template->assign_var('firstname',htmlentities(ucfirst($user->firstname)));
			$template->assign_var('lastname',htmlentities(ucfirst($user->lastname)));
			$template->assign_var('street',htmlentities(ucfirst($user->strasse)));
			$template->assign_var('strnr',htmlentities($user->strnr));
			$template->assign_var('plz',htmlentities($user->plz));
			$template->assign_var('ort',htmlentities(ucfirst($user->ort)));
			$template->assign_var('username',htmlentities($user->name));
			$template->assign_var('usermail',htmlentities($user->email));
			if($user->openimg == 1){
				$template->assign_var('openimgchecked','checked="checked"');
			}else{
				$template->assign_var('openimgchecked','');
			}
			if($user->kanton != ''){
				if(strlen($user->kanton) == 2){
					$template->assign_var('selected'.htmlentities(strtolower($user->kanton)),'selected="selected"');
				}else{
					if($user->kanton == 'Deutschland'){
						$template->assign_var('selectedde','selected="selected"');
					}elseif($user->kanton == 'Frankreich'){
						$template->assign_var('selectedfra','selected="selected"');
					}
				}
			}
			$template->assign_var('bzs'.htmlentities($user->bzstatus),'selected="selected"');
			$template->assign_var('aboutme',htmlentities($user->aboutme));
			$gbdatum = explode('-',$user->gbdatum);
			$range = 100;
			$limit = 14;
			$current = date('Y');
			$eldest = $current - $range;
			$recent = $current - $limit;
			foreach(range($recent,$eldest) as $year){
				$index = $template->add_loop_item('gbdyears');
				$template->assign_loop_var("gbdyears",$index,"year",$year);
				if($year != $gbdatum[0]){
					$template->assign_loop_var("gbdyears",$index,"selected",'');
				}else{
					$template->assign_loop_var("gbdyears",$index,"selected",'selected="selected"');
				}
			}
			$template->assign_var('GBDM'.$gbdatum[1],'selected="selected"');
			$template->assign_var('GBDS'.$gbdatum[2],'selected="selected"');
			$template->assign_var('webpage',htmlentities($user->webpage));
			$template->assign_var('facebook',htmlentities($user->fblink));
			$template->assign_var('twitter',htmlentities($user->twitlink));
			$template->assign_var('gpvalue',htmlentities($user->gplink));
			$template->assign_var('flickrvalue',htmlentities($user->flickrlink));
			$template->assign_var('linkedinvalue',htmlentities($user->linkedinlink));
			$template->assign_var('skypelink',htmlentities($user->skypelink));
			$template->assign_var('gen'.htmlentities($user->gender),'selected="selected"');
			$template->assign_var('job',htmlentities($user->job));
			$template->assign_var('tel',htmlentities($user->tel));
			if($user->fleisch == 1){
				$template->assign_var('checkedfleisch','checked="checked"');
			}
			if($user->fisch == 1){
				$template->assign_var('checkedfisch','checked="checked"');
			}
			if($user->seafood == 1){
				$template->assign_var('checkedseafood','checked="checked"');
			}
			if($user->vegi == 1){
				$template->assign_var('checkedvegi','checked="checked"');
			}
			if($user->vegan == 1){
				$template->assign_var('checkedvegan','checked="checked"');
			}
			if($user->bio == 1){
				$template->assign_var('checkedbio','checked="checked"');
			}
			if($user->saison == 1){
				$template->assign_var('checkedsaisonal','checked="checked"');
			}
			if($user->regional == 1){
				$template->assign_var('checkedregional','checked="checked"');
			}
			if($user->laktose == 1){
				$template->assign_var('checkedlaktose','checked="checked"');
			}
			if($user->gluten == 1){
				$template->assign_var('checkedgluten','checked="checked"');
			}
			if($user->nusse == 1){
				$template->assign_var('checkednusse','checked="checked"');
			}
			if($user->alkohol == 1){
				$template->assign_var('checkedalkohol','checked="checked"');
			}
			if($user->scharf == 1){
				$template->assign_var('checkedscharf','checked="checked"');
			}
			if($user->hefe == 1){
				$template->assign_var('checkedhefe','checked="checked"');
			}
			if($user->koscher == 1){
				$template->assign_var('checkedkoscher','checked="checked"');
			}
			if($user->halal == 1){
				$template->assign_var('checkedhalal','checked="checked"');
			}
			if(Plugin_Profile_Manager::isVerified($user->name) >= 1){
				$template->show_if('personalform',false);
				$template->show_if('personalinfo',true);
				$template->assign_var('cgbdatum',htmlentities($user->gbdatum));
				$template->assign_var('cgender',utf8_encode(Language::directTranslate('plugin_profile_gen'.htmlentities($user->gender))));
				$template->assign_var('userid',htmlentities($user->id));
				$template->assign_var('hiddengender',htmlentities($user->gender));
				$template->assign_var('hiddengbday',$gbdatum[2]);
				$template->assign_var('hiddengbmonth',$gbdatum[1]);
				$template->assign_var('hiddengbyear',$gbdatum[0]);
				$template->assign_var('hiddenkanton',htmlentities($user->kanton));
			}else{
				$template->show_if('personalform',true);
				$template->show_if('personalinfo',false);
			}
			$template->assign_var('allergy',htmlentities($user->allergy));
			$template->assign_var('selectedbs','');
			$template->assign_var('selectedbl','');
			$template->assign_var('selectedag','');
			$template->assign_var('selectedso','');
			$template->assign_var('selectedju','');
			$template->assign_var('selectedar','');
			$template->assign_var('selectedai','');
			$template->assign_var('selectedbe','');
			$template->assign_var('selectedfr','');
			$template->assign_var('selectedge','');
			$template->assign_var('selectedgl','');
			$template->assign_var('selectedgr','');
			$template->assign_var('selectedlu','');
			$template->assign_var('selectedne','');
			$template->assign_var('selectednw','');
			$template->assign_var('selectedow','');
			$template->assign_var('selectedsh','');
			$template->assign_var('selectedsz','');
			$template->assign_var('selectedsg','');
			$template->assign_var('selectedti','');
			$template->assign_var('selectedtg','');
			$template->assign_var('selectedur','');
			$template->assign_var('selectedvd','');
			$template->assign_var('selectedvs','');
			$template->assign_var('selectedzg','');
			$template->assign_var('selectedzh','');
			$template->assign_var('selectedde','');
			$template->assign_var('selectedfra','');
			$template->assign_var('bzs0','');
			$template->assign_var('bzs1','');
			$template->assign_var('bzs2','');
			$template->assign_var('bzs3','');
			$template->assign_var('bzs4','');
			$template->assign_var('bzs5','');
			$template->assign_var('bzs6','');
			$template->assign_var('bzs7','');
			$template->assign_var('bzs8','');
			$template->assign_var('bzs9','');
			$template->assign_var('bzs10','');
			$template->assign_var('bzs11','');
			$template->assign_var('GBDM01','');
			$template->assign_var('GBDM02','');
			$template->assign_var('GBDM03','');
			$template->assign_var('GBDM04','');
			$template->assign_var('GBDM05','');
			$template->assign_var('GBDM06','');
			$template->assign_var('GBDM07','');
			$template->assign_var('GBDM08','');
			$template->assign_var('GBDM09','');
			$template->assign_var('GBDM10','');
			$template->assign_var('GBDM11','');
			$template->assign_var('GBDM12','');
			$template->assign_var('GBDS01','');
			$template->assign_var('GBDS02','');
			$template->assign_var('GBDS03','');
			$template->assign_var('GBDS04','');
			$template->assign_var('GBDS05','');
			$template->assign_var('GBDS06','');
			$template->assign_var('GBDS07','');
			$template->assign_var('GBDS08','');
			$template->assign_var('GBDS09','');
			$template->assign_var('GBDS10','');
			$template->assign_var('GBDS11','');
			$template->assign_var('GBDS12','');
			$template->assign_var('GBDS13','');
			$template->assign_var('GBDS14','');
			$template->assign_var('GBDS15','');
			$template->assign_var('GBDS16','');
			$template->assign_var('GBDS17','');
			$template->assign_var('GBDS18','');
			$template->assign_var('GBDS19','');
			$template->assign_var('GBDS20','');
			$template->assign_var('GBDS21','');
			$template->assign_var('GBDS22','');
			$template->assign_var('GBDS23','');
			$template->assign_var('GBDS24','');
			$template->assign_var('GBDS25','');
			$template->assign_var('GBDS26','');
			$template->assign_var('GBDS27','');
			$template->assign_var('GBDS28','');
			$template->assign_var('GBDS29','');
			$template->assign_var('GBDS30','');
			$template->assign_var('GBDS31','');
			$template->assign_var('GEN0','');
			$template->assign_var('GEN1','');
			$template->assign_var('GEN2','');
			$template->assign_var('GEN3','');
			$template->assign_var('GEN4','');
			$template->assign_var('GEN5','');
			$template->assign_var('checkedfleisch','');
			$template->assign_var('checkedfisch','');
			$template->assign_var('checkedseafood','');
			$template->assign_var('checkedvegi','');
			$template->assign_var('checkedvegan','');
			$template->assign_var('checkedbio','');
			$template->assign_var('checkedsaisonal','');
			$template->assign_var('checkedregional','');
			$template->assign_var('checkedlaktose','');
			$template->assign_var('checkedgluten','');
			$template->assign_var('checkednusse','');
			$template->assign_var('checkedalkohol','');
			$template->assign_var('checkedscharf','');
			$template->assign_var('checkedhefe','');
			echo($template->getCode());
		}

		function getHeader(){
			echo('<script type="text/javascript" src="/system/plugins/profile/js/checkprofile.js"></script>');
			echo('<link href="/system/plugins/bbcode/css/editor.css" rel="stylesheet" type="text/css" />');
			echo('<script type="text/javascript" src="/system/plugins/bbcode/js/editor.js"></script>');
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
