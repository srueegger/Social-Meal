<?PHP
/*
 * admineditratings.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_Profile_AdminEditRatings extends Editor{

		/**
		 *
		 * @param Page $page
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			$template->load('plugin_profile_admineditratings');
			$template->show_if('editrate',false);
			if(!isset($_GET['uid'])){
				$template->show_if('getuser',true);
			}else{
				$template->show_if('getuser',false);
				if(Plugin_Profile_Manager::userExistFromId($_GET['uid'])){
					$template->show_if('nouser',false);
					if(Plugin_Events_Manager::countRatings($_GET['uid']) > 0){
						if(!isset($_GET['rateid'])){
							$template->show_if('nouser',false);
							$template->show_if('noratings',false);
							$template->show_if('ratings',true);
							foreach(Plugin_Events_Manager::getRatings($_GET['uid']) as $rating){
								$index = $template->add_loop_item('ratings');
								$template->assign_loop_var("ratings",$index,"uid",htmlentities($_GET['uid']));
								$template->assign_loop_var("ratings",$index,"rateid",htmlentities($rating->id));
								$template->assign_loop_var("ratings",$index,"stars",htmlentities($rating->stars));
								$template->assign_loop_var("ratings",$index,"fromuser",htmlentities(Plugin_Profile_Manager::getUserDataPerId($rating->userid)->name));
								$template->assign_loop_var("ratings",$index,"ratedate",Plugin_Events_Manager::formatDate($rating->datum));
								$template->assign_loop_var("ratings",$index,"replydate",Plugin_Events_Manager::formatDate($rating->replydatum));
								$template->assign_loop_var("ratings",$index,"rating",nl2br(htmlentities($rating->text)));
								$template->assign_loop_var("ratings",$index,"ratereply",nl2br(htmlentities($rating->ratereply)));
							}
						}else{
							if(isset($_POST['updaterate'])){
								Plugin_Events_Manager::updateRate($_POST,$_GET['rateid']);
							}
							$template->show_if('nouser',false);
							$template->show_if('noratings',false);
							$template->show_if('ratings',false);
							$template->show_if('editrate',true);
							$rating = Plugin_Events_Manager::getRating($_GET['rateid']);
							$template->assign_var('uid',htmlentities($_GET['uid']));
							$template->assign_var('rateid',htmlentities($_GET['rateid']));
							$template->assign_var('stars',htmlentities($rating->stars));
							$template->assign_var('datum',htmlentities($rating->datum));
							$template->assign_var('text',htmlentities($rating->text));
							$template->assign_var('replydatum',htmlentities($rating->replydatum));
							$template->assign_var('ratereply',htmlentities($rating->ratereply));
						}
					}else{
						$template->show_if('nouser',false);
						$template->show_if('noratings',true);
						$template->show_if('ratings',false);
					}
				}else{
					$template->show_if('nouser',true);
					$template->show_if('noratings',false);
					$template->show_if('ratings',false);
				}
			}
			echo($template->getCode());
		}

		function getHeader(){
		}

		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}

		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
