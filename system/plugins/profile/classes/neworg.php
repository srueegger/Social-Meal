<?PHP
/*
 * neworg.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_Profile_NewOrg extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
			if(!isset($_GET['orgprofile'])){
				$page->title = Language::DirectTranslate('plugin_profile_neworgpagetypetitle');
			}else{
				$page->title = Language::DirectTranslate('plugin_profile_editorgprofile');
			}
		}
		public function display(){
			$template = new Template();
			$template->load('plugin_profile_neworg');
			$template->show_if('showupload',true);
			$template->show_if('profilimg',false);
			$template->show_if('showmsg',false);
			$template->show_if('redirect',false);
			$template->show_if('form',true);
			$template->show_if('keinzugriff',false);
			$template->show_if('editok',false);
			$template->show_if('delimg',false);
			$template->show_if('admin',false);
			$template->show_if('adminnoexist',false);
			$template->show_if('adminok',false);
			$template->show_if('admindelok',false);
			$template->show_if('verifystart',false);
			$template->show_if('verify',false);
			$template->show_if('tel',false);
			if(isset($_POST['orgname']) && !isset($_GET['orgprofile'])){
				$orgid = Plugin_Profile_Manager::createOrgProfile($_POST,$_FILES);
				$template->assign_var('orgid',htmlentities($orgid));
				$template->show_if('redirect',true);
			}
			if(isset($_POST['orgname']) && isset($_GET['orgprofile'])){
				Plugin_Profile_Manager::updateOrgProfile($_GET['orgprofile'],$_POST,$_FILES);
				$template->assign_var('orgid',htmlentities($_GET['orgprofile']));
				$template->show_if('editok',true);
			}
			if(isset($_GET['deladmin']) && isset($_GET['orgprofile'])){
				if(Plugin_Profile_Manager::countOrgAdmins($_GET['orgprofile']) > 1){
					if(Plugin_Profile_Manager::isOrgAdmin($_GET['orgprofile'],$_GET['deladmin']) == true){
						Plugin_Profile_Manager::delOrgAdmin($_GET['deladmin']);
						$template->show_if('admindelok',true);
					}
				}
			}
			if(isset($_POST['newadmin'])){
				if($_POST['newadmin'] != ''){
					if(Plugin_Profile_Manager::userExist($_POST['newadmin'])){
						$userdata = Plugin_Profile_Manager::getUserData($_POST['newadmin']);
						if(Plugin_Profile_Manager::countOrgWithUserData($userdata->id,$_GET['orgprofile']) == 0){
							Plugin_Profile_Manager::insertOrgAdmin($userdata->id,$_GET['orgprofile'],0);
							$template->assign_var('newadminusername',htmlentities($userdata->name));
							$template->show_if('adminok',true);
							if(Plugin_Profile_Manager::getUserSetting('orgadmin',$userdata->id) == 1){
								$orginfos = Plugin_Profile_Manager::loadOrganisation($_GET['orgprofile']);
								$mailtext = 'Liebe*r '.ucfirst($userdata->firstname).'<br><br>Wir freuen uns dir mitteilen zu dürfen, dass du soeben zum Administrator des Organisations-Profils von <a href="'.Settings::getInstance()->get("host").'organisationsprofil.html?profile='.$orginfos->id.'">'.$orginfos->orgname.'</a> hinzugefügt wurdest.<br><br>Nachdem du dies in deinem <a href="'.Settings::getInstance()->get("host").'mein-profil.html">Profil</a> bestätigt hast, hast du die Möglichkeit <a href="'.Settings::getInstance()->get("host").'neuen-event-erstellen.html?eventtype=3">Spezial-Meals</a> im Namen dieser Organisation zu erstellen. Wir raten dir, dazu unsere Tipps zur <a href="'.Settings::getInstance()->get("host").'FAQ/erstellung-von-spezial-events.html">Erstellung von Spezial-Meals</a> durchzulesen. Ausserdem hast du auch die Möglichkeit die öffentliche Organisationsseite deiner Organisation zu bearbeiten.<br><br>Wir wünschen dir und deiner Organisation viele gute Erfahrungen als Teil unserer Community!<br>Social Meal<br><br>P.S.: Diesen E-Mail-Typ kannst du in den <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abbestellen, falls er dir überflüssig erscheint.';
								$h2t = new Plugin_Mailer_html2text($mailtext);
								$subject = utf8_decode('Du wurdest auf Social Meal zum Administrator eines Organisations-Profils ernannt');
								$mail = new Plugin_PHPMailer_PHPMailer;
								$mail->IsSMTP();
								$mail->AddAddress($userdata->email);
								$mail->WordWrap = 50;
								$mail->IsHTML(true);
								$mail->Subject = $subject;
								$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
								$mail->AltBody = $h2t->get_text();
								if(!$mail->Send()){
									echo('Mail Error:'.$mail->ErrorInfo);
								}
								unset($orginfos);
							}
						}
					}else{
						$template->assign_var('falseusername',htmlentities($_POST['newadmin']));
						$template->show_if('adminnoexist',true);
					}
				}
			}
			if(isset($_GET['msg'])){
				$template->assign_var('orgid',htmlentities($_GET['orgprofile']));
				$template->show_if('showmsg',true);
			}
			if(!isset($_GET['orgprofile'])){
				$template->assign_var('formaction','/neues-organisationsprofil.html');
				$template->assign_var('orgname','');
				$template->assign_var('orgwebseite','');
				$template->assign_var('orgmail','');
				$template->assign_var('orgfb','');
				$template->assign_var('orgtwitter','');
				$template->assign_var('orggoogleplus','');
				$template->assign_var('desc','');
				$template->assign_var('orgtel','');
				$template->assign_var('street','');
				$template->assign_var('strnr','');
				$template->assign_var('ort','');
				$template->assign_var('plz','');
				$template->assign_var('verify','');
				$template->assign_var('verifystatus','');
			}else{
				if(Plugin_Profile_Manager::canEditOrgProfile($_GET['orgprofile'])){
					$orginfos = Plugin_Profile_Manager::loadOrganisation($_GET['orgprofile']);
					if(isset($_GET['delimg']) == true){
						Plugin_Profile_Manager::delOrgImage($_GET['orgprofile']);
						$template->show_if('delimg',true);
					}
					if(isset($_GET['verifymail']) == true && $orginfos->orgmail != '' && $orginfos->verify == 0){
						Plugin_Profile_Manager::startVerifyOrgMail($_GET['orgprofile']);
						$template->show_if('verifystart',true);
					}
					$template->show_if('admin',true);
					$template->assign_var('formaction','/neues-organisationsprofil.html?orgprofile='.htmlentities($orginfos->id));
					$template->assign_var('orgname',htmlentities($orginfos->orgname));
					$template->assign_var('orgwebseite',htmlentities($orginfos->orgweb));
					$template->assign_var('orgmail',htmlentities($orginfos->orgmail));
					$template->assign_var('orgfb',htmlentities($orginfos->orgfb));
					$template->assign_var('orgtwitter',htmlentities($orginfos->orgtwitter));
					$template->assign_var('orggoogleplus',htmlentities($orginfos->orggplus));
					$template->assign_var('orgtel',htmlentities($orginfos->orgtel));
					$template->assign_var('desc',htmlentities($orginfos->description));
					$template->assign_var('street',htmlentities($orginfos->strasse));
					$template->assign_var('strnr',htmlentities($orginfos->strnr));
					$template->assign_var('plz',htmlentities($orginfos->plz));
					$template->assign_var('ort',htmlentities($orginfos->ort));
					$template->assign_var('verifystatus',htmlentities($orginfos->verify));
					if($orginfos->profilimg != ''){
						$template->show_if('showupload',false);
						$template->show_if('profilimg',true);
						$template->assign_var('profilimg',htmlentities($orginfos->profilimg));
					}
					if($orginfos->orgmail != ''){
						if($orginfos->verify == 0){
							$template->assign_var('verify','<a infotext="'.Language::DirectTranslate('plugin_profile_mailverifyno').'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('cancel').'" /></span></a> <a href="/neues-organisationsprofil.html?orgprofile='.htmlentities($orginfos->id).'&verifymail=true">'.Language::DirectTranslate('plugin_profile_verifymail').'!</a>');
						}else{
							$template->assign_var('verify','<a infotext="'.Language::DirectTranslate('plugin_profile_mailverifyok').'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('tick').'" /></span></a>');
							$template->show_if('verify',true);
							if($orginfos->asmail == 1){
								$template->assign_var('asmailchecked','checked="checked"');
							}else{
								$template->assign_var('asmailchecked','');
							}
						}
					}else{
						$template->assign_var('verify','');
					}
					if($orginfos->orgtel != ''){
						$template->show_if('tel',true);
						if($orginfos->asmailtel == 1){
							$template->assign_var('asmailtelchecked','checked="checked"');
						}else{
							$template->assign_var('asmailtelchecked','');
						}
					}
					$countorgadmins = Plugin_Profile_Manager::countOrgAdmins($orginfos->id);
					foreach(Plugin_Profile_Manager::getOrgAdmins($orginfos->id) as $orgadmin){
						$index = $template->add_loop_item('admins');
						$username = DataBase::Current()->ReadField("SELECT name FROM {'dbprefix'}user WHERE id = '".$orgadmin->userid."'");
						$template->assign_loop_var("admins",$index,"adminusername",htmlentities($username));
						if($countorgadmins > 1){
							$template->assign_loop_var("admins",$index,"adminuserdel",' <a infotext="'.htmlentities($username).' '.Language::DirectTranslate('plugin_profile_neworg17').'" href="/neues-organisationsprofil.html?orgprofile='.htmlentities($orginfos->id).'&deladmin='.htmlentities($orgadmin->id).'" class="infotooltip"><span title=""><img src="'.Icons::getIcon('cross').'" /></span></a>');
						}else{
							$template->assign_loop_var("admins",$index,"adminuserdel",'');
						}
					}
				}else{
					$template->show_if('form',false);
					$template->show_if('keinzugriff',true);
				}
			}
			echo($template->getCode());
		}

		function getHeader(){
			echo('<script type="text/javascript" src="/system/plugins/profile/js/neworg.js"></script>');
			echo('<link href="/system/plugins/bbcode/css/editor.css" rel="stylesheet" type="text/css" />');
			echo('<script type="text/javascript" src="/system/plugins/bbcode/js/editor.js"></script>');
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
