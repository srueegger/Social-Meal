<?PHP
/*
 * adminrangs.php
 *
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
	class Plugin_Profile_AdminRangs extends Editor{

		/**
		 *
		 * @param Page $page
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			$template->load('plugin_profile_adminrangs');
			if(isset($_POST['saverang'])){
				Plugin_Profile_Manager::saveRang($_POST);
			}
			if(isset($_GET['del'])){
				Plugin_Profile_Manager::delRang($_GET['del']);
			}
			foreach(Plugin_Profile_Manager::getRangs() as $rang){
				$index = $template->add_loop_item("rangs");
				$template->assign_loop_var("rangs",$index,"apunkte",htmlentities($rang->apunkte));
				$template->assign_loop_var("rangs",$index,"epunkte",htmlentities($rang->epunkte));
				$template->assign_loop_var("rangs",$index,"name",htmlentities($rang->name));
				$template->assign_loop_var("rangs",$index,"id",htmlentities($rang->id));
			}
			$lastpoints = DataBase::Current()->ReadField("SELECT epunkte FROM {'dbprefix'}plugin_ranglist ORDER BY epunkte DESC LIMIT 1");
			if($lastpoints == 0){
				$lastpoints = -1;
			}
			$template->assign_var('apunkte',$lastpoints+1);
			echo($template->getCode());
		}

		function getHeader(){
		}

		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}

		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
