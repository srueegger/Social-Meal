<?PHP
/*
 * verify.php
 *
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
	class Plugin_Profile_Verify extends Editor{

		/**
		 *
		 * @param Page $page
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			$template->load('plugin_profile_verify');
			$template->show_if('verifyok',false);
			$template->show_if('verifywork',false);
			$template->show_if('verifyform',true);
			$template->show_if('codeform',false);
			$template->show_if('codefalse',false);
			$template->show_if('noimage',false);
			$userinfo = Plugin_Profile_Manager::getUserDataPerId(User::Current()->id);
			if(isset($_POST['sendimage'])){
				if(empty($_FILES['image'])){
					$template->show_if('noimage',true);
					exit();
				}
				$data = Plugin_Profile_Manager::startVerify($_FILES);
				#$path = Plugin_TempDir_Dir::get("plugin", "profile", "verify");
				#define('FPDF_FONTPATH',Settings::getInstance()->get("root").'system/plugins/fpdf/font');
				#$pdf = new Plugin_FPDF_FPDF('P','mm','A4');
				#$pdf->AddFont('Helvetica','',Settings::getInstance()->get("root").'system/plugins/fpdf/font/helvetica.php');
				#$pdf->AddPage();
				#$pdf->SetFont('Helvetica','',16);
				#$pdf->Cell(40,10,'Hello World!');
				#$pdf->Output($path.'test.pdf');
				$subject = 'Neue Verifizierungsanfrage';
				$body = 'Hallo<br />Es gibt eine neue Verifizierungsanfrage von User "'.$userinfo->name.'" (ID: <b>'.$userinfo->id.'</b>)<br />Nachfolgend seine Daten:<br /><br/>Vorname: '.utf8_decode(ucfirst($userinfo->firstname)).'<br />Nachname: '.utf8_decode(ucfirst($userinfo->lastname)).'<br />Adresse: '.utf8_decode(ucfirst($userinfo->strasse)).' '.$userinfo->strnr.' '.$userinfo->plz.' '.ucfirst($userinfo->ort).' '.$userinfo->kanton.'<br>Tel: '.$userinfo->tel.'<br>Geburtsdatum: '.$userinfo->gbdatum.'<br /><br />Hier findest du die Ausweiskopie der Person: <a href="'.Settings::getInstance()->get("host").'content/uploads/verify/'.$data['filename'].'">'.Settings::getInstance()->get("host").'content/uploads/verify/'.$data['filename'].'</a><br /><br />Verifizierungscode fuer '.utf8_decode($userinfo->firstname).': <b>'.$data['verifycode'].'</b>';
				$h2t = new Plugin_Mailer_html2text($body);
				$mail = new Plugin_PHPMailer_PHPMailer;
				$mail->IsSMTP();
				$mail->AddAddress('info@socialmeal.ch');
				$mail->WordWrap = 50;
				$mail->IsHTML(true);
				$mail->Subject = $subject;
				$mail->AddAttachment($path.'test.pdf');
				$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$body);
				$mail->AltBody = $h2t->get_text();
				if(!$mail->Send()){
					echo('Mail Error:'.$mail->ErrorInfo);
				}
				$subject = utf8_decode('Herzlichen Dank für deine Verifzierungsanfrage');
				$body = 'Liebe*r '.utf8_decode(ucfirst($userinfo->firstname)).'<br><br>Vielen Dank, dass du dein <a href="'.Settings::getInstance()->get("host").'mein-profil.html?profile='.$userinfo->name.'">Profil</a> auf Social Meal verifizieren lassen willst!<br><br>Damit trägst du zu einer hohen Sicherheit auf Social Meal bei und mit deiner Spende hilfst du uns unsere Ausgaben zu decken.<br><br>Nach der Überprüfung deiner Angaben werden wir dich zuerst telefonisch kontaktieren und dir dann den Verifizierungscode an deine Postadresse schicken.<br><br>Wir melden uns so bald wie möglich bei dir!<br>Social Meal';
				$h2t = new Plugin_Mailer_html2text($body);
				$mail = new Plugin_PHPMailer_PHPMailer;
				$mail->IsSMTP();
				$mail->AddAddress($userinfo->email);
				$mail->WordWrap = 50;
				$mail->IsHTML(true);
				$mail->Subject = $subject;
				$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$body);
				$mail->AltBody = $h2t->get_text();
				if(!$mail->Send()){
					echo('Mail Error:'.$mail->ErrorInfo);
				}
				Plugin_Profile_Manager::updateVerify(1);
				$template->show_if('verifywork',true);
			}
			if(isset($_POST['sendcode'])){
				if($userinfo->verify == 1){
					$datas = Plugin_PluginData_Data::getData('user_'.User::Current()->id.'','plugin','profile_verify');
					if($datas['verifycode'] == $_POST['code']){
						Plugin_Profile_Manager::updateVerify(2);
						unlink(Settings::getInstance()->get("root")."content/uploads/verify/".$datas['filename']);
						Plugin_Profile_Manager::delVerifyCache();
						$subject = utf8_decode('Herzlichen Dank für deine erfolgreiche Verifzierung');
						$body = 'Liebe*r '.utf8_decode(ucfirst($userinfo->firstname)).'<br><br>Vielen Dank, dass du dein <a href="'.Settings::getInstance()->get("host").'mein-profil.html?profile='.$userinfo->name.'">Profil</a> auf Social Meal erfolgreich verifiziert hast! Ab sofort ist dies überall mit einem grünen Häkchen hinter deinem Benutzernamen ersichtlich.<br><br>Damit trägst du zu einer hohen Sicherheit auf Social Meal bei und mit deiner Spende hilfst du uns unsere Ausgaben zu decken.<br><br>Falls du zukünftig deine Adresse oder Telefonnummer ändern solltest, schicke uns doch bitte ein E-Mail an <a href="mailto:info@socialmeal.ch">info@socialmeal.ch</a> damit wir deine Angaben korrigieren können.<br><br>Wir wünschen dir eine gute Zeit in unserer Community!<br>Social Meal';
						$h2t = new Plugin_Mailer_html2text($body);
						$mail = new Plugin_PHPMailer_PHPMailer;
						$mail->IsSMTP();
						$mail->AddAddress($userinfo->email);
						$mail->WordWrap = 50;
						$mail->IsHTML(true);
						$mail->Subject = $subject;
						$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$body);
						$mail->AltBody = $h2t->get_text();
						if(!$mail->Send()){
							echo('Mail Error:'.$mail->ErrorInfo);
						}
						Plugin_Profile_Manager::updatePunkte(25);
						$template->show_if('verifyok',true);
					}else{
						$template->show_if('codefalse',true);
					}
				}
			}
			if($userinfo->verify == 1){
				$template->show_if('verifywork',true);
				$template->show_if('verifyform',false);
				$template->show_if('codeform',true);
			}elseif($userinfo->verify == 2){
				$template->show_if('verifyok',true);
				$template->show_if('verifyform',false);
			}
			echo($template->getCode());
		}

		function getHeader(){
		}

		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$template = new Template();
			$template->load('plugin_profile_verifyadmin');
			$template->assign_var('sendurl',UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias));
			if(isset($_POST["userid"])){
				$template->show_if('showform',false);
				$template->show_if('verifyok',true);
				$userid = DataBase::Current()->EscapeString($_POST['userid']);
				$data = array();
				$data['verifycode'] = mt_rand();
				$template->assign_var('code',$data['verifycode']);
				Plugin_PluginData_Data::setData('user_'.$userid.'',$data,'profile_verify','plugin');
				DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET verify = '1' WHERE id = '".$userid."'");
				$userinfo = Plugin_Profile_Manager::getUserDataPerId($userid);
				$subject = 'Neue Verifizierungsanfrage';
				$body = 'Hallo<br />Es gibt eine neue Verifizierungsanfrage von User "'.$userinfo->name.'" (ID: <b>'.$userinfo->id.'</b>)<br />Nachfolgend seine Daten:<br /><br/>Vorname: '.utf8_decode($userinfo->firstname).'<br />Nachname: '.utf8_decode($userinfo->lastname).'<br />Adresse: '.utf8_decode($userinfo->strasse).' '.$userinfo->strnr.' '.$userinfo->plz.' '.$userinfo->ort.' '.$userinfo->kanton.'<br>Tel: '.$userinfo->tel.'<br>Geburtsdatum: '.$userinfo->gbdatum.'<br /><br />Verifizierungscode fuer '.utf8_decode($userinfo->firstname).': <b>'.$data['verifycode'].'</b>';
				$h2t = new Plugin_Mailer_html2text($body);
				$mail = new Plugin_PHPMailer_PHPMailer;
				$mail->IsSMTP();
				$mail->AddAddress('info@socialmeal.ch');
				$mail->WordWrap = 50;
				$mail->IsHTML(true);
				$mail->Subject = $subject;
				$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$body);
				$mail->AltBody = $h2t->get_text();
				if(!$mail->Send()){
					echo('Mail Error:'.$mail->ErrorInfo);
				}
			}else{
				$template->show_if('showform',true);
				$template->show_if('verifyok',false);
			}
			return $template->getCode();
		}

		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
