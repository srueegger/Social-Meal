<?PHP
/*
 * invite.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_Profile_Invite extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			$template->load('plugin_profile_invite');
			$template->show_if('singlemail',false);
			$template->show_if('addressbook',false);
			$invitelink = 'https://socialmeal.ch/registrieren.html?invitefrom='.User::Current()->id;
			$shortlink = Plugin_Profile_Manager::inviteShortUrl($invitelink);
			$userdata = Plugin_Profile_Manager::getUserDataPerId(User::Current()->id);
			$template->assign_var('invitelink',htmlentities($invitelink));
			$template->assign_var('shortlink',$shortlink);
			if(isset($_POST['send_singlemail']) && $_POST['singlemail'] != ''){
				$mailtext = 'Hallo, der User: '.User::Current()->name.' ('.ucfirst($userdata->firstname).' '.ucfirst($userdata->lastname).') hat eine E-Mail Adresse eingereicht:<br><br>E-Mail Adresse: <b>'.$_POST['singlemail'].'</b><br>Vorname: <b>'.ucfirst($_POST['firstname']).'</b><br>Nachname: <b>'.ucfirst($_POST['lastname']).'</b><br><br>Persönlicher Link: https://socialmeal.ch/registrieren.html?invitefrom='.User::Current()->id.'';
				$h2t = new Plugin_Mailer_html2text($mailtext);
				$subject = utf8_decode('Wettbewerb - Einzelne E-Mail Adresse');
				$mail = new Plugin_PHPMailer_PHPMailer;
				$mail->IsSMTP();
				$mail->AddAddress('info+wettbewerb@socialmeal.ch');
				$mail->WordWrap = 50;
				$mail->IsHTML(true);
				$mail->Subject = $subject;
				$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
				$mail->AltBody = $h2t->get_text();
				if(!$mail->Send()){
					echo('Mail Error:'.$mail->ErrorInfo);
				}
				$template->show_if('singlemail',true);
			}
			if(isset($_POST['send_addressbook']) && $_FILES['file']!= ''){
				$filename = Plugin_Profile_Manager::inviteUploadAddressbook($_FILES);
				$mailtext = 'Hallo, der User: '.User::Current()->name.' ('.ucfirst($userdata->firstname).' '.ucfirst($userdata->lastname).') hat ein neues Adressbuch eingereicht:<br><br>Links zum File: <a href="https://socialmeal.ch/content/uploads/addressbook/'.$filename.'">https://socialmeal.ch/content/uploads/addressbook/'.$filename.'</a><br><br>Persönlicher Link: https://socialmeal.ch/registrieren.html?invitefrom='.User::Current()->id.'';
				$h2t = new Plugin_Mailer_html2text($mailtext);
				$subject = utf8_decode('Wettbewerb - Neues Adressbuch');
				$mail = new Plugin_PHPMailer_PHPMailer;
				$mail->IsSMTP();
				$mail->AddAddress('info+wettbewerb@socialmeal.ch');
				$mail->WordWrap = 50;
				$mail->IsHTML(true);
				$mail->Subject = $subject;
				$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
				$mail->AltBody = $h2t->get_text();
				if(!$mail->Send()){
					echo('Mail Error:'.$mail->ErrorInfo);
				}
				$template->show_if('addressbook',true);
			}
			echo($template->getCode());
		}

		function getHeader(){
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
