<?php
/*
 * manager.php
 *
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
class Plugin_Profile_Manager {

	public static function getUserData($username){
		$username = DataBase::Current()->EscapeString($username);
		return DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}user WHERE name = '".$username."'");
	}

	public static function getUserDataPerId($id){
		$id = DataBase::Current()->EscapeString($id);
		return DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}user WHERE id = '".$id."'");
	}

	public static function getUserDataPerAL($al){
		$al = DataBase::Current()->EscapeString($al);
		return DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}user WHERE autologin = '".$al."'");
	}

	public static function getUserDataPerEmail($mail){
		$mail = DataBase::Current()->EscapeString($mail);
		return DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}user WHERE email = '".$mail."'");
	}

	public static function countUserPerEmail($mail){
		$mail = DataBase::Current()->EscapeString($mail);
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}user WHERE email = '".$mail."'");
	}

	public static function updatePassword($pw,$mail){
		$pw = DataBase::Current()->EscapeString($pw);
		$mail = DataBase::Current()->EscapeString($mail);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET password = '".$pw."' WHERE email = '".$mail."'");
	}

	public static function showUsername($name,$setclass = 'none'){
		$name = DataBase::Current()->EscapeString($name);
		$userdata = self::getUserData($name);
		$string = '';
		if($setclass == 'none'){
			$class = '';
		}else{
			$class = $setclass;
		}
		if(self::isFriend($userdata->id) or $name == User::Current()->name){
			$string .= '<a class="'.$class.'" href="/mein-profil.html?profile='.$userdata->name.'">'.ucfirst($userdata->firstname).' '.ucfirst($userdata->lastname).'</a>';
		}else{
			$string .= '<a class="'.$class.'" href="/mein-profil.html?profile='.$userdata->name.'">'.$userdata->name.'</a>';
		}
		if(!Mobile::isMobileDevice()){
			if(self::isVerified($name) == 2){
				$string .= ' <a infotext="'.Language::DirectTranslate('plugin_profile_isverify').'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('tick').'" /></span></a>';
			}else{
				$string .= ' <a infotext="'.Language::DirectTranslate('plugin_profile_noverify').'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('cancel').'" /></span></a>';
			}
		}
		if($userdata->role == 5){
			$string = '<s>'.$userdata->name.'</s> <a infotext="'.utf8_encode(Language::DirectTranslate('plugin_profile_profilisdel')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('comment').'" /></span></a>';
		}
		return $string;
	}

	public static function updateUser($data,$file){
		$street				= DataBase::Current()->EscapeString($data['street']);
		$strnr				= DataBase::Current()->EscapeString($data['strnr']);
		$plz				= DataBase::Current()->EscapeString($data['plz']);
		$ort				= DataBase::Current()->EscapeString($data['ort']);
		$kanton				= DataBase::Current()->EscapeString($data['kanton']);
		$bzstatus			= DataBase::Current()->EscapeString($data['bzstatus']);
		if(isset($data['aboutme'])){
			$aboutme		= DataBase::Current()->EscapeString($data['aboutme']);
		}else{
			$aboutme = '';
		}
		$gbday				= DataBase::Current()->EscapeString($data['gbday']);
		$gbmonth			= DataBase::Current()->EscapeString($data['gbmonth']);
		$gbdyear			= DataBase::Current()->EscapeString($data['gbdyear']);
		if(isset($data['fblink'])){
			if(!empty($data['fblink'])){
				$fblink			= self::ifhttp($data['fblink']);
			}
			$fblink			= DataBase::Current()->EscapeString($fblink);
		}else{
			$fblink = '';
		}
		if(isset($data['twitlink'])){
			if(!empty($data['twitlink'])){
				$twitlink			= self::ifhttp($data['twitlink']);
			}
			$twitlink		= DataBase::Current()->EscapeString($twitlink);
		}else{
			$twitlink = '';
		}
		if(isset($data['gplink'])){
			if(!empty($data['gplink'])){
				$gplink			= self::ifhttp($data['gplink']);
			}
			$gplink			= DataBase::Current()->EscapeString($gplink);
		}else{
			$gplink = '';
		}
		if(isset($data['skypelink'])){
			$skypelink		= DataBase::Current()->EscapeString($data['skypelink']);
		}else{
			$skypelink = '';
		}
		if(isset($data['flickrlink'])){
			if(!empty($data['flickrlink'])){
				$flickrlink			= self::ifhttp($data['flickrlink']);
			}
			$flickrlink		= DataBase::Current()->EscapeString($flickrlink);
		}else{
			$flickrlink = '';
		}
		if(isset($data['linkedinlink'])){
			if(!empty($data['linkedinlink'])){
				$linkedinlink			= self::ifhttp($data['linkedinlink']);
			}
			$linkedinlink		= DataBase::Current()->EscapeString($linkedinlink);
		}else{
			$linkedinlink = '';
		}
		if(isset($data['webpage'])){
			if(!empty($data['webpage'])){
				$webpage			= self::ifhttp($data['webpage']);
			}
			$webpage		= DataBase::Current()->EscapeString($webpage);
		}else{
			$webpage = '';
		}
		$fleisch			= DataBase::Current()->EscapeString(isset($data['fleisch']));
		$fisch				= DataBase::Current()->EscapeString(isset($data['fisch']));
		$seafood			= DataBase::Current()->EscapeString(isset($data['seafood']));
		$vegi				= DataBase::Current()->EscapeString(isset($data['vegi']));
		$vegan				= DataBase::Current()->EscapeString(isset($data['vegan']));
		$bio				= DataBase::Current()->EscapeString(isset($data['bio']));
		$saison				= DataBase::Current()->EscapeString(isset($data['saison']));
		$regional			= DataBase::Current()->EscapeString(isset($data['regional']));
		$laktose			= DataBase::Current()->EscapeString(isset($data['laktose']));
		$gluten				= DataBase::Current()->EscapeString(isset($data['gluten']));
		$nusse				= DataBase::Current()->EscapeString(isset($data['nusse']));
		$alkohol			= DataBase::Current()->EscapeString(isset($data['alkohol']));
		$scharf				= DataBase::Current()->EscapeString(isset($data['scharf']));
		$hefe				= DataBase::Current()->EscapeString(isset($data['hefe']));
		$koscher			= DataBase::Current()->EscapeString(isset($data['koscher']));
		$halal				= DataBase::Current()->EscapeString(isset($data['halal']));
		$openimg			= DataBase::Current()->EscapeString(isset($data['openimg']));
		$allergy			= DataBase::Current()->EscapeString($data['allergy']);
		$gender				= DataBase::Current()->EscapeString($data['gender']);
		$job				= DataBase::Current()->EscapeString($data['job']);
		$tel				= DataBase::Current()->EscapeString($data['tel']);
		$datum				= ''.$gbdyear.'-'.$gbmonth.'-'.$gbday.'';
		if(isset($file['profilimg']) != ''){
			$oldimg = self::getUserImage(User::Current()->name);
			if(file_exists(Settings::getInstance()->get("root")."content/uploads/profile/".$oldimg)){
				@unlink(Settings::getInstance()->get("root")."content/uploads/profile/".$oldimg);
			}
			$filename = DataBase::Current()->EscapeString($file['profilimg']['name']);
			$res = false;
			if(FileServer::checkUploadFile($file['profilimg'])){
				$tempname = $file['profilimg']['tmp_name'];
				$file_end 		= substr($filename, -3);
				if($file_end == 'peg'){
					$filename = mt_rand().time().'.jpeg';
				}else{
					$filename = mt_rand().time().'.'.strtolower($file_end);
				}
				$base = Settings::getInstance()->get("root")."content/uploads/profile/";
				$res = copy($tempname, $base."/".$filename);
				self::mkThumb($filename);
			}
		}else{
			$filename = self::getUserImage(User::Current()->name);
		}
		DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET plz = '".$plz."', ort = '".$ort."', kanton = '".$kanton."', strasse = '".$street."', strnr = '".$strnr."', aboutme = '".$aboutme."', fblink = '".$fblink."', twitlink = '".$twitlink."', gplink='".$gplink."', flickrlink = '".$flickrlink."', linkedinlink = '".$linkedinlink."', skypelink = '".$skypelink."', webpage = '".$webpage."', gbdatum = '".$datum."', bzstatus = '".$bzstatus."', profilimg = '".$filename."', fleisch = '".$fleisch."', fisch = '".$fisch."', seafood = '".$seafood."', vegi = '".$vegi."', vegan = '".$vegan."', bio = '".$bio."', saison = '".$saison."', regional = '".$regional."', laktose = '".$laktose."', gluten = '".$gluten."', nusse = '".$nusse."', alkohol = '".$alkohol."', scharf = '".$scharf."', hefe = '".$hefe."', koscher = '".$koscher."', halal = '".$halal."', allergy = '".$allergy."', gender = '".$gender."', job = '".$job."', tel = '".$tel."', openimg = '".$openimg."' WHERE name = '".User::Current()->name."' LIMIT 1");
		$email = DataBase::Current()->EscapeString($data['email']);
		if($data['hiddenmail'] != $email){
			DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET role = '7', email = '".$email."' WHERE name = '".User::Current()->name."' LIMIT 1");
			$newmailcode = mt_rand();
			$verifylink = Settings::getInstance()->get("host").'home.html?verifymail='.User::Current()->id.'&verifycode='.$newmailcode;
			$cachedata = array();
			$cachedata['code'] = $newmailcode;
			$cachedate['mail'] = $email;
			$mailuser = self::getUserData(User::Current()->name);
			DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_plugindata_data WHERE property = 'newmail_".User::Current()->id."' AND area = 'profile'");
			Plugin_PluginData_Data::setData('newmail_'.User::Current()->id.'',$cachedata,'profile','plugin');
			$mailtext = 'Liebe*r '.$mailuser->firstname.'<br><br>Soeben hast du auf Social Meal deine E-Mail-Adresse für dein Profil <a href="'.Settings::getInstance()->get("host").'mein-profil.html?profile='.User::Current()->name.'">'.User::Current()->name.'</a> geändert.<br><br>Klicke auf diesen Link um die E-Mail-Adresse zu bestätigen: <a href="'.$verifylink.'">'.$verifylink.'</a> <br>Danach stehen dir die Funktionen von Social Meal wieder im vollen Umfang zur Verfügung.<br><br>Vielen Dank für dein Engagement.<br>Social Meal';
			$h2t = new Plugin_Mailer_html2text($mailtext);
			$subject = utf8_decode('Bestätige deine neue E-Mail-Adresse auf Social Meal');
			$mail = new Plugin_PHPMailer_PHPMailer;
			$mail->IsSMTP();
			$mail->AddAddress($email);
			$mail->WordWrap = 50;
			$mail->IsHTML(true);
			$mail->Subject = $subject;
			$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
			$mail->AltBody = $h2t->get_text();
			if(!$mail->Send()){
				echo('Mail Error:'.$mail->ErrorInfo);
			}
			User::Current()->logout();
		}
		SessionCache::clear();
	}

	public static function updateUserFromAdmin($data,$file,$userid){
		$userid				= DataBase::Current()->EscapeString($userid);
		$username			= DataBase::Current()->EscapeString($data['username']);
		$role				= DataBase::Current()->EscapeString($data['role']);
		$email				= DataBase::Current()->EscapeString($data['email']);
		$firstname			= DataBase::Current()->EscapeString($data['firstname']);
		$lastname			= DataBase::Current()->EscapeString($data['lastname']);
		$gender				= DataBase::Current()->EscapeString($data['gender']);
		$datum				= DataBase::Current()->EscapeString($data['gbdatum']);
		$plz				= DataBase::Current()->EscapeString($data['plz']);
		$ort				= DataBase::Current()->EscapeString($data['ort']);
		$kanton				= DataBase::Current()->EscapeString($data['kanton']);
		$street				= DataBase::Current()->EscapeString($data['street']);
		$strnr				= DataBase::Current()->EscapeString($data['strnr']);
		$tel				= DataBase::Current()->EscapeString($data['tel']);
		$punkte				= DataBase::Current()->EscapeString($data['punkte']);
		$falselogin			= DataBase::Current()->EscapeString($data['falselogin']);
		$wettbewerb			= DataBase::Current()->EscapeString($data['wettbewerb']);
		if($data['role'] == 5){
			self::delAllFriendsFromUser($userid);
		}
		if(isset($data['aboutme'])){
			$aboutme		= DataBase::Current()->EscapeString($data['aboutme']);
		}else{
			$aboutme = '';
		}
		$job				= DataBase::Current()->EscapeString($data['job']);
		$bzstatus			= DataBase::Current()->EscapeString($data['bzstatus']);
		if(isset($data['fblink'])){
			if(!empty($data['fblink'])){
				$fblink			= self::ifhttp($data['fblink']);
			}
			$fblink			= DataBase::Current()->EscapeString($fblink);
		}else{
			$fblink = '';
		}
		if(isset($data['twitlink'])){
			if(!empty($data['twitlink'])){
				$twitlink			= self::ifhttp($data['twitlink']);
			}
			$twitlink		= DataBase::Current()->EscapeString($twitlink);
		}else{
			$twitlink = '';
		}
		if(isset($data['gplink'])){
			if(!empty($data['gplink'])){
				$gplink			= self::ifhttp($data['gplink']);
			}
			$gplink			= DataBase::Current()->EscapeString($gplink);
		}else{
			$gplink = '';
		}
		if(isset($data['skypelink'])){
			$skypelink		= DataBase::Current()->EscapeString($data['skypelink']);
		}else{
			$skypelink = '';
		}
		if(isset($data['flickrlink'])){
			if(!empty($data['flickrlink'])){
				$flickrlink			= self::ifhttp($data['flickrlink']);
			}
			$flickrlink		= DataBase::Current()->EscapeString($flickrlink);
		}else{
			$flickrlink = '';
		}
		if(isset($data['linkedinlink'])){
			if(!empty($data['linkedinlink'])){
				$linkedinlink			= self::ifhttp($data['linkedinlink']);
			}
			$linkedinlink		= DataBase::Current()->EscapeString($linkedinlink);
		}else{
			$linkedinlink = '';
		}
		if(isset($data['webpage'])){
			if(!empty($data['webpage'])){
				$webpage			= self::ifhttp($data['webpage']);
			}
			$webpage		= DataBase::Current()->EscapeString($webpage);
		}else{
			$webpage = '';
		}
		$openimg			= DataBase::Current()->EscapeString(isset($data['openimg']));
		if(isset($file['profilimg']) != ''){
			$oldimg = self::getUserImageFromId($userid);
			if(file_exists(Settings::getInstance()->get("root")."content/uploads/profile/".$oldimg)){
				@unlink(Settings::getInstance()->get("root")."content/uploads/profile/".$oldimg);
			}
			$filename = DataBase::Current()->EscapeString($file['profilimg']['name']);
			$res = false;
			if(FileServer::checkUploadFile($file['profilimg'])){
				$tempname = $file['profilimg']['tmp_name'];
				$file_end 		= substr($filename, -3);
				if($file_end == 'peg'){
					$filename = mt_rand().time().'.jpeg';
				}else{
					$filename = mt_rand().time().'.'.strtolower($file_end);
				}
				$base = Settings::getInstance()->get("root")."content/uploads/profile/";
				$res = copy($tempname, $base."/".$filename);
				self::mkThumb($filename);
			}
		}else{
			$filename = self::getUserImageFromId($userid);
		}
		DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET name= '".$username."', role = '".$role."', email = '".$email."', firstname = '".$firstname."', lastname = '".$lastname."', plz = '".$plz."', ort = '".$ort."', kanton = '".$kanton."', strasse = '".$street."', strnr = '".$strnr."', aboutme = '".$aboutme."', fblink = '".$fblink."', twitlink = '".$twitlink."', gplink='".$gplink."', flickrlink = '".$flickrlink."', linkedinlink = '".$linkedinlink."', skypelink = '".$skypelink."', webpage = '".$webpage."', gbdatum = '".$datum."', bzstatus = '".$bzstatus."', profilimg = '".$filename."', gender = '".$gender."', job = '".$job."', tel = '".$tel."', openimg = '".$openimg."', punkte = '".$punkte."', falselogin = '".$falselogin."', wettbewerb = '".$wettbewerb."' WHERE id = '".$userid."' LIMIT 1");
		SessionCache::clear();
	}

	public static function setProfileInfos($userid,$data){
		DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET firstname = '".$data['firstname']."', lastname = '".$data['lastname']."', plz = '".$data['plz']."', ort = '".$data['ort']."', kanton = '".$data['kanton']."', strasse = '".$data['street']."', strnr = '".$data['strnr']."', gbdatum = '".$data['gbdatum']."', gender = '".$data['gender']."', tel = '".$data['tel']."', lastrating = NOW() WHERE id = '".$userid."' LIMIT 1");
	}

	public static function profileIsDel($username){
		$username = DataBase::Current()->EscapeString($username);
		$user = self::getUserData($username);
		if($user->role == 5){
			return true;
		}else{
			return false;
		}
	}

	public static function updatePunkte($punkte){
		$punkte = DataBase::Current()->EscapeString($punkte);
		$oldpunkte = DataBase::Current()->ReadField("SELECT punkte FROM {'dbprefix'}user WHERE id = '".User::Current()->id."'");
		$neuepunkte = $oldpunkte + $punkte;
		DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET punkte = ".$neuepunkte." WHERE id = '".User::Current()->id."'");
	}

	public static function updatePunktePerId($punkte,$userid){
		$punkte = DataBase::Current()->EscapeString($punkte);
		$userid = DataBase::Current()->EscapeString($userid);
		$oldpunkte = DataBase::Current()->ReadField("SELECT punkte FROM {'dbprefix'}user WHERE id = '".$userid."'");
		$neuepunkte = $oldpunkte + $punkte;
		DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET punkte = ".$neuepunkte." WHERE id = '".$userid."'");
	}

	public static function updatePunktePerName($punkte,$name){
		$punkte = DataBase::Current()->EscapeString($punkte);
		$name = DataBase::Current()->EscapeString($name);
		$oldpunkte = DataBase::Current()->ReadField("SELECT punkte FROM {'dbprefix'}user WHERE name = '".$name."'");
		$neuepunkte = $oldpunkte + $punkte;
		DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET punkte = ".$neuepunkte." WHERE name = '".$name."'");
	}

	public static function saveRang($data){
		$apunkte = DataBase::Current()->EscapeString($data['apunkte']);
		$epunkte = DataBase::Current()->EscapeString($data['epunkte']);
		$name = DataBase::Current()->EscapeString($data['name']);
		DataBase::Current()->Execute("INSERT INTO {'dbprefix'}plugin_ranglist SET apunkte = '".$apunkte."', epunkte = '".$epunkte."', name = '".$name."'");
	}

	public static function delRang($id){
		$id = DataBase::Current()->EscapeString($id);
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_ranglist WHERE id = '".$id."'");
	}

	public static function getRangs(){
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_ranglist ORDER BY id ASC");
	}

	public static function mkThumb($imgsrc,$imgwidth = '400',$imgheight = '250',$folder_src = './content/uploads/profile',$dessrc = './content/uploads/profile/thumbs'){
		list($src_width,$src_height,$src_typ) = getimagesize($folder_src.'/'.$imgsrc);
		if($src_width >= $src_height){
			$new_image_width = $imgwidth;
			$new_image_height = $src_height * $imgwidth / $src_width;
		}
		if($src_width < $src_height){
			$new_image_height = $imgwidth;
			$new_image_width = $src_width * $imgheight / $src_height;
		}
		if($src_typ == 1){
			//GIF
			$image = imagecreatefromgif($folder_src.'/'.$imgsrc);
			$new_image = imagecreate($new_image_width, $new_image_height);
			imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_image_width, $new_image_height, $src_width, $src_height);
			imagegif($new_image, $dessrc.'/'.$imgsrc,100);
			imagedestroy($image);
			imagedestroy($new_image);
			return true;
		}elseif($src_typ == 2){
			//JPG
			$image = imagecreatefromjpeg($folder_src.'/'.$imgsrc);
			$new_image = imagecreatetruecolor($new_image_width, $new_image_height);
			imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_image_width, $new_image_height, $src_width, $src_height);
			imagegif($new_image, $dessrc.'/'.$imgsrc,100);
			imagedestroy($image);
			imagedestroy($new_image);
			return true;
		}elseif($src_typ == 3){
			//PNG
			$image = imagecreatefrompng($folder_src.'/'.$imgsrc);
			$new_image = imagecreatetruecolor($new_image_width, $new_image_height);
			imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_image_width, $new_image_height, $src_width, $src_height);
			imagegif($new_image, $dessrc.'/'.$imgsrc,100);
			imagedestroy($image);
			imagedestroy($new_image);
			return true;
		}
	}

	public static function markUserAsDel($userid){
		$userid = DataBase::Current()->EscapeString($userid);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET role = '5' WHERE id = '".$userid."' LIMIT 1");
	}

	public static function getUserImage($username){
		$username = DataBase::Current()->EscapeString($username);
		return DataBase::Current()->ReadField("SELECT profilimg FROM {'dbprefix'}user WHERE name = '".$username."'");
	}

	public static function getUserImageFromId($userid){
		$userid = DataBase::Current()->EscapeString($userid);
		return DataBase::Current()->ReadField("SELECT profilimg FROM {'dbprefix'}user WHERE id = '".$userid."'");
	}

	public static function deleteUserImage($imagefile){
		$imagefile = DataBase::Current()->EscapeString($imagefile);
		if(file_exists('./content/uploads/profile/'.$imagefile)){
			unlink('./content/uploads/profile/'.$imagefile);
		}
		if(file_exists('./content/uploads/profile/thumbs/'.$imagefile)){
			unlink('./content/uploads/profile/thumbs/'.$imagefile);
		}
		DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET profilimg = '' WHERE name = '".User::Current()->name."' LIMIT 1");
	}

	public static function formatAge($date){
		list($y,$m,$d) = explode('-',$date);
		if($y == '0000'){
			list($y,$m,$d) = explode('-',date('Y-m-d'));
		}
		$alter = date('Y') - $y;
		$monat = date('m');
		if($monat < $m or ($monat == $m and $d > date('d'))){
			$alter--;
		}
		return $alter;
	}

	public static function updateVerify($status){
		$status = DataBase::Current()->EscapeString($status);
		return DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET verify = '".$status."' WHERE id = '".User::Current()->id."'");
	}

	public static function delVerifyCache(){
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_plugindata_data WHERE property = 'user_".User::Current()->id."' AND area = 'profile_verify'");
	}

	public static function countFriendRequests(){
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_friends WHERE userid2 = '".User::Current()->id."' AND statusid='0'");
	}

	public static function countFriends(){
		$count1 =  DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_friends WHERE userid1 = '".User::Current()->id."' AND statusid='1'");
		$count2 =  DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_friends WHERE userid2 = '".User::Current()->id."' AND statusid='1'");
		$result = $count1 + $count2;
		return $result;
	}
	
	public static function countFriendsFromUserId($userid){
		$count1 =  DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_friends WHERE userid1 = '".$userid."' AND statusid='1'");
		$count2 =  DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_friends WHERE userid2 = '".$userid."' AND statusid='1'");
		$result = $count1 + $count2;
		return $result;
	}

	public static function addFriend($friendid){
		$friendid = DataBase::Current()->EscapeString($friendid);
		DataBase::Current()->Execute("INSERT INTO {'dbprefix'}plugin_friends SET userid1 = '".User::Current()->id."', userid2 = '".$friendid."'");
		if(self::getUserSetting('friend',$friendid) == 1){
			$newfrienddata = self::getUserDataPerId($friendid);
			$selfuserdata = self::getUserDataPerId(User::Current()->id);
			$mailtext = 'Liebe*r '.ucfirst($newfrienddata->firstname).'<br><br>Gerne möchten wir dich darüber informieren, dass du eine neue Freundschaftsanfrage von dem User <a href="'.Settings::getInstance()->get("host").'mein-profil.html?profile='.$selfuserdata->name.'">'.$selfuserdata->name.'</a> erhalten hast.<br>Du kannst die Freundschaftsanfrage <a href="'.Settings::getInstance()->get("host").'mein-profil.html">hier</a> akzeptieren oder ablehnen.<br><br>Beachte: Wenn die Freundschaft von beiden Seiten bestätigt wurde, wird euch in Zukunft gegenseitig der echte Name anstelle des Benutzername angezeigt. Allgemein empfiehlt es sich nur Freund*innen hinzuzufügen, die du bereits gut kennst.<br><br>Wir wünschen dir weiterhin gute Interaktionen in unserer Community!<br>Social Meal<br><br>P.S.: Diesen E-Mail-Typ kannst du in den <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abbestellen, falls er dir überflüssig erscheint.';
			$h2t = new Plugin_Mailer_html2text($mailtext);
			$subject = utf8_decode('Du hast eine neue Freundschaftsanfrage auf Social Meal erhalten');
			$mail = new Plugin_PHPMailer_PHPMailer;
			$mail->IsSMTP();
			$mail->AddAddress(DataBase::Current()->ReadField("SELECT email FROM {'dbprefix'}user WHERE id = '".$friendid."'"));
			$mail->WordWrap = 50;
			$mail->IsHTML(true);
			$mail->Subject = $subject;
			$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
			$mail->AltBody = $h2t->get_text();
			if(!$mail->Send()){
				echo('Mail Error:'.$mail->ErrorInfo);
			}
		}
	}

	public static function getFriendInfos($id){
		$id = DataBase::Current()->EscapeString($id);
		return DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}plugin_friends WHERE id = '".$id."'");
	}

	public static function getFriends(){
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_friends WHERE userid1 = '".User::Current()->id."' OR userid2 = '".User::Current()->id."'");
	}

	public static function getFriendsData($userid){
		$userid = DataBase::Current()->EscapeString($userid);
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_friends WHERE userid1 = '".$userid."' OR userid2 = '".$userid."'");
	}

	public static function getFriendRequest(){
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_friends WHERE userid1 = '".User::Current()->id."' AND statusid = '0'");
	}

	public static function getOwnFriendRequest(){
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_friends WHERE userid2 = '".User::Current()->id."' AND statusid = '0'");
	}

	public static function acceptFriendRequest($id){
		$id = DataBase::Current()->EscapeString($id);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_friends SET statusid = '1' WHERE id = '".$id."' LIMIT 1");
	}

	public static function userExist($username){
		$username = DataBase::Current()->EscapeString($username);
		$count = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}user WHERE name = '".$username."'");
		if($count == 1){
			return true;
		}else{
			return false;
		}
	}

	public static function userExistFromId($userid){
		$userid = DataBase::Current()->EscapeString($userid);
		$count = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}user WHERE id = '".$userid."'");
		if($count == 1){
			return true;
		}else{
			return false;
		}
	}

	public static function delOpenFriend($id){
		$id = DataBase::Current()->EscapeString($id);
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_friends WHERE id = '".$id."' AND userid1 = '".User::Current()->id."' LIMIT 1");
	}

	public static function delFriend($id){
		$id = DataBase::Current()->EscapeString($id);
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_friends WHERE id = '".$id."' LIMIT 1");
	}

	public static function countOpenFriends(){
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_friends WHERE userid1 = '".User::Current()->id."' AND statusid = '0'");
	}

	public static function delAllFriendsFromUser($userid){
		$userid = DataBase::Current()->EscapeString($userid);
		DataBase::Current("DELETE FROM {'dbprefix'}plugin_friends WHERE userid1 = '".$userid."'");
		DataBase::Current("DELETE FROM {'dbprefix'}plugin_friends WHERE userid2 = '".$userid."'");
	}

	public static function countAllFriends(){
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_friends WHERE userid1 = '".User::Current()->id."' or userid2 = '".User::Current()->id."' AND statusid='0'");
	}

	public static function mailExist($mail){
		$mail = DataBase::Current()->EscapeString($mail);
		$count = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}user WHERE email = '".$mail."'");
		if($count == 1){
			return true;
		}else{
			return false;
		}
	}

	public static function averageStars($userid){
		$userid = DataBase::Current()->EscapeString($userid);
		$countRatings = Plugin_Events_Manager::countRatings($userid);
		if($countRatings == 0){
			return 0;
		}else{
			$count = 0;
			foreach(Plugin_Events_Manager::getRatings($userid) as $star){
				$count = $count + $star->stars;
			}
			$average = $count / $countRatings;
			$round = round($average,0);
			return $round;
		}
	}

	public static function sendNLSMMail($username){
		if(is_array($username)){
			$name = $username['name'];
			$firstname = ucfirst($username['firstname']);
			$lastname = ucfirst($username['lastname']);
			$email = $username['email'];
		}else{
			$user = self::getUserData($username);
			$name = $user->name;
			$firstname = ucfirst($user->firstname);
			$lastname = ucfirst($user->lastname);
			$email = $user->email;
		}
		$mailtext = 'Der Benutzer: '.$name.' (Vorname: '.ucfirst($firstname).' Nachname: '.ucfirst($lastname).') will den Newsletter von Social Meal abonnieren!<br><br>Seine Mail Adresse: <strong>'.$email.'</strong>';
		$h2t = new Plugin_Mailer_html2text($mailtext);
		$subject = utf8_decode('Neue Newsletter Anmeldung');
		$mail = new Plugin_PHPMailer_PHPMailer;
		$mail->IsSMTP();
		$mail->AddAddress('info+newsletter@socialmeal.ch');
		$mail->WordWrap = 50;
		$mail->IsHTML(true);
		$mail->Subject = $subject;
		$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
		$mail->AltBody = $h2t->get_text();
		if(!$mail->Send()){
			echo('Mail Error:'.$mail->ErrorInfo);
		}
	}

	public static function sendNLSMMailDeny($userid){
		$user = self::getUserDataPerId($userid);
		$mailtext = 'Der Benutzer: '.$user->name.' (Vorname: '.ucfirst($user->firstname).' Nachname: '.ucfirst($user->lastname).') will den Newsletter von Social Meal nicht mehr abonniert haben!<br><br>Seine Mail Adresse: <strong>'.$user->email.'</strong>';
		$h2t = new Plugin_Mailer_html2text($mailtext);
		$subject = utf8_decode('Newsletter Abmeldung');
		$mail = new Plugin_PHPMailer_PHPMailer;
		$mail->IsSMTP();
		$mail->AddAddress('info+newsletter@socialmeal.ch');
		$mail->WordWrap = 50;
		$mail->IsHTML(true);
		$mail->Subject = $subject;
		$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
		$mail->AltBody = $h2t->get_text();
		if(!$mail->Send()){
			echo('Mail Error:'.$mail->ErrorInfo);
		}
	}

	public static function sendNLOBMail($username){
		if(is_array($username)){
			$name = $username['name'];
			$firstname = ucfirst($username['firstname']);
			$lastname = ucfirst($username['lastname']);
			$email = $username['email'];
		}else{
			$user = self::getUserData($username);
			$name = $user->name;
			$firstname = ucfirst($user->firstname);
			$lastname = ucfirst($user->lastname);
			$email = $user->email;
		}
		$mailtext = 'Der Benutzer: '.$name.' (Vorname: '.ucfirst($firstname).' Nachname: '.ucfirst($lastname).') will den Newsletter von Occupy Basel abonnieren!<br><br>Seine Mail Adresse: <strong>'.$email.'</strong>';
		$h2t = new Plugin_Mailer_html2text($mailtext);
		$subject = utf8_decode('Neue Newsletter Anmeldung');
		$mail = new Plugin_PHPMailer_PHPMailer;
		$mail->IsSMTP();
		$mail->AddAddress('occupy+newsletter@occupybasel.ch');
		$mail->WordWrap = 50;
		$mail->IsHTML(true);
		$mail->Subject = $subject;
		$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
		$mail->AltBody = $h2t->get_text();
		if(!$mail->Send()){
			echo('Mail Error:'.$mail->ErrorInfo);
		}
	}

	public static function sendNLOBMailDeny($userid){
		$user = self::getUserDataPerId($userid);
		$mailtext = 'Der Benutzer: '.$user->name.' (Vorname: '.ucfirst($user->firstname).' Nachname: '.ucfirst($user->lastname).') will den Newsletter von Occupy Basel nicht mehr abonniert haben!<br><br>Seine Mail Adresse: <strong>'.$user->email.'</strong>';
		$h2t = new Plugin_Mailer_html2text($mailtext);
		$subject = utf8_decode('Newsletter Abmeldung');
		$mail = new Plugin_PHPMailer_PHPMailer;
		$mail->IsSMTP();
		$mail->AddAddress('occupy+newsletter@occupybasel.ch');
		$mail->WordWrap = 50;
		$mail->IsHTML(true);
		$mail->Subject = $subject;
		$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
		$mail->AltBody = $h2t->get_text();
		if(!$mail->Send()){
			echo('Mail Error:'.$mail->ErrorInfo);
		}
	}

	public static function setAllSettings($userid){
		$userid = DataBase::Current()->EscapeString($userid);
		DataBase::Current()->Execute("INSERT INTO {'dbprefix'}plugin_usersettings VALUE ('".$userid."','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','0','0','10')");
	}

	public static function setNLSettings($userid,$userdata){
		$userid = DataBase::Current()->EscapeString($userid);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_usersettings SET nlsm = '".$userdata['nlsm']."', nlob = '".$userdata['nlob']."' WHERE userid = '".$userid."' LIMIT 1");
	}

	public static function getAllUserSettings($userid){
		$userid = DataBase::Current()->EscapeString($userid);
		return DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}plugin_usersettings WHERE userid = '".User::Current()->id."'");
	}

	public static function updateSettings($userid,$data){
		$userid = DataBase::Current()->EscapeString($userid);
		$userdata = self::getUserDataPerId($userid);
		$usersettings = self::getAllUserSettings($userid);
		if(isset($data['tb'])){
			$tb = 1;
		}else{
			$tb = 0;
		}
		if(isset($data['friendevent'])){
			$friendevent = 1;
		}else{
			$friendevent = 0;
		}
		if(isset($data['newquestion'])){
			$newquestion = 1;
		}else{
			$newquestion = 0;
		}
		if(isset($data['newanswer'])){
			$newanswer = 1;
		}else{
			$newanswer = 0;
		}
		if(isset($data['newsubscribe'])){
			$newsubscribe = 1;
		}else{
			$newsubscribe = 0;
		}
		if(isset($data['unsubscribe'])){
			$unsubscribe = 1;
		}else{
			$unsubscribe = 0;
		}
		if(isset($data['socialmedia'])){
			$socialmedia = 1;
		}else{
			$socialmedia = 0;
		}
		if(isset($data['profile'])){
			$profile = 1;
		}else{
			$profile = 0;
		}
		if(isset($data['rate'])){
			$rate = 1;
		}else{
			$rate = 0;
		}
		if(isset($data['ratereply'])){
			$ratereply = 1;
		}else{
			$ratereply = 0;
		}
		if(isset($data['friend'])){
			$friend = 1;
		}else{
			$friend = 0;
		}
		if(isset($data['newpm'])){
			$newpm = 1;
		}else{
			$newpm = 0;
		}
		if(isset($data['newevent'])){
			$newevent = 1;
		}else{
			$newevent = 0;
		}
		if(isset($data['afterevent'])){
			$afterevent = 1;
		}else{
			$afterevent = 0;
		}
		if(isset($data['orgadmin'])){
			$orgadmin = 1;
		}else{
			$orgadmin = 0;
		}
		if(isset($data['blogcomment'])){
			$blogcomment = 1;
		}else{
			$blogcomment = 0;
		}
		if(isset($data['nlsm'])){
			$nlsm = 1;
			if($usersettings->nlsm == 0){
				self::sendNLSMMail($userdata->name);
			}
		}else{
			$nlsm = 0;
			if($usersettings->nlsm == 1){
				self::sendNLSMMailDeny($userid);
			}
		}
		if(isset($data['nlob'])){
			$nlob = 1;
			if($usersettings->nlob == 0){
				self::sendNLOBMail($userdata->name);
			}
		}else{
			$nlob = 0;
			if($usersettings->nlob == 1){
				self::sendNLOBMailDeny($userid);
			}
		}
		if(isset($data['anzahlpm'])){
			if(is_int(intval($data['anzahlpm']))){
				if($data['anzahlpm'] > 0){
					$anzahlpm = $data['anzahlpm'];
				}else{
					$anzahlpm = 10;
				}
			}else{
				$anzahlpm = 10;
			}
		}else{
			$anzahlpm = 10;
		}
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_usersettings SET tb = '".$tb."', friendevent = '".$friendevent."', newquestion = '".$newquestion."', newanswer = '".$newanswer."', newsubscribe = '".$newsubscribe."', unsubscribe = '".$unsubscribe."', socialmedia = '".$socialmedia."', rate = '".$rate."', ratereply = '".$ratereply."', friend = '".$friend."', profile = '".$profile."', newpm = '".$newpm."', newevent = '".$newevent."', afterevent = '".$afterevent."', orgadmin = '".$orgadmin."', blogcomment = '".$blogcomment."', nlsm = '".$nlsm."', nlob = '".$nlob."', anzahlpm = '".$anzahlpm."' WHERE userid = '".$userid."' LIMIT 1");
	}

	public static function getUserSetting($setting,$userid){
		$setting = DataBase::Current()->EscapeString($setting);
		$userid = DataBase::Current()->EscapeString($userid);
		return DataBase::Current()->ReadField("SELECT `".$setting."` FROM {'dbprefix'}plugin_usersettings WHERE userid = '".$userid."'");
	}

	public static function isFriend($userid){
		$userid = DataBase::Current()->EscapeString($userid);
		$var = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_friends WHERE userid1 = '".$userid."' AND userid2 = '".User::Current()->id."' AND statusid = '1'");
		$var2 = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_friends WHERE userid1 = '".User::Current()->id."' AND userid2 = '".$userid."' AND statusid = '1'");
		$var3 = $var + $var2;
		if($var3 == 1){
			return true;
		}else{
			return false;
		}
	}
	private static function EscapeOrg($data){
		$data['orgname']			= DataBase::Current()->EscapeString($data['orgname']);
		$data['desc']				= DataBase::Current()->EscapeString($data['desc']);
		if(!empty($data['orgwebseite'])){
			$data['orgwebseite']		= self::ifhttp($data['orgwebseite']);
		}
		$data['orgwebseite']		= DataBase::Current()->EscapeString($data['orgwebseite']);
		$data['orgmail']			= DataBase::Current()->EscapeString($data['orgmail']);
		if(!empty($data['orgfb'])){
			$data['orgfb']				= self::ifhttp($data['orgfb']);
		}
		$data['orgfb']				= DataBase::Current()->EscapeString($data['orgfb']);
		if(!empty($data['orgtwitter'])){
			$data['orgtwitter']			= self::ifhttp($data['orgtwitter']);
		}
		$data['orgtwitter']			= DataBase::Current()->EscapeString($data['orgtwitter']);
		if(!empty($data['orggplus'])){
			$data['orggplus']			= self::ifhttp($data['orggplus']);
		}
		$data['orggplus']			= DataBase::Current()->EscapeString($data['orggplus']);
		$data['orgtel']				= DataBase::Current()->EscapeString($data['orgtel']);
		$data['ort']				= DataBase::Current()->EscapeString($data['ort']);
		$data['plz']				= DataBase::Current()->EscapeString($data['plz']);
		$data['strnr']				= DataBase::Current()->EscapeString($data['strnr']);
		$data['street']				= DataBase::Current()->EscapeString($data['street']);
		$data['oldmail']			= DataBase::Current()->EscapeString($data['oldmail']);
		$data['verifystatus']		= DataBase::Current()->EscapeString($data['verifystatus']);
		if(isset($data['asmail'])){
			$data['asmail']			= DataBase::Current()->EscapeString($data['asmail']);
		}else{
			$data['asmail']			= 0;
		}
		return $data;
	}

	public static function createOrgProfile($data,$file){
		$data = self::EscapeOrg($data);
		if(isset($file['profilimg']) != ''){
			$filename = DataBase::Current()->EscapeString($file['profilimg']['name']);
			$res = false;
			if(FileServer::checkUploadFile($file['profilimg'])){
				$tempname = $file['profilimg']['tmp_name'];
				$file_end 		= substr($filename, -3);
				if($file_end == 'peg'){
					$filename = mt_rand().time().'.jpeg';
				}else{
					$filename = mt_rand().time().'.'.$file_end;
				}
				$base = Settings::getInstance()->get("root")."content/uploads/orgprofile/";
				$res = copy($tempname, $base."/".$filename);
			}
		}else{
			$filename = '';
		}
		DataBase::Current()->Execute("INSERT INTO {'dbprefix'}plugin_orgprofile SET orgname = '".$data['orgname']."', description = '".$data['desc']."', orgweb = '".$data['orgwebseite']."', orgmail = '".$data['orgmail']."', orgfb = '".$data['orgfb']."', orgtwitter = '".$data['orgtwitter']."', orggplus = '".$data['orggplus']."', orgtel = '".$data['orgtel']."', profilimg = '".$filename."', strasse = '".$data['street']."', strnr = '".$data['strnr']."', plz = '".$data['plz']."', ort = '".$data['ort']."'");
		$orgid = DataBase::Current()->InsertID();
		self::insertOrgAdmin(User::Current()->id,$orgid,1);
		return $orgid;
	}

	public static function insertOrgAdmin($userid,$orgid,$statusid){
		$userid = DataBase::Current()->EscapeString($userid);
		$orgid	= DataBase::Current()->EscapeString($orgid);
		$statusid = DataBase::Current()->EscapeString($statusid);
		DataBase::Current()->Execute("INSERT INTO {'dbprefix'}plugin_orgprofileadmin SET userid = '".$userid."', orgid = '".$orgid."', statusid = '".$statusid."'");
	}

	public static function getOrgImage($orgid){
		$orgid = DataBase::Current()->EscapeString($orgid);
		return DataBase::Current()->ReadField("SELECT profilimg FROM {'dbprefix'}plugin_orgprofile WHERE id = '".$orgid."'");
	}

	public static function loadOrganisation($orgid){
		$orgid = DataBase::Current()->EscapeString($orgid);
		return DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}plugin_orgprofile WHERE id = '".$orgid."'");
	}

	public static function canEditOrgProfile($orgid){
		$orgid = DataBase::Current()->EscapeString($orgid);
		$count = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_orgprofileadmin WHERE userid = '".User::Current()->id."' AND orgid = '".$orgid."' AND statusid = '1'");
		if($count == 1){
			return true;
		}else{
			return false;
		}
	}

	public static function isOrgAdmin($orgid,$adminid){
		$orgid = DataBase::Current()->EscapeString($orgid);
		$adminid = DataBase::Current()->EscapeString($adminid);
		$adminuserid = DataBase::Current()->ReadField("SELECT userid FROM {'dbprefix'}plugin_orgprofileadmin WHERE id = '".$adminid."'");
		$count = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_orgprofileadmin WHERE userid = '".$adminuserid."' AND orgid = '".$orgid."' AND statusid = '1'");
		if($count == 1){
			return true;
		}else{
			return false;
		}
	}

	public static function countOrgWithUserData($userid,$orgid){
		$userid = DataBase::Current()->EscapeString($userid);
		$orgid = DataBase::Current()->EscapeString($orgid);
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_orgprofileadmin WHERE userid = '".$userid."' AND orgid = '".$orgid."'");
	}

	public static function updateOrgProfile($orgid,$data,$file){
		$orgid = DataBase::Current()->EscapeString($orgid);
		$data = self::EscapeOrg($data);
		if(isset($file['profilimg']) != ''){
			$oldimg = self::getOrgImage($orgid);
			if(file_exists(Settings::getInstance()->get("root")."content/uploads/orgprofile/".$oldimg)){
				@unlink(Settings::getInstance()->get("root")."content/uploads/orgprofile/".$oldimg);
			}
			$filename = DataBase::Current()->EscapeString($file['profilimg']['name']);
			$res = false;
			if(FileServer::checkUploadFile($file['profilimg'])){
				$tempname = $file['profilimg']['tmp_name'];
				$file_end 		= substr($filename, -3);
				if($file_end == 'peg'){
					$filename = mt_rand().time().'.jpeg';
				}else{
					$filename = mt_rand().time().'.'.$file_end;
				}
				$base = Settings::getInstance()->get("root")."content/uploads/orgprofile/";
				$res = copy($tempname, $base."/".$filename);
			}
		}else{
			$filename = self::getOrgImage($orgid);
		}
		if($data['verifystatus'] == 1){
			if($data['orgmail'] != $data['oldmail']){
				$data['verifystatus'] = 0;
				$data['asmail'] = 0;
			}
		}else{
			$data['verifystatus'] = 0;
		}
		if($data['orgtel'] == ''){
			$data['asmailtel'] = 0;
		}
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_orgprofile SET orgname = '".$data['orgname']."', description = '".$data['desc']."', orgweb = '".$data['orgwebseite']."', orgmail = '".$data['orgmail']."', orgfb = '".$data['orgfb']."', orgtwitter = '".$data['orgtwitter']."', orggplus = '".$data['orggplus']."', orgtel = '".$data['orgtel']."', profilimg = '".$filename."', strasse = '".$data['street']."', strnr = '".$data['strnr']."', plz = '".$data['plz']."', ort = '".$data['ort']."', verify = '".$data['verifystatus']."', asmail = '".$data['asmail']."', asmailtel = '".$data['asmailtel']."' WHERE id = '".$orgid."' LIMIT 1");
	}

	public static function delOrgImage($orgid){
		$orgid = DataBase::Current()->EscapeString($orgid);
		$filename = self::getOrgImage($orgid);
		if(file_exists(Settings::getInstance()->get("root")."content/uploads/orgprofile/".$filename)){
			@unlink(Settings::getInstance()->get("root")."content/uploads/orgprofile/".$filename);
		}
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_orgprofile SET profilimg = '' WHERE id = '".$orgid."' LIMIT 1");
	}

	public static function getOrgAdmins($orgid){
		$orgid = DataBase::Current()->EscapeString($orgid);
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_orgprofileadmin WHERE statusid = '1' AND orgid = '".$orgid."'");
	}

	public static function countOrgAdmins($orgid){
		$orgid = DataBase::Current()->EscapeString($orgid);
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_orgprofileadmin WHERE orgid = '".$orgid."' AND statusid = '1'");
	}

	public static function delOrgAdmin($id){
		$id = DataBase::Current()->EscapeString($id);
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_orgprofileadmin WHERE id = '".$id."' LIMIT 1");
	}

	public static function delOrgAdminRequest($orgid){
		$orgid = DataBase::Current()->EscapeString($orgid);
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_orgprofileadmin WHERE orgid = '".$orgid."' AND userid = '".User::Current()->id."' LIMIT 1");
	}

	public static function acceptOrgAdminRequest($orgid){
		$orgid = DataBase::Current()->EscapeString($orgid);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_orgprofileadmin SET statusid = '1' WHERE orgid = '".$orgid."' AND userid = '".User::Current()->id."' LIMIT 1");
	}

	public static function delOrgProfile($orgid){
		$orgid = DataBase::Current()->EscapeString($orgid);
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_orgprofile WHERE id = '".$orgid."' LIMIT 1");
		self::delOrgImage($orgid);
		self::delOrgAdmins($orgid);
		foreach(Plugin_Events_Manager::getOrgEvents($orgid) as $event){
			echo($event->id);
			Plugin_Events_Manager::deleteEvent($event->id);
			Plugin_Events_Manager::deleteQuestions($event->id);
			Plugin_Events_Manager::removeSubscribersPerEvent($event->id);
			Plugin_Events_Manager::deleteRatingsPerEvent($event->id);
			Plugin_Events_Manager::delEventRead($event->id);
			foreach(Plugin_Events_Manager::getImagesPerEvent($event->id) as $image){
				Plugin_Events_Manager::deleteImage($image->id);
			}
		}
	}

	public static function hasAdminRequest(){
		$count = self::countAdminRequest();
		if($count == 0){
			return false;
		}elseif($count > 0){
			return true;
		}
	}

	public static function delOrgAdmins($orgid){
		$orgid = DataBase::Current()->EscapeString($orgid);
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_orgprofileadmin WHERE orgid = '".$orgid."'");
	}

	public static function countAdminRequest(){
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_orgprofileadmin WHERE userid = '".User::Current()->id."' AND statusid = '0'");
	}

	public static function countOrgAdminsFromUser(){
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_orgprofileadmin WHERE userid = '".User::Current()->id."' AND statusid = '1'");
	}

	public static function requestOrgInfo(){
		$orgid = DataBase::Current()->ReadField("SELECT orgid FROM {'dbprefix'}plugin_orgprofileadmin WHERE userid = '".User::Current()->id."' AND statusid = '0'");
		return self::loadOrganisation($orgid);
	}

	public static function getOrgsByUser(){
		return DataBase::Current()->ReadRows("SELECT orgid FROM {'dbprefix'}plugin_orgprofileadmin WHERE userid = '".User::Current()->id."' AND statusid = '1'");
	}

	public static function getAllOrgs(){
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_orgprofile");
	}

	public static function getAllUsers(){
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}user");
	}

	public static function getAllLanguages(){
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_alllanguages ORDER BY oben DESC, sprache ASC");
	}

	public static function countLanguagesByUser($userid){
		$userid = DataBase::Current()->EscapeString($userid);
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_userlang WHERE userid = '".$userid."'");
	}

	public static function saveLang($langid){
		$langid = DataBase::Current()->EscapeString($langid);
		$count = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_userlang WHERE userid = '".User::Current()->id."' AND langid = '".$langid."'");
		if($count == 0){
			DataBase::Current()->Execute("INSERT INTO {'dbprefix'}plugin_userlang SET userid = '".User::Current()->id."', langid = '".$langid."'");
		}
	}

	public static function getLangByUser($userid){
		$userid = DataBase::Current()->EscapeString($userid);
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_userlang WHERE userid = '".$userid."'");
	}

	public static function delLang($langid){
		$langid = DataBase::Current()->EscapeString($langid);
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_userlang WHERE userid = '".User::Current()->id."' AND langid = '".$langid."' LIMIT 1");
	}

	public static function getLngName($id){
		$id = DataBase::Current()->EscapeString($id);
		return DataBase::Current()->ReadField("SELECT sprache FROM {'dbprefix'}plugin_alllanguages WHERE id = '".$id."'");
	}

	public static function userHasLng($id){
		$id = DataBase::Current()->EscapeString($id);
		$count = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_userlang WHERE userid = '".User::Current()->id."' AND langid = '".$id."'");
		if($count == 0){
			return true;
		}else{
			return false;
		}
	}

	public static function startVerify($file){
		if(isset($file['image']) != ''){
			$oldimg = self::getUserImage(User::Current()->name);
			$filename = $file['image']['name'];
			$res = false;
			if(FileServer::checkUploadFile($file['image'])){
				$tempname = $file['image']['tmp_name'];
				$file_end 		= substr($filename, -3);
				if($file_end == 'peg'){
					$filename = mt_rand().time().'.jpeg';
				}else{
					$filename = mt_rand().time().'.'.$file_end;
				}
				$base = Settings::getInstance()->get("root")."content/uploads/verify/";
				$res = copy($tempname, $base."/".$filename);
			}
		}else{
			$filename = 'Kein Bild hochgeladen';
		}
		$returndata = array();
		$returndata['filename'] = $filename;
		$returndata['verifycode'] = mt_rand();
		Plugin_PluginData_Data::setData('user_'.User::Current()->id.'',$returndata,'profile_verify','plugin');
		return $returndata;
	}

	public static function isVerified($name){
		$name = DataBase::Current()->EscapeString($name);
		return DataBase::Current()->ReadField("SELECT verify FROM {'dbprefix'}user WHERE name = '".$name."'");
	}

	public static function updateCounter($id){
		$id = DataBase::Current()->EscapeString($id);
		$counter = DataBase::Current()->ReadField("SELECT counter FROM {'dbprefix'}user WHERE id = '".$id."'");
		$counter++;
		DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET counter = '".$counter."' WHERE id = '".$id."' LIMIT 1");
	}

	public static function updateWatch($profileid){
		$profileid = DataBase::Current()->EscapeString($profileid);
		$count = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_profilewatch WHERE profileid = '".$profileid."' AND watchid = '".User::Current()->id."'");
		if($count == 0){
			DataBase::Current()->Execute("INSERT INTO {'dbprefix'}plugin_profilewatch SET datum = NOW(), profileid = '".$profileid."', watchid = '".User::Current()->id."'");
		}elseif($count == 1){
			DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_profilewatch SET datum = NOW() WHERE profileid = '".$profileid."' AND watchid = '".User::Current()->id."'");
		}
	}

	public static function getProfileWatchers($userid){
		$userid = DataBase::Current()->EscapeString($userid);
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_profilewatch WHERE profileid = '".$userid."' ORDER BY datum DESC");
	}

	public static function datediff_ymd ($date1, $date2 = 0) {
		if ($date2 == 0) $date2 = time();
		if ($date1 > $date2) $date1 ^= $date2 ^= $date1 ^= $date2;
			$day1 = date('d', $date1);
			$month1 = date('m', $date1);
			$year1 = date('Y', $date1);
			$day2 = date('d', $date2);
			$month2 = date('m', $date2);
			$year2 = date('Y', $date2);
			$days = $day2 - $day1; if ($days < 0) {
			$days += date('t', $date1); $carry = 1;
		}else $carry = 0;
			$months = $month2 - $month1 - $carry; if ($months < 0) {
			$months += 12; $carry = 1;
		}else $carry = 0;
			$years = $year2 - $year1 - $carry;
		$returnarray = array();
		$returnarray['years'] = $years;
		$returnarray['months'] = $months;
		$returnarray['days'] = $days;
		return $returnarray;
	}

	public static function createDateString($array){
		$string = '';
		if($array['years'] != 0){
			if($array['years'] == 1){
				$string .= ' '.$array['years'].' '.Language::DirectTranslate('plugin_profile_year');
			}else{
				$string .= ' '.$array['years'].' '.Language::DirectTranslate('plugin_profile_years');
			}
		}
		if($array['months'] != 0){
			if($array['months'] == 1){
				$string .= ' '.$array['months'].' '.Language::DirectTranslate('plugin_profile_month');
			}else{
				$string .= ' '.$array['months'].' '.Language::DirectTranslate('plugin_profile_months');
			}
		}
		if($array['days'] != 0){
			if($array['days'] == 1){
				$string .= ' '.$array['days'].' '.Language::DirectTranslate('plugin_profile_day');
			}else{
				$string .= ' '.$array['days'].' '.Language::DirectTranslate('plugin_profile_days');
			}
		}
		return $string;
	}

	public static function updateAGBVersion(){
		$userid = DataBase::Current()->EscapeString(User::Current()->id);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}user set agb = '".Settings::getInstance()->get("agb")."' WHERE id = '".$userid."' LIMIT 1");
	}

	public static function updateDatenschutzVersion(){
		$userid = DataBase::Current()->EscapeString(User::Current()->id);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}user set datenschutz = '".Settings::getInstance()->get("datenschutz")."' WHERE id = '".$userid."' LIMIT 1");
	}

	public static function setAGBVersions($userid){
		$userid = DataBase::Current()->EscapeString($userid);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}user set agb = '".Settings::getInstance()->get("agb")."', datenschutz = '".Settings::getInstance()->get("datenschutz")."' WHERE id = '".$userid."' LIMIT 1");
	}

	public static function completeProfil(){
		if(User::Current()->role->ID == 4 && Page::Current()->alias != 'mein-profil-bearbeiten' && Page::Current()->alias != 'anderungen-akzeptieren'){
			$user = DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}user WHERE id = '".User::Current()->id."'");
			if(empty($user->firstname) or empty($user->lastname) or empty($user->plz) or empty($user->ort) or empty($user->tel) or empty($user->kanton) or empty($user->strasse) or empty($user->strnr)){
				return false;
			}else{
				return true;
			}
		}else{
			return true;
		}
	}

	public static function completeProfilEdit(){
		if(User::Current()->role->ID == 4){
			$user = self::getUserDataPerId(User::Current()->id);
			if(empty($user->firstname) or empty($user->lastname) or empty($user->plz) or empty($user->ort) or empty($user->tel) or empty($user->kanton) or empty($user->strasse) or empty($user->strnr)){
				return false;
			}else{
				return true;
			}
		}else{
			return true;
		}
	}

	public static function newAGBS(){
		if(User::Current()->role->ID == 4 && Page::Current()->alias != 'anderungen-akzeptieren' && Page::Current()->alias != 'agb' && Page::Current()->alias != 'FAQ/sichtbarkeit-und-verwendung-der-personlichen-daten'){
			if(Page::Current()->alias == 'anderungen-akzeptieren' or Page::Current()->alias == 'agb'){
				return false;
			}else{
				$user = self::getUserDataPerId(User::Current()->id);
				if($user->agb < Settings::getInstance()->get("agb") or $user->datenschutz < Settings::getInstance()->get("datenschutz")){
					return true;
				}else{
					return false;
				}
			}
		}
	}

	public static function isProfileBlocked(){
		if(User::Current()->role->ID == 6 && Page::Current()->alias != 'profil-gesperrt'){
			if(Page::Current()->alias == 'profil-gesperrt'){
				return false;
			}else{
				if(User::Current()->role->ID == 6){
					return true;
				}else{
					return false;
				}
			}
		}
	}

	public static function isProfileDelete(){
		if(User::Current()->role->ID == 5 && Page::Current()->alias != 'profil-geloscht'){
			if(Page::Current()->alias == 'profil-geloscht'){
				return false;
			}else{
				if(User::Current()->role->ID == 5){
					return true;
				}else{
					return false;
				}
			}
		}
	}

	public static function isProfileChangeMail(){
		if($_SERVER['REQUEST_URI'] == '/e-mail-geandert.html' or $_SERVER['REQUEST_URI'] == '/mein-profil-bearbeiten.html'){
			return false;
			exit();
		}
		if(User::Current()->role->ID == 7){
			return true;
		}else{
			return false;
		}
	}

	public static function hasUnreadBlogs(){
		if(User::Current()->role->ID == 1){
			return 0;
		}else{
			$countblogs = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}pages WHERE editor = 'Plugin_BlogEntries_Editor'");
			$countreads = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_blogread WHERE userid = '".User::Current()->id."'");
			$result = $countblogs - $countreads;
			return $result;
		}
	}

	public static function setBlogRead($id){
		if(User::Current()->role->ID > 1){
			$id = DataBase::Current()->EscapeString($id);
			$count = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_blogread WHERE userid = '".User::Current()->id."' AND blogid = '".$id."'");
			if($count == 0){
				DataBase::Current()->Execute("INSERT INTO {'dbprefix'}plugin_blogread SET userid = '".User::Current()->id."', blogid = '".$id."'");
			}
		}
	}

	public static function setAllBlogsRead($userid){
		$userid = DataBase::Current()->EscapeString($userid);
		foreach(DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}pages WHERE editor = 'Plugin_BlogEntries_Editor'") as $blog){
			DataBase::Current()->Execute("INSERT INTO {'dbprefix'}plugin_blogread SET userid = '".$userid."', blogid = '".$blog->id."'");
		}
	}

	public static function hasNewRatings(){
		if(!User::Current()->isGuest() && Plugin_Events_Manager::countRatings(User::Current()->id) > 0){
			$user = self::getUserDataPerId(User::Current()->id);
			$lastlogin = $user->lastrating;
			$rating = self::newestRating();
			$lastrate = $rating->datum;
			if($lastrate > $lastlogin){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public static function newestRating(){
		return DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}plugin_ratings WHERE rateid = '".User::Current()->id."' ORDER BY id DESC LIMIT 1");
	}

	public static function updateLatestRating(){
		DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET lastrating = NOW() WHERE id = '".User::Current()->id."' LIMIT 1");
	}

	public static function hasUnreadEvents(){
		if(User::Current()->role->ID == 1){
			return 0;
		}else{
			$countevents = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_events WHERE active = '1'");
			$countreads = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_eventread WHERE userid = '".User::Current()->id."'");
			$result = $countevents - $countreads;
			return $result;
		}
	}

	public static function openImg($name){
		$name = DataBase::Current()->EscapeString($name);
		return DataBase::Current()->ReadField("SELECT openimg FROM {'dbprefix'}user WHERE name = '".$name."'");
	}

	public static function openImgPerId($id){
		$id = DataBase::Current()->EscapeString($id);
		return DataBase::Current()->ReadField("SELECT openimg FROM {'dbprefix'}user WHERE id = '".$id."'");
	}

	public static function ifhttp($string){
		$substring = substr($string,0,4);
		if($substring == 'http'){
			return $string;
		}else{
			return 'http://'.$string;
		}
	}

	public static function startVerifyOrgMail($orgid){
		$orgid = DataBase::Current()->EscapeString($orgid);
		$orginfos = self::loadOrganisation($orgid);
		$verifycode = md5(time().$orginfos->orgname.$orginfos->orgmail);
		$subject = utf8_decode('Verifiziere deine Organisations-E-Mail-Adresse auf Social Meal');
		$mailtext = 'Liebe * r Administrator * in von '.$orginfos->orgname.'<br><br>Soeben hast du auf Social Meal die Verifizierung dieser E-Mail-Adresse für das Organisationsprofil <a href="'.Settings::getInstance()->get("host").'organisationsprofil.html?profile='.$orginfos->id.'">'.$orginfos->orgname.'</a> beantragt.<br><br>Klicke auf diesen Link um die E-Mail-Adresse zu verifizieren: <b><a href="'.Settings::getInstance()->get("host").'organisationsprofil.html?profile='.$orginfos->id.'&verifycode='.$verifycode.'">'.Settings::getInstance()->get("host").'organisationsprofil.html?profile='.$orginfos->id.'&verifycode='.$verifycode.'</a></b><br>Danach kannst du unter <a href="'.Settings::getInstance()->get("host").'neues-organisationsprofil.html?orgprofile='.$orginfos->id.'">Organisationsprofil bearbeiten</a> die Option, dass bei deinen Spezial-Meals das automatische E-Mail bei Anmeldeschluss, welches dich über alle definitiven Anmeldungen informiert, auch an diese E-Mail-Adresse versandt wird, aktivieren.<br><br>Vielen dank für dein Engagement.<br>Social Meal';
		$h2t = new Plugin_Mailer_html2text($mailtext);
		$mail = new Plugin_PHPMailer_PHPMailer;
		$mail->IsSMTP();
		$mail->AddAddress($orginfos->orgmail);
		$mail->WordWrap = 50;
		$mail->IsHTML(true);
		$mail->Subject = $subject;
		$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
		$mail->AltBody = $h2t->get_text();
		if(!$mail->Send()){
			echo('Mail Error:'.$mail->ErrorInfo);
		}
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_orgprofile SET verifycode = '".$verifycode."' WHERE id = '".$orginfos->id."' LIMIT 1");
	}

	public static function doVerifyOrgMail($orgid){
		$orgid = DataBase::Current()->EscapeString($orgid);
		$verifycode = '';
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_orgprofile SET verifycode = '".$verifycode."', verify = '1' WHERE id = '".$orgid."' LIMIT 1");
	}

	public static function getRateLink($fromuserid,$rateid,$eventid){
		$fromuserid = DataBase::Current()->EscapeString($fromuserid);
		$rateid = DataBase::Current()->EscapeString($rateid);
		$eventid = DataBase::Current()->EscapeString($eventid);
		$data = DataBase::Current()->ReadRow("SELECT id FROM {'dbprefix'}plugin_ratings WHERE userid = '".$fromuserid."' AND rateid = '".$rateid."' AND eventid = '".$eventid."'");
		return '/bewertungen.html?user='.$rateid.'#'.$data->id;
	}

	public static function changeAdminMark($userid){
		$userid = DataBase::Current()->EscapeString($userid);
		$oldmark = DataBase::Current()->ReadField("SELECT adminmark FROM {'dbprefix'}user WHERE id = '".$userid."'");
		if($oldmark == 0){
			$newmark = 1;
		}elseif($oldmark == 1){
			$newmark = 0;
		}
		DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET adminmark = '".$newmark."' WHERE id = '".$userid."' LIMIT 1");
	}

	public static function isFollowOrg($orgid){
		$orgid = DataBase::Current()->EscapeString($orgid);
		$count = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_orgfollow WHERE uid = '".User::Current()->id."' AND orgid = '".$orgid."'");
		if($count == 0){
			return false;
		}else{
			return true;
		}
	}

	public static function followOrg($orgid){
		$orgid = DataBase::Current()->EscapeString($orgid);
		if(!self::isFollowOrg($orgid)){
			DataBase::Current()->Execute("INSERT INTO {'dbprefix'}plugin_orgfollow SET uid = '".User::Current()->id."', orgid = '".$orgid."'");
		}
	}

	public static function unFollowOrg($orgid){
		$orgid = DataBase::Current()->EscapeString($orgid);
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_orgfollow WHERE uid = '".User::Current()->id."' AND orgid = '".$orgid."' LIMIT 1");
	}

	public static function isFollowUser($userid){
		$userid = DataBase::Current()->EscapeString($userid);
		$count = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_userfollow WHERE uid = '".User::Current()->id."' AND followid = '".$userid."'");
		if($count == 0){
			return false;
		}else{
			return true;
		}
	}

	public static function followUser($userid){
		$userid = DataBase::Current()->EscapeString($userid);
		if(!self::isFollowUser($userid) && !self::isFriend($userid) && User::Current()->id != $userid){
			DataBase::Current()->Execute("INSERT INTO {'dbprefix'}plugin_userfollow SET uid = '".User::Current()->id."', followid = '".$userid."'");
		}
	}

	public static function unFollowUser($userid){
		$userid = DataBase::Current()->EscapeString($userid);
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_userfollow WHERE uid = '".User::Current()->id."' AND followid = '".$userid."' LIMIT 1");
	}

	public static function getUserFollowers(){
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_userfollow WHERE followid = '".User::Current()->id."'");
	}

	public static function getOrgFollowers($orgid){
		$orgid = DataBase::Current()->EscapeString($orgid);
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_orgfollow WHERE orgid = '".$orgid."'");
	}

	public static function inviteShortUrl($url){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://to.ly/api.php?longurl=".urlencode($url));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		$shorturl = curl_exec ($ch);
		curl_close ($ch);
		return htmlentities($shorturl);
	}

	public static function inviteUploadAddressbook($file){
		$filename = $file['file']['name'];
		$res = false;
		if(FileServer::checkUploadFile($file['file'])){
			$tempname = $file['file']['tmp_name'];
			$file_end 		= substr($filename, -3);
			$filename = User::Current()->name.'-'.time().'-'.mt_rand().'.'.strtolower($file_end);
			$base = Settings::getInstance()->get("root")."content/uploads/addressbook/";
			$res = copy($tempname, $base."/".$filename);
			return $filename;
		}
	}
	
	public static function updateLoginCounter($name){
		DataBase::Current()->EscapeString($name);
		$counter = self::getLoginCounter($name);
		$counter++;
		DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET falselogin = '".$counter."' WHERE name = '".$name."' LIMIT 1");
	}
	
	public static function resetLoginCounter($name){
		$name = DataBase::Current()->EscapeString($name);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET falselogin = '0' WHERE name = '".$name."' LIMIT 1");
	}
	
	public static function getLoginCounter($name){
		$name = DataBase::Current()->EscapeString($name);
		return DataBase::Current()->ReadField("SELECT falselogin FROM {'dbprefix'}user WHERE name = '".$name."'");
	}
	
	public static function setUserBlock($name){
		$name = DataBase::Current()->EscapeString($name);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET role = '6' WHERE name = '".$name."' LIMIT 1");
	}
	
	public static function wettbewerbCounter(){
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}user WHERE wettbewerb = '1'");
	}
}
?>
