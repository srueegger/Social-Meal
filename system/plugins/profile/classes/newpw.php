<?PHP
/*
 * newpw.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_Profile_newPW extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			$template->load('plugin_profile_newpw');
			$template->show_if('showform',true);
			$template->show_if('msg',false);
			if(isset($_POST['email'])){
				if($_POST['url'] == ''){
					$template->assign_var('mail',htmlentities($_POST['email']));
					if(Plugin_Profile_Manager::countUserPerEmail($_POST['email']) == 1){
						$userinfo = Plugin_Profile_Manager::getUserDataPerEmail($_POST['email']);
						$template->show_if('showform',false);
						$template->show_if('msg',true);
						$subject = utf8_decode('Du hast ein neues Passwort für deinen Account auf Social Meal angefordert');
						$neuespw = mt_rand();
						$password = DataBase::Current()->EscapeString(/*md5($password)*/md5($neuespw./*md5(*/Settings::getInstance()->get("salt")/*)*/));
						$mailtext = 'Liebe*r '.ucfirst($userinfo->firstname).'<br><br>Für dein Account auf Social Meal wurde ein neues Passwort angefordert.<br>Dein neues Passwort lautet: <b>'.$neuespw.'</b><br><br>Wir empfehlen dir, dass du dich möglichst gleich auf Social Meal <a href="'.Settings::getInstance()->get("host").'login.html">einloggst</a> und dein <a href="'.Settings::getInstance()->get("host").'passwort-aendern.html">Passwort änderst</a>.<br><br>Falls du dieses E-Mail erhältst ohne ein neues Passwort angefordert zu haben, lass uns das bitte sogleich per E-Mail wissen: <a href="mailto:info@socialmeal.ch">info@socialmeal.ch</a><br><br>Social Meal';
						$h2t = new Plugin_Mailer_html2text($mailtext);
						Plugin_Profile_Manager::updatePassword($password,$_POST['email']);
						$mail = new Plugin_PHPMailer_PHPMailer;
						$mail->IsSMTP();
						$mail->AddAddress($_POST['email']);
						$mail->WordWrap = 50;
						$mail->IsHTML(true);
						$mail->Subject = $subject;
						$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
						$mail->AltBody = $h2t->get_text();
						if(!$mail->Send()){
							echo('Mail Error:'.$mail->ErrorInfo);
						}
					}else{
						$template->show_if('showform',false);
						$template->show_if('msg',true);
						$template->assign_var('mail',htmlentities($_POST['mail']));
					}
				}
			}
			echo($template->getCode());
		}

		function getHeader(){
			echo('<script type="text/javascript" src="/system/plugins/profile/js/newpw.js"></script>');
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
