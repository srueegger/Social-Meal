<?PHP
/*
 * friends.php
 *
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
	class Plugin_Profile_Friends extends Editor{

		/**
		 *
		 * @param Page $page
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			$template->load('plugin_profile_friends');
			$template->show_if('newfriendmsg',false);
			$template->show_if('deletefriendmsg',false);
			$template->show_if('deleterequest',false);
			if(isset($_GET['add'])){
				$frienddata = Plugin_Profile_Manager::getFriendInfos($_GET['add']);
				if($frienddata->userid1 == User::Current()->id){
					$userid = $frienddata->userid2;
				}elseif($frienddata->userid2 == User::Current()->id){
					$userid = $frienddata->userid1;
				}
				$userinfo = Plugin_Profile_Manager::getUserDataPerId($userid);
				$template->assign_var('username',htmlentities($userinfo->name));
				Plugin_Profile_Manager::acceptFriendRequest($_GET['add']);
				Plugin_Profile_Manager::updatePunktePerId(3,$frienddata->userid1);
				Plugin_Profile_Manager::updatePunktePerId(3,$frienddata->userid2);
				$template->show_if('newfriendmsg',true);
			}
			if(isset($_GET['delete'])){
				$frienddata = Plugin_Profile_Manager::getFriendInfos($_GET['delete']);
				if($frienddata->userid1 == User::Current()->id){
					$userid = $frienddata->userid2;
				}elseif($frienddata->userid2 == User::Current()->id){
					$userid = $frienddata->userid1;
				}
				$userinfo = Plugin_Profile_Manager::getUserDataPerId($userid);
				$template->assign_var('username',htmlentities($userinfo->name));
				Plugin_Profile_Manager::delFriend($_GET['delete']);
				Plugin_Profile_Manager::updatePunktePerId(-3,$frienddata->userid1);
				Plugin_Profile_Manager::updatePunktePerId(-3,$frienddata->userid2);
				$template->show_if('deletefriendmsg',true);
			}
			if(isset($_GET['deleterequest'])){
				$frienddata = Plugin_Profile_Manager::getFriendInfos($_GET['deleterequest']);
				if($frienddata->userid1 == User::Current()->id){
					$userid = $frienddata->userid2;
				}elseif($frienddata->userid2 == User::Current()->id){
					$userid = $frienddata->userid1;
				}
				$userinfo = Plugin_Profile_Manager::getUserDataPerId($userid);
				$template->assign_var('username',htmlentities($userinfo->name));
				Plugin_Profile_Manager::delOpenFriend($_GET['deleterequest']);
				$template->show_if('deleterequest',true);
			}
			if(Plugin_Profile_Manager::countAllFriends() == 0){
				$template->show_if('shownewfriends',false);
			}else{
				$template->show_if('shownewfriends',true);
				foreach(Plugin_Profile_Manager::getOwnFriendRequest() as $newfriend){
					$index = $template->add_loop_item('newfriends');
					$friendinfo = Plugin_Profile_Manager::getUserDataPerId($newfriend->userid1);
					$template->assign_loop_var("newfriends",$index,"username",htmlentities($friendinfo->name));
					$template->assign_loop_var("newfriends",$index,"friendid",htmlentities($newfriend->id));
					if(Plugin_Profile_Manager::isVerified($friendinfo->name) == 2){
						$template->assign_loop_var("newfriends",$index,'verify','<a infotext="'.Language::DirectTranslate('plugin_profile_isverify').'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('tick').'" /></span></a>');
					}else{
						$template->assign_loop_var("newfriends",$index,'verify','<a infotext="'.Language::DirectTranslate('plugin_profile_noverify').'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('cancel').'" /></span></a>');
					}
				}
			}
			if(isset($_GET['user'])){
				$fromuserdata = Plugin_Profile_Manager::getUserName($_GET['user']);
				$fromuserid = $fromuserdata->id;
				$template->assign_var('FRIENDSTITLE',Language::directTranslate('plugin_profile_friendsof').' '.$_GET['user']);
				$template->show_if('shownewfriends',false);
			}else{
				$fromuserid = User::Current()->id;
				$template->assign_var('FRIENDSTITLE',Language::directTranslate('plugin_profile_myfriends'));
			}
			foreach(Plugin_Profile_Manager::getFriendsData($fromuserid) as $friend){
				if($friend->statusid == 1){
					$index = $template->add_loop_item('friends');
					if($friend->userid1 != $fromuserid){
						$userid = $friend->userid1;
					}
					if($friend->userid2 != $fromuserid){
						$userid = $friend->userid2;
					}
					$userdata = Plugin_Profile_Manager::getUserDataPerId($userid);
					$template->assign_loop_var("friends",$index,"vorname",htmlentities(ucfirst($userdata->firstname)));
					$template->assign_loop_var("friends",$index,"nachname",htmlentities(ucfirst($userdata->lastname)));
					$template->assign_loop_var("friends",$index,"username",htmlentities($userdata->name));
					if(Plugin_OnlineStatus_OnlineStatus::isOnline($userid)){
						$template->assign_loop_var("friends",$index,"friendstatus",'<img src="'.Icons::getIcon('status_online').'" title="online" />');
					}else{
						$template->assign_loop_var("friends",$index,"friendstatus",'<img src="'.Icons::getIcon('status_offline').'" title="offline" />');
					}
					if($fromuserid == User::Current()->id){
						$template->assign_loop_var("friends",$index,"deletelink",'<a onclick="return confirm_alert(this);" href="/freunde.html?delete='.htmlentities($friend->id).'"><img src="'.Icons::getIcon('user_delete').'" /></a>');
					}else{
						$template->assign_loop_var("friends",$index,"deletelink",'');
					}
				}
			}
			echo($template->getCode());
		}

		function getHeader(){
			echo('<script type="text/javascript">
function confirm_alert(node) {
    return confirm("Möchtest du den Freund wirklich löschen?");
}
</script>');
		}

		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}

		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}

	}
?>
