<?PHP
/*
 * settings.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_Profile_Settings extends Editor{
		
		/**
		 *
		 * @param Page $page
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			$template->load('plugin_profile_settings');
			$template->show_if('msg',false);
			$template->show_if('msgmobile',false);
			if(isset($_POST['usersettings_save'])){
				Plugin_Profile_Manager::updateSettings(User::Current()->id,$_POST);
				if(Mobile::isMobileDevice()){
					$template->show_if('msgmobile',true);
				}else{
					$template->show_if('msg',true);
				}
			}
			$usersettings = Plugin_Profile_Manager::getAllUserSettings(User::Current()->id);
			if($usersettings->tb == 1){
				$template->assign_var('tb','checked="checked"');
			}
			if($usersettings->friendevent == 1){
				$template->assign_var('friendevent','checked="checked"');
			}
			if($usersettings->newquestion == 1){
				$template->assign_var('newquestion','checked="checked"');
			}
			if($usersettings->newanswer == 1){
				$template->assign_var('newanswer','checked="checked"');
			}
			if($usersettings->newsubscribe == 1){
				$template->assign_var('newsubscribe','checked="checked"');
			}
			if($usersettings->unsubscribe == 1){
				$template->assign_var('unsubscribe','checked="checked"');
			}
			if($usersettings->socialmedia == 1){
				$template->assign_var('socialmedia','checked="checked"');
			}
			if($usersettings->profile == 1){
				$template->assign_var('profile','checked="checked"');
			}
			if($usersettings->rate == 1){
				$template->assign_var('rate','checked="checked"');
			}
			if($usersettings->ratereply == 1){
				$template->assign_var('ratereply','checked="checked"');
			}
			if($usersettings->friend == 1){
				$template->assign_var('friend','checked="checked"');
			}
			if($usersettings->newpm == 1){
				$template->assign_var('newpm','checked="checked"');
			}
			if($usersettings->newevent == 1){
				$template->assign_var('newevent','checked="checked"');
			}
			if($usersettings->afterevent == 1){
				$template->assign_var('afterevent','checked="checked"');
			}
			if($usersettings->orgadmin == 1){
				$template->assign_var('orgadmin','checked="checked"');
			}
			if($usersettings->blogcomment == 1){
				$template->assign_var('blogcomment','checked="checked"');
			}
			if($usersettings->nlsm == 1){
				$template->assign_var('nlsm','checked="checked"');
			}
			if($usersettings->nlob == 1){
				$template->assign_var('nlob','checked="checked"');
			}
			$template->assign_var('anzahlpm',htmlentities($usersettings->anzahlpm));
			$template->assign_var('tb','');
			$template->assign_var('friendevent','');
			$template->assign_var('newquestion','');
			$template->assign_var('newanswer','');
			$template->assign_var('newsubscribe','');
			$template->assign_var('unsubscribe','');
			$template->assign_var('socialmedia','');
			$template->assign_var('profile','');
			$template->assign_var('rate','');
			$template->assign_var('ratereply','');
			$template->assign_var('friend','');
			$template->assign_var('newpm','');
			$template->assign_var('newevent','');
			$template->assign_var('afterevent','');
			$template->assign_var('nlsm','');
			$template->assign_var('nlob','');
			$template->assign_var('orgadmin','');
			$template->assign_var('blogcomment','');
			echo($template->getCode());
		}

		function getHeader(){
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
