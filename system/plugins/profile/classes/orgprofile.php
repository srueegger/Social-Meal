<?PHP
/*
 * orgprofile.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_Profile_OrgProfile extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
			$orginfo = Plugin_Profile_Manager::loadOrganisation($_GET['profile']);
			$page->title = htmlentities($orginfo->orgname).' - '.Language::DirectTranslate('plugin_profile_orgprofile');
		}
		public function display(){
			$template = new Template();
			if(Mobile::isMobileDevice()){
				$template->load('plugin_profile_orgprofile.mobile');
			}else{
				$template->load('plugin_profile_orgprofile');
			}
			$template->show_if('img',false);
			$template->show_if('kontakt',false);
			$template->show_if('webseite',false);
			$template->show_if('mail',false);
			$template->show_if('socialmedia',false);
			$template->show_if('facebook',false);
			$template->show_if('twitter',false);
			$template->show_if('googleplus',false);
			$template->show_if('tel',false);
			$template->show_if('showedit',false);
			$template->show_if('verifyok',false);
			$template->show_if('meals',false);
			$template->show_if('oldmeals',false);
			$template->show_if('followok',false);
			$template->show_if('unfollowok',false);
			if(isset($_GET['profile'])){
				if(Plugin_Profile_Manager::isFollowOrg($_GET['profile'])){
					$template->show_if('unfollow',true);
					$template->show_if('follow',false);
				}else{
					$template->show_if('unfollow',false);
					$template->show_if('follow',true);
				}
				if(isset($_GET['follow']) == true){
					Plugin_Profile_Manager::followOrg($_GET['profile']);
					$template->show_if('followok',true);
					$template->show_if('unfollow',true);
					$template->show_if('follow',false);
				}
				if(isset($_GET['unfollow']) == true){
					Plugin_Profile_Manager::unFollowOrg($_GET['profile']);
					$template->show_if('unfollowok',true);
					$template->show_if('unfollow',false);
					$template->show_if('follow',true);
				}
				$orginfo = Plugin_Profile_Manager::loadOrganisation($_GET['profile']);
				if(isset($_GET['verifycode'])){
					if($_GET['verifycode'] == $orginfo->verifycode){
						Plugin_Profile_Manager::doVerifyOrgMail($orginfo->id);
						$template->show_if('verifyok',true);
					}
				}
				$template->show_if('noorg',false);
				$template->assign_var('orgid',htmlentities($orginfo->id));
				if(Plugin_Profile_Manager::canEditOrgProfile($orginfo->id)){
					$template->show_if('showedit',true);
				}
				if($orginfo->profilimg != ''){
					$template->show_if('img',true);
					$template->assign_var('orgname',htmlentities($orginfo->orgname));
					$template->assign_var('filename',htmlentities($orginfo->profilimg));
				}
				$bb = new Plugin_BBCode_Translator();
				$template->assign_var('description',$bb->replace($orginfo->description));
				if($orginfo->orgweb != '' or $orginfo->orgmail != '' or $orginfo->orgtel != ''){
					$template->show_if('kontakt',true);
					if($orginfo->orgweb != ''){
						$template->show_if('webseite',true);
						$template->assign_var('orgweb',htmlentities($orginfo->orgweb));
					}
					if($orginfo->orgmail != ''){
						$template->show_if('mail',true);
						$template->assign_var('orgmail',htmlentities($orginfo->orgmail));
					}
					if($orginfo->orgtel != ''){
						$template->show_if('tel',true);
						$template->assign_var('orgtel',htmlentities($orginfo->orgtel));
					}
				}
				if($orginfo->orgfb != '' or $orginfo->orgtwitter != '' or $orginfo->orggplus != ''){
					$template->show_if('socialmedia',true);
					if($orginfo->orgfb != ''){
						$template->show_if('facebook',true);
						$template->assign_var('fb',htmlentities($orginfo->orgfb));
					}
					if($orginfo->orgtwitter != ''){
						$template->show_if('twitter',true);
						$template->assign_var('twitter',htmlentities($orginfo->orgtwitter));
					}
					if($orginfo->orggplus != ''){
						$template->show_if('googleplus',true);
						$template->assign_var('googleplus',htmlentities($orginfo->orggplus));
					}
				}
				if($orginfo->strasse != '' && $orginfo->strnr != '' && $orginfo->plz != '' && $orginfo->ort != ''){
					$template->show_if('adresse',true);
					$template->assign_var('strasse',htmlentities($orginfo->strasse));
					$template->assign_var('strnr',htmlentities($orginfo->strnr));
					$template->assign_var('plz',htmlentities($orginfo->plz));
					$template->assign_var('ort',htmlentities($orginfo->ort));
				}else{
					$template->show_if('adresse',false);
				}
				if(Plugin_Events_Manager::countEventsFromOrg($orginfo->id) >= 1){
					$template->show_if('meals',true);
					foreach(Plugin_Events_Manager::getEventsFromOrg($orginfo->id) as $event){
						$index = $template->add_loop_item("meals");
						if($event->eventtype == 0){
							$template->assign_loop_var("meals",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype0')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_green').'" /></span></a>');
						}elseif($event->eventtype == 1){
							$template->assign_loop_var("meals",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype1')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_red').'" /></span></a>');
						}elseif($event->eventtype == 2){
							$template->assign_loop_var("meals",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype2')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_yellow').'" /></span></a>');
						}elseif($event->eventtype == 3){
							$template->assign_loop_var("meals",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype3')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('asterisk_orange').'" /></span></a>');
						}
						if(Plugin_Events_Manager::getFirstImageFromEvent($event->id) != ''){
							$template->assign_loop_var("meals",$index,"colspan",'1');
							$template->assign_loop_var("meals",$index,"eventimage",'<td style="width:110px; text-align:center;"><img width="150" title="'.htmlentities($event->eventname).'" src="/content/uploads/eat/thumbs/'.Plugin_Events_Manager::getFirstImageFromEvent($event->id).'" /></td>');
						}else{
							$template->assign_loop_var("meals",$index,"colspan",'2');
							$template->assign_loop_var("meals",$index,"eventimage",'');
						}
						$template->assign_loop_var("meals",$index,"eventid",htmlentities($event->id));
						$template->assign_loop_var("meals",$index,"eventname",htmlentities($event->eventname));
						$template->assign_loop_var("meals",$index,"eventdesc",Plugin_Events_Manager::shortText2($event->beschreibung,$event->id,275));
						$template->assign_loop_var("meals",$index,"datum",Plugin_Events_Manager::formatDate($event->eventdate));
					}
				}
				if(Plugin_Events_Manager::countOldEventsFromOrg($orginfo->id) >= 1){
					$template->show_if('oldmeals',true);
					foreach(Plugin_Events_Manager::getOldEventsFromOrg($orginfo->id) as $event){
						$index = $template->add_loop_item("oldmeals");
						$template->assign_loop_var("oldmeals",$index,"eventid",htmlentities($event->id));
						$template->assign_loop_var("oldmeals",$index,"eventname",htmlentities($event->eventname));
						$template->assign_loop_var("oldmeals",$index,"datum",Plugin_Events_Manager::formatDate($event->eventdate));
					}
				}
			}else{
				$template->show_if('noorg',true);
			}
			echo($template->getCode());
		}

		function getHeader(){
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
