<?PHP
/*
 * editlanguages.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_Profile_EditLanguages extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			if(Mobile::isMobileDevice()){
				$template->load('plugin_profile_editlanguages.mobile');
			}else{
				$template->load('plugin_profile_editlanguages');
			}
			$template->show_if('sprachen',false);
			$template->show_if('langok',false);
			$template->show_if('dellang',false);
			if(isset($_POST['submitsprache'])){
				Plugin_Profile_Manager::saveLang($_POST['sprache']);
				$template->show_if('langok',true);
			}
			if(isset($_GET['dellang'])){
				Plugin_Profile_Manager::delLang($_GET['dellang']);
				$template->show_if('dellang',true);
			}
			if(Plugin_Profile_Manager::countLanguagesByUser(User::Current()->id) > 0){
				$template->show_if('sprachen',true);
				foreach(Plugin_Profile_Manager::getLangByUser(User::Current()->id) as $lang){
					$index = $template->add_loop_item('meinesprachen');
					$template->assign_loop_var("meinesprachen",$index,"sprachid",htmlentities($lang->langid));
					$template->assign_loop_var("meinesprachen",$index,"sprache",htmlentities(utf8_encode(Plugin_Profile_Manager::getLngName($lang->langid))));
				}
			}
			if(isset($lang)){
				unset($lang);
			}
			foreach(Plugin_Profile_Manager::getAllLanguages() as $lang){
				if(Plugin_Profile_Manager::userHasLng($lang->id)){
					$index = $template->add_loop_item('sprachen');
					$template->assign_loop_var("sprachen",$index,"sprachid",htmlentities($lang->id));
					$template->assign_loop_var("sprachen",$index,"sprache",htmlentities(utf8_encode($lang->sprache)));
				}
			}
			echo($template->getCode());
		}

		function getHeader(){
			echo('<script type="text/javascript" src="/system/plugins/profile/js/sprachen.js"></script>');
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
