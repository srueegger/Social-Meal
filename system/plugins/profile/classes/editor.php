<?php
/*
 * editor.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_Profile_Editor extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
			if(isset($_GET['profile']) && $_GET['profile'] != User::Current()->name){
				if(Plugin_Profile_Manager::userExist($_GET['profile'])){
					$userdata = Plugin_Profile_Manager::getUserData($_GET['profile']);
					if(Plugin_Profile_Manager::isFriend($userdata->id)){
						$showusername = $userdata->firstname.' ('.$userdata->name.')';
					}else{
						$showusername = htmlentities($userdata->name);
					}
				}
				$page->title = 'Profil von '.$showusername.'';
				if(!Plugin_Profile_Manager::userExist($_GET['profile'])){
					$page->title = Language::DirectTranslate('plugin_profile_noprofilefound');
				}
			}else{
				$page->title = Language::DirectTranslate('plugin_profile_myprofile');
			}
		}
		public function display(){
			$template = new Template();
			if(isset($_GET['profile'])){
				if(!Plugin_Profile_Manager::userExist($_GET['profile'])){
					$template->load('plugin_profile_noprofile');
					echo($template->getCode());
					exit();
				}
			}
			$user = Plugin_Profile_Manager::getUserData(isset($_GET['profile']));
			if($user->role == 5){
				$template->load('plugin_profile_noprofile');
				echo($template->getCode());
				exit();
			}
			if(Mobile::isMobileDevice()){
				$template->load('plugin_profile_overview.mobile');
			}else{
				$template->load('plugin_profile_overview');
			}
			$template->show_if('message',false);
			$template->show_if('showfblink',false);
			$template->show_if('showtwitlink',false);
			$template->show_if('showgplink',false);
			$template->show_if('showskypelink',false);
			$template->show_if('showflickrlink',false);
			$template->show_if('showlinkedinlink',false);
			$template->show_if('webpage',false);
			$template->show_if('myprofile',false);
			$template->show_if('otherprofile',false);
			$template->show_if('addfriendmsg',false);
			$template->show_if('kontaktinfo',true);
			$template->show_if('newadmin',false);
			$template->show_if('admindel',false);
			$template->show_if('adminok',false);
			$template->show_if('orgs',false);
			$template->show_if('iam',false);
			$template->show_if('ilike',false);
			$template->show_if('noeatsme',false);
			$template->show_if('noeats',false);
			$template->show_if('sprachen',false);
			$template->show_if('redirect',false);
			$template->show_if('rapiddel',false);
			$template->show_if('ofdel',false);
			$template->show_if('openfriends',false);
			$template->show_if('userblock',false);
			$template->show_if('followok',false);
			$template->show_if('unfollowok',false);
			$template->show_if('follow',false);
			$template->show_if('unfollow',false);
			$template->show_if('hasfriends',false);
			$template->show_if('othernofriends',false);
			$template->show_if('menofriends',false);
			$template->show_if('noevents1',false);
			$template->show_if('hasevents1',false);
			$template->show_if('hasgastgeber1',false);
			$template->show_if('nogastgeberown',false);
			$template->show_if('nogastgeberother',false);
			$template->show_if('hasteilnehmer1',false);
			$template->show_if('noteilnehmerown',false);
			$template->show_if('noteilnehmerother',false);
			$template->show_if('hasgastgeber2',false);
			$template->show_if('nooldgastgeberown',false);
			$template->show_if('nooldgastgeberother',false);
			$template->show_if('hasteilnehmer2',false);
			$template->show_if('nooldteilnehmerown',false);
			$template->show_if('nooldteilnehmerother',false);
			if(isset($_GET['rapiddel'])){
				$evid = DataBase::Current()->EscapeString($_GET['rapiddel']);
				if(Plugin_Events_Manager::eventExist($evid) == 1){
					$evdata = Plugin_Events_Manager::loadEvent($evid);
					if($evdata->userid == User::Current()->id && $evdata->active == 0){
						foreach(Plugin_Events_Manager::getImagesPerEvent($evid) as $image){
							Plugin_Events_Manager::deleteImage($image->id);
						}
						Plugin_Events_Manager::deleteQuestions($evid);
						Plugin_Events_Manager::deleteEvent($evid);
						$template->show_if('rapiddel',true);
					} 
				}
			}
			if(isset($_GET['delfriend'])){
				Plugin_Profile_Manager::delOpenFriend($_GET['delfriend']);
				$template->show_if('ofdel',true);
			}
			if(!isset($_GET['profile'])){
				$user = Plugin_Profile_Manager::getUserData(User::Current()->name);
				$template->show_if('myprofile',true);
				$template->show_if('showfriend',false);
				$template->show_if('kontaktinfo',false);
				$template->show_if('melden',false);
				$template->assign_var('showusername',htmlentities($user->firstname.' '.$user->lastname));
				$template->assign_var('firstname',htmlentities($user->firstname));
				if(isset($_GET['admindel'])){
					Plugin_Profile_Manager::delOrgAdminRequest($_GET['admindel']);
					$template->show_if('admindel',true);
				}
				if(isset($_GET['adminok'])){
					Plugin_Profile_Manager::acceptOrgAdminRequest($_GET['adminok']);
					$orginfos = Plugin_Profile_Manager::loadOrganisation($_GET['adminok']);
					$template->assign_var('orgid',htmlentities($orginfos->id));
					$template->assign_var('orgname',htmlentities($orginfos->orgname));
					$template->show_if('adminok',true);
				}
				if(Plugin_Profile_Manager::hasAdminRequest() == true){
					$orginfo = Plugin_Profile_Manager::requestOrgInfo();
					$template->assign_var('orgprofilname',htmlentities($orginfo->orgname));
					$template->assign_var('orgid',htmlentities($orginfo->id));
					$template->show_if('newadmin',true);
				}
				if(Plugin_Profile_Manager::countOrgAdminsFromUser() > 0){
					$template->show_if('orgs',true);
					foreach(Plugin_Profile_Manager::getOrgsByUser() as $orgid){
						$index = $template->add_loop_item('orgs');
						$orginfo = Plugin_Profile_Manager::loadOrganisation($orgid->orgid);
						$template->assign_loop_var("orgs",$index,"orgid",htmlentities($orginfo->id));
						$template->assign_loop_var("orgs",$index,"orgname",htmlentities($orginfo->orgname));
					}
				}
				if(Plugin_Profile_Manager::countOpenFriends() > 0){
					$template->show_if('openfriends',true);
				}
				foreach(Plugin_Profile_Manager::getFriendRequest() as $ofriend){
					$ofrienddata = Plugin_Profile_Manager::getUserDataPerId($ofriend->userid2);
					$index = $template->add_loop_item('openfriends');
					$template->assign_loop_var("openfriends",$index,"ofusername",htmlentities($ofrienddata->name));
					$template->assign_loop_var("openfriends",$index,"ofid",htmlentities($ofriend->id));
				}
			}else{
				$template->show_if('melden',true);
				$template->show_if('otherprofile',true);
				$user = Plugin_Profile_Manager::getUserData($_GET['profile']);
				if(Plugin_Profile_Manager::isFriend($user->id)){
					$template->assign_var('showusername',htmlentities($user->firstname.' '.$user->lastname));
					$template->assign_var('firstname',htmlentities($user->firstname));
				}else{
					$template->assign_var('showusername',htmlentities($user->name));
					$template->assign_var('firstname',htmlentities($user->name));
				}
				if(isset($_GET['follow'])){
					Plugin_Profile_Manager::followUser($user->id);
					$template->show_if('followok',true);
				}
				if(isset($_GET['unfollow'])){
					Plugin_Profile_Manager::unFollowUser($user->id);
					$template->show_if('unfollowok',true);
				}
				if(User::Current()->id == $user->id){
					$template->show_if('redirect',true);
				}
				if(Plugin_Profile_Manager::isFriend($user->id)){
					$template->show_if('follow',false);
					$template->show_if('unfollow',false);
				}else{
					if(Plugin_Profile_Manager::isFollowUser($user->id)){
						$template->show_if('follow',false);
						$template->show_if('unfollow',true);
					}else{
						$template->show_if('follow',true);
						$template->show_if('unfollow',false);
					}
				}
				if($user->id != User::Current()->id){
					Plugin_Profile_Manager::updateCounter($user->id);
					unset($user);
					$user = Plugin_Profile_Manager::getUserData($_GET['profile']);
					$watch1 = Plugin_Profile_Manager::getUserSetting('profile',$user->id);
					$watch2 = Plugin_Profile_Manager::getUserSetting('profile',User::Current()->id);
					if($watch1 == 1 && $watch2 == 1){
						Plugin_Profile_Manager::updateWatch($user->id);
					}
				}
				if(!Plugin_Profile_Manager::isFriend($user->id) && User::Current()->id != $user->id){
					$template->show_if('showfriend',true);
				}else{
					$template->show_if('showfriend',false);
				}
				if(isset($_GET['friendrequest']) == true && User::Current()->id != $user->id){
					if($count1 == 0 && $count2 == 0){
						Plugin_Profile_Manager::addFriend($user->id);
						$template->show_if('showfriend',false);
						$template->show_if('addfriendmsg',true);
					}
				}
			}
			if($user->firstname == ''){
				$template->show_if('message',true);
			}
			$template->assign_var('username',htmlentities($user->name));
			$template->assign_var('userid',htmlentities($user->id));
			if($user->id == User::Current()->id){
				$template->assign_var('firstname',htmlentities(ucfirst($user->firstname)));
				$template->assign_var('lastname',' '.htmlentities(ucfirst($user->lastname)));
			}elseif(Plugin_Profile_Manager::isFriend($user->id) == true){
				$template->assign_var('firstname',htmlentities(ucfirst($user->firstname)));
				$template->assign_var('lastname',' '.htmlentities(ucfirst($user->lastname)));
			}else{
				$template->assign_var('firstname',htmlentities($user->name));
				$template->assign_var('lastname','');
			}
			$bb = new Plugin_BBCode_Translator();
			if($user->aboutme != ''){
				$template->show_if('aboutme',true);
				$template->assign_var('aboutme',$bb->replace($user->aboutme));
			}else{
				$template->show_if('aboutme',false);
			}
			$template->assign_var('ort',' aus '.htmlentities(ucfirst($user->ort)));
			$template->assign_var('kanton',htmlentities($user->kanton));
			$template->assign_var('age',Plugin_Profile_Manager::formatAge($user->gbdatum));
			$template->assign_var('job',htmlentities(', '.$user->job));
			$template->assign_var('gender',utf8_encode(Language::directTranslate('plugin_profile_gen'.$user->gender)));
			$template->assign_var('bzstatus',Language::directTranslate('plugin_profile_bzs'.$user->bzstatus));
			$template->assign_var('aufrufe',htmlentities($user->counter));
			$template->assign_var('countratings',Plugin_Events_Manager::countRatings($user->id));
			$regdate = Plugin_Profile_Manager::datediff_ymd(strtotime($user->create_timestamp));
			$formatregdate = Plugin_Profile_Manager::createDateString($regdate);
			if($formatregdate == ''){
				$formatregdate = 'Heute';
			}
			if($user->role == 6){
				$template->show_if('userblock',true);
			}
			$template->assign_var('regdatum',$formatregdate);
			if(Plugin_Profile_Manager::countLanguagesByUser($user->id) > 0){
				$template->show_if('sprachen',true);
				foreach(Plugin_Profile_Manager::getLangByUser($user->id) as $lang){
					$index = $template->add_loop_item('sprachen');
					$sprachname = Plugin_Profile_Manager::getLngName($lang->langid);
					$template->assign_loop_var("sprachen",$index,"sprachname",htmlentities(utf8_encode($sprachname)));
				}
			}
			if(Plugin_Profile_Manager::isVerified($user->name) == 2){
				$template->show_if('isverify',false);
				if(Mobile::isMobileDevice()){
					$template->assign_var('verify',Language::DirectTranslate('plugin_profile_isverify'));
				}else{
					$template->assign_var('verify',''.Language::DirectTranslate('plugin_profile_isverify').' <a infotext="'.Language::DirectTranslate('plugin_profile_isverify').'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('tick').'" /></span></a>');
				}
			}else{
				$template->show_if('isverify',true);
				if(Mobile::isMobileDevice()){
					$template->assign_var('verify',Language::DirectTranslate('plugin_profile_noverify'));
				}else{
					$template->assign_var('verify',''.Language::DirectTranslate('plugin_profile_noverify').' <a infotext="'.Language::DirectTranslate('plugin_profile_noverify').'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('cancel').'" /></span></a>');
				}
			}
			$stars = Plugin_Profile_Manager::averageStars($user->id);
			if($stars >= 1){
				if(Mobile::isMobileDevice()){
					$template->assign_var("star1","iconStarDark");
				}else{
					$template->assign_var("star1",'<li class="on"></li>');
				}
			}else{
				if(Mobile::isMobileDevice()){
					$template->assign_var("star1","iconStar");
				}else{
					$template->assign_var("star1",'');
				}
			}
			if($stars >= 2){
				if(Mobile::isMobileDevice()){
					$template->assign_var("star2","iconStarDark");
				}else{
					$template->assign_var("star2",'<li class="on"></li>');
				}
			}else{
				if(Mobile::isMobileDevice()){
					$template->assign_var("star2","iconStar");
				}else{
					$template->assign_var("star2",'');
				}
			}
			if($stars >= 3){
				if(Mobile::isMobileDevice()){
					$template->assign_var("star3","iconStarDark");
				}else{
					$template->assign_var("star3",'<li class="on"></li>');
				}
			}else{
				if(Mobile::isMobileDevice()){
					$template->assign_var("star3","iconStar");
				}else{
					$template->assign_var("star3",'');
				}
			}
			if($stars >= 4){
				if(Mobile::isMobileDevice()){
					$template->assign_var("star4","iconStarDark");
				}else{
					$template->assign_var("star4",'<li class="on"></li>');
				}
			}else{
				if(Mobile::isMobileDevice()){
					$template->assign_var("star4","iconStar");
				}else{
					$template->assign_var("star4",'');
				}
			}
			if($stars == 5){
				if(Mobile::isMobileDevice()){
					$template->assign_var("star5","iconStarDark");
				}else{
					$template->assign_var("star5",'<li class="on"></li>');
				}
			}else{
				if(Mobile::isMobileDevice()){
					$template->assign_var("star5","iconStar");
				}else{
					$template->assign_var("star5",'');
				}
			}
			if($stars == 0){
				$template->assign_var('showratings',Language::DirectTranslate('plugin_profile_noratingsfound'));
			}else{
				$template->assign_var('showratings','<a href="/bewertungen.html?user='.htmlentities($user->id).'">'.Language::DirectTranslate('plugin_profile_showratings').'</a>');
			}
			if($user->profilimg == ''){
				$template->assign_var('profilimgthumb', Settings::getInstance()->get("host").'system/skins/socialmeal/images/ourteamimg.jpg');
				$template->assign_var('profilimg', Settings::getInstance()->get("host").'system/skins/socialmeal/images/ourteamimg.jpg');
			}else{
				$fileend = substr($user->profilimg, -3);
				if($fileend == "bmp"){
					$template->assign_var('profilimgthumb', Settings::getInstance()->get("host").'content/uploads/profile/'.htmlentities($user->profilimg));
				}else{
					$template->assign_var('profilimgthumb', Settings::getInstance()->get("host").'content/uploads/profile/thumbs/'.htmlentities($user->profilimg));
				}
				$template->assign_var('profilimg', Settings::getInstance()->get("host").'content/uploads/profile/'.htmlentities($user->profilimg));
			}
			if(Plugin_OnlineStatus_OnlineStatus::isOnline($user->id)){
				$template->assign_var('onlinestatus',' '.Language::DirectTranslate('plugin_profile_isonline'));
			}else{
				$lastdate = Plugin_Profile_Manager::datediff_ymd(strtotime($user->plugin_onlinestatus_lastOnline));
				if($lastdate['years'] == 0 && $lastdate['months'] == 0 && $lastdate['days'] == 0){
					$onlinestatus = ' '.Language::DirectTranslate('plugin_profile_wasonline');
				}else{
					if($user->plugin_onlinestatus_lastOnline == '0000-00-00 00:00:00'){
						$onlinestatus = ' '.Language::DirectTranslate('plugin_profile_neveronline');
					}else{
						$onlinestatus = ' '.Language::DirectTranslate('plugin_profile_before').' '.Plugin_Profile_Manager::createDateString($lastdate).' '.Language::DirectTranslate('plugin_profile_online');
					}
				}
				$template->assign_var('onlinestatus',$onlinestatus);
			}
			if($user->fblink != ''){
				$template->show_if('showfblink',true);
				$template->assign_var('fblink',htmlentities($user->fblink));
			}
			if($user->twitlink != ''){
				$template->show_if('showtwitlink',true);
				$template->assign_var('twitlink',htmlentities($user->twitlink));
			}
			if($user->gplink != ''){
				$template->show_if('showgplink',true);
				$template->assign_var('gplink',htmlentities($user->gplink));
			}
			if($user->skypelink != ''){
				$template->show_if('showskypelink',true);
				$template->assign_var('skypelink',htmlentities($user->skypelink));
			}
			if($user->flickrlink != ''){
				$template->show_if('showflickrlink',true);
				$template->assign_var('flickrlink',htmlentities($user->flickrlink));
			}
			if($user->linkedinlink != ''){
				$template->show_if('showlinkedinlink',true);
				$template->assign_var('linkedinlink',htmlentities($user->linkedinlink));
			}
			if($user->webpage != ''){
				$template->show_if('webpage');
				$template->assign_var('webpage',htmlentities($user->webpage));
			}
			//Nicht fertiggestellte Events
			if(Plugin_Events_Manager::countAllInactiveEventsbyUser($user->id) > '0'){
				$template->show_if('hasevents1',true);
				foreach(Plugin_Events_Manager::getAllInactiveEventsbyUser($user->id) as $event){
					$index = $template->add_loop_item("notfinish");
					if($event->eventtype == 0){
						$template->assign_loop_var("notfinish",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype0')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_green').'" /></span></a>');
					}elseif($event->eventtype == 1){
						$template->assign_loop_var("notfinish",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype1')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_red').'" /></span></a>');
					}elseif($event->eventtype == 2){
						$template->assign_loop_var("notfinish",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype2')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_yellow').'" /></span></a>');
					}elseif($event->eventtype == 3){
						$template->assign_loop_var("notfinish",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype3')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('asterisk_orange').'" /></span></a>');
					}
					if(Plugin_Events_Manager::getFirstImageFromEvent($event->id) != ''){
						$template->assign_loop_var("notfinish",$index,"colspan",'1');
						$template->assign_loop_var("notfinish",$index,"eventimage",'<td style="width:110px; text-align:center;"><img width="150" title="'.htmlentities($event->eventname).'" src="/content/uploads/eat/thumbs/'.Plugin_Events_Manager::getFirstImageFromEvent($event->id).'" /></td>');
					}else{
						$template->assign_loop_var("notfinish",$index,"colspan",'2');
						$template->assign_loop_var("notfinish",$index,"eventimage",'');
					}
					$template->assign_loop_var("notfinish",$index,"eventid",htmlentities($event->id));
					$template->assign_loop_var("notfinish",$index,"eventname",htmlentities($event->eventname));
				}
			}else{
				$template->show_if('noevents1',true);
			}
			//Aktive Events
			if(Plugin_Events_Manager::countEventsByUser($user->id) > 0){
			$template->show_if('hasgastgeber1',true);
				foreach(Plugin_Events_Manager::getEventsByUser($user->id) as $gevent){
					$index = $template->add_loop_item("gastgeber");
					if($gevent->eventtype == 0){
						$template->assign_loop_var("gastgeber",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype0')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_green').'" /></span></a>');
					}elseif($gevent->eventtype == 1){
						$template->assign_loop_var("gastgeber",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype1')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_red').'" /></span></a>');
					}elseif($gevent->eventtype == 2){
						$template->assign_loop_var("gastgeber",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype2')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_yellow').'" /></span></a>');
					}elseif($gevent->eventtype == 3){
						$template->assign_loop_var("gastgeber",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype3')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('asterisk_orange').'" /></span></a>');
					}
					$template->assign_loop_var("gastgeber",$index,"eventid",htmlentities($gevent->id));
					$template->assign_loop_var("gastgeber",$index,"eventname",htmlentities($gevent->eventname));
					$template->assign_loop_var("gastgeber",$index,"datum",Plugin_Events_Manager::formatDate($gevent->eventdate));
					if(Plugin_Events_Manager::getFirstImageFromEvent($gevent->id) != ''){
						$template->assign_loop_var("gastgeber",$index,"colspan",'1');
						$template->assign_loop_var("gastgeber",$index,"eventimage",'<td style="width:110px; text-align:center;"><img width="150" title="'.$gevent->eventname.'" src="/content/uploads/eat/thumbs/'.Plugin_Events_Manager::getFirstImageFromEvent($gevent->id).'" /></td>');
					}else{
						$template->assign_loop_var("gastgeber",$index,"colspan",'2');
						$template->assign_loop_var("gastgeber",$index,"eventimage",'');
					}
					if($gevent->userid == User::Current()->id){
						$manageevents = '';
						if(Plugin_Events_Manager::countSubscribers($gevent->id) == 0){
							if(Mobile::isMobileDevice()){
								$manageevents .= '<a href="/event-bearbeiten.html?event='.$gevent->id.'" class="buttonWrapper buttonDefault buttonEdit">'.Language::DirectTranslate('plugin_events_pagetypetitleedit').'</a>';
							}else{
								$manageevents .= '<a infotext="'.Language::DirectTranslate('plugin_events_pagetypetitleedit').'" class="infotooltip" href="/event-bearbeiten.html?event='.$gevent->id.'"><span title=""><img src="'.Icons::getIcon('page_edit').'" /></span></a>';
							}
						}
						if(date('Y-m-d H:i') < $gevent->anmeldeschluss){
							if(Mobile::isMobileDevice()){
								$manageevents .= '<td><a href="/event-details.html?event='.$gevent->id.'" class="buttonWrapper buttonDefault buttonArrowRight">'.Language::DirectTranslate('plugin_events_filter3').'</a> <a href="/event-loeschen.html?event='.$gevent->id.'" class="buttonWrapper buttonRed buttonNo">'.Language::DirectTranslate('plugin_events_pagetypetitledelete').'</a></td>';
							}else{
								$manageevents .= '<td><a infotext="'.Language::DirectTranslate('plugin_events_filter3').'" class="infotooltip" href="/event-details.html?event='.$gevent->id.'"><span title=""><img src="'.Icons::getIcon('page_find').'" /></span></a> <a infotext="'.Language::DirectTranslate('plugin_events_pagetypetitledelete').'" class="infotooltip" href="/event-loeschen.html?event='.$gevent->id.'"><span title=""><img src="'.Icons::getIcon('cross').'" /></span></a></td>';
							}
						}
						$template->assign_loop_var("gastgeber",$index,'manageevents',$manageevents);
					}else{
						$template->assign_loop_var("gastgeber",$index,'manageevents','');
					}
					if(Plugin_Events_Manager::countSubscribers($gevent->id) == 0){
						$userstring = '';
					}else{
						$userstring = '<br /><br />Bisherige Teilnehmer*innen:';
						if(!Mobile::isMobileDevice()){
							$userstring .= '<ul class="listoption2">';
						}
					}
					foreach(Plugin_Events_Manager::getSubscribesByEvent($gevent->id) as $subscriber){
						$subuserinfo = Plugin_Profile_Manager::getUserDataPerId($subscriber->userid);
						if(!Mobile::isMobileDevice()){
							$userstring .= '<li>';
							if(Plugin_Profile_Manager::isVerified($subuserinfo->name) == 2){
								$userstring .= '<a infotext="'.Language::DirectTranslate('plugin_profile_isverify').'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('tick').'" /></span></a> ';
							}else{
								$userstring .= '<a infotext="'.Language::DirectTranslate('plugin_profile_noverify').'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('cancel').'" /></span></a> ';
							}
						}
						$userstring .= '<a href="/mein-profil.html?profile='.htmlentities($subuserinfo->name).'">';
						if(Plugin_Profile_Manager::isFriend($subuserinfo->id) or $subuserinfo->id == User::Current()->id){
							$userstring .= htmlentities(ucfirst($subuserinfo->firstname).' '.ucfirst($subuserinfo->lastname));
						}else{
							$userstring .= htmlentities($subuserinfo->name);
						}
						$userstring .= '</a> ';
						if($user->id == User::Current()->id && date('Y-m-d H:i') < $gevent->anmeldeschluss){
							$userstring .= '<a infotext="'.Language::DirectTranslate('plugin_events_pagetypetitleunload').'" class="infotooltip_event" href="/teilnehmer-ausladen.html?event='.$gevent->id.'&user='.htmlentities($subscriber->userid).'"><span title=""><img src="'.Icons::getIcon('cross').'" /></span></a>';
						}
						$userstring .= '</li>';
					}
					if(Plugin_Events_Manager::countSubscribers($gevent->id) == 0){
						$userstring .= '';
					}else{
						$userstring .= '</ul>';
					}
					$template->assign_loop_var("gastgeber",$index,"teilnehmerstring",$userstring);
				}
			}else{
				if(User::Current()->id == $user->id){
					$template->show_if('nogastgeberown',true);
				}else{
					$template->show_if('nogastgeberother',true);
				}
			}
			$teilcount = 0;
			foreach(Plugin_Events_Manager::getSubscribesByUser($user->id) as $tevent){
				$teventdata = Plugin_Events_Manager::loadEvent($tevent->eventid,'new');
				if(is_object($teventdata)){
					$index = $template->add_loop_item('teilnehmer');
					if($teventdata->eventtype == 0){
						$template->assign_loop_var("teilnehmer",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype0')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_green').'" /></span></a>');
					}elseif($teventdata->eventtype == 1){
						$template->assign_loop_var("teilnehmer",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype1')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_red').'" /></span></a>');
					}elseif($teventdata->eventtype == 2){
						$template->assign_loop_var("teilnehmer",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype2')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_yellow').'" /></span></a>');
					}elseif($teventdata->eventtype == 3){
						$template->assign_loop_var("teilnehmer",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype3')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('asterisk_orange').'" /></span></a>');
					}
					$template->assign_loop_var("teilnehmer",$index,"teileventid",htmlentities($teventdata->id));
					$template->assign_loop_var("teilnehmer",$index,"teileventname",htmlentities($teventdata->eventname));
					$template->assign_loop_var("teilnehmer",$index,"datum",Plugin_Events_Manager::formatDate($teventdata->eventdate));
					if(Plugin_Events_Manager::getFirstImageFromEvent($teventdata->id) != ''){
						$template->assign_loop_var("teilnehmer",$index,"colspan",'1');
						$template->assign_loop_var("teilnehmer",$index,"eventimage",'<td style="width:110px; text-align:center;"><img width="150" title="'.$teventdata->eventname.'" src="/content/uploads/eat/thumbs/'.Plugin_Events_Manager::getFirstImageFromEvent($teventdata->id).'" /></td>');
					}else{
						$template->assign_loop_var("teilnehmer",$index,"colspan",'2');
						$template->assign_loop_var("teilnehmer",$index,"eventimage",'');
					}
					if($teventdata->anmeldeschluss > date("Y-m-d H:i:s") && $user->id == User::Current()->id){
						if(Mobile::isMobileDevice()){
							$template->assign_loop_var("teilnehmer",$index,"unsubscribe",'<td><a href="/event-abmelden.html?event='.htmlentities($teventdata->id).'" class="buttonWrapper buttonRed buttonNo">'.Language::DirectTranslate('plugin_profile_returnfromevent').'</a></td>');
						}else{
							$template->assign_loop_var("teilnehmer",$index,"unsubscribe",'<td><a infotext="'.Language::DirectTranslate('plugin_profile_returnfromevent').'" class="infotooltip" href="/event-abmelden.html?event='.htmlentities($teventdata->id).'"><span title=""><img src="'.Icons::getIcon('cross').'" /></span></a></td>');
						}
					}else{
						$template->assign_loop_var("teilnehmer",$index,"unsubscribe",'');
					}
					$teilcount++;
				}
			}
			if($teilcount == 0){
				if(User::Current()->id == $user->id){
					$template->show_if('noteilnehmerown',true);
				}else{
					$template->show_if('noteilnehmerother',true);
				}
			}elseif($teilcount >= 1){
				$template->show_if('hasteilnehmer1',true);
			}
			//Inaktive Events
			if(Plugin_Events_Manager::countOldEventsByUser($user->id) > 0){
				$template->show_if('hasgastgeber2',true);
				foreach(Plugin_Events_Manager::getOldEventsByUser($user->id) as $gevent){
					$index = $template->add_loop_item("oldgastgeber");
					$template->assign_loop_var("oldgastgeber",$index,"oldeventid",htmlentities($gevent->id));
					$template->assign_loop_var("oldgastgeber",$index,"oldeventname",htmlentities($gevent->eventname));
					$template->assign_loop_var("oldgastgeber",$index,"datum",Plugin_Events_Manager::formatDate($gevent->eventdate));
					if(Plugin_Events_Manager::getFirstImageFromEvent($gevent->id) != ''){
						$template->assign_loop_var("oldgastgeber",$index,"colspan",'1');
						$template->assign_loop_var("oldgastgeber",$index,"eventimage",'<td style="width:110px; text-align:center;"><img width="150" title="'.htmlentities($gevent->eventname).'" src="/content/uploads/eat/thumbs/'.Plugin_Events_Manager::getFirstImageFromEvent($gevent->id).'" /></td>');
					}else{
						$template->assign_loop_var("oldgastgeber",$index,"colspan",'2');
						$template->assign_loop_var("oldgastgeber",$index,"eventimage",'');
					}
					if($gevent->eventtype == 0){
						$template->assign_loop_var("oldgastgeber",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype0')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_green').'" /></span></a>');
					}elseif($gevent->eventtype == 1){
						$template->assign_loop_var("oldgastgeber",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype1')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_red').'" /></span></a>');
					}elseif($gevent->eventtype == 2){
						$template->assign_loop_var("oldgastgeber",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype2')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_yellow').'" /></span></a>');
					}elseif($gevent->eventtype == 3){
						$template->assign_loop_var("oldgastgeber",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype3')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('asterisk_orange').'" /></span></a>');
					}
					if(Plugin_Events_Manager::countSubscribers($gevent->id) == 0){
						$userstring = '';
					}else{
						$userstring = '<br /><br />'.Language::DirectTranslate('plugin_profile_teilnehmerinnen').':';
						if(!Mobile::isMobileDevice()){
							$userstring .= '<ul class="listoption2">';
						}
					}
					foreach(Plugin_Events_Manager::getSubscribesByEvent($gevent->id) as $subscriber){
						$subuserinfo = Plugin_Profile_Manager::getUserDataPerId($subscriber->userid);
						if(!Mobile::isMobileDevice()){
							$userstring .= '<li>';
							if(Plugin_Profile_Manager::isVerified($subuserinfo->name) == 2){
								$userstring .= '<a infotext="'.Language::DirectTranslate('plugin_profile_isverify').'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('tick').'" /></span></a> ';
							}else{
								$userstring .= '<a infotext="'.Language::DirectTranslate('plugin_profile_noverify').'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('cancel').'" /></span></a> ';
							}
						}
						$userstring .= '<a href="./mein-profil.html?profile='.htmlentities($subuserinfo->name).'">';
						if(Plugin_Profile_Manager::isFriend($subuserinfo->id) or $subuserinfo->id == User::Current()->id){
							$userstring .= htmlentities(ucfirst($subuserinfo->firstname).' '.ucfirst($subuserinfo->lastname));
						}else{
							$userstring .= htmlentities($subuserinfo->name);
						}
						$userstring .= '</a>';
						if(!Mobile::isMobileDevice()){
							if(Plugin_Events_Manager::eventHasRating($gevent->id,$user->id,$subscriber->userid) == 0){
								if($gevent->userid == User::Current()->id){
									$userstring .= ' (<a href="/bewertung-abgeben.html?eventid='.htmlentities($gevent->id).'&rateid='.htmlentities($subscriber->userid).'">'.Language::DirectTranslate('plugin_events_pagetypetitlerate').'</a>)';
								}
							}else{
								$userstring .= ' (<a href="'.Plugin_Profile_Manager::getRateLink($user->id,$subscriber->userid,$gevent->id).'">'.Language::DirectTranslate('plugin_profile_showrating').'</a>)';
							}
						}
						if(Mobile::isMobileDevice()){
							$userstring .= '<br />';
						}else{
							$userstring .= '</li>';
						}
					}
					if(Plugin_Events_Manager::countSubscribers($gevent->id) == 0){
						$userstring .= '';
					}else{
						$userstring .= '</ul>';
					}
					$template->assign_loop_var("oldgastgeber",$index,"oldteilnehmerstring",$userstring);
				}
			}else{
				if(User::Current()->id == $user->id){
					$template->show_if('nooldgastgeberown',true);
				}else{
					$template->show_if('nooldgastgeberother',true);
				}
			}
			$teilcount = 0;
			foreach(Plugin_Events_Manager::getSubscribesByUser($user->id) as $tevent){
				$teventdata = Plugin_Events_Manager::loadEvent($tevent->eventid,'old');
				if(is_object($teventdata)){
					$index = $template->add_loop_item('oldteilnehmer');
					if($teventdata->eventtype == 0){
						$template->assign_loop_var("oldteilnehmer",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype0')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_green').'" /></span></a>');
					}elseif($teventdata->eventtype == 1){
						$template->assign_loop_var("oldteilnehmer",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype1')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_red').'" /></span></a>');
					}elseif($teventdata->eventtype == 2){
						$template->assign_loop_var("oldteilnehmer",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype2')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('flag_yellow').'" /></span></a>');
					}elseif($teventdata->eventtype == 3){
						$template->assign_loop_var("oldteilnehmer",$index,"eventtype",'<a infotext="'.utf8_encode(Language::directTranslate('plugin_events_eventtype3')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('asterisk_orange').'" /></span></a>');
					}
					if(Plugin_Events_Manager::getFirstImageFromEvent($teventdata->id) != ''){
						$template->assign_loop_var("oldteilnehmer",$index,"colspan",'1');
						$template->assign_loop_var("oldteilnehmer",$index,"eventimage",'<td style="width:110px; text-align:center;"><img width="150" title="'.htmlentities($teventdata->eventname).'" src="/content/uploads/eat/thumbs/'.Plugin_Events_Manager::getFirstImageFromEvent($teventdata->id).'" /></td>');
					}else{
						$template->assign_loop_var("oldteilnehmer",$index,"colspan",'2');
						$template->assign_loop_var("oldteilnehmer",$index,"eventimage",'');
					}
					$template->assign_loop_var("oldteilnehmer",$index,"oldteileventid",htmlentities($teventdata->id));
					$template->assign_loop_var("oldteilnehmer",$index,"oldteileventname",htmlentities($teventdata->eventname));
					$template->assign_loop_var("oldteilnehmer",$index,"datum",Plugin_Events_Manager::formatDate($teventdata->eventdate));
					if(Plugin_Events_Manager::eventHasRating($teventdata->id,$user->id,$teventdata->userid) == 0){
						if($tevent->userid == User::Current()->id){
							$template->assign_loop_var("oldteilnehmer",$index,"oldrating",'(<a href="/bewertung-abgeben.html?eventid='.htmlentities($teventdata->id).'&rateid='.htmlentities($teventdata->userid).'">'.Language::DirectTranslate('plugin_events_rategastgeber').'</a>)');
						}else{
							$template->assign_loop_var("oldteilnehmer",$index,"oldrating",'');
						}
					}else{
						$template->assign_loop_var("oldteilnehmer",$index,"oldrating",'(<a href="'.Plugin_Profile_Manager::getRateLink($user->id,$teventdata->userid,$teventdata->id).'">'.Language::DirectTranslate('plugin_profile_showrating').'</a>)');
					}
					$teilcount++;
				}
			}
			if($teilcount >= 1){
				$template->show_if('hasteilnehmer2',true);
			}else{
				if(User::Current()->id == $user->id){
					$template->show_if('nooldteilnehmerown',true);
				}else{
					$template->show_if('nooldteilnehmerother',true);
				}
			}
			$ilike = '';
			if($user->fleisch == 1){
				$ilike .= '<li>'.Language::DirectTranslate('plugin_profile_ichmag').' '.Language::DirectTranslate('plugin_events_fleisch').'</li>';
			}
			if($user->fisch == 1){
				$ilike .= '<li>'.Language::DirectTranslate('plugin_profile_ichmag').' '.Language::DirectTranslate('plugin_events_fisch').'</li>';
			}
			if($user->seafood == 1){
				$ilike .= '<li>'.Language::DirectTranslate('plugin_profile_ichmag').' '.utf8_encode(Language::DirectTranslate('plugin_events_seafood')).'</li>';
			}
			if($user->vegi == 1){
				$ilike .= '<li>'.Language::DirectTranslate('plugin_profile_ichmag').' '.Language::DirectTranslate('plugin_profile_vegi').'</li>';
			}
			if($user->vegan == 1){
				$ilike .= '<li>'.Language::DirectTranslate('plugin_profile_ichmag').' '.Language::DirectTranslate('plugin_profile_vegan').'</li>';
			}
			if($user->scharf == 1){
				$ilike .= '<li>'.Language::DirectTranslate('plugin_profile_ichmag').' '.Language::DirectTranslate('plugin_events_scharf').'</li>';
			}
			if($user->alkohol == 1){
				$ilike .= '<li>'.Language::DirectTranslate('plugin_profile_ichmag').' '.Language::DirectTranslate('plugin_profile_noalk').'</li>';
			}
			if($user->bio == 1){
				$ilike .= '<li>'.Language::DirectTranslate('plugin_profile_iprefer').' '.Language::DirectTranslate('plugin_events_bio').'</li>';
			}
			if($user->saison == 1){
				$ilike .= '<li>'.Language::DirectTranslate('plugin_profile_iprefer').' '.Language::DirectTranslate('plugin_events_saisonal').'</li>';
			}
			if($user->regional == 1){
				$ilike .= '<li>'.Language::DirectTranslate('plugin_profile_iprefer').' '.Language::DirectTranslate('plugin_events_regional').'</li>';
			}
			if($user->koscher == 1){
				$ilike .= '<li>'.Language::DirectTranslate('plugin_profile_iprefer').' '.Language::DirectTranslate('plugin_profile_koscher').'</li>';
			}
			if($user->halal == 1){
				$ilike .= '<li>'.Language::DirectTranslate('plugin_profile_iprefer').' '.Language::DirectTranslate('plugin_profile_halal').'</li>';
			}
			if($ilike != ''){
				$template->show_if('ilike',true);
			}
			if($user->laktose == 1 or $user->gluten == 1 or $user->nusse == 1 or $user->allergy != ''){
				$iam = '';
				if($user->laktose == 1){
					$iam .= '<li>'.utf8_encode(Language::DirectTranslate('plugin_profile_lactose1')).' <a infotext="'.utf8_encode(Language::DirectTranslate('plugin_profile_lactose2')).'" target="_blank" href="https://de.wikipedia.org/wiki/Laktoseintoleranz" class="infotooltip"><span title=""><img title="" width="18px" src="'.Icons::getIcon('infobutton').'" /></span></a></li>';
				}
				if($user->gluten == 1){
					$iam .= '<li>'.utf8_encode(Language::DirectTranslate('plugin_profile_gluten1')).' <a infotext="'.utf8_encode(Language::DirectTranslate('plugin_profile_gluten2')).'" target="_blank" href="https://de.wikipedia.org/wiki/Zöliakie" class="infotooltip_event"><span title=""><img title="" width="18px" src="'.Icons::getIcon('infobutton').'" /></span></a></li>';
				}
				if($user->nusse == 1){
					$iam .= '<li>'.Language::DirectTranslate('plugin_profile_nuss').'</li>';
				}
				if($user->hefe == 1){
					$iam .= '<li>'.Language::DirectTranslate('plugin_profile_hefe').'</li>';
				}
				if($user->allergy != ''){
					$iam .= '<li>'.Language::DirectTranslate('plugin_profile_myallergy').': '.htmlentities($user->allergy).'</li>';
				}
				$template->assign_var('iam',$iam);
				$template->show_if('iam',true);
			}
			$template->assign_var('ilike',$ilike);
			if($ilike == '' && $iam == ''){
				if(User::Current()->name == $user->name){
					$template->show_if('noeatsme',true);
				}else{
					$template->show_if('noeats',true);
				}
			}
			if(Plugin_Profile_Manager::countFriendsFromUserId($user->id) > 0){
				$template->show_if('hasfriends',true);
			}else{
				if(User::Current()->id == $user->id){
					$template->show_if('menofriends',true);
				}else{
					$template->show_if('othernofriends',true);
				}
			}
			foreach(DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_friends WHERE userid1 = '".$user->id."' OR userid2 = '".$user->id."'") as $friend){
				if($friend->statusid == 1){
					$index = $template->add_loop_item('friends');
					if($friend->userid1 != $user->id){
						$userid = $friend->userid1;
					}
					if($friend->userid2 != $user->id){
						$userid = $friend->userid2;
					}
					$userdata = Plugin_Profile_Manager::getUserDataPerId($userid);
					$template->assign_loop_var("friends",$index,"usernamelink",htmlentities($userdata->name));
					if(Plugin_Profile_Manager::isFriend($userdata->id) or $userdata->id == User::Current()->id){
						$template->assign_loop_var("friends",$index,"vorname",htmlentities(ucfirst($userdata->firstname)));
						$template->assign_loop_var("friends",$index,"nachname",htmlentities(ucfirst($userdata->lastname)));
						$template->assign_loop_var("friends",$index,"username",'');
					}else{
						$template->assign_loop_var("friends",$index,"vorname",'');
						$template->assign_loop_var("friends",$index,"nachname",'');
						$template->assign_loop_var("friends",$index,"username",htmlentities($userdata->name));
					}
					if(Plugin_Profile_Manager::isVerified($userdata->name) == 2){
						$template->assign_loop_var("friends",$index,'verify','<a infotext="'.Language::DirectTranslate('plugin_profile_isverify').'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('tick').'" /></span></a>');
					}else{
						$template->assign_loop_var("friends",$index,'verify','<a infotext="'.Language::DirectTranslate('plugin_profile_noverify').'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('cancel').'" /></span></a>');
					}
					if(Plugin_OnlineStatus_OnlineStatus::isOnline($userid)){
						$template->assign_loop_var("friends",$index,"friendstatus",'<a infotext="'.ucfirst(Language::DirectTranslate('plugin_profile_online')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('status_online').'" /></span></a>');
					}else{
						$template->assign_loop_var("friends",$index,"friendstatus",'<a infotext="'.ucfirst(Language::DirectTranslate('plugin_profile_offline')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('status_offline').'" /></span></a>');
					}
					if($user->id == User::Current()->id){
						if(Mobile::isMobileDevice()){
							$template->assign_loop_var("friends",$index,"deletelink",'<a onclick="return confirm_alert(this);" href="/freunde.html?delete='.htmlentities($friend->id).'" class="buttonWrapper buttonRed buttonNo">'.utf8_encode(Language::DirectTranslate('plugin_profile_delfriend')).'</a>');
						}else{
							$template->assign_loop_var("friends",$index,"deletelink",'<a class="infotooltip_event" infotext="'.utf8_encode(Language::DirectTranslate('plugin_profile_delfriend')).'" onclick="return confirm_alert(this);" href="/freunde.html?delete='.htmlentities($friend->id).'"><span title=""><img src="'.Icons::getIcon('user_delete').'" /></span></a>');
						}
					}else{
						$template->assign_loop_var("friends",$index,"deletelink",'');
					}
				}
			}
			if($user->id == User::Current()->id){
				if(Plugin_Profile_Manager::getUserSetting('profile',$user->id) == 1){
					$template->show_if('nowatch',false);
					$template->show_if('profilewatch',true);
					$template->show_if('nothing',false);
					$watchcounter = 0;
					foreach(Plugin_Profile_Manager::getProfileWatchers($user->id) as $watch){
						$watchinfo = Plugin_Profile_Manager::getUserDataPerId($watch->watchid);
						if(Plugin_Profile_Manager::getUserSetting('profile',$watchinfo->id) == 1){
							$watchcounter++;
							$index = $template->add_loop_item('watch');
							$template->assign_loop_var("watch",$index,"datum",Plugin_Events_Manager::formatDate($watch->datum));
							$template->assign_loop_var("watch",$index,"usernamelink",htmlentities($watchinfo->name));
							if(Plugin_Profile_Manager::isFriend($watchinfo->id)){
								$template->assign_loop_var("watch",$index,"vorname",htmlentities(ucfirst($watchinfo->firstname)));
								$template->assign_loop_var("watch",$index,"nachname",htmlentities(ucfirst($watchinfo->lastname)));
								$template->assign_loop_var("watch",$index,"username",'');
							}else{
								$template->assign_loop_var("watch",$index,"vorname",'');
								$template->assign_loop_var("watch",$index,"nachname",'');
								$template->assign_loop_var("watch",$index,"username",htmlentities($watchinfo->name));
							}
							if(Plugin_OnlineStatus_OnlineStatus::isOnline($watchinfo->id)){
								$template->assign_loop_var("watch",$index,"onlinestatus",'<a infotext="'.ucfirst(Language::DirectTranslate('plugin_profile_online')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('status_online').'" /></span></a>');
							}else{
								$template->assign_loop_var("watch",$index,"onlinestatus",'<a infotext="'.ucfirst(Language::DirectTranslate('plugin_profile_offline')).'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('status_offline').'" /></span></a>');
							}
							if(Plugin_Profile_Manager::isVerified($watchinfo->name) == 2){
								$template->assign_loop_var("watch",$index,'verify','<a infotext="'.Language::DirectTranslate('plugin_profile_isverify').'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('tick').'" /></span></a>');
							}else{
								$template->assign_loop_var("watch",$index,'verify','<a infotext="'.Language::DirectTranslate('plugin_profile_noverify').'" class="infotooltip_event"><span title=""><img src="'.Icons::getIcon('cancel').'" /></span></a>');
							}
						}
					}
					if($watchcounter == 0){
						$template->show_if('nothing',true);
						$template->show_if('profilewatch',false);
					}
				}else{
					$template->show_if('nowatch',true);
					$template->show_if('profilewatch',false);
					$template->show_if('nothing',false);
				}
			}
			echo($template->getCode());
		}

		function getHeader(){
			echo('<script type="text/javascript" src="/system/plugins/profile/js/friends.js"></script>');
			echo('<script type="text/javascript" src="/system/plugins/profile/js/urlhash.js"></script>');
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
