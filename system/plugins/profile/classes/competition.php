<?PHP
/*
 * competition.php
 * 
 * Copyright 2016 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_Profile_Competition extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			$template->load('plugin_profile_competition');
			$template->show_if('wettbewerb',false);
			$template->show_if('teilgenommen',false);
			$template->show_if('danke',false);
			$userdata = Plugin_Profile_Manager::getUserData(User::Current()->name);
			$preiscounter = Plugin_Profile_Manager::wettbewerbCounter();
			$template->assign_var('counterpreis',$preiscounter);
			if($userdata->wettbewerb == 0){
				$template->show_if('wettbewerb',true);
			}else{
				$template->show_if('teilgenommen',true);
			}
			if(isset($_POST["teilnehmen"])){
				$mailtext = 'Hallo, der User: '.User::Current()->name.' ('.ucfirst($userdata->firstname).' '.ucfirst($userdata->lastname).') hat beim Wettbewerb mitgemacht<br><br>Er gab folgende Antworten:<br><br><b>Frage 1:</b><br>'.$_POST['frage1'].'<br><br><b>Frage 2:</b><ul>';
				foreach($_POST['frage2'] as $frage2){
					$mailtext .= '<li>'.$frage2.'</li>';
				}
				$mailtext .= '</ul><br><br><b>Frage 3:</b><br>'.$_POST['frage3'].'';
				$h2t = new Plugin_Mailer_html2text($mailtext);
				$subject = utf8_decode('Neue Wettbewerbsteilnahme');
				$mail = new Plugin_PHPMailer_PHPMailer;
				$mail->IsSMTP();
				$mail->AddAddress('info+wettbewerb@socialmeal.ch');
				$mail->WordWrap = 50;
				$mail->IsHTML(true);
				$mail->Subject = $subject;
				$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
				$mail->AltBody = $h2t->get_text();
				if(!$mail->Send()){
					echo('Mail Error:'.$mail->ErrorInfo);
				}
				DataBase::Current()->Execute("UPDATE {'dbprefix'}user SET wettbewerb = '1' WHERE id = '".User::Current()->id."' LIMIT 1");
				$template->show_if('wettbewerb',false);
				$template->show_if('teilgenommen',false);
				$template->show_if('danke',true);
			}
			echo($template->getCode());
		}

		function getHeader(){
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>