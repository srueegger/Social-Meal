<?PHP
/*
 * activate.php
 *
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
Language::ClearCache();
DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Profile_Editor', '{LANG:PLUGIN_PROFILE_PAGETYPETITLE}'); ");
DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Profile_EditProfile', '{LANG:PLUGIN_PROFILE_EDITPROFILEPAGETYPETITLE}');");
DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Profile_newPW', '{LANG:PLUGIN_PROFILE_NEWPWPAGETYPETITLE}');");
DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Profile_Friends', '{LANG:PLUGIN_PROFILE_FRIENDSPAGETYPETITLE}');");
DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Profile_Settings', '{LANG:PLUGIN_PROFILE_SETTINGSPAGETYPETITLE}');");
DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Profile_NewOrg', '{LANG:PLUGIN_PROFILE_NEWORGPAGETYPETITLE}');");
DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Profile_OrgProfile', '{LANG:PLUGIN_PROFILE_ORGPROFILEPAGETYPETITLE}');");
DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Profile_EditLanguages', '{LANG:PLUGIN_PROFILE_ELANGUAGESPAGETYPETITLE}');");
DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Profile_Verify', '{LANG:PLUGIN_PROFILE_VERIFYPAGETYPETITLE}');");
DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Profile_newAGB', '{LANG:PLUGIN_PROFILE_NEWAGBPAGETYPETITLE}');");
DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Profile_Invite', '{LANG:PLUGIN_PROFILE_INVITEPAGETYPETITLE}');");
DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Profile_AdminEditRatings', '{LANG:PLUGIN_PROFILE_RATEEDITPAGETYPETITLE}');");
DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Profile_AdminRangs', '{LANG:PLUGIN_PROFILE_RANGEDITPAGETYPETITLE}');");
DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (`id`, `class`, `name`) VALUES (NULL, 'Plugin_Profile_Competition', '{LANG:PLUGIN_PROFILE_COMPETITIONPAGETYPETITLE}');");
?>
