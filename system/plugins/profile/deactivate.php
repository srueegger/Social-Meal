<?PHP
/*
 * deactivate.php
 *
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
DataBase::Current()->Execute("DELETE FROM `{'dbprefix'}pagetypes` WHERE `class` = 'Plugin_Profile_Editor'; ");
DataBase::Current()->Execute("DELETE FROM `{'dbprefix'}pagetypes` WHERE `class` = 'Plugin_Profile_EditProfile'; ");
DataBase::Current()->Execute("DELETE FROM `{'dbprefix'}pagetypes` WHERE `class` = 'Plugin_Profile_newPW'; ");
DataBase::Current()->Execute("DELETE FROM `{'dbprefix'}pagetypes` WHERE `class` = 'Plugin_Profile_Friends'; ");
DataBase::Current()->Execute("DELETE FROM `{'dbprefix'}pagetypes` WHERE `class` = 'Plugin_Profile_Settings'; ");
DataBase::Current()->Execute("DELETE FROM `{'dbprefix'}pagetypes` WHERE `class` = 'Plugin_Profile_NewOrg'; ");
DataBase::Current()->Execute("DELETE FROM `{'dbprefix'}pagetypes` WHERE `class` = 'Plugin_Profile_OrgProfile'; ");
DataBase::Current()->Execute("DELETE FROM `{'dbprefix'}pagetypes` WHERE `class` = 'Plugin_Profile_EditLanguages'; ");
DataBase::Current()->Execute("DELETE FROM `{'dbprefix'}pagetypes` WHERE `class` = 'Plugin_Profile_Verify'; ");
DataBase::Current()->Execute("DELETE FROM `{'dbprefix'}pagetypes` WHERE `class` = 'Plugin_Profile_newAGB'; ");
DataBase::Current()->Execute("DELETE FROM `{'dbprefix'}pagetypes` WHERE `class` = 'Plugin_Profile_Invite'; ");
DataBase::Current()->Execute("DELETE FROM `{'dbprefix'}pagetypes` WHERE `class` = 'Plugin_Profile_AdminEditRatings'; ");
DataBase::Current()->Execute("DELETE FROM `{'dbprefix'}pagetypes` WHERE `class` = 'Plugin_Profile_AdminRangs'; ");
DataBase::Current()->Execute("DELETE FROM `{'dbprefix'}pagetypes` WHERE `class` = 'Plugin_Profile_Competition'; ");
?>
