<?PHP
/*
 * de.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	$tokens["plugin_description"]			= utf8_decode("Ermöglicht das Senden und Empfangen von Nachrichten zwischen den Usern");
	$tokens["pagetypetitlenewpm"]			= "Neue Nachricht schreiben";
	$tokens["pagetypetitleinbox"]			= "Posteingang";
	$tokens["pagetypetitleoutbox"]			= "Postausgang";
	$tokens["pagetypetitleadminsendall"]	= "Nachricht an alle Mitglieder schreiben";
	$tokens["to"]							= utf8_decode("Empfänger");
	$tokens["from"]							= "Absender";
	$tokens["message"]						= "Nachricht";
	$tokens["sendmessage"]					= "Nachricht senden";
	$tokens["subject"]						= "Betreff";
	$tokens["nomessages"]					= "Es sind keine Nachrichten vorhanden!";
	$tokens["date"]							= "Datum";
	$tokens['markdel']						= utf8_decode("Markierte löschen");
	$tokens['delok']						= utf8_decode("Die Nachricht(en) wurde(n) erfolgreich gelöscht.");
	$tokens['nosubject']					= "Kein Betreff";
	$tokens["sendok"]						= "Deine Nachricht wurde erfolgreich gesendet.";
	$tokens["gooutbox"]						= "Zum Postausgang";
	$tokens["goinbox"]						= "Zum Posteingang";
	$tokens["pmopen"]						= utf8_decode("Nachricht öffnen");
	$tokens["pmdel"]						= utf8_decode("Nachricht löschen");
	$tokens["preview"]						= "Vorschau";
	$tokens["inbox1"]						= "Bitte beachte unsere";
	$tokens["inbox2"]						= "Netiquette";
	$tokens["newpm1"]						= "Deine Nachricht wurde erfolgreich gesendet.";
	$tokens["newpm2"]						= "Du kannst keine Nachricht an dich selber schreiben!";
	$tokens["newpm3"]						= "Der Benutzer";
	$tokens["newpm4"]						= "existiert nicht! Die Nachricht wurde nicht gesendet.";
	$tokens["newpm5"]						= utf8_decode("Oder einen Freund auswählen");
	$tokens["admininfo"]					= "Zum formatieren der Nachricht kannst du die Social Meal - BBCodes verwenden. Du kannst die Nachricht erst senden, wenn du dir die Vorschau angesehen hast! Bitte nutze dieses Tool nicht um sinnloses Spam zu versenden.";
	$tokens["previewtitle"]					= "Vorschau der Nachricht";
?>
