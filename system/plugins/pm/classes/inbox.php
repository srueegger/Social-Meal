<?PHP
/*
 * inbox.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_PM_Inbox extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			$template->load('plugin_pm_inbox');
			$template->show_if('delmsg',false);
			$template->show_if('msg',false);
			$template->show_if('showmessage',false);
			if(!isset($_GET['message'])){
				$_GET['message'] = '';
			}
			if(isset($_GET['del']) != ''){
				Plugin_PM_Manager::deleteMessage($_GET['del']);
				$template->show_if('delmsg',true);
			}
			if(isset($_POST['plugin_pm_delsubmit'])){
				foreach ($_POST['id'] as $mid) {
					Plugin_PM_Manager::deleteMessage($mid);
				}
				$template->show_if('delmsg',true);
			}
			if(isset($_POST['plugin_pm_send']) && isset($_POST['message']) != '' && Plugin_Profile_Manager::userExist($_POST['to']) && $_POST['to'] != User::Current()->name){
				if(!Plugin_Profile_Manager::profileIsDel($_POST['to'])){
					if($_POST['subject'] == ''){
						$_POST['subject'] = Language::directTranslate('plugin_pm_nosubject');
					}
					Plugin_PM_Manager::saveMessage($_POST);
					$template->show_if('msg',true);
				}
			}
			if(Plugin_PM_Manager::countMessages(User::Current()->id) > 0){
				if(Plugin_PM_Manager::MessageExists($_GET['message']) && Plugin_PM_Manager::hasReadRight($_GET['message'])){
					$template->show_if('showmessage',true);
					$message = Plugin_PM_Manager::loadMessage($_GET['message']);
					$pmuserinfo = Plugin_Profile_Manager::getUserDataPerId($message->fromid);
					$template->assign_var('getshowusername',Plugin_Profile_Manager::showUsername($pmuserinfo->name,'author'));
					if(Plugin_Profile_Manager::isFriend($pmuserinfo->id) or $pmuserinfo->id == User::Current()->id){
						$template->assign_var('showusername',htmlentities(ucfirst($pmuserinfo->firstname).' '.ucfirst($pmuserinfo->lastname)));
					}else{
						$template->assign_var('showusername',htmlentities($pmuserinfo->name));
					}
					$template->assign_var('subject',htmlentities($message->subject));
					$template->assign_var('username',htmlentities($pmuserinfo->name));
					$template->assign_var('datum',Plugin_Events_Manager::formatDate($message->datum));
					if(Plugin_Profile_Manager::getUserImage($pmuserinfo->name) == ''){
						$template->assign_var('imgurl', Settings::getInstance()->get("host").'system/skins/socialmeal/images/avatar.jpg');
					}else{
						$fileend = substr(Plugin_Profile_Manager::getUserImage($pmuserinfo->name), -3);
						if($fileend == "bmp"){
							$template->assign_var('imgurl', Settings::getInstance()->get("host").'content/uploads/profile/'.Plugin_Profile_Manager::getUserImage($pmuserinfo->name));
						}else{
							$template->assign_var('imgurl', Settings::getInstance()->get("host").'content/uploads/profile/thumbs/'.Plugin_Profile_Manager::getUserImage($pmuserinfo->name));
						}
					}
					$bb = new Plugin_BBCode_Translator();
					$template->assign_var('message',$bb->replace($message->message));
					$template->assign_var('oldmessage',"\n\n\n[quote]".$message->message.'[/quote]');
					Plugin_PM_Manager::setAsRead($_GET['message']);
				}else{
					$template->show_if('showmessage',false);
				}
				$template->show_if('NOMESSAGE',false);
				$template->show_if('MESSAGE',true);
				$allmessages = Plugin_PM_Manager::getAllMessagesToUser(User::Current()->id);
				$countmessages = count($allmessages);
				$nav = new Plugin_PageNavigation_PageNavigation($countmessages,Plugin_Profile_Manager::getUserSetting('anzahlpm',User::Current()->id));
				$allmessages = Plugin_PM_Manager::getAllMessagesToUser(User::Current()->id,$nav->sql_limit);
				foreach($allmessages as $pm){
					$index = $template->add_loop_item('messages');
					$userinfo = Plugin_Profile_Manager::getUserDataPerId($pm->fromid);
					if($pm->readstatus == 0){
						$template->assign_loop_var("messages",$index,"subject",'<strong><font size="4"><a href="./posteingang.html?message='.htmlentities($pm->id).'#gomessage">'.htmlentities($pm->subject).'</a></font></strong>');
						$template->assign_loop_var("messages",$index,"readstatus",Icons::getIcon('LIGHTBULB'));
					}else{
						$template->assign_loop_var("messages",$index,"subject",'<strong><a href="./posteingang.html?message='.htmlentities($pm->id).'#gomessage">'.htmlentities($pm->subject).'</a></strong>');
						$template->assign_loop_var("messages",$index,"readstatus",Icons::getIcon('LIGHTBULB_OFF'));
					}
					$template->assign_loop_var("messages",$index,"showfromname",Plugin_Profile_Manager::showUsername($userinfo->name));
					$template->assign_loop_var("messages",$index,"datum",Plugin_Events_Manager::formatDate($pm->datum));
					$template->assign_loop_var("messages",$index,"id",$pm->id);
				}
			}else{
				$template->show_if('NOMESSAGE',true);
				$template->show_if('MESSAGE',false);
			}
			echo($template->getCode());
			echo('<br /><br /><br />'.$nav->createPageBar());
		}

		function getHeader(){
			echo('<script type="text/javascript" src="'.Settings::getInstance()->get("host").'system/plugins/pm/js/smilie.js"></script>');
			echo('<script type="text/javascript" src="'.Settings::getInstance()->get("host").'system/plugins/pm/js/checkall.js"></script>');
			echo('<link href="/system/plugins/bbcode/css/editor.css" rel="stylesheet" type="text/css" />');
			echo('<script type="text/javascript" src="/system/plugins/bbcode/js/editor.js"></script>');
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
