<?PHP
/*
 * adminsendall.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_PM_AdminSendAll extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			$template->load('plugin_pm_adminsendall');
			$template->show_if('preview',false);
			$template->show_if('sendok',false);
			if(isset($_POST['preview'])){
				$template->show_if('preview',true);
				$bb = new Plugin_BBCode_Translator();
				$template->assign_var('subject',htmlentities($_POST['subject']));
				$template->assign_var('message',$bb->replace($_POST['message']));
				$template->assign_var('ofmessage',htmlentities($_POST['message']));
			}else{
				$template->assign_var('subject','');
				$template->assign_var('ofmessage','');
			}
			if(isset($_POST['sendmessage'])){
				foreach(Plugin_Profile_Manager::getAllUsers() as $user){
					$data = array();
					$data['subject'] = $_POST['subject'];
					$data['message'] = $_POST['message'];
					$data['to'] = $user->name;
					Plugin_PM_Manager::saveMessage($data);
					unset($data);
				}
				$template->show_if('sendok',true);
			}
			echo($template->getCode());
		}

		function getHeader(){
			echo('<script type="text/javascript" src="'.Settings::getInstance()->get("host").'system/plugins/pm/js/smilie.js"></script>');
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
