<?php
/*
 * manager.php
 *
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
class Plugin_PM_Manager {

	public static function saveMessage($data){
		$to			= DataBase::Current()->EscapeString($data['to']);
		$toid 		= DataBase::Current()->ReadField("SELECT id FROM {'dbprefix'}user WHERE name = '".$to."'");
		if($data['subject'] == ''){
			$subject = '('.Language::directTranslate('plugin_pm_nosubject').')';
		}else{
			$subject	= DataBase::Current()->EscapeString($data['subject']);
		}
		$message	= DataBase::Current()->EscapeString($data['message']);
		DataBase::Current()->Execute("INSERT INTO {'dbprefix'}plugin_pm SET fromid = '".User::Current()->id."', toid = '".$toid."', subject = '".$subject."', message = '".$message."', datum = NOW() ");
		Plugin_Profile_Manager::updatePunkte(1);
		if(Plugin_Profile_Manager::getUserSetting('newpm',$toid) == 1){
			$touserinfo = Plugin_Profile_Manager::getUserDataPerId($toid);
			$subject = utf8_decode("Du hast eine neue Nachricht auf Social Meal erhalten");
			$mailtext = 'Liebe*r '.ucfirst($touserinfo->firstname).'<br><br>Gerne möchten wir dich darüber informieren, dass du eine neue Nachricht von dem User <a href="'.Settings::getInstance()->get("host").'mein-profil.html?profile='.User::Current()->name.'">'.User::Current()->name.'</a> erhalten hast.<br><br>Hier kannst du den <a href="'.Settings::getInstance()->get("host").'posteingang.html">Posteingang</a> öffnen.<br><br>Beachte: Bitte befolge die <a href="'.Settings::getInstance()->get("host").'FAQ/netiquette.html">Netiquette</a>.<br><br>Falls du der Meinung bist, dass dieser User die Nachrichten-Funktion missbräuchlich verwendet, melde uns das bitte über die entsprechende <a href="'.Settings::getInstance()->get("host").'melden.html?source=mail&g=newpm_'.User::Current()->id.'">Melde-Seite</a>.<br><br>Wir wünschen dir weiterhin gute Interaktionen in unserer Community!<br>Social Meal<br><br>P.S.: Diesen E-Mail-Typ kannst du in den <a href="'.Settings::getInstance()->get("host").'meine-einstellungen.html">Einstellungen</a> abbestellen, falls er dir überflüssig erscheint.';
			$h2t = new Plugin_Mailer_html2text($mailtext);
			$mail = new Plugin_PHPMailer_PHPMailer;
			$mail->IsSMTP();
			$mail->AddAddress($touserinfo->email);
			$mail->WordWrap = 50;
			$mail->IsHTML(true);
			$mail->Subject = $subject;
			$mail->Body = Plugin_Mailer_Mailer::makeHtml($subject,$mailtext);
			$mail->AltBody = $h2t->get_text();
			if(!$mail->Send()){
				echo('Mail Error:'.$mail->ErrorInfo);
			}
		}
	}

	public static function countMessages($userid){
		$userid 	= DataBase::Current()->EscapeString($userid);
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_pm WHERE toid = '".$userid."' AND del = '0'");
	}

	public static function countMessagesOutbox(){
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_pm WHERE fromid = '".User::Current()->id."' AND del = '0'");
	}

	public static function countUnreadMessages($userid){
		$userid 	= DataBase::Current()->EscapeString($userid);
		return DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_pm WHERE toid = '".$userid."' AND del = '0' AND readstatus = '0'");
	}

	public static function getAllMessagesToUser($userid,$limit = 'none'){
		$userid = DataBase::Current()->EscapeString($userid);
		if($limit == 'none'){
			$ret = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_pm WHERE toid = '".$userid."' AND del = '0' ORDER BY datum DESC");
		}else{
			$ret = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_pm WHERE toid = '".$userid."' AND del = '0' ORDER BY datum DESC LIMIT ".$limit);
		}
		return $ret;
	}

	public static function getAllMessagesFromUser($limit = 'none'){
		if($limit == 'none'){
			$ret = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_pm WHERE fromid = '".User::Current()->id."' AND delout = '0' ORDER BY datum DESC");
		}else{
			$ret = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_pm WHERE fromid = '".User::Current()->id."' AND delout = '0' ORDER BY datum DESC LIMIT ".$limit);
		}
		return $ret;
	}

	public static function MessageExists($id){
		$id = DataBase::Current()->EscapeString($id);
		$count = DataBase::Current()->ReadField("SELECT COUNT(*) FROM {'dbprefix'}plugin_pm WHERE id = '".$id."' AND del = '0'");
		if($count == 1){
			return true;
		}else{
			return false;
		}
	}

	public static function hasReadRight($id){
		$data = self::loadMessage($id);
		if($data->toid == User::Current()->id or $data->fromid == User::Current()->id){
			return true;
		}else{
			return false;
		}
	}

	public static function loadMessage($id){
		$id = DataBase::Current()->EscapeString($id);
		return DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}plugin_pm WHERE id = '".$id."'");
	}

	public static function setAsRead($id){
		$id = DataBase::Current()->EscapeString($id);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_pm SET readstatus = '1' WHERE id = '".$id."' LIMIT 1");
	}

	public static function deleteMessage($id){
		$id = DataBase::Current()->EscapeString($id);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_pm SET del = '1' WHERE id = '".$id."' LIMIT 1");
	}

	public static function deleteMessageOutbox($id){
		$id = DataBase::Current()->EscapeString($id);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}plugin_pm SET delout = '1' WHERE id = '".$id."' LIMIT 1");
	}

	public static function formatSmilies($text){
		$text = str_replace(':-)','<img src="'.Icons::getIcon('EMOTICON_SMILE').'">', $text);
		$text = str_replace(':)','<img src="'.Icons::getIcon('EMOTICON_SMILE').'">', $text);
		$text = str_replace(':-D','<img src="'.Icons::getIcon('EMOTICON_GRIN').'">', $text);
		$text = str_replace(':D','<img src="'.Icons::getIcon('EMOTICON_GRIN').'">', $text);
		$text = str_replace(':-P','<img src="'.Icons::getIcon('EMOTICON_TONGUE').'">', $text);
		$text = str_replace(':P','<img src="'.Icons::getIcon('EMOTICON_TONGUE').'">', $text);
		$text = str_replace(':-O','<img src="'.Icons::getIcon('EMOTICON_SURPRISED').'">', $text);
		$text = str_replace(':O','<img src="'.Icons::getIcon('EMOTICON_SURPRISED').'">', $text);
		$text = str_replace(':-(','<img src="'.Icons::getIcon('EMOTICON_UNHAPPY').'">', $text);
		$text = str_replace(':(','<img src="'.Icons::getIcon('EMOTICON_UNHAPPY').'">', $text);
		return $text;
	}

}
?>
