<?PHP
/*
 * newpm.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_PM_newPM extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			$template->load('plugin_pm_newpm');
			$template->show_if('msg',false);
			$template->show_if('showfriend',false);
			$template->show_if('messagetome',false);
			$template->show_if('nouser',false);
			if(isset($_POST['plugin_pm_send'])){
				if(isset($_POST['tofriend']) && $_POST['tofriend'] != 'nofriend'){
					$_POST['to'] = $_POST['tofriend'];
					Plugin_PM_Manager::saveMessage($_POST);
					$template->show_if('msg',true);
					unset($_POST);
				}elseif(isset($_POST['to'])){
					if($_POST['to'] != User::Current()->name){
						if(Plugin_Profile_Manager::userExist($_POST['to'])){
							if(!Plugin_Profile_Manager::profileIsDel($_POST['to'])){
								Plugin_PM_Manager::saveMessage($_POST);
								$template->show_if('msg',true);
								unset($_POST);
							}else{
								//Benuter ist gelöscht
								$template->show_if('nouser',true);
								$template->assign_var('nousername',htmlentities($_POST['to']));
							}
						}else{
							//Benutzer existiert nicht
							$template->show_if('nouser',true);
							$template->assign_var('nousername',htmlentities($_POST['to']));
						}
					}else{
						//Nachricht an sich selbst ;-)
						$template->show_if('messagetome',true);
					}
				}
			}
			if(Plugin_Profile_Manager::countFriends() > 0){
				$template->show_if('showfriend',true);
				foreach(Plugin_Profile_Manager::getFriends() as $friend){
					if($friend->statusid == 1){
						$index = $template->add_loop_item('friends');
						if($friend->userid1 != User::Current()->id){
							$userid = $friend->userid1;
						}
						if($friend->userid2 != User::Current()->id){
							$userid = $friend->userid2;
						}
						$userdata = Plugin_Profile_Manager::getUserDataPerId($userid);
						$template->assign_loop_var("friends",$index,"lusername",htmlentities($userdata->name));
						$template->assign_loop_var("friends",$index,"firstname",htmlentities(ucfirst($userdata->firstname)));
						$template->assign_loop_var("friends",$index,"lastname",htmlentities(ucfirst($userdata->lastname)));
					}
				}
			}
			if(isset($_GET['an']) != ''){
				$template->assign_var('to',htmlentities($_GET['an']));
			}else{
				$template->assign_var('to','');
			}
			if(isset($_POST['subject'])){
				$template->assign_var('subject',htmlentities($_POST['subject']));
			}else{
				$template->assign_var('subject','');
			}
			if(isset($_POST['message'])){
				$template->assign_var('message',htmlentities($_POST['message']));
			}else{
				$template->assign_var('message','');
			}
			echo($template->getCode());
		}

		function getHeader(){
			echo('<script type="text/javascript" src="'.Settings::getInstance()->get("host").'system/plugins/pm/js/smilie.js"></script>');
			echo('<script type="text/javascript" src="'.Settings::getInstance()->get("host").'system/plugins/pm/js/formcheck.js"></script>');
			echo('<link href="/system/plugins/bbcode/css/editor.css" rel="stylesheet" type="text/css" />');
			echo('<script type="text/javascript" src="/system/plugins/bbcode/js/editor.js"></script>');
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
