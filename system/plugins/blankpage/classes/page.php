<?php 
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

	abstract class Plugin_BlankPage_Page extends Editor
	{
		/**
		 * Constructor
		 * @param Page $page 
		 */
		public function __construct(Page $page)
		{
			$this->page = $page;
		}
		
		/**
		 * The first thing that is calles. 
		 */
		public function ExecuteHttpHeader()
		{
			//Clear may displayed stuff
			ob_end_clean();
			
			//Call the display-function of the child class
			$this->display();
			
			//End execution
			exit;
		}
		
		/**
		 * We don't need this. 
		 */
		public function getHeader()
		{
		}
		
		/**
		 * Force url entering
		 * @return string
		 */
		public function getEditableCode()
		{
		}
		
		/**
		 * We don't need this. 
		 */
		public function save(Page $newPage, Page $oldPage)
		{
		}
	}
?>
