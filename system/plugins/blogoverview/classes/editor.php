<?PHP
/*
 * editor.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_BlogOverview_Editor extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
			if(isset($_GET['cat'])){
				$catname = Plugin_Blogentries_Manager::getCatName($_GET['cat']);
				$page->title = ''.utf8_encode(Language::DirectTranslate('plugin_bloglanguagepacks_entriespercat')).' '.htmlentities($catname);
			}elseif(isset($_GET['author'])){
				$authordata = Plugin_Profile_Manager::getUserData($_GET['author']);
				if(Plugin_Profile_Manager::isFriend($authordata->id) or $authordata->id == User::Current()->id){
					$page->title = Language::DirectTranslate('plugin_bloglanguagepacks_entry1').': '.$authordata->firstname.' '.$authordata->lastname;
				}else{
					$page->title = ''.Language::DirectTranslate('plugin_bloglanguagepacks_entry1').': '.htmlentities($authordata->name);
				}
			}
		}
	
		public function display(){
				echo('<p>'.utf8_encode(Language::DirectTranslate('plugin_bloglanguagepacks_over1')).' <a href="/FAQ/so-funktioniert-unser-blog.html" target="_blank">'.Language::DirectTranslate('plugin_bloglanguagepacks_over2').'</a>.</p>');
				$plugin_blogoverview_settings = Plugin_PluginData_Data::getData('settings','plugin','blogoverview');
				$plugin_blogoverview_datas = Plugin_BlogOverview_Manager::getEntriesData();
				if(isset($_GET['cat'])){
					$catcount = 0;
					foreach(Plugin_BlogOverview_Manager::getEntriesData() as $cc){
						$entryMetaData = Plugin_PluginData_Data::getData('blogentrymeta_'.$cc->id.'','plugin','blogentries');
						if($entryMetaData['category'] == $_GET['cat']){
							$catcount++;
						}
					}
					$plugin_blogoverview_count = $catcount;
				}elseif(isset($_GET['author'])){
					$authorcount = 0;
					foreach(Plugin_BlogOverview_Manager::getEntriesData() as $ac){
						$entryMetaData = Plugin_PluginData_Data::getData('blogentrymeta_'.$ac->id.'','plugin','blogentries');
						if($entryMetaData['author'] == $_GET['author']){
							$authorcount++;
						}
					}
					$plugin_blogoverview_count = $authorcount;
				}else{
					$plugin_blogoverview_count = count($plugin_blogoverview_datas);
				}
				$plugin_blogoverview_nav = new Plugin_PageNavigation_PageNavigation($plugin_blogoverview_count,$plugin_blogoverview_settings['entriesperpage']);
				$plugin_blogoverview_datas = Plugin_BlogOverview_Manager::getEntriesDataLimit($plugin_blogoverview_nav->sql_limit);
				foreach($plugin_blogoverview_datas as $plugin_blogoverview_data){
					$entryData = Plugin_BlogEntries_Manager::getEntryData($plugin_blogoverview_data->id);
					$entryMetaData = Plugin_PluginData_Data::getData('blogentrymeta_'.$plugin_blogoverview_data->id.'','plugin','blogentries');
					$template = new Template();
					if(Mobile::isMobileDevice()){
						$template->load('plugin_blogentries_entry.mobile');
					}else{
						$template->load('plugin_blogentries_entry');
					}
					$template->show_if('readmore',true);
					$template->show_if('authorinfo',false);
					$template->show_if('showtitle',true);
					if(isset($_GET['author'])){
						//Nur Beiträge von einem Autor anzeigen
						if($entryMetaData['author'] == $_GET['author']){
							if($plugin_blogoverview_settings['excerpt'] > 0){
								$plugin_blogoverview_content = Plugin_BlogOverview_Manager::excerptContent($entryData->content,$plugin_blogoverview_settings['excerpt'],UrlRewriting::GetUrlByAlias($entryData->alias));
							}else{
								$plugin_blogoverview_content = $entryData->content;
							}
							if(Mobile::isMobileDevice()){
								$template->load('plugin_blogentries_entry.mobile');
							}else{
								$template->load('plugin_blogentries_entry');
							}
							if(Mobile::isMobileDevice()){
								$template->assign_var('SHOWAUTHOR',Plugin_Profile_Manager::showUsername($entryMetaData['author'],'singleIconWrapper singleIconText iconEditDark postInfo postAuthor postInfoNoMargin'));
							}else{
								$template->assign_var('SHOWAUTHOR',Plugin_Profile_Manager::showUsername($entryMetaData['author'],'author'));
							}
							$template->assign_var('ENTRYTITLE',htmlentities($entryData->title));
							$template->assign_var('ENTRYURL',UrlRewriting::GetUrlByAlias($entryData->alias));
							$template->assign_var('ENTRYCONTENT',$plugin_blogoverview_content);
							$template->assign_var('DATETIME',Plugin_Events_Manager::formatDate($entryMetaData['lastchange']));
							$template->assign_var('AUTHORNAMEURL',htmlentities($entryMetaData['author']));
							$template->assign_var('AUFRUFE',htmlentities($entryData->aufrufe));
							if(isset($entryMetaData['image']) != ''){
								$template->show_if('blogimage',true);
								$template->assign_var('IMGPATH',htmlentities($entryMetaData['image']));
								$imgdata = ImageServer::getImageData($entryMetaData['image']);
								if($imgdata->description != ''){
									$template->show_if('imgdesc',true);
									$template->assign_var('imgdesc',htmlentities($imgdata->description));
								}else{
									$template->show_if('imgdesc',false);
								}
							}else{
								$template->show_if('blogimage',false);
								$template->show_if('imgdesc',false);
							}
							$template->assign_var('CATID',htmlentities($entryMetaData['category']));
							$template->assign_var('CATNAME',Plugin_BlogEntries_Manager::getCatName($entryMetaData['category']));
							if(Plugin_Blogcomments_Manager::countComments($entryData->id) == 0){
								$template->assign_var('comments',Language::DirectTranslate('plugin_bloglanguagepacks_nocomment'));
							}elseif(Plugin_Blogcomments_Manager::countComments($entryData->id) == 1){
								$template->assign_var('comments',Plugin_Blogcomments_Manager::countComments($entryData->id).' '.Language::DirectTranslate('plugin_bloglanguagepacks_comment'));
							}else{
								$template->assign_var('comments',Plugin_Blogcomments_Manager::countComments($entryData->id).' '.Language::DirectTranslate('plugin_bloglanguagepacks_comments'));
							}
							echo $template->getCode();
						}
					}elseif(isset($_GET['cat']) != ''){
						//Nur Beiträge von einer Kategorie anzeigen
						if($entryMetaData['category'] == $_GET['cat']){
							if($plugin_blogoverview_settings['excerpt'] > 0){
								$plugin_blogoverview_content = Plugin_BlogOverview_Manager::ExcerptContent($entryData->content,$plugin_blogoverview_settings['excerpt'],UrlRewriting::GetUrlByAlias($entryData->alias));
							}else{
								$plugin_blogoverview_content = $entryData->content;
							}
							if(Mobile::isMobileDevice()){
								$template->assign_var('SHOWAUTHOR',Plugin_Profile_Manager::showUsername($entryMetaData['author'],'singleIconWrapper singleIconText iconEditDark postInfo postAuthor postInfoNoMargin'));
							}else{
								$template->assign_var('SHOWAUTHOR',Plugin_Profile_Manager::showUsername($entryMetaData['author'],'author'));
							}
							$template->assign_var('ENTRYTITLE',htmlentities($entryData->title));
							$template->assign_var('ENTRYURL',UrlRewriting::GetUrlByAlias($entryData->alias));
							$template->assign_var('ENTRYCONTENT',$plugin_blogoverview_content);
							$template->assign_var('DATETIME',Plugin_Events_Manager::formatDate($entryMetaData['lastchange']));
							$template->assign_var('AUTHORNAMEURL',htmlentities($entryMetaData['author']));
							$template->assign_var('AUFRUFE',htmlentities($entryData->aufrufe));
							if(isset($entryMetaData['image']) != ''){
								$template->show_if('blogimage',true);
								$template->assign_var('IMGPATH',htmlentities($entryMetaData['image']));
								$imgdata = ImageServer::getImageData($entryMetaData['image']);
								if($imgdata->description != ''){
									$template->show_if('imgdesc',true);
									$template->assign_var('imgdesc',htmlentities($imgdata->description));
								}else{
									$template->show_if('imgdesc',false);
								}
							}else{
								$template->show_if('blogimage',false);
								$template->show_if('imgdesc',false);
							}
							$template->assign_var('CATNAME',Plugin_BlogEntries_Manager::getCatName($entryMetaData['category']));
							$template->assign_var('CATID',htmlentities($entryMetaData['category']));
							if(Plugin_Blogcomments_Manager::countComments($entryData->id) == 0){
								$template->assign_var('comments',Language::DirectTranslate('plugin_bloglanguagepacks_nocomment'));
							}elseif(Plugin_Blogcomments_Manager::countComments($entryData->id) == 1){
								$template->assign_var('comments',Plugin_Blogcomments_Manager::countComments($entryData->id).' '.Language::DirectTranslate('plugin_bloglanguagepacks_comment'));
							}else{
								$template->assign_var('comments',Plugin_Blogcomments_Manager::countComments($entryData->id).' '.Language::DirectTranslate('plugin_bloglanguagepacks_comments'));
							}
							echo $template->getCode();
						}
					}else{
						//Alle Beiträge anzeigen
						if($plugin_blogoverview_settings['excerpt'] > 0){
							$plugin_blogoverview_content = Plugin_BlogOverview_Manager::ExcerptContent($entryData->content,$plugin_blogoverview_settings['excerpt'],UrlRewriting::GetUrlByAlias($entryData->alias));
						}else{
							$plugin_blogoverview_content = $entryData->content;
						}
						if(Mobile::isMobileDevice()){
							$template->assign_var('SHOWAUTHOR',Plugin_Profile_Manager::showUsername($entryMetaData['author'],'singleIconWrapper singleIconText iconEditDark postInfo postAuthor postInfoNoMargin'));
						}else{
							$template->assign_var('SHOWAUTHOR',Plugin_Profile_Manager::showUsername($entryMetaData['author'],'author'));
						}
						$template->assign_var('ENTRYTITLE',htmlentities($entryData->title));
						$template->assign_var('ENTRYURL',UrlRewriting::GetUrlByAlias($entryData->alias));
						$template->assign_var('ENTRYCONTENT',$plugin_blogoverview_content);
						$template->assign_var('DATETIME',Plugin_Events_Manager::formatDate($entryMetaData['lastchange']));
						$template->assign_var('AUFRUFE',htmlentities($entryData->aufrufe));
						if(isset($entryMetaData['image']) != ''){
							$template->show_if('blogimage',true);
							$template->assign_var('IMGPATH',htmlentities($entryMetaData['image']));
							$imgdata = ImageServer::getImageData($entryMetaData['image']);
							if($imgdata->description != ''){
								$template->show_if('imgdesc',true);
								$template->assign_var('imgdesc',htmlentities($imgdata->description));
							}else{
								$template->show_if('imgdesc',false);
							}
						}else{
							$template->show_if('blogimage',false);
							$template->show_if('imgdesc',false);
						}
						$template->assign_var('CATID',$entryMetaData['category']);
						$template->assign_var('CATNAME',Plugin_Blogentries_Manager::getCatName($entryMetaData['category']));
						if(Plugin_Blogcomments_Manager::countComments($entryData->id) == 0){
							$template->assign_var('comments',Language::DirectTranslate('plugin_bloglanguagepacks_nocomment'));
						}elseif(Plugin_Blogcomments_Manager::countComments($entryData->id) == 1){
							$template->assign_var('comments',Plugin_Blogcomments_Manager::countComments($entryData->id).' '.Language::DirectTranslate('plugin_bloglanguagepacks_comment'));
						}else{
							$template->assign_var('comments',Plugin_Blogcomments_Manager::countComments($entryData->id).' '.Language::DirectTranslate('plugin_bloglanguagepacks_comments'));
						}
						echo $template->getCode();
					}
				}
				echo $plugin_blogoverview_nav->createPageBar();
				if(!Mobile::isMobileDevice()){
					//Sidebar
					include('./system/skins/socialmeal/sidebar.php');
				}
		}

		function getHeader(){
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			if(isset($_POST['save'])){
				DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_plugindata_data WHERE area = 'blogoverview' AND property = 'settings'");
				$plugin_blogoverview_settings = array();
				$plugin_blogoverview_settings['excerpt'] = DataBase::Current()->EscapeString($_POST['plugin_blogoverview_excerpt']);
				$plugin_blogoverview_settings['entriesperpage'] = DataBase::Current()->EscapeString($_POST['plugin_blogoverview_entriesperpage']);
				Plugin_PluginData_Data::setData('settings',$plugin_blogoverview_settings,'blogoverview','plugin');
				Cache::clear();
			}
			
			$plugin_blogoverview_datas = Plugin_PluginData_Data::getData('settings','plugin','blogoverview');
			if($plugin_blogoverview_datas['excerpt'] == ''){
				$plugin_blogoverview_datas['excerpt'] = 0;
			}
			$template = new Template();
			$template->load('plugin_blogoverview_overvieweditable');
			$template->assign_var('PLUGIN_BLOGOVERVIEW_EXCERPT',$plugin_blogoverview_datas['excerpt']);
			$template->assign_var('PLUGIN_BLOGOVERVIEW_ENTRIESPERPAGE',$plugin_blogoverview_datas['entriesperpage']);
			$template->assign_var('PLUGIN_BLOGOVERVIEW_SAVEURL',UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias));
			return $template->getCode();
		}
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
				$this->page = $newPage;
				$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
				$this->page->save();
		}
		
	}
?>
