<?php
/*
 * manager.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
class Plugin_BlogOverview_Manager{

	public static function getEntriesDataLimit($sqllimit){
		return DataBase::Current()->ReadRows("SELECT id FROM {'dbprefix'}pages WHERE editor = 'Plugin_BlogEntries_Editor' ORDER BY id DESC LIMIT ".$sqllimit);
	}
	
	public static function getEntriesData(){
		return DataBase::Current()->ReadRows("SELECT id FROM {'dbprefix'}pages WHERE editor = 'Plugin_BlogEntries_Editor' ORDER BY id DESC");
	}
	
	public static function ExcerptContent($content,$marker,$url){
		if(strlen($content) >= $marker){
			$content = preg_replace("/[^ ]*$/", '',substr($content,0,$marker));
			$content .= '...';
			return $content;
		}else{
			return $content;
		}
	}
}
?>
