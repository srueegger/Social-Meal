<?PHP
	$tokens['plugin_description'] = "Stellt Plugins die Möglichkeit zur Verfügung, eine Blätterfunktion zu realisieren.";
	$tokens['go'] = "Los";
	$tokens['gotosite'] = "Gehe zur Seite %p";
	$tokens['prevpage'] = "Vorherige Seite";
	$tokens['nextpage'] = utf8_decode("Nächste Seite");
?>