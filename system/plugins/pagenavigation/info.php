<?PHP
/*
 * info.php
 * 
 * Copyright 2015 Samuel R�egger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	$pluginInfo						= new PluginInfo();
	$pluginInfo->path				= "pagenavigation";
	$pluginInfo->name				= "Page Navigation";
	$pluginInfo->authorName			= "Thorsten Rotering and Samuel R�egger";
	$pluginInfo->authorLink			= "http://rueegger.me";
	$pluginInfo->version			= "1.0.5";
	$pluginInfo->license			= "GPL 3";
	$pluginInfo->licenseUrl			= "http://www.gnu.org/licenses/gpl.html";
	$pluginInfo->supportedLanguages = array("de");
	$this->Add($pluginInfo);
?>
