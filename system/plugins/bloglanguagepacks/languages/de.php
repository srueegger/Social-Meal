<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
	$tokens["plugin_description"]		= "Dieses Plugin ist ein Teil von der ContenLion Blog Plugin Sammlung.";
	$tokens["editabletitle"]			= "Übersichtseinstellungen";
	$tokens["excerpt"]					= "Anzahl Zeichen bis der Eintrag gekürzt wird.";
	$tokens["zero"]						= "Bei Eingabe 0 wird nicht gekürzt.";
	$tokens["more"]						= "Weiterlesen...";
	$tokens["datetime"]					= "Veröffentlicht:";
	$tokens["category"]					= "Kategorie";
	$tokens["tags"]						= "Stichwörter";
	$tokens["title_managecomments"]		= "Blog Kommentare verwalten";
	$tokens["comments"]					= "Kommentare";
	$tokens["comment"]					= "Kommentar";
	$tokens["newcomment"]				= "Neuer Kommentar";
	$tokens["email"]					= "E-Mail Adresse";
	$tokens["captcha"]					= "Spam Kontrolle";
	$tokens["gravatar"]					= "Gravatar";
	$tokens["gravatardescription"]		= "Soll Gravatar in den Kommentaren aktiviert sein?";
	$tokens["NOCOMMENT"]				= "Keine Kommentare";
	$tokens["EDITCOMMENTS"]				= "Zu welchem Eintrag sollen die Kommentare verwaltet werden?";
	$tokens["entriesperpage"]			= "Wie viele Beiträge pro Seite sollen angezeigt werden?";
	$tokens["entriesperuser"]			= "Alle Beiträge von:";
	$tokens["entriespercat"]			= "Alle Beiträge der Kategorie:";
	$tokens["mustlogin"]				= "Um einen Kommentar zu hinterlassen, musst du eingeloggt sein.";
	$tokens["von"]						= "von";
	$tokens["lastchange"]				= "Zuletzt bearbeitet";
	$tokens["aufrufe"]					= "Aufrufe";
	$tokens["entry1"]					= "Alle Blog-Artikel von";
	$tokens["entry2"]					= "anzeigen";
	$tokens["entry3"]					= "Zum Profil von";
	$tokens["over1"]					= "Bitte berücksichtige wie";
	$tokens["over2"]					= "unser Blog funktioniert";
	$tokens["newest"]					= "Neuster Blogbeitrag";
	$tokens["blog"]						= "Blog";
	$tokens["wewrite"]					= "Wir schreiben";
?>
