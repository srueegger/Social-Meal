<?php
/*
 * tweet.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
/**
* Base file for tmhOAuthExamples. This class extends tmhOAuth which means
* it can do everything tmhOAuth does. All examples implement this class as
* opposed to tmhOAuth directly.
*
* In production you shouldn't use this file, but you could use the idea of
* extended tmhOAuth with any includes or helper functions you might want
* for all your scripts.
*
* Although this uses a static user_token and user_secret, you can always
* set them at runtime from a database or local cache or other user tokens/secrets
* that users have allowed you to use on their behalf.
*
* Instructions:
* 1) Ensure you have tmhOAuth checked out or installed via composer.
* 2) If you don't have one already, create a Twitter application on
*      https://dev.twitter.com/apps
* 3) From the application details page copy the consumer key and consumer
*      secret into the place in this code marked with (YOUR_CONSUMER_KEY
*      and YOUR_CONSUMER_SECRET)
* 4) From the application details page copy the access token and access token
*      secret into the place in this code marked with (A_USER_TOKEN
*      and A_USER_SECRET)
* 5) In a terminal or on a server run any of the example scripts:
*      php verify_credentials.php
*
* @author themattharris
*/
define('TMH_INDENT', 25);
class Plugin_Twitter_Tweet extends Plugin_Twitter_tmhAuth {
  public function __construct($config = array()) {
    $this->config = array_merge(
      array(
        // change the values below to ones for your application
        'consumer_key'    => 'JFXNC66dDPdwLEuBU5hH2gZMB',
        'consumer_secret' => 'FLkT7SLIh9NLaPL04qVvbVFgyzE0AdNS2Mz7VsJKORo2CBJc6g',
        'token'           => '2965793530-vveiC7dkg55WBoAzqSfebMG5VJxbvsse2MUhkoR',
        'secret'          => 'e9ubtj36cOjliWAaxJKmqiFmc1NxF6jOUvQAfqOHrZIE7',
        'bearer'          => '',
        'user_agent'      => 'tmhOAuth ' . parent::VERSION . '',
      ),
      $config
    );
    if (file_exists(__DIR__.DIRECTORY_SEPARATOR.'tmh_myconfig.php')) {
      include __DIR__.DIRECTORY_SEPARATOR.'tmh_myconfig.php';
      $this->config = array_merge(
        $this->config,
        tmh_myconfig()
      );
    }
    parent::__construct($this->config);
  }
  // below are helper functions for rendering the response from the Twitter API in a command line interface
  public function render_response() {
    self::eko('Request Settings', false, '=');
    self::eko_kv($this->request_settings, 0, TMH_INDENT);
    self::eko('');
    self::eko('Request Headers', false, '=');
    self::eko_kv($this->convert_headers($this->response['info']['request_header']), 0, TMH_INDENT);
    self::eko('');
    self::eko('Request Data', false, '=');
    $d = $this->response['info'];
    unset($d['request_header']);
    self::eko_kv($d, 0, TMH_INDENT);
    self::eko('');
    self::eko('Response Headers', false, '=');
    self::eko_kv($this->response['headers'], 0, TMH_INDENT);
    self::eko('');
    if (defined(JSON_PRETTY_PRINT)) {
      self::eko('Response Body (Formatted)', false, '=');
      $d = json_decode($this->response['response']);
      $d = json_encode($d, JSON_PRETTY_PRINT);
      self::eko($d);
    } else {
      self::eko('Response Body (As an Object)', false, '=');
      $d = json_decode($this->response['response'], true);
      var_dump($d);
    }
    self::eko('');
    self::eko('Raw response', true, '=');
    self::eko($this->response['raw'], true);
  }
  private function convert_headers($headers) {
    $headers = explode(PHP_EOL, $headers);
    $_headers = array();
    foreach ($headers as $header) {
      list($key, $value) = array_pad(explode(':', trim($header), 2), 2, null);
      $_headers[trim($key)] = trim($value);
    }
    return $_headers;
  }
  private static function eko_kv($items, $indent=0, $padding=10) {
    foreach ((array)$items as $k => $v) {
      if (is_array($v) && !empty($v)) {
        $text = str_pad('', $indent) . str_pad($k, $padding);
        self::eko($text);
        foreach ($v as $k2 => $v2) {
          self::eko_kv(array($k2 => $v2), $indent+5, $padding);
        }
      } else {
        $text = str_pad('', $indent) . str_pad($k, $padding) . implode('',(array)$v);
        self::eko($text);
      }
    }
  }
  private static function eko($text, $newline=false, $underline=NULL) {
    echo $text . PHP_EOL;
    if (!empty($underline))
      echo str_pad('', strlen($text), $underline) . PHP_EOL;
    if ($newline)
      echo PHP_EOL;
  }
}
