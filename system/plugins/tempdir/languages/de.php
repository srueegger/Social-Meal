<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

	$tokens['plugin_description'] = "Erm�glicht es Plugins, Dateien tempor�r zu speichern";
	$tokens['rightserror'] = "Nur noch eine H&uuml;rde ist zu &uuml;berwinden:<br />Bitte setzen Sie die Rechte des Ordners system/plugins/tempdir/data noch auf '0777'!";
?>
