<?php
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

class Plugin_TempDir_Dir {
    
    protected static $instance = null;
	private static $root = null;
    
    /*
     * 
     * returns the path to the tempdir.
     * @param string $areaType
     * @param string $area
     * @param string $subfolder optional Default: empty
     * @return string $path
    */
    public static function get ($areaType, $area, $subfolder = "") {
        
        if (self::$instance == null) {
            self::$instance = new Plugin_TempDir_Dir();
        }
        
        return self::$instance->getPath($areaType, $area, $subfolder);
        
    }
    
    public static function Current () {
        
        if (self::$instance == null) {
            self::$instance = new Plugin_TempDir_Dir();
        }
        
        return self::$instance;
        
    }
    
    /*
     *
     * returns the path to the tempdir.
     * @param string $areaType
     * @param string $area
     * @param string $subfolder optional Default: empty
     * @return string $path
    */
    public function getPath ($areaType, $area, $subfolder = "") {
        
        $path = Settings::getInstance()->specify("plugin", "tempdir")->get("temp_dir");
		if(empty($path))
		{
			$path = Settings::getValue("root")."system/plugins/tempdir/data/";
		}
		self::$root = $path;
        $area = md5($area);

        //Exists a .htaccess-file?
        if (!file_exists($path . ".htaccess")) {
            //No? -> Create!
            self::createHtaccess($path);
        }

        /*
         *  Creates the folder-structur
        */
        if (!file_exists($path . "plugin")) {
            mkdir($path . "plugin", 0777);
        }

        if (!file_exists($path . "skin")) {
            mkdir($path . "skin", 0777);
        }

        /*
         *  creates the directorys and returns the path
        */
        if ($areaType == "plugin") {
            $path .= "plugin/" . $area . "";
        } else if ($areaType == "skin") {
            $path .= "skin/" . $area . "";
        } else {
            if (!file_exists($path . "store")) {
                mkdir($path . "store", 0777);
            }

            $path .= "store/" . $area . "";
        }

        if (!file_exists($path)) {
            mkdir($path, 0777);
        }

        //Create the subfolder, if neccessary
        if ($subfolder && $subfolder != "") {
            $path .= "/" . md5($subfolder);

            if (!file_exists($path)) {
                mkdir($path, 0777);
            }
        }

        //Pfad zur�ckgeben.
        return $path . "/";
        
    }
    
    /*
     *
     * Destructor 
    */
    public function __destruct () {
		if(file_exists(self::$root."plugin"))
		{
			FileSystem::deleteDir(self::$root."plugin");
		}
		
		if(file_exists(self::$root."skin"))
		{
			FileSystem::deleteDir(self::$root."skin");
		}
		
		if(file_exists(self::$root."store"))
		{
			FileSystem::deleteDir(self::$root."store");
		}
    }
    
        /*
         * 
         * Creates a .htaccess file 
         * @param String $path
        */
        protected static function createHtaccess ($path) {
            
            if (!file_exists($path . ".htaccess")) {
                
            $handle = fopen($path . ".htaccess", "w");
            
            fwrite($handle, "order deny,allow\r\n
            deny from all\r\n");
            
            fclose($handle);
            
            }
            
        }
    
}

?>
