<?php
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

include(Settings::getValue("root")."/system/plugins/tempdir/language/de.php");
DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}settings` (
`role`, `dir`, `area`, `areaType`, `property`, `value`, `activated`, `description`, `type`
) VALUES (
'3', 'global', 'tempdir', 'plugin', 'temp_dir', '" . Settings::getInstance()->get("root") . "system/plugins/tempdir/data/', '0', 'Temp-Dir', 'textbox'
); ");

if (chmod(Settings::getInstance()->get("root") . "system/plugins/tempdir/data/", 0777)) {
    //Alles ok, Ordnerrechte sind gesetzt
} else {
    echo $tokens['plugin_description'];
}

?>
