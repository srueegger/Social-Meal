<?php
/*
 * manager.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
class Plugin_BlogEntries_Manager{

	public static function updateAufrufe($aufrufe,$id){
		$aufrufe = DataBase::Current()->EscapeString($aufrufe);
		DataBase::Current()->Execute("UPDATE {'dbprefix'}pages SET aufrufe = '".$aufrufe."' WHERE id = '".$id."'");
	}
	
	public static function getEntryData($id){
		$id = DataBase::Current()->EscapeString($id);
		$data = DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}pages WHERE id = '".$id."'");
		return $data;
	}
	
	public static function getBLogOverviewAlias(){
		return DataBase::Current()->ReadField("SELECT alias FROM {'dbprefix'}pages WHERE editor = 'Plugin_BlogOverview_Editor'");
	}
	
	public static function getCatName($cat){
		$cat = DataBase::Current()->EscapeString($cat);
		return utf8_encode(DataBase::Current()->ReadField("SELECT name FROM {'dbprefix'}plugin_blogcats WHERE id = '".$cat."'"));
	}
	
	public static function deleteMetaData($id){
		$id = DataBase::Current()->EscapeString($id);
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_plugindata_data WHERE area = 'blogentries' AND property = 'blogentrymeta_".$id."'");
	}
	
	public static function getNewBlog(){
		return DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}pages WHERE editor = 'Plugin_BlogEntries_Editor' ORDER BY id DESC LIMIT 1");
	}
	
	public static function getCats(){
		return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_blogcats");
	}
	
	public static function countEntriesPerCat($catid){
		$catid = DataBase::Current()->EscapeString($catid);
		$counter = 0;
		foreach(Plugin_BlogOverview_Manager::getEntriesData() as $datas){
			$entryMetaData = Plugin_PluginData_Data::getData('blogentrymeta_'.$datas->id.'','plugin','blogentries');
			if($entryMetaData['category'] == $catid){
				$counter++;
			}
		}
		return $counter;
	}
	
	public static function addCat($name){
		$name = DataBase::Current()->EscapeString($name);
		DataBase::Current()->Execute("INSERT INTO {'dbprefix'}plugin_blogcats SET name='".$name."'");
	}
	
	public static function delCat($catid){
		$catid = DataBase::Current()->EscapeString($catid);
		DataBase::Current()->Execute("DELETE FROM {'dbprefix'}plugin_blogcats WHERE id = '".$catid."' LIMIT 1");
	}
}
?>
