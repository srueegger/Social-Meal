<?PHP
/*
 * editor.php
 *
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
	class Plugin_BlogEntries_Editor extends Editor{

		/**
		 *
		 * @param Page $page
		 */
		function __construct(Page $page){
			$this->page = $page;
		}

		public function display(){
			$template = new Template();
			if(Mobile::isMobileDevice()){
				$template->load('plugin_blogentries_entry.mobile');
			}else{
				$template->load("plugin_blogentries_entry");
			}
			$template->show_if('readmore',false);
			$template->show_if('authorinfo',true);
			$template->show_if('showtitle',false);
			$entryData = Plugin_BlogEntries_Manager::getEntryData($this->page->id);
			$aufrufe = $entryData->aufrufe + 1;
			Plugin_BlogEntries_Manager::updateAufrufe($aufrufe,$entryData->id);
			unset($entryData);
			$entryData = Plugin_BlogEntries_Manager::getEntryData($this->page->id);
			$entryMetaData = Plugin_PluginData_Data::getData('blogentrymeta_'.$this->page->id.'','plugin','blogentries');
			Plugin_Profile_Manager::setBlogRead($entryData->id);
			$plugin_blogentries_overviewalias = Plugin_BlogEntries_Manager::getBLogOverviewAlias();
			$template->assign_var('ENTRYTITLE',htmlentities($entryData->title));
			$template->assign_var('ENTRYURL',UrlRewriting::GetUrlByAlias($entryData->alias));
			$template->assign_var('ENTRYCONTENT',$entryData->content);
			$template->assign_var('DATETIME',Plugin_Events_Manager::formatDate($entryMetaData['lastchange']));
			$template->assign_var('AUFRUFE',$entryData->aufrufe);
			$authordata = Plugin_Profile_Manager::getUserData($entryMetaData['author']);
			if(Plugin_Profile_Manager::isFriend($authordata->id) or $entryMetaData['author'] == User::Current()->name){
				$template->assign_var('AUTHOR',ucfirst($authordata->firstname).' '.ucfirst($authordata->lastname));
			}else{
				$template->assign_var('AUTHOR',htmlentities($entryMetaData['author']));
			}
			if(Mobile::isMobileDevice()){
				$template->assign_var('SHOWAUTHOR',Plugin_Profile_Manager::showUsername($entryMetaData['author'],'singleIconWrapper singleIconText iconEditDark postInfo postAuthor postInfoNoMargin'));
			}else{
				$template->assign_var('SHOWAUTHOR',Plugin_Profile_Manager::showUsername($entryMetaData['author'],'author'));
			}
			if(isset($entryMetaData['image']) != ''){
				$template->show_if('blogimage',true);
				$template->assign_var('imgpath',htmlentities($entryMetaData['image']));
				$imgdata = ImageServer::getImageData($entryMetaData['image']);
				if($imgdata->description != ''){
					$template->show_if('imgdesc',true);
					$template->assign_var('imgdesc',htmlentities($imgdata->description));
				}else{
					$template->show_if('imgdesc',false);
				}
			}elseif(!isset($entryMetaData['image'])){
				$template->show_if('blogimage',false);
				$template->show_if('imgdesc',false);
			}
			$template->assign_var('AUTHORURL',UrlRewriting::GetUrlByAlias($plugin_blogentries_overviewalias).'?author='.$entryMetaData['author']);
			$template->assign_var('AUTHORNAMEURL',htmlentities($entryMetaData['author']));
			$template->assign_var('CATID',htmlentities($entryMetaData['category']));
			$template->assign_var('CATNAME',Plugin_BlogEntries_Manager::getCatName($entryMetaData['category']));
			if(User::Current()->role->ID == 1 && Plugin_Profile_Manager::openImg($entryMetaData['author']) == 0){
				$authorimage = Sys::getFullSkinPath().'images/avtar.png';
			}else{
				$authorimage = '/content/uploads/profile/thumbs/'.Plugin_Profile_Manager::getUserImage($entryMetaData['author']);
				if($authorimage == '/content/uploads/profile/thumbs/'){
					$authorimage = Sys::getFullSkinPath().'images/avtar.png';
				}
			}
			$template->assign_var('authorimg',htmlentities($authorimage));
			$authorinfo = substr($authordata->aboutme,0, 250);
			$template->assign_var('authorinfo',htmlentities($authorinfo));
			if(Plugin_Blogcomments_Manager::countComments($entryData->id) == 0){
				$template->assign_var('comments',Language::DirectTranslate('plugin_bloglanguagepacks_nocomment'));
			}elseif(Plugin_Blogcomments_Manager::countComments($entryData->id) == 1){
				$template->assign_var('comments',Plugin_Blogcomments_Manager::countComments($entryData->id).' '.Language::DirectTranslate('plugin_bloglanguagepacks_comment'));
			}else{
				$template->assign_var('comments',Plugin_Blogcomments_Manager::countComments($entryData->id).' '.Language::DirectTranslate('plugin_bloglanguagepacks_comments'));
			}
			echo $template->getCode();
			EventManager::RaiseEvent('plugin_blogentry_bottom_content',$entryData);
			if(!Mobile::isMobileDevice()){
				//Sidebar
				include('./system/skins/socialmeal/sidebar.php');
			}
		}

		function getHeader(){
			EventManager::RaiseEvent('plugin_blogentries_header',null);
			echo('<link href="/system/plugins/bbcode/css/editor.css" rel="stylesheet" type="text/css" />');
			echo('<script type="text/javascript" src="/system/plugins/bbcode/js/editor.js"></script>');
			if(User::Current()->role->ID != 1){
				echo('<script type="text/javascript" src="/system/plugins/blogcomments/js/checkform.js"></script>');
			}
			$entryData = Plugin_BlogEntries_Manager::getentryData($this->page->id);
			$entryMetaData = Plugin_PluginData_Data::getData('blogentrymeta_'.$this->page->id.'','plugin','blogentries');
			echo('
				<meta property="og:site_name" content="Social Meal - Meal Sharing für die Region Basel" />
				<meta property="og:title" content="Blog: '.htmlentities($entryData->title).'" />
				<meta property="og:type" content="blog" />
				<meta property="og:url" content="'.UrlRewriting::GetUrlByAlias($entryData->alias).'" />
				<meta property="og:image:secure_url" content="'.htmlentities($entryMetaData['image']).'" />
				<meta property="og:image" content="'.htmlentities($entryMetaData['image']).'" />
				<link rel="image_src" href="'.htmlentities($entryMetaData['image']).'" />
				');
		}

		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$template = new Template();
			$template->load("plugin_blogentries_wysiwyg");
			$entryMetaData = Plugin_PluginData_Data::getData('blogentrymeta_'.$this->page->id.'','plugin','blogentries');
			$template->assign_var("CONTENT",$this->page->getEditorContent($this));
			$template->assign_var("HOST",Settings::getInstance()->get("host"));
			$template->assign_var("ALIAS",$this->page->alias);
			$template->assign_var("URL",UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias));
			$template->assign_var("LANG",strtolower(Settings::getInstance()->get("language")));
			$template->assign_var("PREVIEWURL",$this->page->GetUrl());
			foreach(ImageServer::getImages() as $image){
				$index = $template->add_loop_item("BLOGIMAGES");
				$template->assign_loop_var("BLOGIMAGES",$index,"IMGPATH",$image->path);
				$template->assign_loop_var("BLOGIMAGES",$index,"IMGNAME",htmlentities($image->name));
				if($image->path != $entryMetaData['image']){
					$template->assign_loop_var("BLOGIMAGES",$index,"SELECTED",'');
				}else{
					$template->assign_loop_var("BLOGIMAGES",$index,"SELECTED",'selected="selected"');
				}
			}
			foreach(DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}plugin_blogcats") as $category){
				$index = $template->add_loop_item("CATEGORY");
				$template->assign_loop_var("CATEGORY",$index,"CATID",htmlentities($category->id));
				$template->assign_loop_var("CATEGORY",$index,"CATNAME",htmlentities(utf8_encode($category->name)));
				if($category->id != $entryMetaData['category']){
					$template->assign_loop_var("CATEGORY",$index,"SELECTED",'');
				}else{
					$template->assign_loop_var("CATEGORY",$index,"SELECTED",'selected="selected"');
				}
			}
			foreach(DataBase::Current()->ReadRows("SELECT name FROM {'dbprefix'}user") as $bloguser){
				$index = $template->add_loop_item("USERS");
				$template->assign_loop_var("USERS",$index,"USERNAME",$bloguser->name);
				if($entryMetaData['author'] != $bloguser->name){
					$template->assign_loop_var("USERS",$index,"SELECTED",'');
				}else{
					$template->assign_loop_var("USERS",$index,"SELECTED",'selected="selected"');
				}
			}
			return $template->getCode();
		}

		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage
		 */
		public function save(Page $newPage,Page $oldPage){
			$plugin_blogentries_meta = array();
			$plugin_blogentries_meta['author'] = $_POST['user'];
			$plugin_blogentries_meta['image'] = $_POST['blogimage'];
			$plugin_blogentries_meta['category'] = $_POST['category'];
			$plugin_blogentries_meta['aufrufe'] = 0;
			$plugin_blogentries_meta['lastchange'] = date('Y-m-d H:i:s');
			Plugin_BlogEntries_Manager::deleteMetaData($this->page->id);
			Plugin_PluginData_Data::setData('blogentrymeta_'.$this->page->id.'',$plugin_blogentries_meta,'blogentries','plugin');
			Plugin_Profile_Manager::updatePunktePerName(15,$_POST['user']);
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",$_POST['content']));
			$this->page->save();
		}
	}
?>
