<?PHP
/*
 * editblogcats.php
 * 
 * Copyright 2015 Samuel Rüegger <samuel@rueegger.me>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	class Plugin_Blogentries_EditBlogCats extends Editor{
		
		/**
		 *
		 * @param Page $page 
		 */
		function __construct(Page $page){
			$this->page = $page;
		}
		public function display(){
			$template = new Template();
			$template->load('plugin_blogentries_editblogcats');
			if(isset($_POST['savenewcat']) && !empty($_POST['newcat'])){
				Plugin_BlogEntries_Manager::addCat($_POST['newcat']);
			}
			if(isset($_GET['dodel'])){
				if(Plugin_BlogEntries_Manager::countEntriesPerCat($_GET['dodel']) == 0){
					Plugin_BlogEntries_Manager::delCat($_GET['dodel']);
				}
			}
			if(isset($_POST) && empty($_POST['newcat'])){
				
			}
			foreach(Plugin_BlogEntries_Manager::getCats() as $cat){
				$index = $template->add_loop_item("cats");
				$template->assign_loop_var("cats",$index,"id",htmlentities($cat->id));
				$template->assign_loop_var("cats",$index,"name",$cat->name);
				$template->assign_loop_var("cats",$index,"countentries",Plugin_BlogEntries_Manager::countEntriesPerCat($cat->id));
				if(Plugin_BlogEntries_Manager::countEntriesPerCat($cat->id) == 0){
					$template->assign_loop_var("cats",$index,"dellink",'<a href="./blog-kategorien.html?dodel='.$cat->id.'"><img title="Löschen" src="'.Icons::getIcon('cross').'" /></a>');
				}else{
					$template->assign_loop_var("cats",$index,"dellink",'');
				}
			}
			echo($template->getCode());
		}

		function getHeader(){
		}
		
		/**
		 *
		 * @return string
		 */
		public function getEditableCode(){
			$change = 'Speichern';
			return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
		}
		
		/**
		 *
		 * @param Page $newPage
		 * @param Page $oldPage 
		 */
		public function save(Page $newPage,Page $oldPage){
			$this->page = $newPage;
			$this->page->setEditorContent(str_replace("\\\"","\"",isset($_POST['content'])));
			$this->page->save();
		}
	}
?>
