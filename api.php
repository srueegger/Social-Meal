<?php
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
function callback_trim($buffer){
	return trim($buffer);
}
ob_start("callback_trim");
include('const.php');
include('autoload.php');
$db = new MySQL('system/dbsettings.php');
$db->Connect();
$language = new Language();
$datatype = new DataType($_GET['datatype']);
$serializer = new XmlSerializer();
if(isset($_GET['apikey']) && $datatype->allowShare($_GET['apikey'])){ 
	if($_GET['action'] == "export"){
		$list = $datatype->getAll();
		$serializer->serialize($list,$datatype);
	}
	//At this time to unsafe
	/*else if($_GET['action'] == "import" && $_GET['url'] != ""){
		$serializer->importFromUrl($_GET['url']);
	}*/
}else{
	$serializer->raiseError("1", "Access denied.");
}
ob_flush();
?>
