var tinyMCELinkList = new Array(
	<?PHP
	/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

          include('../const.php');
	  include('../system/classes/database.php');
	  include('../system/classes/language.php');
	  include('../system/classes/settings.php');
	  include('../system/classes/cache.php');
	  include('../system/classes/filecache.php');
	  include('../system/classes/mysql.php');
          include('../system/classes/user.php');
          include('../system/classes/role.php');
          include('../system/classes/page.php');
          include('../system/classes/sys.php');
          include('../system/classes/contentlionexception.php');
          include('../system/classes/accessdeniedexception.php');
          include('../system/classes/urlrewriting.php');
	  $db = new MySQL('../system/dbsettings.php');
      $db->Connect();
      $language = new Language();
	  $language->root = '../';
          $pages = Page::GetAllPages();
          $i = 1;
  	  foreach($pages as $page){
	      ?>
["<?PHP echo htmlentities(utf8_encode($page->title)); ?>", "<?PHP echo $page->GetUrl(); ?>"]
		  <?PHP
		  if($i < count($pages)){
		    echo ",";
		  }
		  $i++;
	  }
	?>
);
