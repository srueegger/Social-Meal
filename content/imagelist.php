var tinyMCEImageList = new Array(
	<?PHP
		/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

	  include('../system/classes/imageserver.php');
	  include('../system/classes/database.php');
	  include('../system/classes/language.php');
	  include('../system/classes/settings.php');
	  include('../system/classes/cache.php');
	  include('../system/classes/filecache.php');
	  include('../system/classes/mysql.php');
	  include('../system/classes/user.php');
	  include('../system/classes/role.php');
	  include('../system/classes/page.php');
	  include('../system/classes/sys.php');
          include('../const.php');
	  $db = new MySQL('../system/dbsettings.php');
      $db->Connect();
      DataBase::SetCurrent($db);
      $language = new Language();
      Settings::$allowFileCache = false;
	  $images = ImageServer::getImages();
	  if($images){
	    $i = 1;
  	    foreach($images as $image){
	      ?>
		  ["<?PHP echo $image->name; ?>", "<?PHP echo $image->path; ?>"]
		  <?PHP
		  if($i < count($images)){
		    echo ",";
		  }
		  $i++;
	    }
	  }
	?>
);
