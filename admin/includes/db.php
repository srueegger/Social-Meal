<!-- /*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
-->
<h1>Datenbank</h1>
<table>
  <thead>
    <tr>
      <td>Name</td>
      <td>Aktionen</td>
      <td>Eintr&auml;ge</td>
    </tr>
  </thead>
  <tbody>
    <?PHP
      foreach(MySQL::getTables($_GET['dbpage'] * 20,20) as $table){
        echo "<tr>
                <td>".$table."</td>
                <td>
                  <a title=\"Anzeigen\" 
	                href=\"index.php?page=table-show&table=".urlencode($table)."\">
                      <img src=\"/system/images/icons/table.png\" />
		          </a>
                  <a title=\"Bearbeiten\" 
                    href=\"index.php?page=table-edit&&table=".urlencode($table)."\">
                      <img src=\"/system/images/icons/table_edit.png\" />
		          </a>
                  <a title=\"L&ouml;schen\" 
	                href=\"index.php?page=table-delete&table=".urlencode($table)."\">
                      <img src=\"/system/images/icons/table_delete.png\" />
		          </a>
                </td>
                <td>".MySQL::countTableEntries($table)."</td>
               </tr>";
      }
    ?>
  </tbody>
</table>

<?PHP
  $pagecount = ceil(MySQL::$countTables / 20);
  for($cPage = 0;$cPage < $pagecount;$cPage++){
    echo "<a href=\"/admin/index.php?page=db&dbpage=".$cPage."\">".($cPage + 1)."</a> ";
  }
?>
