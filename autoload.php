<?PHP
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

$loadedClasses = 0;
  function __autoload($class_name){
      $imported = false;
      $namespaces = explode("_",strtolower($class_name));
      if(sizeOf($namespaces) == 3){
        if($namespaces[0] == "plugin"){
          if(file_exists("system/plugins/".$namespaces[1]."/classes/".$namespaces[2].".php")){
            require_once "system/plugins/".$namespaces[1]."/classes/".$namespaces[2].".php";
            $imported = true;
          }
        }
        else if($namespaces[0] == "skin"){
          if(file_exists("system/skins/".$namespaces[1]."/classes/".$namespaces[2].".php")){
            require_once "system/skins/".$namespaces[1]."/classes/".$namespaces[2].".php";
            $imported = true;
          }
        }
      }
      if(!$imported){
          if(file_exists("system/classes/".strtolower($class_name).".php")){
            require_once "system/classes/".strtolower($class_name).".php";
          }
      }
      $GLOBALS['loadedClasses']++;
  }
?>
