<?php
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
if($_SERVER["HTTPS"] != "on") {
	header("HTTP/1.1 301 Moved Permanently");
	header("Location: https://socialmeal.ch");
	exit();	
}
ini_set("zlib.output_compression", "On");
include('const.php');
include('autoload.php');
require_once __DIR__ . '/fbloader.php';
session_start();
if(!DEVELOPMENT){
	error_reporting(0);
		ini_set('display_errors', 0);
		ini_set('error_reporting', E_ALL);
		ini_set('log_errors',1);
}else{
	error_reporting(-1);
	ini_set('display_errors', 1);
}
setlocale(LC_ALL, "de_CH");
sys::parseGetParams();
if(User::Current()->isGuest() && isset($_COOKIE['smal'])){
	$autologin = Plugin_Profile_Manager::getUserDataPerAL($_COOKIE['smal']);
	User::Current()->Autologin($autologin->name,$autologin->password);
}
array_walk_recursive($_POST, create_function('&$val', '$val = stripslashes($val);'));
if(!isset($_GET['include'])){
	$_GET['include'] = '';
}
Page::Current()->ExecuteHttpHeader();
EventManager::RaiseEvent("pre_page_load",null);
//Start Redirects
//Weiterleitung zum Profil bearbeiten, wenn es nicht vollständig ausgefüllt ist.
if(Plugin_Profile_Manager::completeProfil() == false){
	header('Location: '.Settings::getInstance()->get("host").'mein-profil-bearbeiten.html');
}
//Weiterleitung zur AGB bestätigen Seite, wenn nicht die neuste Version der AGB akzeptiert sind.
if(Plugin_Profile_Manager::newAGBS() == true){
	header('Location: '.Settings::getInstance()->get("host").'anderungen-akzeptieren.html');
}
//Weiterleitung zur "Profil gesperrt"-Seite, wenn das Profil gesperrt ist
if(Plugin_Profile_Manager::isProfileBlocked() == true){
	header('Location: '.Settings::getInstance()->get("host").'profil-gesperrt.html');
}
//Weiterleitung zur "E-Mail geändert"-Seite, wenn das Profil gesperrt ist
if(!isset($_GET['verifymail'])){
	if(Plugin_Profile_Manager::isProfileChangeMail() == true){
		header('Location: '.Settings::getInstance()->get("host").'e-mail-geandert.html');
	}
}
//End Redirects
if(file_exists(Settings::getInstance()->get("root").$_GET['include'].".htm")){
	include(Settings::getInstance()->get("root").$_GET['include'].".htm");
}else{
	SkinController::displayCurrent();
}
Scheduler::runTasks();
?>
